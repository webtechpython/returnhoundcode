import os
from setuptools import setup, find_packages


def requirements():
    requires = []
    project_root = os.path.dirname(os.path.abspath(__file__))
    requirements_pth = os.path.join(project_root, 'requirements', 'base.txt')
    if os.path.exists(requirements_pth):
        with open(requirements_pth) as fh:
            requires = [p.strip() for p in fh.readlines()]
    return requires


setup(
    name='return_hound',
    description='ReturnHound website',
    author='Byte Orbit',
    author_email='info@byteorbit.com',
    url='http://byteorbit.com/',
    install_requires=requirements(),
    # use_scm_version=True,  # Version from hg tags using setuptools_scm
    # setup_requires=['setuptools_scm'],
    version='1.0',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    zip_safe=False,
)
