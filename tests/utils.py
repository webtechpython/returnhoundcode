from rest_framework.test import APITestCase, APIClient
from tests import factories


def get_client(token=None):
    token = factories.TokenFactory() if not token else token
    client = APIClient(HTTP_AUTHORIZATION='Token {0}'.format(token.key))
    client.auth_token = token
    client.user = token.user
    return client


class AuthenticatedAPITestCase(APITestCase):
    """
    Base class for tests that need authenticated access to the APIs.
    """

    def setup_hotel(self):
        self.hotel = factories.HotelFactory()
        # hotel admin...
        self.hoteluser_admin = factories.HotelAdminUserFactory(employer=self.hotel)
        self.client_hotel_admin = self.client_for(self.hoteluser_admin.employee)
        # hotel staff...
        self.hoteluser_staff = factories.HotelUserFactory(employer=self.hotel)
        self.client_hotel_staff = self.client_for(self.hoteluser_staff.employee)

    def client_for(self, user):
        token = factories.TokenFactory(user=user)  # type: factories.TokenFactory.Meta.model
        client = get_client(token=token)
        client.user = token.user
        return client
