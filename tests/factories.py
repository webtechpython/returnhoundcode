from datetime import datetime
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.utils.text import slugify
from cities_light.models import Country, Region
from factory import DjangoModelFactory, SubFactory, Sequence

from return_hound.apps.aramex.models import AramexCountry, AramexState
from return_hound.apps.api.models import Token
from return_hound import enums
from return_hound import models
from factory import LazyAttribute


class UserFactory(DjangoModelFactory):
    email = 'test@byteorbit.com'
    first_name = 'test user'
    last_name = 'Test'
    contact_number = '1234567890'
    _PASSWORD = 'TestPass1@'
    password = make_password(_PASSWORD)

    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ('email', )

    @classmethod
    def _after_postgeneration(cls, obj, create, results=None):
        obj._PASSWORD = cls._PASSWORD


class SuperUserFactory(UserFactory):
    email = 'admin.test@byteorbit.com'
    last_name = 'test admin user'
    is_superuser = True
    is_staff = True


class TokenFactory(DjangoModelFactory):
    class Meta:
        model = Token
        django_get_or_create = ['user']

    user = SubFactory(UserFactory)


class CountryFactory(DjangoModelFactory):
    code2 = "AE"
    code3 = "ARE"
    name = "United Arab Emirates"
    slug = LazyAttribute(lambda x: slugify(x.name))
    name_ascii = LazyAttribute(lambda x: x.name)
    tld = "ae"
    continent = "AS"
    phone = "971"
    geoname_id = Sequence(lambda n: n)

    class Meta:
        model = Country
        django_get_or_create = ('code2', )


class RegionFactory(DjangoModelFactory):
    name = "Dubai"
    geoname_id = Sequence(lambda n: n)
    geoname_code = "03"
    country = SubFactory(CountryFactory)
    name_ascii = LazyAttribute(lambda x: x.name)
    slug = LazyAttribute(lambda x: slugify(x.name))
    display_name = LazyAttribute(lambda x: '{}, {}'.format(x.name, x.country.name))

    class Meta:
        model = Region
        django_get_or_create = ('country', 'slug')


class JordanCountryFactory(CountryFactory):
    code2 = "JO"
    code3 = "JOR"
    name = "Jordan"
    tld = "jo"
    phone = "962"


class JordanRegionFactory(RegionFactory):
    country = SubFactory(JordanCountryFactory)
    name = 'Amman'
    geoname_code = '16'


class AramexCountryFactory(DjangoModelFactory):
    name = "Jordan"
    code2 = "JO"
    code3 = "JOR"
    state_required = False
    postcode_required = False

    class Meta:
        model = AramexCountry
        django_get_or_create = ('code2', )


class AramexStateFactory(DjangoModelFactory):
    name = 'Amman'
    aramex_country = SubFactory(AramexCountryFactory)

    class Meta:
        model = AramexState
        django_get_or_create = ('aramex_country', 'name')


class AddressFactory(DjangoModelFactory):
    aramex_country = SubFactory(AramexCountryFactory)
    aramex_state = SubFactory(AramexStateFactory)
    line1 = '7 Jumeira Towers'
    line2 = ''
    city = 'Amman'
    post_code = ''

    class Meta:
        model = models.Address


class DubaiCountryFactory(AramexCountryFactory):
    name = 'Dubai'
    code2 = 'AE'


class DubaiAddressFactory(AddressFactory):
    city = 'Dubai'
    aramex_country = SubFactory(DubaiCountryFactory)
    aramex_state = SubFactory(AramexStateFactory, name='Dubai',
                              aramex_country=SubFactory(DubaiCountryFactory))


class CategoryFactory(DjangoModelFactory):
    name = 'Clothing'

    class Meta:
        model = models.Category
        django_get_or_create = ('name',)


class ItemTypeFactory(DjangoModelFactory):
    name = 'Shirt'
    category = SubFactory(CategoryFactory)

    class Meta:
        model = models.CategoryItem
        django_get_or_create = ('category', 'name')


class HotelFactory(DjangoModelFactory):
    name = 'Test hotel'
    number_of_rooms = 100
    address = SubFactory(AddressFactory)
    is_verified = True
    is_hidden = False

    class Meta:
        model = models.Hotel
        django_get_or_create = ('name',)


class ReportStyleFactory(DjangoModelFactory):
    hotel = SubFactory(HotelFactory)

    class Meta:
        model = models.ReportStyle
        django_get_or_create = ('hotel',)


class HotelUserFactory(DjangoModelFactory):
    employer = SubFactory(HotelFactory)
    employee = SubFactory(UserFactory, email='staff@hotel.com')

    class Meta:
        model = models.HotelUser
        django_get_or_create = ('employee',)


class HotelAdminUserFactory(HotelUserFactory):
    employee = SubFactory(UserFactory, email='admin@hotel.com')
    is_admin = True
    is_creator = True


class LostItemFactory(DjangoModelFactory):
    hotel = SubFactory(HotelFactory)
    created_by = SubFactory(UserFactory, email='LostItem.creator@user.com', address=SubFactory(AddressFactory))
    category = SubFactory(CategoryFactory)
    item_type = SubFactory(ItemTypeFactory)
    date = datetime.today().strftime("%Y-%m-%d")
    arrival_date = datetime.today().strftime("%Y-%m-%d")
    departure_date = datetime.today().strftime("%Y-%m-%d")
    status = None
    status_text = ''
    location = 'Bedroom'
    room_number = 'RN001'
    description = 'Test description'
    serial_number = 'SN2016'
    specification = 'Test specification'

    class Meta:
        model = models.LostItem


class FoundItemFactory(DjangoModelFactory):
    hotel = SubFactory(HotelFactory)
    created_by = SubFactory(UserFactory, email='staff@hotel.com')
    category = SubFactory(CategoryFactory)
    item_type = SubFactory(ItemTypeFactory)
    date = datetime.today().strftime("%Y-%m-%d")
    owner_send_email = True
    location = 'Bedroom'
    room_number = 'RN001'
    description = 'Test description'
    serial_number = 'SN2016'
    specification = 'Test specification'
    found_by = 'Test user'
    status = None
    status_text = None
    width = 10
    height = 0.5
    length = 10
    weight = 0.5

    class Meta:
        model = models.FoundItem


class MatchedItemFactory(DjangoModelFactory):
    status = models.MatchedItem.STATUS.pending
    lost_item = SubFactory(LostItemFactory)
    found_item = SubFactory(FoundItemFactory)
    assigned_status = None
    status_text = ""

    class Meta:
        model = models.MatchedItem


class ShipmentFactory(DjangoModelFactory):

    class Meta:
        model = models.Shipment


class ShippingItemFactory(DjangoModelFactory):
    parcel = SubFactory(MatchedItemFactory)
    details = SubFactory(ShipmentFactory)

    class Meta:
        model = models.ShippingItem


class TransactionFactory(DjangoModelFactory):
    shipping_details = SubFactory(ShipmentFactory)
    service_charge = 18.00
    charged_amount = 100.00
    insurance_fee = 15.00
    item_value = 200.00
    surcharge_amount = 20.00
    item_value_currency = enums.ItemValueCurrencies.AED

    is_paid = False

    class Meta:
        model = models.Transaction
        django_get_or_create = ['shipping_details']


class EmailTemplateFactory(DjangoModelFactory):
    class Meta:
        model = models.EmailTemplate
    is_active = True
    subject = 'New Item reported'
    hotel = SubFactory(HotelFactory)
    template = enums.EmailTemplates.new_report_received_hotel


class WelcomeEmail(EmailTemplateFactory):
    subject = 'welcome'
    template = enums.EmailTemplates.welcome_hotel_user


class WelcomeClientEmail(EmailTemplateFactory):
    hotel = None
    subject = 'welcome'
    is_system_default = True
    template = enums.EmailTemplates.welcome_client_user


class ReportDetailsEmail(EmailTemplateFactory):
    subject = 'item details'
    template = enums.EmailTemplates.report_details


class SocialLinkFactory(DjangoModelFactory):
    class Meta:
        model = models.SocialLink
    hotel = SubFactory(HotelFactory)
    provider = enums.SocialProviders.FACEBOOK
    link = 'http://facebook.com/123'


class InvoiceFactory(DjangoModelFactory):
    shipment = SubFactory(ShipmentFactory)
    tax_rate = 10
    awb = 'abc'
    rep = '123'
    fob = 'qwerty'
    hotel = 'hotel name'
    tracking_number = '12345'
    shipping_price = 0

    class Meta:
        model = models.Invoice


class InvoiceItemFactory(DjangoModelFactory):
    invoice = SubFactory(InvoiceFactory)
    name = 'item'
    description = 'description'
    price = 10

    class Meta:
        model = models.InvoiceItem
