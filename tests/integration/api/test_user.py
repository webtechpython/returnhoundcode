from unittest.mock import patch, MagicMock
from django.core.urlresolvers import reverse
from tests import factories
from tests.utils import AuthenticatedAPITestCase


@patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', MagicMock(return_value=True))
class UserTestCase(AuthenticatedAPITestCase):
    def setUp(self):
        self.address = factories.AddressFactory()
        self.user = factories.UserFactory(email='simple@user.com', address=self.address)
        self.client = self.client_for(self.user)
        self.url_user = reverse("user")

    def test_user_profile(self):
        response = self.client.get(self.url_user)
        assert response.data == {
            'id': self.user.pk,
            'hotel': {},
            'last_name': self.user.last_name,
            'first_name': self.user.first_name,
            'contact_number': self.user.contact_number,
            'address': {
                'id': self.address.id,
                'post_code': '',
                'line1': self.address.line1,
                'line2': '',
                'region': {
                    'id': self.address.region.pk,
                    'name': self.address.region.name
                },
                'country': {
                    'id': self.address.country.pk,
                    'code2': self.address.country.code2,
                    'name': self.address.country.name,
                    'postal_codes': [],
                    'postcode_required': False,
                    'state_required': False
                },
                'city': self.address.city
            },
            'email': self.user.email,
            'is_hotel_user': False,
            'is_hotel_creator': False
        }

    def test_user_update(self):
        response = self.client.patch(self.url_user, data={
            'first_name': 'First name update',
        })
        assert response.data == {
            'id': self.user.pk,
            'hotel': {},
            'last_name': self.user.last_name,
            'first_name': 'First name update',
            'contact_number': self.user.contact_number,
            'address': {
                'id': self.address.id,
                'post_code': '',
                'line1': self.address.line1,
                'line2': '',
                'region': {
                    'id': self.address.region.pk,
                    'name': self.address.region.name
                },
                'country': {
                    'id': self.address.country.pk,
                    'code2': self.address.country.code2,
                    'name': self.address.country.name,
                    'postal_codes': [],
                    'postcode_required': False,
                    'state_required': False
                },
                'city': self.address.city
            },
            'email': self.user.email,
            'is_hotel_user': False,
            'is_hotel_creator': False
        }

