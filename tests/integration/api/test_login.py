from django.conf import settings
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from tests.factories import UserFactory


class LoginTestCase(APITestCase):
    @property
    def url(self):
        return reverse('login')

    def setUp(self):
        self.user = UserFactory(email='login.tests@byteorbit.com')
        self.data = {
            'email': self.user.email,
            'password': self.user._PASSWORD
        }

    def test_login(self):
        response = self.client.post(self.url, data=self.data)
        self.assertEqual(response.data, {'key': self.user.auth_token.key.hex})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_login(self):
        self.data['password'] = 'wrong-password'
        response = self.client.post(self.url, data=self.data)
        self.assertEqual(response.data, {
            'non_field_errors': ['The email address or password you have entered is incorrect.']
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_account_locked_after_x_failed_logins(self):
        user = UserFactory(email='lockme@byteorbit.com')
        data = {'email': user.email, 'password': 'WRONG_PASSWORD'}

        for i in range(1, settings.AXES_LOGIN_FAILURE_LIMIT):
            resp = self.client.post(self.url, data)
            assert resp.status_code == 400
            assert resp.data == {'non_field_errors': ['The email address or password you have entered is incorrect.']}

        resp = self.client.post(self.url, data)
        assert resp.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        self.assertEqual(resp.data, {
            'non_field_errors': [
                'Your account has been locked for 24 hours! Too many incorrect login attempts. '
                'For urgent account reactivation, please send an email to support@returnhound.com'
            ]
        })
        # Valid credentials but account still locked after third failed attempt.
        resp = self.client.post(self.url, data)
        assert resp.status_code == status.HTTP_429_TOO_MANY_REQUESTS
        self.assertEqual(resp.data, {
            'non_field_errors': [
                'Your account has been locked for 24 hours! Too many incorrect login attempts. '
                'For urgent account reactivation, please send an email to support@returnhound.com'
            ]
        })

    def test_inactive_login(self):
        user = UserFactory(is_active=False, email='inactive.user@byteorbit.com')
        data = {
            'email': user.email,
            'password': user._PASSWORD
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.data, {'non_field_errors': ['This account has been disabled']})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
