from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.models import FoundItem, MatchedItem
from return_hound.enums import FoundItemStatus
from tests.factories import FoundItemFactory
from tests.utils import AuthenticatedAPITestCase


class FoundItemTestCase(AuthenticatedAPITestCase):

    def setUp(self):
        self.setup_hotel()
        self.found_item = FoundItemFactory()
        self.client = self.client_hotel_admin  # default access as Hotel admin user

    def test_found_item_owner_create(self):
        data = {
            "created_by": self.found_item.created_by.pk,
            "category": self.found_item.category.pk,
            "item_type": self.found_item.item_type.pk,
            "description": "Owner create",
            "serial_number": self.found_item.serial_number,
            "hotel": self.found_item.hotel.pk,
            "specification": self.found_item.specification,
            "found_by": self.found_item.found_by,
            "date": self.found_item.date,
            "room_number": self.found_item.room_number,
            "location": self.found_item.location,
            "owner_send_email": self.found_item.owner_send_email,
            "owner_name": "John",
            "owner_email": "john@john.com",
            "owner_surname": "Test",
            "owner_contact_number": "123456798",
            "width": "10.00",
            "height": "0.50",
            "length": "10.00",
            "weight": "0.50",
            "stored_at": ""
        }
        response = self.client.post(reverse("hotel_found_items"), data=data)
        self.assertEqual(response.data, {
            "id": response.data.get('id'),
            "created": response.data.get('created'),
            "created_by": self.found_item.created_by.pk,
            "category": self.found_item.category.pk,
            "item_type": self.found_item.item_type.pk,
            "description": "Owner create",
            "serial_number": self.found_item.serial_number,
            "hotel": self.found_item.hotel.pk,
            "specification": self.found_item.specification,
            "found_by": self.found_item.found_by,
            "date": self.found_item.date,
            "room_number": self.found_item.room_number,
            "location": self.found_item.location,
            "owner_name": "John",
            "owner_send_email": self.found_item.owner_send_email,
            "owner_email": "john@john.com",
            "images": [],
            "owner_surname": "Test",
            "owner_contact_number": "123456798",
            "category_name": self.found_item.category.name,
            "match_count": 0,
            "item_type_name": self.found_item.item_type.name,
            "width": "10.00",
            "height": "0.50",
            "length": "10.00",
            "weight": "0.50",
            "stored_at": "",
            "status": None,
            "status_text": None,
        })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Your lost item on ReturnHound has been found - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertTrue(MatchedItem.objects.filter(found_item__pk=response.data.get('id'),
                                                   lost_item__created_by__email='john@john.com'))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_found_item_owner_update(self):
        data = {
            "owner_name": "Rob",
            "owner_email": "rob@rob.com",
            "owner_surname": "Test",
            "owner_contact_number": "123456798",
        }
        response = self.client.patch(reverse("hotel_found_items-detail", kwargs={'pk': self.found_item.pk}), data=data)
        self.assertEqual(response.data, {
            "id": self.found_item.pk,
            "created": self.found_item.created,
            "created_by": self.found_item.created_by.pk,
            "category": self.found_item.category.pk,
            "item_type": self.found_item.item_type.pk,
            "description": self.found_item.description,
            "serial_number": self.found_item.serial_number,
            "hotel": self.found_item.hotel.pk,
            "specification": self.found_item.specification,
            "found_by": self.found_item.found_by,
            "date": self.found_item.date,
            "room_number": self.found_item.room_number,
            "location": self.found_item.location,
            "owner_name": "Rob",
            "owner_send_email": self.found_item.owner_send_email,
            "owner_email": "rob@rob.com",
            "owner_surname": "Test",
            "owner_contact_number": "123456798",
            "images": [],
            "category_name": self.found_item.category.name,
            "match_count": 0,
            "item_type_name": self.found_item.item_type.name,
            "width": "10.00",
            "height": "0.50",
            "length": "10.00",
            "weight": "0.50",
            "stored_at": "",
            "status": None,
            "status_text": None,
        })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Your lost item on ReturnHound has been found - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertTrue(MatchedItem.objects.filter(found_item__pk=response.data.get('id'),
                                                   lost_item__created_by__email='rob@rob.com'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_found_item_list(self):
        response = self.client.get(reverse("hotel_found_items"))
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": self.found_item.pk,
                "created": self.found_item.created,
                "created_by": self.found_item.created_by.pk,
                "category": self.found_item.category.pk,
                "item_type": self.found_item.item_type.pk,
                "description": self.found_item.description,
                "serial_number": self.found_item.serial_number,
                "hotel": self.found_item.hotel.pk,
                "specification": self.found_item.specification,
                "found_by": self.found_item.found_by,
                "date": self.found_item.date,
                "room_number": self.found_item.room_number,
                "location": self.found_item.location,
                "owner_send_email": self.found_item.owner_send_email,
                "owner_name": self.found_item.owner_name,
                "owner_email": self.found_item.owner_email,
                "images": [],
                "owner_surname": self.found_item.owner_surname,
                "owner_contact_number": self.found_item.owner_contact_number,
                "category_name": self.found_item.category.name,
                "match_count": 0,
                "item_type_name": self.found_item.item_type.name,
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
                "status": None,
                "status_text": None,
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_found_item_detail(self):
        response = self.client.get(reverse("hotel_found_items-detail", kwargs={'pk': self.found_item.pk}))
        self.assertEqual(response.data, {
            "id": self.found_item.pk,
            "created": self.found_item.created,
            "created_by": self.found_item.created_by.pk,
            "category": self.found_item.category.pk,
            "item_type": self.found_item.item_type.pk,
            "description": self.found_item.description,
            "serial_number": self.found_item.serial_number,
            "hotel": self.found_item.hotel.pk,
            "specification": self.found_item.specification,
            "found_by": self.found_item.found_by,
            "date": self.found_item.date,
            "room_number": self.found_item.room_number,
            "location": self.found_item.location,
            "owner_send_email": self.found_item.owner_send_email,
            "owner_name": self.found_item.owner_name,
            "owner_email": self.found_item.owner_email,
            "images": [],
            "owner_surname": self.found_item.owner_surname,
            "owner_contact_number": self.found_item.owner_contact_number,
            "category_name": self.found_item.category.name,
            "match_count": 0,
            "item_type_name": self.found_item.item_type.name,
            "width": "10.00",
            "height": "0.50",
            "length": "10.00",
            "weight": "0.50",
            "stored_at": "",
            "status": None,
            "status_text": None,
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_found_item_update(self):
        data = {
            'description': 'Update',
            'status': FoundItemStatus.ARCHIVED.value,
        }

        response = self.client.patch(reverse("hotel_found_items-detail", kwargs={'pk': self.found_item.pk}), data=data)
        self.assertEqual(response.data, {'status_text': ['Please specify a resolution.']})

        data.update({'status_text': 'Auctioned'})
        response = self.client.patch(reverse("hotel_found_items-detail", kwargs={'pk': self.found_item.pk}), data=data)
        self.assertEqual(response.data, {
            "id": self.found_item.pk,
            "created": self.found_item.created,
            "created_by": self.found_item.created_by.pk,
            "category": self.found_item.category.pk,
            "item_type": self.found_item.item_type.pk,
            "description": "Update",
            "serial_number": self.found_item.serial_number,
            "hotel": self.found_item.hotel.pk,
            "specification": self.found_item.specification,
            "found_by": self.found_item.found_by,
            "date": self.found_item.date,
            "room_number": self.found_item.room_number,
            "location": self.found_item.location,
            "owner_send_email": self.found_item.owner_send_email,
            "owner_name": self.found_item.owner_name,
            "owner_email": self.found_item.owner_email,
            "images": [],
            "owner_surname": self.found_item.owner_surname,
            "owner_contact_number": self.found_item.owner_contact_number,
            "category_name": self.found_item.category.name,
            "match_count": 0,
            "item_type_name": self.found_item.item_type.name,
            "width": "10.00",
            "height": "0.50",
            "length": "10.00",
            "weight": "0.50",
            "stored_at": "",
            "status": {
                'value': FoundItemStatus.ARCHIVED.value,
                'name': FoundItemStatus.ARCHIVED.name,
                'label': FoundItemStatus.ARCHIVED.label,
            },
            "status_text": "Auctioned",
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_found_item_delete(self):
        response = self.client.delete(reverse("hotel_found_items-detail", kwargs={'pk': self.found_item.pk}))
        self.assertTrue(FoundItem.objects.get(pk=self.found_item.pk).is_deleted, True)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_found_item_archive_list(self):
        """
        List archived found items
        """
        archived_item = FoundItemFactory(status=FoundItemStatus.ARCHIVED.value, status_text="Auctioned")

        response = self.client.get(reverse("hotel_archived_found_items"))
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": archived_item.id,
                "created": archived_item.created,
                "category": {
                    "id": archived_item.category.pk,
                    "name": archived_item.category.name
                },
                "item_type": {
                    "id": archived_item.item_type.pk,
                    "name": archived_item.item_type.name,
                    "category": archived_item.item_type.category.pk
                },
                "description": archived_item.description,
                "status": {
                    "value": FoundItemStatus.ARCHIVED.value,
                    "name": FoundItemStatus.ARCHIVED.name,
                    "label": FoundItemStatus.ARCHIVED.label

                },
                "status_text": archived_item.status_text
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)