from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound import enums
from tests import factories
from tests.utils import AuthenticatedAPITestCase


class EmailTemplateAPI(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.client = self.client_hotel_admin  # default access as Hotel admin user
        # global system template...
        self.rh_base_email_template = factories.EmailTemplateFactory(
            hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.email_base)
        # hotel template...
        self.hotel_mail_template = factories.EmailTemplateFactory(
            is_active=True, is_system_default=False,
            template=enums.EmailTemplates.email_base)
        # urls:
        self.url = reverse('email_template')
        self.url_detail = reverse('email_template-detail', kwargs={'pk': self.hotel_mail_template.pk})

    def test_list_view(self):
        # The staff/limited user can view list of email templates...
        response = self.client_hotel_staff.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'count': 1,
            'next': None, 'previous': None,
            'results': [
                {
                    'is_system_default': False,
                    'created': response.data['results'][0]['created'],
                    'modified': response.data['results'][0]['modified'],
                    'body_text_colour': '#000',
                    'footer_text': None,
                    'body_text': None,
                    'footer_colour': '#fff',
                    'footer_text_colour': '#000',
                    'footer_border_colour': '#000',
                    'body_text_colour_2': '#000',
                    'subject': 'New Item reported',
                    'banner_colour': '#fff',
                    'is_active': True,
                    'banner_border_colour': '#000',
                    'hotel': self.hotel.pk,
                    'logo': [],
                    'id': self.hotel_mail_template.pk,
                    'body_colour': '#fff',
                    'template': {
                        'name': self.hotel_mail_template.template.name,
                        'value': self.hotel_mail_template.template.value,
                        'label': self.hotel_mail_template.template.label
                    },
                }
            ]
        })

    def test_create_view(self):
        template = enums.EmailTemplates.welcome_hotel_user
        response = self.client.post(self.url, data={'template': template.value})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, {
            'id': response.data['id'],
            'logo': [],
            'hotel': self.hotel.pk,
            'subject': None,
            'is_system_default': False,
            'is_active': False,
            'created': response.data['created'],
            'modified': response.data['modified'],
            'banner_colour': '#fff',
            'banner_border_colour': '#000',
            'body_text': None,
            'body_text_colour': '#000',
            'body_text_colour_2': '#000',
            'body_colour': '#fff',
            'footer_colour': '#fff',
            'footer_text': None,
            'footer_text_colour': '#000',
            'footer_border_colour': '#000',
            'template': {
                'name': template.name,
                'value': template.value,
                'label': template.label
            },
        })

    def test_update_view(self):
        # we will update some values...
        assert self.hotel_mail_template.template == enums.EmailTemplates.email_base
        assert self.hotel_mail_template.subject == 'New Item reported'
        found_template = enums.EmailTemplates.item_found
        data = {
            'subject': 'New Item found',
            # change the template... TODO: review...
            'template': found_template.value,
        }
        response = self.client.put(self.url_detail, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
                'is_system_default': False,
                'created': response.data['created'],
                'modified': response.data['modified'],
                'body_text_colour': '#000',
                'footer_text': None,
                'body_text': None,
                'footer_colour': '#fff',
                'footer_text_colour': '#000',
                'footer_border_colour': '#000',
                'body_text_colour_2': '#000',
                'subject': 'New Item found',
                'banner_colour': '#fff',
                'is_active': True,
                'banner_border_colour': '#000',
                'hotel': self.hotel.pk,
                'logo': [],
                'id': self.hotel_mail_template.pk,
                'body_colour': '#fff',
                'template': {
                    'name': found_template.name,
                    'value': found_template.value,
                    'label': found_template.label
                },
            }
        )

    def test_template_filtering(self):
        # TODO: this should be improved...
        template = self.hotel_mail_template.template
        response = self.client_hotel_staff.get('{}?template={}'.format(self.url, template.name))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'count': 1,
            'next': None, 'previous': None,
            'results': [{
                'is_system_default': False,
                'created': response.data['results'][0]['created'],
                'modified': response.data['results'][0]['modified'],
                'body_text_colour': '#000',
                'footer_text': None,
                'body_text': None,
                'footer_colour': '#fff',
                'footer_text_colour': '#000',
                'footer_border_colour': '#000',
                'body_text_colour_2': '#000',
                'subject': 'New Item reported',
                'banner_colour': '#fff',
                'is_active': True,
                'banner_border_colour': '#000',
                'hotel': self.hotel.pk,
                'logo': [],
                'id': self.hotel_mail_template.pk,
                'body_colour': '#fff',
                'template': {
                    'name': template.name,
                    'value': template.value,
                    'label': template.label
                },
            }]
        })

    def test_delete_view(self):
        response = self.client.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_simple_hotel_user_cannot_create_template(self):
        data = {
            'template': enums.EmailTemplates.email_base.value
        }
        response = self.client_hotel_staff.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_simple_hotel_user_cannot_update_template(self):
        data = {
            'template': enums.EmailTemplates.email_base.value
        }
        response = self.client_hotel_staff.patch(self.url_detail, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_simple_hotel_user_cannot_delete_template(self):
        response = self.client_hotel_staff.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
