from decimal import Decimal
from django.core.urlresolvers import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.fields import DecimalField
from return_hound.models import Invoice
from tests import factories
from tests.utils import AuthenticatedAPITestCase


class InvoiceAPI(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.shipping_item = factories.ShippingItemFactory()
        self.shipment = self.shipping_item.details
        self.matched_item = self.shipping_item.parcel
        self.invoice = factories.InvoiceFactory(shipment=self.shipment)
        self.invoice_item = factories.InvoiceItemFactory(invoice=self.invoice)
        self.shipment_address = factories.AddressFactory(content_object=self.shipment)
        # default client
        self.client = self.client_hotel_admin
        # urls
        self.url_invoice_list = reverse("invoice")
        self.url_invoice_detail = reverse("invoice-detail", kwargs={'pk': self.invoice.pk})

    def test_invoice_list(self):
        response = self.client.get(self.url_invoice_list)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "company": None,
                "fob": "qwerty",
                "shipment": self.shipment.pk,
                "hotel": "hotel name",
                "tracking_number": "12345",
                "created": response.data['results'][0]['created'],
                "modified": response.data['results'][0]['modified'],
                "tax_rate": "10.00",
                "rep": "123",
                "awb": "abc",
                "shipping_price": "0.00",
                "attention": None,
                "contact_number": "",
                "items": [{
                    'id': self.invoice_item.pk,  # existing item
                    "name": self.invoice_item.name,
                    "description": self.invoice_item.description,
                    "price": DecimalField(10, 2).to_representation(self.invoice_item.price),
                    "quantity": self.invoice_item.quantity,
                    "invoice": self.invoice.id  # FIXME: why?
                }],
                "address": {
                    "id": self.shipment_address.id,
                    "line1": self.shipment_address.line1,
                    "line2": self.shipment_address.line2,
                    "post_code": self.shipment_address.post_code,
                    "city": self.shipment_address.city,
                    "country": {
                        "id": self.shipment_address.country.id,
                        "code2": self.shipment_address.country.code2,
                        "name": self.shipment_address.country.name,
                        'postal_codes': [],
                        "state_required": self.shipment_address.country.state_required,
                        "postcode_required": self.shipment_address.country.postcode_required
                    },
                    "region": {
                        "id": self.shipment_address.region.id,
                        "name": self.shipment_address.region.name
                    }
                },
                "id": self.invoice.pk
            }]
        }

    def test_update_invoice_requires_at_least_one_item(self):
        data = {
            "rep": "12",
            "awb": "123",
            "fob": "123",
            "tax_rate": "12.00",
            "shipping_price": "12.00",
            "attention": None,
            "contact_number": "081000000",
            "hotel": "hotel name",
            "tracking_number": "12345",
            "shipment": self.shipment.pk,
            "items": [],
        }
        response = self.client.patch(self.url_invoice_detail, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'non_field_errors': ['An Invoice needs at least 1 Item']})

    def test_update_invoice_add_item(self):
        data = {
            "rep": "12",
            "awb": "123",
            "fob": "123",
            "tax_rate": "12.00",
            "shipping_price": "12.00",
            "attention": None,
            "contact_number": "081000000",
            "hotel": "hotel name",
            "tracking_number": "12345",
            "shipment": self.shipment.pk,
            "items": [
                {
                    # add new item...
                    "name": "aphiwe", "description": "aphiwe", "price": "12.00", "quantity": 2
                },
                {
                    'id': self.invoice_item.pk,  # existing item
                    "name": "aphiwe", "description": "aphiwe", "price": "12.00", "quantity": 2,
                }
            ],
            "address": {
                "id": self.shipment_address.pk,
                "post_code": self.shipment_address.post_code,
                "city": self.shipment_address.city,
                "region": self.shipment_address.region.pk,
                "country": self.shipment_address.country.pk,
                "line1": 'address.line1',
                "line2": 'address.line2'
            }
        }
        response = self.client.put(self.url_invoice_detail, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        items = self.invoice.items.all()
        assert items.count() == 2
        assert response.data == {
            "id": response.data['id'],
            "created": response.data['created'],
            "modified": response.data['modified'],
            "rep": "12",
            "awb": "123",
            "fob": "123",
            "tax_rate": "12.00",
            "shipping_price": "12.00",
            "hotel": "hotel name",
            "tracking_number": "12345",
            "attention": None,
            "contact_number": "081000000",
            "company": None,
            "shipment": self.shipment.pk,
            "address": {
                "id": self.shipment_address.id,
                "line1": 'address.line1',
                "line2": 'address.line2',
                "post_code": self.shipment_address.post_code,
                "city": self.shipment_address.city,
                "country": {
                    "id": self.shipment_address.country.id,
                    "code2": self.shipment_address.country.code2,
                    "name": self.shipment_address.country.name,
                    'postal_codes': [],
                    "state_required": self.shipment_address.country.state_required,
                    "postcode_required": self.shipment_address.country.postcode_required
                },
                "region": {
                    "id": self.shipment_address.region.id,
                    "name": self.shipment_address.region.name
                }
            },
            "items": [
                {
                    "id": item.id,
                    "name": "aphiwe",
                    "description": "aphiwe",
                    "price": "12.00",
                    "quantity": 2,
                    "invoice": self.invoice.id
                } for item in items
            ]

        }

    def test_hotel_admin_can_create_invoice(self):
        new_shipping_item = factories.ShippingItemFactory()
        new_shipment = new_shipping_item.details
        address = factories.AddressFactory(content_object=new_shipment)
        response = self.client_hotel_admin.post(self.url_invoice_list, data={
            "rep": "12",
            "awb": "123",
            "fob": "123",
            "tax_rate": "12.00",
            "attention": None,
            "hotel": "hotel name",
            "tracking_number": "12345",
            "contact_number": "081000000",
            "shipping_price": "12.00",
            "shipment": new_shipment.pk,
            "items": [{"name": "aphiwe", "description": "aphiwe", "price": "12.00", "quantity": 2}],
            "address": {
                "id": address.pk,
                "post_code": address.post_code,
                "city": address.city,
                "region": address.region.pk,
                "country": address.country.pk,
                "line1": address.line1,
                "line2": address.line2
            }
        })
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data == {
            "id": response.data['id'],
            "created": response.data['created'],
            "modified": response.data['modified'],
            "rep": "12",
            "awb": "123",
            "fob": "123",
            "tax_rate": "12.00",
            "hotel": "hotel name",
            "tracking_number": "12345",
            "attention": None,
            "contact_number": "081000000",
            "shipping_price": "12.00",
            "company": None,
            "shipment": new_shipment.pk,
            "address": {
                "id": address.pk,
                "line1": address.line1,
                "line2": address.line2,
                "post_code": address.post_code,
                "city": address.city,
                "country": {
                    "id": address.country.id,
                    "code2": address.country.code2,
                    "name": address.country.name,
                    'postal_codes': [],
                    "state_required": address.country.state_required,
                    "postcode_required": address.country.postcode_required
                },
                "region": {
                    "id": address.region.id,
                    "name": address.region.name
                }
            },
            "items": [{
                "id": response.data['items'][0]['id'],
                "name": "aphiwe",
                "description": "aphiwe",
                "price": "12.00",
                "quantity": 2,
                "invoice": response.data['id']
            }]
        }

    def test_hotel_admin_user_can_delete(self):
        self.assertEqual(Invoice.objects.count(), 1)
        response = self.client_hotel_admin.delete(self.url_invoice_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_simple_hotel_user_cannot_view_invoices(self):
        response = self.client_hotel_staff.get(self.url_invoice_list)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class InvoiceModelTests(TestCase):
    def test_invoice_totals(self):
        invoice = factories.InvoiceFactory(shipping_price=200)
        item = factories.InvoiceItemFactory(invoice=invoice, price=10)
        self.assertEqual(item.total, round((item.price + (item.price * Decimal(invoice.tax_rate / 100))) * item.quantity), 2)
        self.assertEqual(invoice.sub_total, round(sum([x.total for x in invoice.items.all()])), 2)
        self.assertEqual(invoice.total, round(invoice.sub_total + invoice.shipping_price, 2))
