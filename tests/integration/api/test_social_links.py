from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.db.transaction import atomic
from rest_framework import status
from return_hound import enums
from tests import factories
from tests.utils import AuthenticatedAPITestCase


class SocialLinkAPI(AuthenticatedAPITestCase):
    def setUp(self):
        super(SocialLinkAPI, self).setUp()
        self.setup_hotel()
        self.client = self.client_hotel_admin
        self.url = reverse("social_link")

    def test_list_view(self):
        # RH site Facebook link... should NOT be in list response
        factories.SocialLinkFactory(hotel=None)
        # Hotel Facebook link... should be in list response
        link = factories.SocialLinkFactory()
        # Links should be unique on (hotel, provider)
        # Should not be allowed to create duplicate link:
        with atomic():
            with self.assertRaises(IntegrityError):
                factories.SocialLinkFactory()
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            'count': 1,
            'next': None, 'previous': None,
            'results': [
                {
                    'id': link.pk,
                    'link': link.link,
                    'created': response.data['results'][0]['created'],
                    'modified': response.data['results'][0]['modified'],
                    'provider': {
                        'value': link.provider.value,
                        'label': link.provider.label,
                        'name': link.provider.name
                    }
                }
            ]
        }

    def test_create_view(self):
        data = {
            'link': 'http://facebook.com/123',
            'provider': enums.SocialProviders.FACEBOOK.value
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data == {
            'id': response.data['id'],
            'link': 'http://facebook.com/123',
            'created': response.data['created'],
            'modified': response.data['modified'],
            'provider': {
                'value': enums.SocialProviders.FACEBOOK.value,
                'label': enums.SocialProviders.FACEBOOK.label,
                'name': enums.SocialProviders.FACEBOOK.name
            }
        }

    def test_update_view(self):
        data = {
            'link': 'http://google.com/123',
            'provider': enums.SocialProviders.GOOGLE.value
        }
        social_link = factories.SocialLinkFactory(hotel=self.hotel)
        url = reverse("social_link-detail", kwargs={'pk': social_link.pk})
        response = self.client.put(url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            'id': response.data['id'],
            'link': 'http://google.com/123',
            'created': response.data['created'],
            'modified': response.data['modified'],
            'provider': {
                'value': enums.SocialProviders.GOOGLE.value,
                'label': enums.SocialProviders.GOOGLE.label,
                'name': enums.SocialProviders.GOOGLE.name
            }
        }

    def test_delete_view(self):
        social_link = factories.SocialLinkFactory(hotel=self.hotel)
        url = reverse("social_link-detail", kwargs={'pk': social_link.pk})
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT
