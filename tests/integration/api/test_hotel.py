from unittest.mock import patch, MagicMock

from django.conf import settings
from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.fields import DateTimeField
from rest_framework.test import APITestCase

from return_hound.models import Hotel
from tests.factories import HotelFactory, HotelUserFactory, ReportStyleFactory, UserFactory, DubaiAddressFactory
from tests.utils import AuthenticatedAPITestCase


@patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', MagicMock(return_value=True))
class HotelTestCase(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.client = self.client_hotel_admin  # default access as Hotel admin user
        self.report_style = ReportStyleFactory()

    def test_hotel_profile(self):
        response = self.client.get(reverse("hotel_profile"))
        self.assertEqual(response.data, {
            "id": self.hotel.pk,
            "domain_name": self.hotel.domain_name,
            "number_of_rooms": self.hotel.number_of_rooms,
            "name": "Test hotel",
            "slug": "test-hotel",
            "address": {
                "id": self.hotel.address.id,
                "line1": self.hotel.address.line1,
                "line2": self.hotel.address.line2,
                "city": self.hotel.address.city,
                "post_code": "",
                'region': {
                    'id': self.hotel.address.region.pk,
                    'name': self.hotel.address.region.name
                },
                'country': {
                    'id': self.hotel.address.country.pk,
                    'code2': self.hotel.address.country.code2,
                    'name': self.hotel.address.country.name,
                    'postal_codes': [],
                    'postcode_required': False,
                    'state_required': False
                },
            },
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_hotel_update_address(self):
        assert self.hotel.address.line2 == ''  # we will try to change this
        data = {
            "address": {
                "line1": self.hotel.address.line1,
                "line2": 'TEST UPDATE',
                "city": 'TEST UPDATE',
                "post_code": 'TEST UPDATE',
                'region': self.hotel.address.region.pk,
                'country': self.hotel.address.country.pk,
            },
        }
        response = self.client.patch(reverse("hotel_profile"), data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "id": self.hotel.pk,
            "domain_name": self.hotel.domain_name,
            "number_of_rooms": self.hotel.number_of_rooms,
            "name": "Test hotel",
            "slug": "test-hotel",
            "address": {
                "id": self.hotel.address.id,
                "line1": self.hotel.address.line1,
                "line2": 'TEST UPDATE',
                "city": 'TEST UPDATE',
                "post_code": 'TEST UPDATE',
                'region': {
                    'id': self.hotel.address.region.pk,
                    'name': self.hotel.address.region.name
                },
                'country': {
                    'id': self.hotel.address.country.pk,
                    'code2': self.hotel.address.country.code2,
                    'postal_codes': [],
                    'name': self.hotel.address.country.name,
                    'postcode_required': False,
                    'state_required': False
                },
            },
        }

    def test_simple_hotel_user_cannot_update_hotel_profile(self):
        data = {
            "address": "Address update",
        }
        response = self.client_hotel_staff.patch(reverse("hotel_profile"), data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_hotel_report_style(self):
        response = self.client.get(reverse("hotel_report_style"))
        self.assertEqual(response.data, {
            "icon_color": "#027fba",
            "button_primary_color": "#027fba",
            "form_label_color": "#2e2e2e",
            "header_active_text_color": "#FFFFFF",
            "button_primary_text_color": "#FFFFFF",
            "form_field_border_color": "#cccccc",
            "form_field_color": "#2e2e2e",
            "hotel": self.report_style.hotel.pk,
            "button_border_radius": 6,
            "hotel_logo": [],
            "id": self.report_style.pk,
            "header_inactive_color": "#027fba",
            "show_hotel_logo": False,
            "header_inactive_text_color": "#027fba",
            "form_background_color": "#FFFFFF",
            "cover_images": [],
            "form_header_color": "#666666",
            "form_field_border_radius": 6,
            "button_secondary_color": "#484848",
            "show_cover_image": False,
            "button_secondary_text_color": "#FFFFFF",
            "header_color": "#027fba",
            "header_active_color": "#2e2e2e"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_report_style_update(self):
        data = {
             "icon_color": "#000000",
        }
        response = self.client.patch(reverse("hotel_report_style"), data=data)
        self.assertEqual(response.data, {
            "icon_color": "#000000",
            "button_primary_color": "#027fba",
            "form_label_color": "#2e2e2e",
            "header_active_text_color": "#FFFFFF",
            "button_primary_text_color": "#FFFFFF",
            "form_field_border_color": "#cccccc",
            "form_field_color": "#2e2e2e",
            "hotel": self.report_style.hotel.pk,
            "button_border_radius": 6,
            "hotel_logo": [],
            "id": self.report_style.pk,
            "header_inactive_color": "#027fba",
            "show_hotel_logo": False,
            "header_inactive_text_color": "#027fba",
            "form_background_color": "#FFFFFF",
            "cover_images": [],
            "form_header_color": "#666666",
            "form_field_border_radius": 6,
            "button_secondary_color": "#484848",
            "show_cover_image": False,
            "button_secondary_text_color": "#FFFFFF",
            "header_color": "#027fba",
            "header_active_color": "#2e2e2e"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_simple_hotel_user_cannot_update_hotel_report_style(self):
        data = {
            "icon_color": "#000000",
        }
        response = self.client_hotel_staff.patch(reverse("hotel_report_style"), data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_hotel_report(self):
        url = reverse("hotel_report-detail", kwargs={'pk': self.hotel.pk})
        response = self.client.get(url, data={'slug': self.hotel.slug})
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            'id': self.hotel.pk,
            'slug': self.hotel.slug,
            'domain_name': self.hotel.domain_name,
            "number_of_rooms": self.hotel.number_of_rooms,
            'name': self.hotel.name,
            'report_style': {
                'cover_images': [],
                'show_cover_image': False,
                'header_active_color': '#2e2e2e',
                'form_header_color': '#666666',
                'icon_color': '#027fba',
                'id': self.report_style.pk,
                'button_primary_color': '#027fba',
                'form_field_border_color': '#cccccc',
                'header_color': '#027fba',
                'button_border_radius': 6,
                'show_hotel_logo': False,
                'form_field_color': '#2e2e2e',
                'form_background_color': '#FFFFFF',
                'button_primary_text_color': '#FFFFFF',
                'header_inactive_text_color': '#027fba',
                'hotel': self.hotel.pk,
                'header_active_text_color': '#FFFFFF',
                'button_secondary_text_color': '#FFFFFF',
                'form_label_color': '#2e2e2e',
                'hotel_logo': [],
                'button_secondary_color': '#484848',
                'header_inactive_color': '#027fba',
                'form_field_border_radius': 6
            },
            'address': {
                "id": self.hotel.address.id,
                'country': {
                    'id': self.hotel.address.country.pk,
                    'code2': self.hotel.address.country.code2,
                    'name': self.hotel.address.country.name,
                    'postal_codes': [],
                    'postcode_required': False,
                    'state_required': False
                },
                'region': {
                    'id': self.hotel.address.region.pk,
                    'name': self.hotel.address.region.name
                },
                "line1": self.hotel.address.line1,
                "line2": self.hotel.address.line2,
                "city": self.hotel.address.city,
                "post_code": "",
            },
        }

    def test_invalid_hotel_report(self):
        response = self.client.get(reverse("hotel_report-detail", kwargs={'pk': self.hotel.pk}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class HotelListAPI(APITestCase):
    def setUp(self):
        self.url = reverse('hotel')
        self.hotel_1 = HotelFactory(name='C')
        self.hotel_2 = HotelFactory(name='E')
        self.hotel_3 = HotelFactory(name='B')
        self.hotel_4 = HotelFactory(name='D')
        self.hotel_5 = HotelFactory(name='F')
        # previous hotels are setup with Jordan(Amman) address
        # setup a hotel in Dubai to test HotelFilter
        self.hotel_6 = HotelFactory(name='A', address=DubaiAddressFactory())

    def test_hidden_hotels_not_in_list_response(self):
        # create a hidden hotel
        hidden = HotelFactory(is_hidden=True)
        resp = self.client.get(self.url)
        assert hidden.pk not in [h['id'] for h in resp.data['results']]

    def test_hotel_model_default_order(self):
        qs = Hotel.objects.all()
        assert [h.name for h in qs] == ['A', 'B', 'C', 'D', 'E', 'F']

    def test_hotel_list_alphabetical_order(self):
        response = self.client.get(self.url)
        self.assertEqual(response.data, {
            "count": 6,
            "next": None,
            "previous": None,
            "results": [{
                'id': self.hotel_6.pk,
                'name': self.hotel_6.name
            },
              {
                'id': self.hotel_3.pk,
                'name': self.hotel_3.name
              },
              {
                'id': self.hotel_1.pk,
                'name': self.hotel_1.name
              },
              {
                'id': self.hotel_4.pk,
                'name': self.hotel_4.name

              },
              {
                'id': self.hotel_2.pk,
                'name': self.hotel_2.name

              },
              {
                'id': self.hotel_5.pk,
                'name': self.hotel_5.name
              }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_hotel_list_filter(self):
        country_code = self.hotel_6.address.aramex_country.code2  # Dubai
        region_id = self.hotel_6.address.aramex_state.id  # Dubai
        response = self.client.get(self.url, data={
            'country': country_code,
            'region': region_id
        })
        assert response.data['count'] == 1
        assert response.data['results'][0]['id'] == self.hotel_6.pk


class HotelUserApiTests(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.client = self.client_hotel_admin  # default access as Hotel admin/creator
        self.hotel_creator = self.hoteluser_admin
        self.hotel_user_limited = self.hoteluser_staff

    def datetime(self, model_value):
        return DateTimeField().to_representation(model_value)

    def test_hotel_creator_or_admin_can_see_users_list(self):
        hotel_users = [self.hotel_creator, self.hotel_user_limited]
        response = self.client.get(reverse("hotel_users"))
        self.assertEqual(response.data, {
            "count": 2,
            "next": None,
            "previous": None,
            "results": [{
                "id": hu.pk,
                "created": self.datetime(hu.created),
                "is_admin": hu.is_admin,
                "is_creator": hu.is_creator,
                'is_supervisor': hu.is_supervisor,
                "is_mail_recipient": hu.is_mail_recipient,
                "employer": self.hotel.pk,
                "user_first_name": "test user",
                "user_last_name": "Test",
                "user_contact_number": "1234567890"
            } for hu in hotel_users]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # TODO: test with admin user
        # TODO: test for 403:Forbiddden with limited (non admin) user

    def test_hotel_user_admin_permission_update(self):
        data = {
            'is_creator': True,
            'is_admin': False,
            'is_mail_recipient': True,
        }
        response = self.client.patch(reverse("hotel_users-detail", kwargs={'pk': self.hotel_creator.pk}), data=data)
        self.assertEqual(response.data, {
            "id": self.hotel_creator.pk,
            "created": self.datetime(self.hotel_creator.created),
            "is_admin": False,
            'is_supervisor': False,
            "employer": self.hotel_creator.employer_id,
            "is_creator": True,
            "is_mail_recipient": True,
            "user_first_name": "test user",
            "user_last_name": "Test",
            "user_contact_number": "1234567890"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Your ReturnHound account permissions have been updated.')

    def test_max_mail_recipients(self):
        for i in range(settings.HOTEL_MAX_MAIL_USERS):
            HotelUserFactory(
                employer=self.hotel, is_mail_recipient=True,
                employee=UserFactory(email='mail.user{}@hotel.com'.format(i)))

        response = self.client.patch(reverse("hotel_users-detail", kwargs={'pk': self.hoteluser_staff.pk}), data={
            'is_creator': True,
            'is_admin': False,
            'is_mail_recipient': True,
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'is_mail_recipient': ['A maximum of {} users has been reached.'.format(settings.HOTEL_MAX_MAIL_USERS)]
        })

    def test_hotel_creator_can_create_user(self):
        data = {
            "contact_number": "082000000",
            "email": "hoteuser@test.com",
            "employer": self.hotel_creator.employer_id,
            "first_name": "TEST",
            "is_admin": False,
            'is_supervisor': True,
            'is_mail_recipient': True,
            "last_name": "TEST",
        }
        response = self.client.post(reverse("hotel_users"), data=data)
        self.assertEqual(response.data, {
            "id": response.data.get('id'),
            "created": response.data.get('created'),
            "is_admin": False,
            'is_supervisor': True,
            'is_mail_recipient': True,
            "employer": self.hotel_creator.employer_id,
            "is_creator": False,
            "user_first_name": "TEST",
            "user_last_name": "TEST",
            "user_contact_number": "082000000"
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Welcome to your ReturnHound account.')

    def test_create_user_duplicate_emails_not_allowed(self):
        data = {
            "contact_number": "082000000",
            "email": self.hoteluser_staff.employee.email,  # existing email
            "employer": self.hotel.id,
            "first_name": "TEST",
            "is_admin": False,
            "last_name": "TEST",
        }
        response = self.client.post(reverse("hotel_users"), data=data)
        self.assertEqual(response.data, {'email': ['This email address is in use.']})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(mail.outbox), 0)

    def test_limited_user_cant_increase_own_permissions(self):
        user_detail_url = reverse("hotel_users-detail", kwargs={'pk': self.hotel_user_limited.pk})
        response = self.client_hotel_staff.patch(user_detail_url, data={
            'is_admin': True,
            'is_mail_recipient': True,
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(len(mail.outbox), 0)

    def test_limited_user_can_update_own_details(self):
        limited_user_own_detail = reverse("hotel_users-detail", kwargs={'pk': self.hotel_user_limited.pk})
        response = self.client.patch(limited_user_own_detail, data={
            'first_name': 'Update first name'
        })
        self.assertEqual(response.data, {
            "id": self.hotel_user_limited.pk,
            "created": self.datetime(self.hotel_user_limited.created),
            "is_admin": False,
            'is_supervisor': False,
            "employer": self.hotel_user_limited.employer_id,
            "is_creator": False,
            "is_mail_recipient": False,
            "user_first_name": "Update first name",
            "user_last_name": "Test",
            "user_contact_number": "1234567890"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_limited_user_cant_update_details_of_other_users(self):
        # try to change the details of the admin/creator user
        other_user_url = reverse("hotel_users-detail", kwargs={'pk': self.hotel_creator.pk})
        response = self.client_hotel_staff.patch(other_user_url, data={
            'first_name': 'Update first name',
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_limited_user_cant_create_users(self):
        response = self.client_hotel_staff.post(reverse("hotel_users"), data={
            "contact_number": "082000000",
            "email": "hoteuser@test.com",
            "employer": self.hotel_user_limited.employer.pk,
            "first_name": "TEST",
            "is_admin": False,
            'is_supervisor': True,
            'is_mail_recipient': True,
            "last_name": "TEST",
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(len(mail.outbox), 0)

    def test_limited_user_cant_delete_users(self):
        own_url = reverse("hotel_users-detail", kwargs={'pk': self.hotel_user_limited.pk})
        response = self.client_hotel_staff.delete(own_url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_there_must_be_at_least_one_hotel_creator(self):
        creator_detail_url = reverse("hotel_users-detail", kwargs={'pk': self.hotel_creator.pk})
        response = self.client.patch(creator_detail_url, data={
            'is_creator': False
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'is_creator': ['There must be at least one account owner.']})
        # TODO: test transfer of ownership (is_creator) to another HotelUser
