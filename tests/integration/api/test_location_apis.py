from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from return_hound.apps.aramex.models import AramexCountry
from tests import factories


class LocationApiTests(APITestCase):
    def setUp(self):
        self.country_api = reverse('country')
        self.region_api = reverse('region')
        # country without region...
        self.za = factories.AramexCountryFactory(name='South Africa', code2='ZA')
        # country with region...
        self.jordan = factories.AramexCountryFactory()
        self.amman = factories.AramexStateFactory()  # state/region in Jordan

    def find_result(self, resp, field, value):
        return next((i for i in resp.data if i[field] == value), None)

    def test_country_list(self):
        resp = self.client.get(self.country_api)
        self.assertTrue(isinstance(resp.data, list))
        self.assertEqual(len(resp.data), AramexCountry.objects.count())
        self.assertIsNotNone(self.find_result(resp, 'name', 'South Africa'))

    def test_country_list_search(self):
        resp = self.client.get(self.country_api, data={'search': 'south africa'})
        assert resp.data == [{
            'id': self.za.id,
            'code2': 'ZA',
            'name': 'South Africa',
            'postal_codes': [],
            'postcode_required': self.za.postcode_required,
            'state_required': self.za.state_required
        }]

    def test_regions_list(self):
        resp = self.client.get(self.region_api, data={'country': 'JO'})
        self.assertIsNotNone(self.find_result(resp, 'name', 'Amman'))
