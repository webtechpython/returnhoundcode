from unittest.mock import patch, MagicMock
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.models import MatchedItem
from tests import factories
from tests.utils import AuthenticatedAPITestCase


MOCK_SHIPMENT = MagicMock(return_value={
    'id': 'ARAMEX-TRACKING-NUMBER-123',
    'label': b'moo',
})


def mock_rate(**kwargs):
    values = {
        'amount': '100.0',
        'currency': 'AED',
        'shipping_group': 'Domestic',
        'shipping_type': 'Priority Parcel Domestic',
        'has_error': False,
        'errors': []
    }
    values.update(kwargs)
    return MagicMock(return_value=values)


@patch('return_hound.apps.aramex.api.AramexShippingClient.create_shipment', MOCK_SHIPMENT)
class ShippingRatesTestCase(AuthenticatedAPITestCase):
    """
    Currency rates returned from the api should be in AED if the origin country is UAE.
    Rates for all other accepted international countries should be returned in USD and should be the
    correctly converted amount.
    current accepted currencies: ['AED', 'OMR', 'QAR', 'SAR', 'KWD', 'JOD', 'BHD', 'USD', 'EGP']
    """

    def setUp(self):
        self.setup_hotel()
        self.shipping_item = factories.ShippingItemFactory()
        self.shipping_item.parcel.status = MatchedItem.STATUS.accepted
        self.shipping_item.parcel.save()
        self.user_client = self.client_for(self.shipping_item.parcel.lost_item.created_by)

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate())
    def test_default_currency(self):
        """
        Full response test, the rest of the test will only check the currency and rate
        """
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, {
            'shipping_rate': {
                'shipping_group': 'Domestic',
                'shipping_type': 'Priority Parcel Domestic',
                'has_error': False,
                'currency': 'AED',
                'amount': '100.0',
                'errors': []
            }
        })

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='ZAR'))
    def test_unsupported_currency(self):
        """ZAR is currently an unsupported currency"""
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['server_error'], 'ZAR is an unsupported currency.')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='OMR'))
    def test_OMR_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '259.74')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='QAR'))
    def test_QAR_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '27.47')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='SAR'))
    def test_SAR_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '26.67')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='KWD'))
    def test_KWD_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '327.87')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='JOD'))
    def test_JOD_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '141.04')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='BHD'))
    def test_BHD_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '265.96')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='USD'))
    def test_USD_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '100.0')

    @patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', mock_rate(currency='EGP'))
    def test_EGP_rates(self):
        response = self.user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['shipping_rate']['currency'], 'USD')
        self.assertEqual(response.data['shipping_rate']['amount'], '5.52')
