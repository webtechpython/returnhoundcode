from django.core.urlresolvers import reverse
from unittest import skip

from rest_framework import status
from return_hound.models import Transaction

from tests import factories
from tests.utils import AuthenticatedAPITestCase


class PaymentGatewayTest(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.matched_item = factories.MatchedItemFactory()
        self.shipment = factories.ShipmentFactory()
        self.shipping_item = factories.ShippingItemFactory(details=self.shipment, parcel=self.matched_item)
        self.address = factories.AddressFactory(content_object=self.shipment)
        self.transaction = factories.TransactionFactory(shipping_details=self.shipment,
                                                        charged_amount=100.00,
                                                        surcharge_amount=20.00,
                                                        is_paid=False)
        self.no_item_transaction = factories.TransactionFactory()
        # payment requests are done by LostItem creator...
        self.client = self.client_for(self.matched_item.lost_item.created_by)

    @skip(reason="Need mock api response for Telr")
    def test_valid_payment(self):
        data = {
            'transaction': self.transaction.pk,
            'terms': True
        }
        response = self.client.post(reverse('payment'), data=data)
        self.assertEqual(response.data, {
            'transaction': self.transaction.pk,
            'charged_amount': 100.00,
            'is_paid': self.transaction.is_paid,
            'transaction_url': {
                'trace': response.data['transaction_url']['trace'],
                'method': 'create',
                'order': {
                    'ref': response.data['transaction_url']['order']['ref'],
                    'url': response.data['transaction_url']['order']['url']
                }
            }
        })
        self.assertEqual(Transaction.objects.get(pk=self.transaction.pk).reference_number,
                         response.data['transaction_url']['order']['ref'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_invalid_payment(self):
        data = {
            'transaction': self.no_item_transaction.pk,
            'terms': False
        }
        response = self.client.post(reverse('payment'), data=data)
        self.assertEqual(response.data, {
            'transaction': ['There is no items linked to this transaction.'],
            'terms': ['Please accept the terms and conditions.']
        })
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @skip(reason="Need mock api response for Telr")
    def test_payment_status(self):
        data = {
            'transaction': self.transaction.pk,
            'terms': True
        }
        self.client.post(reverse('payment'), data=data)
        resp = self.client.get(reverse('payment_status-detail', kwargs={'pk': self.transaction.pk}))
        self.assertEqual(resp.data, {
            'id': self.transaction.pk,
            'is_paid': False,
            'charged_amount': '100.00',
            'transaction_status': {
                'order': {
                    'amount': '118.00',
                    'url': resp.data['transaction_status']['order']['url'],
                    'cartid': resp.data['transaction_status']['order']['cartid'],
                    'ref': resp.data['transaction_status']['order']['ref'],
                    'description': 'Test description',
                    'currency': 'AED',
                    'status': {
                        'code': resp.data['transaction_status']['order']['status']['code'],
                        'text': resp.data['transaction_status']['order']['status']['text']
                    },
                    'test': 1
                },
                'method': 'check',
                'trace': resp.data['transaction_status']['trace']
            },
        })
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_insurance_service_charge_update(self):
        data = {
            'service_charge': 0,
            'insurance_fee': 0
        }
        response = self.client.put(reverse('payment_status-charges', kwargs={'pk': self.transaction.pk}), data=data)
        self.assertEqual(response.data, {
            'id': self.transaction.id,
            'service_charge': "0.00",
            'insurance_fee': "0.00"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_payment_status_not_found(self):
        resp = self.client.get(reverse('payment_status-detail', kwargs={'pk': self.no_item_transaction.pk}))
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)
