from datetime import timedelta
from django.core.urlresolvers import reverse
from django.utils.timezone import now

from rest_framework import status

from tests.utils import AuthenticatedAPITestCase
from tests.factories import (
    HotelFactory, HotelUserFactory, ReportStyleFactory, TransactionFactory, ShipmentFactory, MatchedItemFactory,
    ShippingItemFactory, FoundItemFactory, UserFactory
)


class PaymentReportTestCase(AuthenticatedAPITestCase):

    def setUp(self):
        self.setup_hotel()
        self.report_style = ReportStyleFactory()
        self.shipment = ShipmentFactory()
        self.matched_item = MatchedItemFactory()
        self.shipping_item = ShippingItemFactory(details=self.shipment, parcel=self.matched_item)
        self.transaction = TransactionFactory(
            shipping_details=self.shipment, charged_amount=100.00, surcharge_amount=20.00, is_paid=False
        )
        # this Transaction belongs to hotel 2 and the logged in user belongs to hotel 1, he should never see it
        user = UserFactory(email='test2@byteorbit.com')
        shipment = ShipmentFactory()
        matched_item = MatchedItemFactory(
            found_item=FoundItemFactory(created_by=user, hotel=HotelFactory(name='Test hotel2'))
        )
        ShippingItemFactory(details=shipment, parcel=matched_item)
        TransactionFactory(
            shipping_details=shipment, charged_amount=100.00, surcharge_amount=20.00, is_paid=False
        )

    def test_list_view(self):
        response = self.client_hotel_admin.get(reverse("payment_report"))
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_view_filters(self):
        timestamp = now()
        frmt = '%d/%m/%Y'
        query = '?created__gte={}&created__lte={}'.format(timestamp.strftime(frmt), timestamp.strftime(frmt))
        response = self.client_hotel_admin.get(reverse("payment_report") + query)
        self.assertEqual(response.data, {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # should result one object, transactions from hotel should be excluded
        query = '?start={}&end={}'.format(
            self.transaction.created.strftime(frmt), (timestamp + timedelta(days=30)).strftime(frmt)
        )
        response = self.client_hotel_admin.get(reverse("payment_report") + query)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_export_csv(self):
        response = self.client_hotel_admin.get(reverse("payment_report") + '?format=csv')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.accepted_media_type, 'text/csv')
