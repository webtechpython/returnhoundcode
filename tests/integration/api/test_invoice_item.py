from django.core.urlresolvers import reverse
from rest_framework import status
from tests import factories
from tests.utils import AuthenticatedAPITestCase


class InvoiceItemTestCase(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.shipping_item = factories.ShippingItemFactory()
        self.shipment = self.shipping_item.details
        self.invoice = factories.InvoiceFactory(shipment=self.shipment)
        self.invoice_item = factories.InvoiceItemFactory(invoice=self.invoice)
        self.url_item_detail = reverse("invoice_item-detail", kwargs={'pk': self.invoice_item.pk})

    def test_hotel_admin_can_delete_invoice_item(self):
        response = self.client_hotel_admin.delete(self.url_item_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_hotel_staff_user_cannot_delete_invoice_item(self):
        response = self.client_hotel_staff.delete(self.url_item_detail)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
