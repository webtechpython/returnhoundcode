from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.enums import FoundItemStatus, MatchedItemStatus
from return_hound.models import LostItem, FoundItem, MatchedItem
from tests import factories
from tests.utils import AuthenticatedAPITestCase


class MatchedItemTestCase(AuthenticatedAPITestCase):

    def setUp(self):
        self.setup_hotel()
        self.matched_item = factories.MatchedItemFactory()
        self.lost_item = self.matched_item.lost_item  # type: LostItem
        self.found_item = self.matched_item.found_item  # type: FoundItem
        self.client = self.client_hotel_admin  # TODO: more client role/permission cleanup
        self.reporter = self.client_for(self.lost_item.created_by)

    def test_lost_item_match(self):
        """
        The user who reported the LostItem can see a list of matches
        """
        response = self.reporter.get(reverse("my_lost_items-matched-items", kwargs={'pk': self.matched_item.lost_item.pk}))
        self.assertEqual(response.data, [{
            "id": self.matched_item.pk,
            "created": self.matched_item.created,
            "found_item": {
                "id": self.matched_item.found_item.pk,
                "created": self.matched_item.found_item.created,
                "created_by": {
                    "id": self.found_item.created_by.pk,
                    "first_name": self.found_item.created_by.first_name,
                    "last_name": self.found_item.created_by.last_name,
                    "contact_number": self.found_item.created_by.contact_number,
                    "email": self.found_item.created_by.email
                },
                "category": self.matched_item.found_item.category.pk,
                "item_type": self.matched_item.found_item.item_type.pk,
                "description": self.matched_item.found_item.description,
                "serial_number": self.matched_item.found_item.serial_number,
                "specification": self.matched_item.found_item.specification,
                "hotel": self.matched_item.found_item.hotel.pk,
                "found_by": self.matched_item.found_item.found_by,
                "date": self.matched_item.found_item.date,
                "room_number": self.matched_item.found_item.room_number,
                "location": self.matched_item.found_item.location,
                "owner_name": self.matched_item.found_item.owner_name,
                "owner_email": self.matched_item.found_item.owner_email,
                "owner_surname": self.matched_item.found_item.owner_surname,
                "owner_contact_number": self.matched_item.found_item.owner_contact_number,
                "category_name": self.matched_item.found_item.category.name,
                "item_type_name": self.matched_item.found_item.item_type.name,
                "status": None,
                "status_text": None,
                "images": [],
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
            },
            "status": self.matched_item.status,
            "status_display": self.matched_item.get_status_display(),
            "assigned_status": None,
            "status_text": '',
            "shipment_status": False
        }])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_matched_item_update(self):
        """
        User that reported/created the LostItem can accept the match
        """
        data = {
            "status": MatchedItem.STATUS.accepted
        }
        url = reverse("matched_items-detail", kwargs={'pk': self.matched_item.pk})
        response = self.reporter.patch(url, data=data)
        self.assertEqual(response.data, {
            "id": self.matched_item.pk,
            "created": self.matched_item.created,
            "found_item": {
                "id": self.matched_item.found_item.pk,
                "created": self.matched_item.found_item.created,
                "created_by": {
                    "id": self.matched_item.found_item.created_by.pk,
                    "first_name": self.matched_item.found_item.created_by.first_name,
                    "last_name": self.matched_item.found_item.created_by.last_name,
                    "contact_number": self.matched_item.found_item.created_by.contact_number,
                    "email": self.matched_item.found_item.created_by.email
                },
                "category": self.matched_item.found_item.category.pk,
                "item_type": self.matched_item.found_item.item_type.pk,
                "description": self.matched_item.found_item.description,
                "serial_number": self.matched_item.found_item.serial_number,
                "specification": self.matched_item.found_item.specification,
                "hotel": self.matched_item.found_item.hotel.pk,
                "found_by": self.matched_item.found_item.found_by,
                "date": self.matched_item.found_item.date,
                "room_number": self.matched_item.found_item.room_number,
                "location": self.matched_item.found_item.location,
                "owner_name": self.matched_item.found_item.owner_name,
                "owner_email": self.matched_item.found_item.owner_email,
                "owner_surname": self.matched_item.found_item.owner_surname,
                "owner_contact_number": self.matched_item.found_item.owner_contact_number,
                "category_name": self.matched_item.found_item.category.name,
                "item_type_name": self.matched_item.found_item.item_type.name,
                "images": [],
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
                "status": None,
                "status_text": None,
            },
            "status": MatchedItem.STATUS.accepted,
            "assigned_status": None,
            "status_text": '',
            "status_display": "Accepted",
            "shipment_status": False
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_hotel_matched_item_create(self):
        data = {
            'lost_item': self.lost_item.pk,
            'found_item': self.found_item.pk
        }
        response = self.client.post(reverse("hotel_matched_items"), data=data)
        self.assertEqual(response.data, {
            'id': response.data.get('id'),
            'created': response.data.get('created'),
            'status': MatchedItem.STATUS.pending,
            'status_display': 'Pending',
            'lost_item': self.lost_item.pk,
            'assigned_status': None,
            'status_text': '',
            'found_item': self.found_item.pk,
            "lost_item_object": {
                "id": self.lost_item.pk,
                "created_by": {
                    "id": self.lost_item.created_by.pk,
                    "first_name": self.lost_item.created_by.first_name,
                    "last_name": self.lost_item.created_by.last_name,
                    "contact_number": self.lost_item.created_by.contact_number,
                    "email": self.lost_item.created_by.email
                },
                "arrival_date": self.lost_item.arrival_date,
                "departure_date": self.lost_item.departure_date,
                "description": self.lost_item.description,
                "specification": self.lost_item.specification,
                "serial_number": self.lost_item.serial_number,
                'category': {
                    'name': 'Clothing',
                    'id': self.lost_item.category.pk
                },
                "item_type": {
                    'name': 'Shirt',
                    'id': self.lost_item.item_type.pk,
                    'category': self.lost_item.category.pk
                },
                "images": [],
                "location": self.lost_item.location,
                "room_number": self.lost_item.room_number,
                "created": self.lost_item.created,
                "date": self.lost_item.date
            },
            "found_item_object": {
                "id": self.found_item.pk,
                "created": self.found_item.created,
                "created_by": {
                    "id": self.matched_item.found_item.created_by.pk,
                    "first_name": self.matched_item.found_item.created_by.first_name,
                    "last_name": self.matched_item.found_item.created_by.last_name,
                    "contact_number": self.matched_item.found_item.created_by.contact_number,
                    "email": self.matched_item.found_item.created_by.email
                },
                "category": self.found_item.category.pk,
                "item_type": self.found_item.item_type.pk,
                "description": self.found_item.description,
                "serial_number": self.found_item.serial_number,
                "specification": self.found_item.specification,
                "hotel": self.found_item.hotel.pk,
                "found_by": self.found_item.found_by,
                "date": self.found_item.date,
                "room_number": self.found_item.room_number,
                "location": self.found_item.location,
                "owner_name": self.found_item.owner_name,
                "owner_email": self.found_item.owner_email,
                "owner_surname": self.found_item.owner_surname,
                "owner_contact_number": self.found_item.owner_contact_number,
                "category_name": self.found_item.category.name,
                "item_type_name": self.found_item.item_type.name,
                "images": [],
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
                "status": None,
                "status_text": None,
            }
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Your lost item on ReturnHound has been found - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertEqual(mail.outbox[0].to, [self.lost_item.created_by.email])

    def test_hotel_matched_items(self):
        response = self.client.get(reverse("hotel_matched_items"))
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": self.matched_item.pk,
                "created": self.matched_item.created,
                "lost_item": self.matched_item.lost_item.pk,
                "found_item": self.matched_item.found_item.pk,
                "status": self.matched_item.status,
                "assigned_status": None,
                "status_text": '',
                "status_display": self.matched_item.get_status_display(),
                "lost_item_object": {
                    "id": self.matched_item.lost_item.pk,
                    "created_by": {
                        "id": self.matched_item.lost_item.created_by.pk,
                        "first_name": self.matched_item.lost_item.created_by.first_name,
                        "last_name": self.matched_item.lost_item.created_by.last_name,
                        "contact_number": self.matched_item.lost_item.created_by.contact_number,
                        "email": self.matched_item.lost_item.created_by.email
                    },
                    "arrival_date": self.matched_item.lost_item.arrival_date,
                    "departure_date": self.matched_item.lost_item.departure_date,
                    "description": self.matched_item.lost_item.description,
                    "specification": self.matched_item.lost_item.specification,
                    "serial_number": self.matched_item.lost_item.serial_number,
                    'category': {
                        'name': 'Clothing',
                        'id': self.matched_item.lost_item.category.pk
                    },
                    "item_type": {
                        'name': 'Shirt',
                        'id': self.matched_item.lost_item.item_type.pk,
                        'category': self.matched_item.lost_item.category.pk
                    },
                    "images": [],
                    "location": self.matched_item.lost_item.location,
                    "room_number": self.matched_item.lost_item.room_number,
                    "created": self.matched_item.lost_item.created,
                    "date": self.matched_item.lost_item.date
                },
                "found_item_object": {
                    "id": self.matched_item.found_item.pk,
                    "created": self.matched_item.found_item.created,
                    "created_by": {
                        "id": self.matched_item.found_item.created_by.pk,
                        "first_name": self.matched_item.found_item.created_by.first_name,
                        "last_name": self.matched_item.found_item.created_by.last_name,
                        "contact_number": self.matched_item.found_item.created_by.contact_number,
                        "email": self.matched_item.found_item.created_by.email
                    },
                    "category": self.matched_item.found_item.category.pk,
                    "item_type": self.matched_item.found_item.item_type.pk,
                    "description": self.matched_item.found_item.description,
                    "serial_number": self.matched_item.found_item.serial_number,
                    "specification": self.matched_item.found_item.specification,
                    "hotel": self.matched_item.found_item.hotel.pk,
                    "found_by": self.matched_item.found_item.found_by,
                    "date": self.matched_item.found_item.date,
                    "room_number": self.matched_item.found_item.room_number,
                    "location": self.matched_item.found_item.location,
                    "owner_name": self.matched_item.found_item.owner_name,
                    "owner_email": self.matched_item.found_item.owner_email,
                    "owner_surname": self.matched_item.found_item.owner_surname,
                    "owner_contact_number": self.matched_item.found_item.owner_contact_number,
                    "category_name": self.matched_item.found_item.category.name,
                    "item_type_name": self.matched_item.found_item.item_type.name,
                    "images": [],
                    "width": "10.00",
                    "height": "0.50",
                    "length": "10.00",
                    "weight": "0.50",
                    "stored_at": "",
                    "status": None,
                    "status_text": None,
                }
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_hotel_matched_item_detail(self):
        response = self.client.get(reverse("hotel_matched_items-detail", kwargs={'pk': self.matched_item.pk}))
        self.assertEqual(response.data, {
            "id": self.matched_item.pk,
            "created": self.matched_item.created,
            "lost_item": self.matched_item.lost_item.pk,
            "found_item": self.matched_item.found_item.pk,
            "status": self.matched_item.status,
            "status_display": self.matched_item.get_status_display(),
            "assigned_status": None,
            "status_text": '',
            "lost_item_object": {
                "id": self.matched_item.lost_item.pk,
                "created_by": {
                    "id": self.matched_item.lost_item.created_by.pk,
                    "first_name": self.matched_item.lost_item.created_by.first_name,
                    "last_name": self.matched_item.lost_item.created_by.last_name,
                    "contact_number": self.matched_item.lost_item.created_by.contact_number,
                    "email": self.matched_item.lost_item.created_by.email
                },
                "arrival_date": self.matched_item.lost_item.arrival_date,
                "departure_date": self.matched_item.lost_item.departure_date,
                "description": self.matched_item.lost_item.description,
                "specification": self.matched_item.lost_item.specification,
                "serial_number": self.matched_item.lost_item.serial_number,
                'category': {
                    'name': 'Clothing',
                    'id': self.matched_item.lost_item.category.pk
                },
                "item_type": {
                    'name': 'Shirt',
                    'id': self.matched_item.lost_item.item_type.pk,
                    'category': self.matched_item.lost_item.category.pk
                },
                "images": [],
                "location": self.matched_item.lost_item.location,
                "room_number": self.matched_item.lost_item.room_number,
                "created": self.matched_item.lost_item.created,
                "date": self.matched_item.lost_item.date
            },
            "found_item_object": {
                "id": self.matched_item.found_item.pk,
                "created": self.matched_item.found_item.created,
                "created_by": {
                    "id": self.found_item.created_by.pk,
                    "first_name": self.matched_item.found_item.created_by.first_name,
                    "last_name": self.matched_item.found_item.created_by.last_name,
                    "contact_number": self.matched_item.found_item.created_by.contact_number,
                    "email": self.matched_item.found_item.created_by.email
                },
                "category": self.matched_item.found_item.category.pk,
                "item_type": self.matched_item.found_item.item_type.pk,
                "description": self.matched_item.found_item.description,
                "serial_number": self.matched_item.found_item.serial_number,
                "specification": self.matched_item.found_item.specification,
                "hotel": self.matched_item.found_item.hotel.pk,
                "found_by": self.matched_item.found_item.found_by,
                "date": self.matched_item.found_item.date,
                "room_number": self.matched_item.found_item.room_number,
                "location": self.matched_item.found_item.location,
                "owner_name": self.matched_item.found_item.owner_name,
                "owner_email": self.matched_item.found_item.owner_email,
                "owner_surname": self.matched_item.found_item.owner_surname,
                "owner_contact_number": self.matched_item.found_item.owner_contact_number,
                "category_name": self.matched_item.found_item.category.name,
                "item_type_name": self.matched_item.found_item.item_type.name,
                "images": [],
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
                "status": None,
                "status_text": None,
            }
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_owner_matched_item(self):
        response = self.client.get(reverse("matched_items-known-owner-feedback", kwargs={'pk': str(self.matched_item.token)}))
        self.assertTrue(response.data, {
            'id': self.matched_item.pk,
            'created': self.matched_item.created,
            'found_item': self.matched_item.found_item.pk,
            'lost_item': self.matched_item.lost_item.pk,
            'status_display': self.matched_item.get_status_display(),
            'user_token': response.data.get('user_token'),
            'status': self.matched_item.status
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_owner_matched_item_update(self):
        data = {
            'status': MatchedItem.STATUS.accepted
        }
        response = self.client.put(reverse("matched_items-known-owner-feedback", kwargs={'pk': str(self.matched_item.token)}), data=data)
        self.assertTrue(response.data, {
            'id': self.matched_item.pk,
            'created': self.matched_item.created,
            'found_item': self.matched_item.found_item.pk,
            'lost_item': self.matched_item.lost_item.pk,
            'status_display': 'Accepted',
            'user_token': response.data.get('user_token'),
            'status': MatchedItem.STATUS.accepted
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_owner_invalid_matched_item(self):
        self.matched_item.status = MatchedItem.STATUS.accepted
        self.matched_item.save()
        response = self.client.get(reverse("matched_items-known-owner-feedback", kwargs={'pk': str(self.matched_item.token)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_simple_hotel_user_cannot_view_hotel_matched_items(self):
        response = self.client_hotel_staff.get(reverse("hotel_matched_items"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_matched_item_archive(self):
        data = {
            'assigned_status': MatchedItemStatus.UNCLAIMED.value,
        }

        response = self.client.patch(reverse("hotel_matched_items-detail", kwargs={"pk": self.matched_item.pk}), data=data)
        self.assertEqual(response.data, {"status_text": ["Please specify a resolution."]})

        data.update({'status_text': 'Auctioned'})
        response = self.client.patch(reverse("hotel_matched_items-detail", kwargs={'pk': self.matched_item.pk}), data=data)
        self.assertEqual(response.data, {
            "id": self.matched_item.pk,
            "created": self.matched_item.created,
            "lost_item": self.matched_item.lost_item.pk,
            "found_item": self.matched_item.found_item.pk,
            "status": self.matched_item.status,
            "status_display": self.matched_item.get_status_display(),
            "assigned_status": {
                'value': MatchedItemStatus.UNCLAIMED.value,
                'name': MatchedItemStatus.UNCLAIMED.name,
                'label': MatchedItemStatus.UNCLAIMED.label,
            },
            "status_text": 'Auctioned',
            "lost_item_object": {
                "id": self.matched_item.lost_item.pk,
                "created_by": {
                    "id": self.matched_item.lost_item.created_by.pk,
                    "first_name": self.matched_item.lost_item.created_by.first_name,
                    "last_name": self.matched_item.lost_item.created_by.last_name,
                    "contact_number": self.matched_item.lost_item.created_by.contact_number,
                    "email": self.matched_item.lost_item.created_by.email
                },
                "arrival_date": self.matched_item.lost_item.arrival_date,
                "departure_date": self.matched_item.lost_item.departure_date,
                "description": self.matched_item.lost_item.description,
                "specification": self.matched_item.lost_item.specification,
                "serial_number": self.matched_item.lost_item.serial_number,
                'category': {
                    'name': 'Clothing',
                    'id': self.matched_item.lost_item.category.pk
                },
                "item_type": {
                    'name': 'Shirt',
                    'id': self.matched_item.lost_item.item_type.pk,
                    'category': self.matched_item.lost_item.category.pk
                },
                "images": [],
                "location": self.matched_item.lost_item.location,
                "room_number": self.matched_item.lost_item.room_number,
                "created": self.matched_item.lost_item.created,
                "date": self.matched_item.lost_item.date
            },
            "found_item_object": {
                "id": self.matched_item.found_item.pk,
                "created": self.matched_item.found_item.created,
                "created_by": {
                    "id": self.found_item.created_by.pk,
                    "first_name": self.matched_item.found_item.created_by.first_name,
                    "last_name": self.matched_item.found_item.created_by.last_name,
                    "contact_number": self.matched_item.found_item.created_by.contact_number,
                    "email": self.matched_item.found_item.created_by.email
                },
                "category": self.matched_item.found_item.category.pk,
                "item_type": self.matched_item.found_item.item_type.pk,
                "description": self.matched_item.found_item.description,
                "serial_number": self.matched_item.found_item.serial_number,
                "specification": self.matched_item.found_item.specification,
                "hotel": self.matched_item.found_item.hotel.pk,
                "found_by": self.matched_item.found_item.found_by,
                "date": self.matched_item.found_item.date,
                "room_number": self.matched_item.found_item.room_number,
                "location": self.matched_item.found_item.location,
                "owner_name": self.matched_item.found_item.owner_name,
                "owner_email": self.matched_item.found_item.owner_email,
                "owner_surname": self.matched_item.found_item.owner_surname,
                "owner_contact_number": self.matched_item.found_item.owner_contact_number,
                "category_name": self.matched_item.found_item.category.name,
                "item_type_name": self.matched_item.found_item.item_type.name,
                "images": [],
                "width": "10.00",
                "height": "0.50",
                "length": "10.00",
                "weight": "0.50",
                "stored_at": "",
                "status": None,
                "status_text": None,
            }
        })

    def test_matched_item_archived_list(self):
        """
        Test list for archived matched items
        """
        archived_item = factories.MatchedItemFactory(assigned_status=MatchedItemStatus.UNCLAIMED.value,
                                                     status_text='Unreachable')
        response = self.client.get(reverse("hotel_archived_matched_items"))
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": archived_item.pk,
                "created": archived_item.created,
                "lost_item": archived_item.lost_item.pk,
                "found_item": archived_item.found_item.pk,
                "assigned_status": {
                    "value": MatchedItemStatus.UNCLAIMED.value,
                    "label": MatchedItemStatus.UNCLAIMED.label,
                    "name": MatchedItemStatus.UNCLAIMED.name
                },
                "status_text": archived_item.status_text,
                "found_item_category": archived_item.found_item.category.name,
                "found_item_type": archived_item.found_item.item_type.name,
                "found_item_Specification": archived_item.found_item.specification,
                "found_item_description": archived_item.found_item.description,
                "lost_item_category": archived_item.lost_item.category.name,
                "lost_item_type": archived_item.lost_item.item_type.name,
                "lost_item_specification": archived_item.lost_item.specification,
                "lost_item_description": archived_item.lost_item.description
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
