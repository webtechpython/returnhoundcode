from datetime import datetime
from unittest.mock import MagicMock, patch
from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.apps.aramex.api import AramexAPIError
from tests import factories
from tests.utils import AuthenticatedAPITestCase


def raise_address_validation_exception(*args, **kwargs):
    raise AramexAPIError({
        'city': 'Invalid combination of city/zipcode',
        'post_code': 'Invalid combination of city/zipcode',
    })

ADDRESS_VALIDATION_FAIL = MagicMock(side_effect=raise_address_validation_exception)
MOCK_SUCCESS = MagicMock(return_value={'success': True})


class ReportWizardTestCase(AuthenticatedAPITestCase):

    def setUp(self):
        self.user = factories.UserFactory(email='report.test@user.com')
        self.user_client = self.client_for(self.user)
        self.country = factories.CountryFactory()
        self.region = factories.RegionFactory()
        self.aramex_country = factories.AramexCountryFactory()
        self.aramex_state = factories.AramexStateFactory()
        self.category = factories.CategoryFactory()
        self.item_type = factories.ItemTypeFactory()
        self.hotel = factories.HotelFactory()
        self.email = {
            'welcome': factories.WelcomeClientEmail(),
            'new_report': factories.EmailTemplateFactory(hotel=self.hotel),
            'report_details': factories.ReportDetailsEmail(hotel=self.hotel),
            'system_new_report': factories.EmailTemplateFactory(subject='test123', hotel=None, is_system_default=True),
        }
        self.hotel_user = factories.HotelUserFactory(
            employer=self.hotel, employee=factories.UserFactory(),
            is_admin=True, is_creator=False, is_mail_recipient=True
        )
        self.hotel_creator = factories.HotelUserFactory(
            employer=self.hotel, employee=factories.UserFactory(email='test1@byteorbit.com'),
            is_admin=False, is_creator=True, is_mail_recipient=False
        )

    @patch('return_hound.apps.api.serializers.get_recaptcha_response', MOCK_SUCCESS)
    @patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', MOCK_SUCCESS)
    def test_report_wizard_existing_user_with_address(self):
        """
        Looks like this was intented to test the "Known user flow"
        User was created by a hotel admin/staff when logging a found Item
        """
        # setup the existing user
        factories.UserFactory(email="known@user.com",
                              address=factories.AddressFactory())
        data = {
            "email": "known@user.com",
            "first_name": "test",
            "last_name": "test",
            "contact_number": "test",
            "hotel": self.hotel.pk,
            "date": datetime.today().strftime("%Y-%m-%d"),
            "arrival_date": datetime.today().strftime("%Y-%m-%d"),
            "departure_date": datetime.today().strftime("%Y-%m-%d"),
            "location": "Test",
            "room_number": "123",
            "address": {
                "line1": "Test",
                "line2": "Test",
                "post_code": "123",
                "country": self.aramex_country.pk,
                "region": self.aramex_state.pk,
                "city": "Test",
            },
            "items": [
                {
                    "category": {
                        "id": self.category.pk
                    },
                    "item_type": {
                        "id": self.item_type.pk
                    },
                    "specification": "Test",
                    "description": "Test",
                    "serial": "Test",
                }
            ],
            "recaptcha": "TestCaptcha",
        }
        response = self.user_client.post(reverse("report_wizard"), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.data)
        self.assertEqual(response.data, {})

    def test_report_wizard_register_login(self):
        user = factories.UserFactory(email='report.login@byteorbit.com')
        data = {
            'email': user.email,
            'password': user._PASSWORD
        }
        response = self.client.post(reverse("report_register"), data=data)
        self.assertEqual(response.data, {'user': user.pk, 'token': user.auth_token.key.hex})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    @patch('return_hound.apps.api.serializers.get_recaptcha_response', MOCK_SUCCESS)
    @patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', MOCK_SUCCESS)
    def test_report_wizard_user_info_update(self):
        data = {
            "email": "test@test.com",
            "first_name": "test",
            "last_name": "test",
            "contact_number": "test",
            "hotel": self.hotel.pk,
            "date": datetime.today().strftime("%Y-%m-%d"),
            "arrival_date": datetime.today().strftime("%Y-%m-%d"),
            "departure_date": datetime.today().strftime("%Y-%m-%d"),
            "location": "Test",
            "room_number": "123",
            "address": {
                "line1": "Test",
                "line2": "Test",
                "post_code": "123",
                "country": self.aramex_country.pk,
                "region": self.aramex_state.pk,
                "city": "Test",
            },
            "items": [
                {
                    "category": {
                        "id": self.category.pk
                    },
                    "item_type": {
                        "id": self.item_type.pk
                    },
                    "description": "Test",
                    "serial": "Test",
                    "specification": "Test",
                }
            ],
            'recaptcha': 'TestCaptcha',
        }
        response = self.user_client.post(reverse("report_wizard"), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    @patch('return_hound.apps.api.serializers.get_recaptcha_response')
    @patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', ADDRESS_VALIDATION_FAIL)
    def test_invalid_report_wizard(self, get_recaptcha_response_mock):
        get_recaptcha_response_mock.return_value = {
            'success': False,
            'error-codes': ['invalid-input-response'],
        }
        data = {
            "email": "",
            "first_name": "",
            "last_name": "",
            "contact_number": "",
            "hotel": "",
            "date": "",
            "arrival_date": "",
            "departure_date": "",
            "location": "",
            "room_number": "",
            "address": {
                "line1": "Test",
                "line2": "Test",
                "city": "FAIL",
                "post_code": "FAIL",
                "country": self.aramex_country.pk,
                "region": self.aramex_state.pk,
            },
            "items": [],
            'recaptcha': 'TestCaptcha',
        }
        response = self.user_client.post(reverse("report_wizard"), data=data)
        assert response.data == {
            'contact_number': ['This field may not be blank.'],
            'first_name': ['This field may not be blank.'],
            'departure_date': ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]].'],
            'date': ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]].'],
            'email': ['This field may not be blank.'],
            'arrival_date': ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]].'],
            'items': ['This field is required.'],
            'hotel': ['This field may not be null.'],
            'recaptcha': ['Please make sure you pass the reCAPTCHA challenge'],
            'address': {
                'post_code': ['Invalid combination of city/zipcode'],
                'city': ['Invalid combination of city/zipcode']
            }
        }
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
