from unittest.mock import patch, MagicMock
from django.core import mail
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.models import MatchedItem
from return_hound import enums
from tests import factories
from tests.utils import AuthenticatedAPITestCase


MOCK_RATE = MagicMock(return_value={
    'amount': '0.0',
    'currency': 'JOD',
    'shipping_group': 'Domestic',
    'shipping_type': 'Priority Parcel Domestic',
    'has_error': False,
    'errors': []
})

MOCK_SHIPMENT = MagicMock(return_value={
    'id': 'ARAMEX-TRACKING-NUMBER-123',
    'label': b'moo',
})


@patch('return_hound.apps.aramex.api.AramexRateClient.get_rate', MOCK_RATE)
@patch('return_hound.apps.aramex.api.AramexShippingClient.create_shipment', MOCK_SHIPMENT)
class ShippingItemTestCase(AuthenticatedAPITestCase):

    def setUp(self):
        self.setup_hotel()
        self.shipping_item = factories.ShippingItemFactory()
        self.transaction = factories.TransactionFactory(
            shipping_details=self.shipping_item.details,
            is_paid=True, charged_amount=300)
        self.address = factories.AddressFactory(
            content_object=self.shipping_item.details  # shipment
        )
        # urls:
        self.url_item_list = reverse("hotel_collect_items")
        self.url_item_detail = reverse("hotel_collect_items-detail", kwargs={'pk': self.shipping_item.pk})

    def test_collect_item_list(self):
        # import pdb; pdb.set_trace()
        response = self.client_hotel_admin.get(self.url_item_list)
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": self.shipping_item.pk,
                "parcel": {
                    "id": self.shipping_item.parcel.pk,
                    "created_by": {
                        "id": self.shipping_item.parcel.lost_item.created_by.pk,
                        "first_name": self.shipping_item.parcel.lost_item.created_by.first_name,
                        "last_name": self.shipping_item.parcel.lost_item.created_by.last_name,
                        "contact_number": self.shipping_item.parcel.lost_item.created_by.contact_number,
                        "email": self.shipping_item.parcel.lost_item.created_by.email
                    },
                    "found_item": {
                        "id": self.shipping_item.parcel.found_item.pk,
                        "created": self.shipping_item.parcel.found_item.created,
                        "created_by": {
                            "id": self.shipping_item.parcel.found_item.created_by.pk,
                            "first_name": self.shipping_item.parcel.found_item.created_by.first_name,
                            "last_name": self.shipping_item.parcel.found_item.created_by.last_name,
                            "contact_number": self.shipping_item.parcel.found_item.created_by.contact_number,
                            "email": self.shipping_item.parcel.found_item.created_by.email
                        },
                        "category": self.shipping_item.parcel.found_item.category.pk,
                        "item_type": self.shipping_item.parcel.found_item.item_type.pk,
                        "description": self.shipping_item.parcel.found_item.description,
                        "serial_number": self.shipping_item.parcel.found_item.serial_number,
                        "specification": self.shipping_item.parcel.found_item.specification,
                        "hotel": self.shipping_item.parcel.found_item.hotel.pk,
                        "found_by": self.shipping_item.parcel.found_item.found_by,
                        "date": self.shipping_item.parcel.found_item.date,
                        "room_number": self.shipping_item.parcel.found_item.room_number,
                        "location": self.shipping_item.parcel.found_item.location,
                        "owner_name": self.shipping_item.parcel.found_item.owner_name,
                        "owner_email": self.shipping_item.parcel.found_item.owner_email,
                        "owner_surname": self.shipping_item.parcel.found_item.owner_surname,
                        "owner_contact_number": self.shipping_item.parcel.found_item.owner_contact_number,
                        "category_name": self.shipping_item.parcel.found_item.category.name,
                        "item_type_name": self.shipping_item.parcel.found_item.item_type.name,
                        "images": [],
                        "width": "10.00",
                        "height": "0.50",
                        "length": "10.00",
                        "weight": "0.50",
                        "stored_at": "",
                        "status": None,
                        "status_text": None,
                    }
                },
                "details": {
                    "id": self.shipping_item.details.pk,
                    "collector_name": "",
                    "collector_last_name": "",
                    "collector_email": "",
                    "collector_contact_number": "",
                    "is_collection": self.shipping_item.details.is_collection,
                    "is_complete": self.shipping_item.details.is_complete,
                    "label_file": None,
                    "tracking_number": self.shipping_item.details.tracking_number,
                    "transaction": {
                        "id": self.transaction.pk,
                        "is_paid": self.transaction.is_paid,
                        "service_charge": "18.00",
                        "item_value": "200.00",
                        "insurance_fee": "15.00",
                        "charged_amount": "300.00",
                        "payment_date": self.transaction.payment_date,
                        "currency": "AED",
                        "calculated_service_charge": self.transaction.calculated_service_charge,
                        "calculated_insurance_fee": self.transaction.calculated_insurance_fee,
                        "item_value_currency": {
                            "value": enums.ItemValueCurrencies.AED.value,
                            "label": enums.ItemValueCurrencies.AED.label,
                            "name": enums.ItemValueCurrencies.AED.name
                        },
                    },
                    "collected_date": self.shipping_item.details.collected_date,
                    "delivery_address": {
                        "id": self.address.id,
                        "line1": self.address.line1,
                        "line2": self.address.line2,
                        "post_code": self.address.post_code,
                        "country": {
                            'id': self.address.country.pk,
                            'name': self.address.country.name,
                            'code2': self.address.country.code2,
                            'postal_codes': [],
                            'postcode_required': self.address.country.postcode_required,
                            'state_required': self.address.country.state_required,
                        },
                        "region": {
                            'id': self.address.region.pk,
                            'name': self.address.region.name,
                        },
                        "city": self.address.city,
                    },
                    "invoice": None
                },
                "created": self.shipping_item.created,
                "modified": self.shipping_item.modified,
                "is_deleted": self.shipping_item.is_deleted
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_collect_item_detail(self):
        response = self.client_hotel_admin.get(self.url_item_detail)
        self.assertEqual(response.data, {
            "id": self.shipping_item.pk,
            "parcel": {
                "id": self.shipping_item.parcel.pk,
                "created_by": {
                    "id": self.shipping_item.parcel.lost_item.created_by.pk,
                    "first_name": self.shipping_item.parcel.lost_item.created_by.first_name,
                    "last_name": self.shipping_item.parcel.lost_item.created_by.last_name,
                    "contact_number": self.shipping_item.parcel.lost_item.created_by.contact_number,
                    "email": self.shipping_item.parcel.lost_item.created_by.email
                },
                "found_item": {
                    "id": self.shipping_item.parcel.found_item.pk,
                    "created": self.shipping_item.parcel.found_item.created,
                    "created_by": {
                        "id": self.shipping_item.parcel.found_item.created_by.pk,
                        "first_name": self.shipping_item.parcel.found_item.created_by.first_name,
                        "last_name": self.shipping_item.parcel.found_item.created_by.last_name,
                        "contact_number": self.shipping_item.parcel.found_item.created_by.contact_number,
                        "email": self.shipping_item.parcel.found_item.created_by.email
                    },
                    "category": self.shipping_item.parcel.found_item.category.pk,
                    "item_type": self.shipping_item.parcel.found_item.item_type.pk,
                    "description": self.shipping_item.parcel.found_item.description,
                    "serial_number": self.shipping_item.parcel.found_item.serial_number,
                    "specification": self.shipping_item.parcel.found_item.specification,
                    "hotel": self.shipping_item.parcel.found_item.hotel.pk,
                    "found_by": self.shipping_item.parcel.found_item.found_by,
                    "date": self.shipping_item.parcel.found_item.date,
                    "room_number": self.shipping_item.parcel.found_item.room_number,
                    "location": self.shipping_item.parcel.found_item.location,
                    "owner_name": self.shipping_item.parcel.found_item.owner_name,
                    "owner_email": self.shipping_item.parcel.found_item.owner_email,
                    "owner_surname": self.shipping_item.parcel.found_item.owner_surname,
                    "owner_contact_number": self.shipping_item.parcel.found_item.owner_contact_number,
                    "category_name": self.shipping_item.parcel.found_item.category.name,
                    "item_type_name": self.shipping_item.parcel.found_item.item_type.name,
                    "images": [],
                    "width": "10.00",
                    "height": "0.50",
                    "length": "10.00",
                    "weight": "0.50",
                    "stored_at": "",
                    "status": None,
                    "status_text": None,
                }
            },
            "details": {
                "id": self.shipping_item.details.pk,
                "collector_name": "",
                "collector_last_name": "",
                "collector_email": "",
                "collector_contact_number": "",
                "is_collection": self.shipping_item.details.is_collection,
                "is_complete": self.shipping_item.details.is_complete,
                "label_file": None,
                "tracking_number": self.shipping_item.details.tracking_number,
                "transaction": {
                    "id": self.transaction.pk,
                    "is_paid": self.transaction.is_paid,
                    "service_charge": "18.00",
                    "item_value": "200.00",
                    "insurance_fee": "15.00",
                    "charged_amount": "300.00",
                    "payment_date": self.transaction.payment_date,
                    "currency": "AED",
                    "calculated_service_charge": self.transaction.calculated_service_charge,
                    "calculated_insurance_fee": self.transaction.calculated_insurance_fee,
                    "item_value_currency": {
                        "value": enums.ItemValueCurrencies.AED.value,
                        "label": enums.ItemValueCurrencies.AED.label,
                        "name": enums.ItemValueCurrencies.AED.name
                    },
                },
                "collected_date": self.shipping_item.details.collected_date,
                "delivery_address": {
                    "id": self.address.id,
                    "line1": self.address.line1,
                    "line2": self.address.line2,
                    "post_code": self.address.post_code,
                    "country": {
                        'id': self.address.country.pk,
                        'name': self.address.country.name,
                        'code2': self.address.country.code2,
                        'postal_codes': [],
                        'postcode_required': self.address.country.postcode_required,
                        'state_required': self.address.country.state_required,
                    },
                    "region": {
                        'id': self.address.region.pk,
                        'name': self.address.region.name,
                    },
                    "city": self.address.city,
                },
                "invoice": None
            },
            "created": self.shipping_item.created,
            "modified": self.shipping_item.modified,
            "is_deleted": self.shipping_item.is_deleted
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_collect_item_update(self):
        # TODO: This seems to create a Shipment... WAT???
        response = self.client_hotel_admin.patch(self.url_item_detail, data={})
        self.assertEqual(response.status_code, 200, msg='({0.status_code}): {0.data}'.format(response))
        self.assertEqual(response.data, {
            "id": self.shipping_item.pk,
            "parcel": {
                "id": self.shipping_item.parcel.pk,
                "created_by": {
                    "id": self.shipping_item.parcel.lost_item.created_by.pk,
                    "first_name": self.shipping_item.parcel.lost_item.created_by.first_name,
                    "last_name": self.shipping_item.parcel.lost_item.created_by.last_name,
                    "contact_number": self.shipping_item.parcel.lost_item.created_by.contact_number,
                    "email": self.shipping_item.parcel.lost_item.created_by.email
                },
                "found_item": {
                    "id": self.shipping_item.parcel.found_item.pk,
                    "created": self.shipping_item.parcel.found_item.created,
                    "created_by": {
                        "id": self.shipping_item.parcel.found_item.created_by.pk,
                        "first_name": self.shipping_item.parcel.found_item.created_by.first_name,
                        "last_name": self.shipping_item.parcel.found_item.created_by.last_name,
                        "contact_number": self.shipping_item.parcel.found_item.created_by.contact_number,
                        "email": self.shipping_item.parcel.found_item.created_by.email
                    },
                    "category": self.shipping_item.parcel.found_item.category.pk,
                    "item_type": self.shipping_item.parcel.found_item.item_type.pk,
                    "description": self.shipping_item.parcel.found_item.description,
                    "serial_number": self.shipping_item.parcel.found_item.serial_number,
                    "specification": self.shipping_item.parcel.found_item.specification,
                    "hotel": self.shipping_item.parcel.found_item.hotel.pk,
                    "found_by": self.shipping_item.parcel.found_item.found_by,
                    "date": self.shipping_item.parcel.found_item.date,
                    "room_number": self.shipping_item.parcel.found_item.room_number,
                    "location": self.shipping_item.parcel.found_item.location,
                    "owner_name": self.shipping_item.parcel.found_item.owner_name,
                    "owner_email": self.shipping_item.parcel.found_item.owner_email,
                    "owner_surname": self.shipping_item.parcel.found_item.owner_surname,
                    "owner_contact_number": self.shipping_item.parcel.found_item.owner_contact_number,
                    "category_name": self.shipping_item.parcel.found_item.category.name,
                    "item_type_name": self.shipping_item.parcel.found_item.item_type.name,
                    "images": [],
                    "width": "10.00",
                    "height": "0.50",
                    "length": "10.00",
                    "weight": "0.50",
                    "stored_at": "",
                    "status": None,
                    "status_text": None,
                }
            },
            "details": {
                "id": self.shipping_item.details.pk,
                "collector_name": "",
                "collector_last_name": "",
                "collector_email": "",
                "collector_contact_number": "",
                "is_collection": self.shipping_item.details.is_collection,
                "is_complete": self.shipping_item.details.is_complete,
                "label_file": response.data.get('details')['label_file'],
                "tracking_number": response.data.get('details')['tracking_number'],
                "transaction": {
                    "id": self.transaction.pk,
                    "is_paid": self.transaction.is_paid,
                    "service_charge": "18.00",
                    "charged_amount": "300.00",
                    "item_value": "200.00",
                    "insurance_fee": "15.00",
                    "payment_date": self.transaction.payment_date,
                    "currency": "AED",
                    "calculated_service_charge": self.transaction.calculated_service_charge,
                    "calculated_insurance_fee": self.transaction.calculated_insurance_fee,
                    "item_value_currency": {
                        "value": enums.ItemValueCurrencies.AED.value,
                        "label": enums.ItemValueCurrencies.AED.label,
                        "name": enums.ItemValueCurrencies.AED.name
                    },
                },
                "collected_date": self.shipping_item.details.collected_date,
                "delivery_address": {
                    "id": self.address.id,
                    "line1": self.address.line1,
                    "line2": self.address.line2,
                    "post_code": self.address.post_code,
                    "country": {
                        'id': self.address.country.pk,
                        'name': self.address.country.name,
                        'code2': self.address.country.code2,
                        'postal_codes': [],
                        'postcode_required': self.address.country.postcode_required,
                        'state_required': self.address.country.state_required,
                    },
                    "region": {
                        'id': self.address.region.pk,
                        'name': self.address.region.name,
                    },
                    "city": self.address.city,
                },
                "invoice": None
            },
            "created": self.shipping_item.created,
            "modified": response.data.get('modified'),
            "is_deleted": self.shipping_item.is_deleted
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Shipment scheduled - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertEqual(mail.outbox[0].to, [self.shipping_item.parcel.lost_item.created_by.email])

    def test_collect_item_update_complete(self):
        # FIXME: test isolation
        # TODO: comments... WAT is happening here...
        self.shipping_item.details.tracking_number = '123'
        self.shipping_item.details.save()
        response = self.client_hotel_admin.patch(self.url_item_detail, data={})
        assert response.data == {
            "id": self.shipping_item.pk,
            "parcel": {
                "id": self.shipping_item.parcel.pk,
                "created_by": {
                    "id": self.shipping_item.parcel.lost_item.created_by.pk,
                    "first_name": self.shipping_item.parcel.lost_item.created_by.first_name,
                    "last_name": self.shipping_item.parcel.lost_item.created_by.last_name,
                    "contact_number": self.shipping_item.parcel.lost_item.created_by.contact_number,
                    "email": self.shipping_item.parcel.lost_item.created_by.email
                },
                "found_item": {
                    "id": self.shipping_item.parcel.found_item.pk,
                    "created": self.shipping_item.parcel.found_item.created,
                    "created_by": {
                        "id": self.shipping_item.parcel.found_item.created_by.pk,
                        "first_name": self.shipping_item.parcel.found_item.created_by.first_name,
                        "last_name": self.shipping_item.parcel.found_item.created_by.last_name,
                        "contact_number": self.shipping_item.parcel.found_item.created_by.contact_number,
                        "email": self.shipping_item.parcel.found_item.created_by.email
                    },
                    "category": self.shipping_item.parcel.found_item.category.pk,
                    "item_type": self.shipping_item.parcel.found_item.item_type.pk,
                    "description": self.shipping_item.parcel.found_item.description,
                    "serial_number": self.shipping_item.parcel.found_item.serial_number,
                    "specification": self.shipping_item.parcel.found_item.specification,
                    "hotel": self.shipping_item.parcel.found_item.hotel.pk,
                    "found_by": self.shipping_item.parcel.found_item.found_by,
                    "date": self.shipping_item.parcel.found_item.date,
                    "room_number": self.shipping_item.parcel.found_item.room_number,
                    "location": self.shipping_item.parcel.found_item.location,
                    "owner_name": self.shipping_item.parcel.found_item.owner_name,
                    "owner_email": self.shipping_item.parcel.found_item.owner_email,
                    "owner_surname": self.shipping_item.parcel.found_item.owner_surname,
                    "owner_contact_number": self.shipping_item.parcel.found_item.owner_contact_number,
                    "category_name": self.shipping_item.parcel.found_item.category.name,
                    "item_type_name": self.shipping_item.parcel.found_item.item_type.name,
                    "images": [],
                    "width": "10.00",
                    "height": "0.50",
                    "length": "10.00",
                    "weight": "0.50",
                    "stored_at": "",
                    "status": None,
                    "status_text": None,
                }
            },
            "details": {
                "id": self.shipping_item.details.pk,
                "collector_name": "",
                "collector_last_name": "",
                "collector_email": "",
                "collector_contact_number": "",
                "is_collection": self.shipping_item.details.is_collection,
                "is_complete": True,
                "label_file": None,
                "tracking_number": response.data.get('details')['tracking_number'],
                "transaction": {
                    "id": self.transaction.pk,
                    "is_paid": self.transaction.is_paid,
                    "service_charge": "18.00",
                    "item_value": "200.00",
                    "insurance_fee": "15.00",
                    "charged_amount": "300.00",
                    "payment_date": self.transaction.payment_date,
                    "currency": "AED",
                    "calculated_service_charge": self.transaction.calculated_service_charge,
                    "calculated_insurance_fee": self.transaction.calculated_insurance_fee,
                    "item_value_currency": {
                        "value": enums.ItemValueCurrencies.AED.value,
                        "label": enums.ItemValueCurrencies.AED.label,
                        "name": enums.ItemValueCurrencies.AED.name
                    },
                },
                "collected_date": response.data.get('details')['collected_date'],
                "delivery_address": {
                    "id": self.address.id,
                    "line1": self.address.line1,
                    "line2": self.address.line2,
                    "post_code": self.address.post_code,
                    "country": {
                        'id': self.address.country.pk,
                        'name': self.address.country.name,
                        'code2': self.address.country.code2,
                        'postal_codes': [],
                        'postcode_required': self.address.country.postcode_required,
                        'state_required': self.address.country.state_required,
                    },
                    "region": {
                        'id': self.address.region.pk,
                        'name': self.address.region.name,
                    },
                    "city": self.address.city,
                },
                "invoice": None
            },
            "created": self.shipping_item.created,
            "modified": response.data.get('modified'),
            "is_deleted": self.shipping_item.is_deleted
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Item collected - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertEqual(mail.outbox[0].to, [self.shipping_item.parcel.lost_item.created_by.email])

    def test_shipping_rate(self):
        # FIXME: test isolation
        self.shipping_item.parcel.status = MatchedItem.STATUS.accepted
        self.shipping_item.parcel.save()
        user_client = self.client_for(self.shipping_item.parcel.lost_item.created_by)
        response = user_client.get(reverse('shipping_rate-detail', kwargs={'pk': self.shipping_item.parcel.pk}))
        self.assertDictEqual(response.data, {
            'shipping_rate': {
                'shipping_group': 'Domestic',
                'shipping_type': 'Priority Parcel Domestic',
                'has_error': False,
                'currency': 'USD',
                'amount': '0.0',
                'errors': []
            }
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_simple_hotel_user_cannot_view_collect_item_list(self):
        response = self.client_hotel_staff.get(reverse("hotel_collect_items"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
