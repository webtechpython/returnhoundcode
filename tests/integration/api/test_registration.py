from unittest.mock import MagicMock, patch
from django.core import urlresolvers
from rest_framework import status
from rest_framework.test import APITestCase
from tests.factories import AramexCountryFactory, AramexStateFactory, UserFactory

from return_hound.models import HotelUser, ReportStyle


MOCK_SUCCESS = MagicMock(return_value={'success': True})


@patch('return_hound.apps.aramex.api.AramexLocationClient.validate_address', MOCK_SUCCESS)
class RegistrationTestCase(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.country = AramexCountryFactory()
        self.region = AramexStateFactory()

    @patch('return_hound.apps.api.serializers.get_recaptcha_response', MOCK_SUCCESS)
    def test_hotel_registration(self):
        data = {
            'email': 'test@hotel.com',
            'domain_name': '',
            'first_name': 'test',
            'last_name': 'test',
            'contact_number': '0825655596',
            'number_of_rooms': 0,
            'terms': True,
            'name': 'Hotel',
            'address': {
                'line1': 'Street',
                'city': 'Cape Town',
                'post_code': '123',
                'region': self.region.pk,
                'country': self.country.pk,
            },
            'recaptcha': 'TestCaptcha',
        }

        response = self.client.post(urlresolvers.reverse('register_hotel'), data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue('id' in response.data)
        self.assertTrue(HotelUser.objects.filter(employer__pk=response.data['id'], employee__email='test@hotel.com',
                                                 is_admin=True, is_creator=True).exists())
        self.assertTrue(ReportStyle.objects.filter(hotel__pk=response.data['id']).exists())

    @patch('return_hound.apps.api.serializers.get_recaptcha_response')
    def test_invalid_hotel_registration(self, get_recaptcha_response_mock):
        get_recaptcha_response_mock.return_value = {
            'success': False,
            'error-codes': ['invalid-input-response'],
        }
        data = {
            'email': self.user.email,
            'first_name': '',
            'last_name': '',
            'contact_number': '',
            'terms': True,
            'name': '',
            'recaptcha': 'TestCaptcha',
        }
        response = self.client.post(urlresolvers.reverse('register_hotel'), data=data)
        assert response.data == {
            'address': ['This field is required.'],
            'contact_number': ['This field may not be blank.'],
            'name': ['This field may not be blank.'],
            'email': ['The email address already exists.'],
            'first_name': ['This field may not be blank.'],
            'recaptcha': ['Please make sure you pass the reCAPTCHA challenge'],
        }
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
