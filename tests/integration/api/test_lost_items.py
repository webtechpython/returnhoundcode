from actstream.models import target_stream
from django.core.urlresolvers import reverse
from rest_framework import status
from return_hound.models import LostItem
from return_hound.enums import LostItemStatus
from tests.factories import LostItemFactory
from tests.utils import AuthenticatedAPITestCase


class MyLostItemsAPI(AuthenticatedAPITestCase):
    def setUp(self):
        self.lost_item = LostItemFactory()
        self.hotel = self.lost_item.hotel
        self.client = self.client_for(self.lost_item.created_by)
        self.url = reverse("my_lost_items")
        self.url_detail = reverse("my_lost_items-detail", kwargs={'pk': self.lost_item.pk})

    def test_lost_item_list(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": self.lost_item.pk,
                "created_by": {
                    "id": self.lost_item.created_by.pk,
                    "first_name": self.lost_item.created_by.first_name,
                    "last_name": self.lost_item.created_by.last_name,
                    "contact_number": self.lost_item.created_by.contact_number,
                    "email": self.lost_item.created_by.email
                },
                "arrival_date": self.lost_item.arrival_date,
                "departure_date": self.lost_item.departure_date,
                "description": self.lost_item.description,
                "specification": self.lost_item.specification,
                "serial_number": self.lost_item.serial_number,
                'category': {
                    'name': 'Clothing',
                    'id': self.lost_item.category.pk
                },
                "item_type": {
                    'name': 'Shirt',
                    'id': self.lost_item.item_type.pk,
                    'category': self.lost_item.category.pk
                },
                "images": [],
                "location": self.lost_item.location,
                "room_number": self.lost_item.room_number,
                "created": self.lost_item.created,
                "date": self.lost_item.date,
                "hotel": {
                    "id": self.hotel.pk,
                    "name": self.hotel.name,
                    'slug': self.hotel.slug,
                    'domain_name': self.hotel.domain_name,
                    'number_of_rooms': self.hotel.number_of_rooms,
                    'address': {
                        'region': {
                            'id': self.hotel.address.region.pk,
                            'name': self.hotel.address.region.name
                        },
                        'country': {
                            'id': self.hotel.address.country.pk,
                            'code2': self.hotel.address.country.code2,
                            'name': self.hotel.address.country.name,
                            'postal_codes': [],
                            'postcode_required': False,
                            'state_required': False
                        },
                        "id": self.hotel.address.id,
                        "line1": self.hotel.address.line1,
                        "line2": self.hotel.address.line2,
                        "post_code": self.hotel.address.post_code,
                        "city": self.hotel.address.city,
                    }
                },
                "notifications": [{
                    "timestamp": action.timestamp,
                    "verb": "reported",
                    "sent_by": "test user Test"
                } for action in target_stream(self.lost_item)]
            }]
        }

    def test_lost_item_detail(self):
        response = self.client.get(self.url_detail)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "id": self.lost_item.pk,
            "created_by": {
                "id": self.lost_item.created_by.pk,
                "first_name": self.lost_item.created_by.first_name,
                "last_name": self.lost_item.created_by.last_name,
                "contact_number": self.lost_item.created_by.contact_number,
                "email": self.lost_item.created_by.email
            },
            "arrival_date": self.lost_item.arrival_date,
            "departure_date": self.lost_item.departure_date,
            "description": self.lost_item.description,
            "specification": self.lost_item.specification,
            "serial_number": self.lost_item.serial_number,
            'category': {
                'name': 'Clothing',
                'id': self.lost_item.category.pk
            },
            "item_type": {
                'name': 'Shirt',
                'id': self.lost_item.item_type.pk,
                'category': self.lost_item.category.pk
            },
            "images": [],
            "location": self.lost_item.location,
            "room_number": self.lost_item.room_number,
            "created": self.lost_item.created,
            "date": self.lost_item.date,
            "hotel": {
                "id": self.hotel.pk,
                "name": self.hotel.name,
                'slug': self.hotel.slug,
                'domain_name': self.hotel.domain_name,
                'number_of_rooms': self.hotel.number_of_rooms,
                'address': {
                    'region': {
                        'id': self.hotel.address.region.pk,
                        'name': self.hotel.address.region.name
                    },
                    'country': {
                        'id': self.hotel.address.country.pk,
                        'code2': self.hotel.address.country.code2,
                        'name': self.hotel.address.country.name,
                        'postal_codes': [],
                        'postcode_required': False,
                        'state_required': False
                    },
                    "id": self.hotel.address.id,
                    "line1": self.hotel.address.line1,
                    "line2": self.hotel.address.line2,
                    "post_code": self.hotel.address.post_code,
                    "city": self.hotel.address.city,
                }
            },
            "notifications": [{
                    "timestamp": action.timestamp,
                    "verb": "reported",
                    "sent_by": "test user Test"
            } for action in target_stream(self.lost_item)]
        }

    def test_lost_item_update(self):
        # try to update the description field
        response = self.client.patch(self.url_detail, data={'description': 'Update'})
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "id": self.lost_item.pk,
            "created_by": {
                "id": self.lost_item.created_by.pk,
                "first_name": self.lost_item.created_by.first_name,
                "last_name": self.lost_item.created_by.last_name,
                "contact_number": self.lost_item.created_by.contact_number,
                "email": self.lost_item.created_by.email
            },
            "arrival_date": self.lost_item.arrival_date,
            "departure_date": self.lost_item.departure_date,
            "description": 'Update',
            "specification": self.lost_item.specification,
            "serial_number": self.lost_item.serial_number,
            'category': {
                'name': 'Clothing',
                'id': self.lost_item.category.pk
            },
            "item_type": {
                'name': 'Shirt',
                'id': self.lost_item.item_type.pk,
                'category': self.lost_item.category.pk
            },
            "images": [],
            "location": self.lost_item.location,
            "room_number": self.lost_item.room_number,
            "created": self.lost_item.created,
            "date": self.lost_item.date,
            "hotel": {
                "id": self.hotel.pk,
                "name": self.hotel.name,
                'slug': self.hotel.slug,
                'domain_name': self.hotel.domain_name,
                'number_of_rooms': self.hotel.number_of_rooms,
                'address': {
                    "country": {
                        'id': self.lost_item.hotel.address.country.pk,
                        'name': self.lost_item.hotel.address.country.name,
                        'code2': self.lost_item.hotel.address.country.code2,
                        'postal_codes': [],
                        'postcode_required': self.lost_item.hotel.address.country.postcode_required,
                        'state_required': self.lost_item.hotel.address.country.state_required,
                    },
                    "region": {
                        'id': self.lost_item.hotel.address.region.pk,
                        'name': self.lost_item.hotel.address.region.name,
                    },
                    "id": self.hotel.address.id,
                    "city": self.lost_item.hotel.address.city,
                    "line1": self.lost_item.hotel.address.line1,
                    "line2": self.lost_item.hotel.address.line2,
                    "post_code": self.lost_item.hotel.address.post_code,
                }
            },
            "notifications": [{
                    "timestamp": action.timestamp,
                    "verb": "reported",
                    "sent_by": "test user Test"
            } for action in target_stream(self.lost_item)]
        }

    def test_lost_item_delete(self):
        response = self.client.delete(self.url_detail)
        self.assertTrue(LostItem.objects.get(pk=self.lost_item.pk).is_deleted, True)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class HotelLostItemsAPI(AuthenticatedAPITestCase):
    def setUp(self):
        self.setup_hotel()
        self.lost_item = LostItemFactory()
        self.client = self.client_hotel_admin  # must be hotel admin user to see lost items
        self.url = reverse("hotel_lost_items")
        self.url_detail = reverse("hotel_lost_items-detail", kwargs={'pk': self.lost_item.pk})

    def test_hotel_lost_item_list(self):
        response = self.client.get(self.url)
        assert response.data == {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": self.lost_item.pk,
                "created_by": {
                    "id": self.lost_item.created_by.pk,
                    "first_name": self.lost_item.created_by.first_name,
                    "last_name": self.lost_item.created_by.last_name,
                    "contact_number": self.lost_item.created_by.contact_number,
                    "email": self.lost_item.created_by.email
                },
                "arrival_date": self.lost_item.arrival_date,
                "departure_date": self.lost_item.departure_date,
                "description": self.lost_item.description,
                "specification": self.lost_item.specification,
                "serial_number": self.lost_item.serial_number,
                'category': {
                    'name': 'Clothing',
                    'id': self.lost_item.category.pk
                },
                "item_type": {
                    'name': 'Shirt',
                    'id': self.lost_item.item_type.pk,
                    'category': self.lost_item.category.pk
                },
                "images": [],
                "location": self.lost_item.location,
                "room_number": self.lost_item.room_number,
                "created": self.lost_item.created,
                "date": self.lost_item.date,
                "match_count": 0,
                "status": None,
                "status_text": ''

            }]
        }

    def test_hotel_lost_item_detail(self):
        response = self.client.get(self.url_detail)
        assert response.data == {
            "id": self.lost_item.pk,
            "created_by": {
                "id": self.lost_item.created_by.pk,
                "first_name": self.lost_item.created_by.first_name,
                "last_name": self.lost_item.created_by.last_name,
                "contact_number": self.lost_item.created_by.contact_number,
                "email": self.lost_item.created_by.email
            },
            "arrival_date": self.lost_item.arrival_date,
            "departure_date": self.lost_item.departure_date,
            "description": self.lost_item.description,
            "specification": self.lost_item.specification,
            "serial_number": self.lost_item.serial_number,
            'category': {
                'name': 'Clothing',
                'id': self.lost_item.category.pk
            },
            "item_type": {
                'name': 'Shirt',
                'id': self.lost_item.item_type.pk,
                'category': self.lost_item.category.pk
            },
            "images": [],
            "location": self.lost_item.location,
            "room_number": self.lost_item.room_number,
            "created": self.lost_item.created,
            "date": self.lost_item.date,
            "match_count": 0,
            "status": None,
            "status_text": ''
        }

    def test_simple_hotel_user_cannot_view_hotel_lost_items(self):
        response = self.client_hotel_staff.get(reverse("hotel_lost_items"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_lost_item_archive_list(self):
        """
        List archived lost items
        """
        archived_item = LostItemFactory(status=LostItemStatus.LOST_NOT_FOUND.value, status_text="1 Month past")
        response = self.client.get(reverse("hotel_archived_lost_items"))
        self.assertEqual(response.data, {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [{
                "id": archived_item.id,
                "created": archived_item.created,
                "category": {
                    "id": archived_item.category.pk,
                    "name": archived_item.category.name
                },
                "item_type": {
                    "id": archived_item.item_type.pk,
                    "name": archived_item.item_type.name,
                    "category": archived_item.item_type.category.pk
                },
                "description": archived_item.description,
                "status": {
                    "value": LostItemStatus.LOST_NOT_FOUND.value,
                    "name": LostItemStatus.LOST_NOT_FOUND.name,
                    "label": LostItemStatus.LOST_NOT_FOUND.label
                },
                "status_text": archived_item.status_text
            }]
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
