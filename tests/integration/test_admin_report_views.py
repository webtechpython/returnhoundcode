from django.contrib.auth.models import Permission
from django.core.urlresolvers import reverse
from django.test import TestCase
from tests import factories


class AdminReportViewsTest(TestCase):
    def setUp(self):
        self.url = reverse('admin-reports-overview')
        # superuser (has implicit perm)
        superuser = factories.SuperUserFactory()
        self.client.login(email=superuser.email, password=superuser._PASSWORD)

    def logged_in_client(self, user):
        client = self.client_class()
        client.login(email=user.email, password=user._PASSWORD)
        return client

    def test_superuser_can_access(self):
        self.assertEqual(200, self.client.get(self.url).status_code)

    def test_staff_user_without_reports_permission_is_redirected_to_login(self):
        staff_user = factories.UserFactory(email='limited-staff@returnhound.com', is_staff=True)
        response = self.logged_in_client(staff_user).get(self.url)
        self.assertEqual(response.status_code, 302)
        self.assertTrue('login/?next' in response.url)

    def test_staff_user_with_reports_permission_can_access(self):
        staff_user = factories.UserFactory(email='reports@returnhound.com', is_staff=True)
        reports_perm = Permission.objects.get(codename='reports', content_type__app_label='return_hound')
        staff_user.user_permissions.add(reports_perm)
        response = self.logged_in_client(staff_user).get(self.url)
        self.assertEqual(200, response.status_code)
