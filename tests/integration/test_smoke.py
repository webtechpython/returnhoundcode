from django.test import TestCase
from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from tests import factories

URLS = [
    '/api/login/',
    '/api/logout/',
    '/api/reset-password/',
    '/api/reset-password-confirm/',
]

AUTH_URLS = [
    '/api/password-change/',
    '/api/user/',
]


class PublicApiTest(APITestCase):

    def test_get_not_allowed_urls(self):
        for url in URLS:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 405)

    def test_auth_urls(self):
        for url in AUTH_URLS:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 401)


class InvoiceDetailViewTests(TestCase):
    def setUp(self):
        self.user = factories.UserFactory(is_staff=True)
        self.token = factories.TokenFactory(user=self.user)
        self.hotel = factories.HotelFactory()
        self.hotel_user = factories.HotelUserFactory(employee=self.user, employer=self.hotel)
        self.client.login(email=self.user.email, token=self.token)
        found_item = factories.FoundItemFactory(hotel=self.hotel)
        matched_item = factories.MatchedItemFactory(found_item=found_item)
        shipment = factories.ShipmentFactory()
        factories.ShippingItemFactory(details=shipment, parcel=matched_item)
        invoice = factories.InvoiceFactory(shipment=shipment)
        self.url = reverse('invoice_pdf-detail', kwargs={'pk': invoice.pk})

    def test_invoice_detail_view(self):
        response = self.client.get(self.url, data={'token': self.token.key.hex})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('return_hound/invoice.html')

    def test_invoice_detail_token_validation(self):
        """
        Should not explode with invalid token. Should return 401 (Unauthorized).
        """
        resp = self.client.get(self.url, data={'token': 'lol'})
        assert resp.status_code == 401


class AdminCustomViewsTestCase(TestCase):

    def setUp(self):
        self.user = factories.UserFactory(is_staff=True)
        self.token = factories.TokenFactory(user=self.user)
        self.hotel = factories.HotelFactory()
        self.hotel_user = factories.HotelUserFactory(employee=self.user, employer=self.hotel)
        self.template = factories.EmailTemplateFactory()
        self.kwargs = {'pk': self.template.pk}
        self.url = reverse('email_preview', kwargs=self.kwargs)

    def test_email_template_preview_view(self):
        self.client.login(email=self.user.email, password=self.user._PASSWORD)
        res = self.client.get(reverse('admin:email_preview', kwargs=self.kwargs))
        self.assertEqual(res.status_code, 302)
        self.client.logout()

        self.client.login(email=self.hotel_user.employee.email, password=self.hotel_user.employee._PASSWORD)
        res = self.client.get(self.url, data={'token': self.token.key.hex})
        self.assertEqual(res.status_code, 200)
