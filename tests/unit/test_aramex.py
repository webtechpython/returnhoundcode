import os
from datetime import datetime
from unittest import mock, skipUnless
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, override_settings
from django.utils import timezone
from return_hound.apps.aramex.api import AramexShippingClient, AramexRateClient, AramexLocationClient, AramexAPIError
from return_hound.models import Shipment, User
from tests import factories

ARAMEX_TESTS = os.getenv('ARAMEX', False)


@skipUnless(ARAMEX_TESTS, 'Aramex API tests disabled')
class AramexRateClientTests(TestCase):
    def setUp(self):
        self.aramex = AramexRateClient()
        self.address_za = self.aramex.e(
            'Address', Line1='ABCD', City='Stellenbosch',
            StateOrProvinceCode='Western Cape', CountryCode='ZA', PostCode='7600')
        self.address_dubai = self.aramex.e(
            'Address', Line1='ABCD', City='Dubai',
            StateOrProvinceCode='Dubai', CountryCode='AE')
        self.weight = self.aramex.e('Weight', Value=1, Unit='KG')
        self.dimensions = self.aramex.e('Dimensions', Unit='CM', Height=10, Length=10, Width=10)
        self.transaction = self.aramex.e('Transaction', Reference1='ABC')
        self.shipment_item = self.aramex.e('ShipmentItem', Quantity=1, PackageType='Shirt', Weight=self.weight)

    def get_shipment_details(self, origin, destination):
        shipment_type = self.aramex.shipment_type(
            origin_country=origin.CountryCode,
            destination_country=destination.CountryCode)
        return self.aramex.e(
            'ShipmentDetails',
            Dimensions=self.dimensions,
            NumberOfPieces=1,
            ActualWeight=self.weight,
            ChargeableWeight=self.weight,
            ProductGroup=shipment_type['group'],
            ProductType=shipment_type['type'],
            PaymentType='P',  # Prepaid type
            DescriptionOfGoods='Clothes',
            GoodsOriginCountry='ZA',
            Items=[self.shipment_item],
        )

    def test_valid_rate_calculator(self):
        # Should be able to get a Rate from Dubai to ZA
        origin = self.address_dubai
        destination = self.address_za
        shipment_details = self.get_shipment_details(origin, destination)
        resp = self.aramex.calculate_rate(self.transaction, origin, destination, shipment_details)
        assert getattr(resp, 'HasErrors') is False
        assert getattr(resp.TotalAmount, 'CurrencyCode') == 'AED'
        assert getattr(resp.TotalAmount, 'Value') > 0

    def test_invalid_rate_calculator(self):
        # should fail to get a Rate from ZA to Dubai
        origin = self.address_za
        destination = self.address_dubai
        shipment_details = self.get_shipment_details(origin, destination)
        resp = self.aramex.calculate_rate(self.transaction, origin, destination, shipment_details)
        self.assertTrue(resp.HasErrors)
        self.assertEqual(resp.Notifications.Notification[0].Code, 'ERR60')
        self.assertEqual(
            resp.Notifications.Notification[0].Message,
            'OriginAddress - Cannot get the default cash account for the origin entity'
        )


@skipUnless(ARAMEX_TESTS, 'Aramex API tests disabled')
class AramexShippingClientTests(TestCase):

    def setUp(self):
        self.aramex = AramexShippingClient()

    @override_settings(ARAMEX_LEAD_TIME_MINUTES=120)
    def test_pickup_time_earliest(self):
        """
        Earliest pickup time is now + 2hours
        """
        tz = timezone.get_current_timezone()
        two_o_clock = datetime(2016, 1, 1, 14, 00, tzinfo=tz)
        four_o_clock = datetime(2016, 1, 1, 16, 00, tzinfo=tz)
        earliest = self.aramex.get_pickup_datetime(now=two_o_clock)
        assert earliest == four_o_clock

    def test_pickup_time_earliest_is_next_day(self):
        """
        Pickup time should take aramex/hotel closing times into account.
        """
        tz = timezone.get_current_timezone()
        four_o_clock = datetime(2016, 1, 1, 16, 00, tzinfo=tz)
        next_day_9am = datetime(2016, 1, 2, 9, 0, tzinfo=tz)
        earliest = self.aramex.get_pickup_datetime(now=four_o_clock)
        assert earliest == next_day_9am  # should schedule for next day 9AM

    @mock.patch('django.utils.timezone.now')
    def test_valid_shipment(self, mocknow):
        tz = timezone.get_current_timezone()
        mocknow.return_value = datetime.now(tz=tz)
        # setup data for Jordan
        jordan = factories.AramexCountryFactory()
        amman = factories.AramexStateFactory()  # Amman is a City and State/Region in Jordan
        jo_hotel = factories.HotelFactory(aramex_country=jordan, region=amman, city='Amman')
        hotel_user = factories.HotelUserFactory(employer=jo_hotel)  # type: factories.models.HotelUser
        matched_item = factories.MatchedItemFactory(
            lost_item=factories.LostItemFactory(hotel=jo_hotel),
            found_item=factories.FoundItemFactory(hotel=jo_hotel))
        shipping_item = factories.ShippingItemFactory(parcel=matched_item)  # type: factories.models.ShippingItem
        shipment = shipping_item.details  # type: Shipment
        # setup fake payment transaction
        factories.TransactionFactory(shipping_details=shipment, is_paid=True, charged_amount=300)
        # create generic relation address records
        factories.AddressFactory(content_object=shipment)
        # TODO: hotel/orgin and user/destination address should be in different cities at least
        factories.AddressFactory(content_object=shipping_item.parcel.lost_item.created_by)

        user = hotel_user.employee  # type: User
        resp = self.aramex.create_shipment(user, shipping_item)
        self.assertEqual(resp.HasErrors, False, resp)
        pi = resp.ProcessedPickup.ProcessedShipments.ProcessedShipment[0]
        assert pi.HasErrors is False
        assert pi.ID is not None  # has ID (this is the tracking number)
        assert pi.Notifications is None  # should have no Error messages here
        assert pi.ShipmentLabel.LabelFileContents is not None  # should have label file content


@skipUnless(ARAMEX_TESTS, 'Aramex API tests disabled')
class AramexLocationClientTests(TestCase):
    def setUp(self):
        self.aramex_location = AramexLocationClient()

    def test_aramex_methods_available(self):
        # Only latest Aramex API (v2) has the `FetchStates` method
        methods = {str(m.name) for m in self.aramex_location.soap.wsdl.services[0].ports[0].methods.values()}
        self.assertEqual(methods, {
            'FetchCountries',
            'FetchCountry',
            'FetchCities',
            'FetchStates',
            'ValidateAddress',
            'FetchOffices',
        })

    def test_invalid_city_name(self):
        with self.assertRaises(AramexAPIError) as cm:
            self.aramex_location.validate_address(
                city='Old City',
                country_code='IL',
                state_code='Jerusalem',
                post_code='7550')
        self.assertEqual({
            'city': 'City name is invalid (Old City)',
            'post_code': 'Invalid combination of city/zipcode',
        }, cm.exception.args[0])

    def test_invalid_city_postcode_combo(self):
        # invalid city/post_code combination
        with self.assertRaises(AramexAPIError) as cm:
            self.aramex_location.validate_address(
                city='Cape Town',
                country_code='ZA',
                state_code='Western Cape',
                post_code='7550')
        self.assertEqual({
            'city': 'City name is invalid',
            'post_code': 'Invalid combination of city/zipcode',
        }, cm.exception.args[0])

    def test_invalid_blank_postcode(self):
        # should fail with blank postcode
        with self.assertRaises(AramexAPIError) as cm:
            self.aramex_location.validate_address(
                city='Cape Town',
                country_code='ZA',
                state_code='Western Cape',
                post_code='')
        self.assertEqual({'post_code': 'Address - Invalid Zipcode'}, cm.exception.args[0])

    def test_valid_address(self):
        resp = self.aramex_location.validate_address(
            line_1='Durbanville',
            city='Durbanville',
            country_code='ZA',
            state_code='Western Cape',
            post_code='7550'
        )
        self.assertEqual(resp.HasErrors, False)

        resp = self.aramex_location.validate_address(
            city='Cape Town',
            country_code='ZA',
            state_code='Western Cape',
            post_code='8001'
            )
        self.assertEqual(resp.HasErrors, False)

    def test_aramex_countries(self):
        countries = self.aramex_location.get_countries()
        self.assertEqual(len(countries), 240)
        get = lambda code: next((c for c in countries if c['Code'] == code), None)
        # check for South Africa
        self.assertIsNotNone(get('ZA'))
        # check for Dubai
        self.assertIsNotNone(get('AE'))
        # check for Jordan
        self.assertIsNotNone(get('JO'))
        # check for Singapore
        self.assertIsNotNone(get('SG'))

    def test_aramex_country_detail(self):
        # South Africa
        za = self.aramex_location.get_country('ZA')
        self.assertEqual(za, {
            'Name': 'South Africa',
            'Code': 'ZA',
            'IsoCode': 'ZAF',
            'PostCodeRequired': True,
            'PostCodeRegex': None,
            'StateRequired': True,
            'InternationalCallingNumber': '27'})
        # Singapore
        sg = self.aramex_location.get_country('SG')
        self.assertEqual(sg, {
            'PostCodeRequired': True,  # -- SG requires a PostCode/ZipCode
            'StateRequired': True,  # ???
            'InternationalCallingNumber': '65',
            'Code': 'SG',
            'IsoCode': 'SGP',
            'Name': 'Singapore',
            'PostCodeRegex': None})

    def test_aramex_states(self):
        states = self.aramex_location.get_states(country='ZA')
        self.assertEqual(states, [
            {'code': '', 'name': 'Eastern Cape'},
            {'code': '', 'name': 'Free State'},
            {'code': '', 'name': 'Gauteng'},
            {'code': '', 'name': 'KwaZulu-Natal'},
            {'code': '', 'name': 'Limpopo'},
            {'code': '', 'name': 'Mpumalanga'},
            {'code': '', 'name': 'North West'},
            {'code': '', 'name': 'Northern Cape'},
            {'code': '', 'name': 'Western Cape'}
        ])

    def test_aramex_cities(self):
        # Singapore
        cities = self.aramex_location.get_cities('SG')
        self.assertEqual(cities, ['Singapore', 'Singapore WSI'])

    def test_aramex_cities_filter(self):
        # Try to find Cape Town
        # if we dont specify at least state/province we dont find it
        cities = self.aramex_location.get_cities('ZA', starts_with='Cape', state='Western Cape')
        self.assertEqual(cities, ['CAPE NATURE RESERVE', 'CAPE OGH NATURE RES', 'CAPE RIVIERA', 'Cape Town'])

    def test_aramex_cities_invalid_state_returns_empty_list(self):
        cities = self.aramex_location.get_cities('ZA', starts_with='Cape', state='Moo')
        self.assertEqual(cities, [])  # return empty list not 500 error

    def test_validate_with_zipcode_required_country(self):
        resp = self.aramex_location.validate_address(
            country_code='SG',
            state_code='Singapore',
            city='Singapore',
            post_code='486830'
        )
        self.assertEqual(resp.HasErrors, False)

    def test_validate_with_blank_state(self):
        # singapore zipcode: 486830
        resp = self.aramex_location.validate_address(
            country_code='SG',
            state_code='',
            city='Singapore',
            post_code='486830'
        )
        self.assertEqual(resp.HasErrors, False)

    def test_validate_with_only_country_and_zipcode(self):
        resp = self.aramex_location.validate_address(
            country_code='SG',
            post_code='486830'
        )
        self.assertEqual(resp.HasErrors, False)
        # Durbanville (???: ZA fails without city)
        resp = self.aramex_location.validate_address('ZA', city='Durbanville', post_code='7550')
        self.assertEqual(resp.HasErrors, False)
        # Cape Town (???: ZA fails without city)
        resp = self.aramex_location.validate_address('ZA', city='Cape Town', post_code='8001')
        self.assertEqual(resp.HasErrors, False)
