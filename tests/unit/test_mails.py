from django.core import mail
from rest_framework.test import APITestCase
from return_hound import enums
from return_hound import mails
from tests import factories


class MailTestCase(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.country = factories.CountryFactory()
        cls.region = factories.RegionFactory()
        cls.category = factories.CategoryFactory()
        cls.item_type = factories.ItemTypeFactory()
        cls.hotel = factories.HotelFactory()
        cls.hotel_user = factories.HotelUserFactory(
            employer=cls.hotel, employee=factories.UserFactory(),
            is_admin=True, is_creator=False, is_mail_recipient=True
        )
        cls.hotel_creator = factories.HotelUserFactory(
            employer=cls.hotel, employee=factories.UserFactory(email='test1@byteorbit.com'),
            is_admin=False, is_creator=True, is_mail_recipient=False
        )
        cls.lost_item = factories.LostItemFactory(hotel=cls.hotel)
        cls.matched_item = factories.MatchedItemFactory()
        cls.shipping_item = factories.ShippingItemFactory(parcel=cls.matched_item)

    def setUp(self):
        mail.outbox = []

    def test_new_item_mail(self):
        """
        Emailer should use the default emailing system
        if there are no custom emails objects for the hotel or the system
            - 1st scenario: no custom email object for the system and hotel
            - 2nd scenario: custom email object for the system and not for the hotel
            - 3rd scenario: custom email object for the system and for the hotel
        """

        mails.send_hotel_item_report_mail(self.lost_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'New Report received')

        system_email = factories.EmailTemplateFactory(
            subject='New Item reported @ returnhound', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.new_report_received_hotel
        )
        mails.send_hotel_item_report_mail(self.lost_item)
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].subject, system_email.subject)

        hotel_email = factories.EmailTemplateFactory(
            subject='New Item reported @ hotel', hotel=self.hotel, is_active=True,
            template=enums.EmailTemplates.new_report_received_hotel
        )
        factories.EmailTemplateFactory(
            subject='New Item reported @ hotel 2',
            hotel=factories.HotelFactory(name='Test hotel 2'), is_active=True,
            template=enums.EmailTemplates.new_report_received_hotel
        )
        mails.send_hotel_item_report_mail(self.lost_item)
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[2].subject, hotel_email.subject)

    def test_hotel_account_user(self):
        system_email = factories.EmailTemplateFactory(
            subject='Account', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.hotel_account_user
        )
        mails.send_hotel_user_permissions_mail(self.hotel_user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_hotel_shipment_created(self):
        system_email = factories.EmailTemplateFactory(
            subject='shipment', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.hotel_shipment_created
        )
        mails.send_hotel_shipment_created_mail(self.shipping_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_item_found(self):
        system_email = factories.EmailTemplateFactory(
            subject='found item', hotel=None, is_active=False, is_system_default=True,
            template=enums.EmailTemplates.item_found
        )
        mails.send_hotel_item_found_mail(self.matched_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Your lost item on ReturnHound has been found - {} ({})'.format(self.hotel.name, self.hotel.address.city))
        self.assertNotEqual(mail.outbox[0].subject, system_email.subject)

    def test_item_found_reminder(self):
        system_email = factories.EmailTemplateFactory(
            subject='reminder for your found item', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.item_found_reminder
        )
        mails.send_found_item_reminder_mail(self.matched_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_known_owner_match(self):
        system_email = factories.EmailTemplateFactory(
            subject='Your found item', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.known_owner_match_item
        )
        mails.send_known_owner_match_mail(self.matched_item, True, 'password')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_new_demo_request(self):
        details = {
            'email': self.hotel_user.employee.email,
            'full_name': '',
            'contact_number': '',
            'hotel_name': '',

        }
        system_email = factories.EmailTemplateFactory(
            subject='new demo request', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.new_demo_request
        )
        mails.send_hotel_demo_mail(details)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_new_hotel_user(self):
        system_email = factories.EmailTemplateFactory(
            subject='new user', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.new_hotel_user
        )
        mails.send_hotel_user_created_mail(self.hotel_user, 'password')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_new_message_received(self):
        system_email = factories.EmailTemplateFactory(
            subject='new message received', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.new_hotel_user
        )
        mails.send_hotel_user_created_mail(self.hotel_user, 'password')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_report_details(self):
        system_email = factories.EmailTemplateFactory(
            subject='report details', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.report_details
        )
        mails.send_client_item_report_mail(self.lost_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_shipment_collected(self):
        system_email = factories.EmailTemplateFactory(
            subject='shipment collected', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.shipment_collected
        )
        mails.send_shipment_collected_mail(self.shipping_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_shipment_created(self):
        system_email = factories.EmailTemplateFactory(
            subject='shipment created', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.shipment_created
        )
        mails.send_user_shipment_created_mail(self.shipping_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_shipment_scheduled(self):
        system_email = factories.EmailTemplateFactory(
            subject='shipment scheduled', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.shipment_scheduled
        )
        mails.send_shipment_scheduled_mail(self.shipping_item)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_welcome_client_user(self):
        system_email = factories.EmailTemplateFactory(
            subject='welcome', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.welcome_client_user
        )
        mails.send_report_user_registration_mail(self.hotel_user.employee, 'password')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_welcome_hotel_user(self):
        system_email = factories.EmailTemplateFactory(
            subject='welcome', hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.welcome_hotel_user
        )
        mails.send_hotel_signup_mail(self.hotel_user)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, system_email.subject)

    def test_password_reset_email(self):
        factories.EmailTemplateFactory(
            subject=None, hotel=None, is_active=True, is_system_default=True,
            template=enums.EmailTemplates.password_reset_email
        )
        mails.send_password_reset_mail(self.hotel_user.employee)
        self.assertEqual(len(mail.outbox), 1)
        self.assertNotEqual(mail.outbox[0].subject, None)
        self.assertEqual(mail.outbox[0].subject, 'ReturnHound password reset.')
