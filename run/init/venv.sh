#!/usr/bin/env bash

echo "-------------------------------------------------"
PY="$(which python3 ||:)"

if ! [ ${PY} ]; then
  echo "Python not found. You must install Python or run with Docker or Vagrant."
  exit 1
fi

if [ ${VIRTUAL_ENV} ]; then
    echo "Found VIRTUAL_ENV : using activated VENV"
else
    echo "Use project-local VENV: ${VENV}"
    # Recreate VENV when running with NUKE=1
    if [ ${NUKE} ]; then
        if [ -e ${VENV}/bin/activate ]; then
            rm -rf ${VENV}/*
        fi
    fi

    # setup python virtualenv
    if ! [ -e ${VENV}/bin/activate ]
    then
        ${PY} -m venv ${VENV}
    fi

    # activate the VENV
    echo "Activating ... ${VENV}"
    . ${VENV}/bin/activate
fi

# Install packages
FIRST_RUN=$(python -c 'import project' 2>&1 ||:)
if [ ${UPDATE} ] || [ "${FIRST_RUN}" ]; then
    echo "Installing packages with pip ..."
    test -z ${VERBOSE} && quiet="--quiet"
    # Install project.
    pip install ${quiet} -e .
    # Install extra requirements
    test -f requirements/extra.txt && pip install ${quiet} -r requirements/extra.txt
fi
