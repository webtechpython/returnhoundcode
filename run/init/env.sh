#!/usr/bin/env bash

export OS=${OS:-$(uname)} # "Linux" or "Darwin"(MacOS)
export PROJECT="returnhound"
export BASE_DIR="${BASE_DIR:-"./dev"}"

# Set HOST_IP if we are running inside docker
if [ ${IS_DOCKER} ]; then
    # Docker on Mac has a host we can query
    HOST_IP=$(getent hosts docker.for.mac.localhost | awk '{ print $1 }')
    # Docker on Linux has host ip as default gateway route
    if [ ! ${HOST_IP} ]; then
        export HOST_IP=$(ip route| awk '/^default/ {print $3}')
    fi
fi

test -n "${NUKE}" && echo "Nuke..."

# setup env, settings
export DEBUG=${DEBUG:-1}
export DJANGO_SETTINGS_MODULE="${DJANGO_SETTINGS_MODULE:-"project.settings_dev"}"
export WSGI_MODULE="${WSGI_MODULE:-"project.wsgi"}"
export STATIC_ROOT=${STATIC_ROOT:-"${BASE_DIR}/static_root"}
export MEDIA_ROOT=${MEDIA_ROOT:-"${BASE_DIR}/media_root"}
export DB_NAME="${DB_NAME:-${PROJECT}}"
export DB_HOST="${DB_HOST:-${HOST}}"
export DB_USER="${DB_USER:-${USER}}"
export VENV=${VENV:-"${PWD}/venv"}


if [ ${VERBOSE} ]; then
    echo "ENV..."
    echo "OS: ${OS}"
    echo "HOME: ${HOME}"
    echo "PROJECT: ${PROJECT}"
    echo "BASE_DIR: ${BASE_DIR}"
    echo "VENV: ${VENV}"
    echo "DEBUG: ${DEBUG}"
    echo "DJANGO_SETTINGS_MODULE: ${DJANGO_SETTINGS_MODULE}"

    echo "IS_DOCKER: ${IS_DOCKER}"
    echo "HOST_IP: ${HOST_IP}"
    echo "DB_HOST: ${DB_HOST}"
    echo "DB_NAME: ${DB_NAME}"
    echo "DB_USER: ${DB_USER}"
fi
