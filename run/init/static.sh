#!/usr/bin/env bash

NPM_BIN="$(which npm ||:)"

if ! [ ${NPM_BIN} ]
then
  echo "NPM not found. Install NodeJS and NPM."
  exit 1
fi

# Install Bower and friends
BOWER_BIN="$(which bower ||:)"
YARN_BIN="$(which yarn ||:)"
LESS_BIN="$(which lessc ||:)"
if [ ! ${BOWER_BIN} ] || [ ! ${YARN_BIN} ] || [ ! ${LESS_BIN} ]; then
    npm install -g --quiet bower yarn yuglify uglify-js@1 less
fi

# Delete any existing "node_modules" dirs
if [ ${NUKE} ]; then
    for modules_dir in $(find . -name node_modules -type d)
    do
        rm -rf ${modules_dir}
    done
fi

# install npm/yarn libs
if [ -e ./package.json ]; then
    echo "Found package.json ..."
    if [ ${UPDATE} ] || [ ! -e node_modules ]
    then
        yarn
    fi
fi

# TODO: migrate all projects to yarn
# install bower libs
if [ -e ./bower.json ]; then
    echo "Bower (bower.json) detected..."
    if [ ! "$(find . -name bower_components)" ]; then
        echo "Bower install..."
        bower install --quiet
    else
        if [ ${UPDATE} ]; then
            echo "Bower update..."
            bower update --quiet
            bower prune
        fi
    fi
fi

# collectstatic
if [ ${UPDATE} ] || [ ! -e ${STATIC_ROOT} ]; then
    if [ ${VERBOSE} ]; then
        django-admin collectstatic --noinput
    else
        django-admin collectstatic --noinput --verbosity 0
    fi
fi
