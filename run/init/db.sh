#!/usr/bin/env bash

PSQL="$(which psql ||:)"

if ! [ ${DB_NAME} ]
then
  echo "Required env 'DB_NAME' not found"
  exit 1
fi

if [ ${DB_HOST} ]
then
  PGHOST="-h $DB_HOST"
else
  PGHOST=""
fi


if [ ${PSQL} ]
then
    # Drop database when running with NUKE=1
    if [ ${NUKE} ]; then
        echo "DB drop: ${DB_NAME}"
        dropdb ${PGHOST} -U ${DB_USER} --if-exists ${DB_NAME} ||:
    fi
    # Create database if it does not exist:
    echo "Check if DB exists..."
    db_exists=$(echo $(psql ${PGHOST} -U ${DB_USER} -lqt | cut -d \| -f 1 | grep -E "^\s+${DB_NAME}\s+$"))
    if [ -n "${db_exists}" ]
    then
        echo "DB exists: ${db_exists}"
    else
        echo "DB create: ${DB_NAME}"
        createdb ${PGHOST} -U ${DB_USER} ${DB_NAME}
        echo "Django migrate ..."
        django-admin migrate
        echo "Load initial data ..."
        django-admin loaddata initial.json
    fi

else
    echo "No psql... cannot create database"
    echo "Install PostgreSQL and setup PATH to 'psql'"
    exit 1
fi

# Only run migrations when running with UPDATE flag
if [ ${UPDATE} ]; then
    echo "Django migrate ..."
    django-admin migrate
fi
