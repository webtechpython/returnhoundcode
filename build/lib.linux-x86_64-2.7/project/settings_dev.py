from project.settings import *

ALLOWED_HOSTS = ['*']

# Amazon S3 credentials
AWS_ACCESS_KEY_ID = 'AKIAI722GZEG2CIBTD6Q'  # user: server-dev
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'returnhound-dev'

# Celery
CELERY_ALWAYS_EAGER = True  # Disable queue (execute inline)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# NB: only for development... index lock errors with concurrent requests
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# Yammer
YAMMER_CLIENT_ID = 'Y9kQrrd92HRt6vbX8yUr0g'
YAMMER_CLIENT_SECRET_KEY = 'L3xMI7jBhxX0S5e7ZDekLtHgnPk8BtHfAt9idU8uq8'

# Google reCAPTCHA credentials
RECAPTCHA_SITE_KEY = '6LfdOA0UAAAAAAE8SnKYo_3mWjAvoK-nTz9d8efU'  # Live: 6LezeBsTAAAAAJQyaJPqhiVnJClHOFdwk95hy2cZ
RECAPTCHA_SECRET_KEY = '6LfdOA0UAAAAABhBdTWfDxOgLTkl8VL7rKm0URYG'  # FIXME (Live): To be determined.

# -----------------------------------------------------------------------------
# Mock responses for Aramex
# -----------------------------------------------------------------------------
INSTALLED_APPS += ('project.mocks',)
PROJECT_MOCKS = ['project.mocks.aramex']
