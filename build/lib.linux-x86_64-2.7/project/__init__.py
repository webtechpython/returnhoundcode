from __future__ import absolute_import
from django.utils.functional import lazy

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app
from .celery import app as celery_app


def lazy_setting(key, return_type):
    def getter():
        from django.conf import settings
        return getattr(settings, key)
    return lazy(getter, return_type)


def pipeline_storage():
    """
    In DEV environment (DEBUG=True) we want to use standard PipelineStorage.
    - css/js changes reflect instantly
    - recompiles less/sass on every request (slow)

    On the server (DEBUG=False) we use a cached storage class
    - cached + hashed
    - does not compile/compress on request (fast!)
    - assets are pre-compiled with `collectstatic`
    - need to run `collectstatic` before server startup
    """
    def getter():
        from django.conf import settings
        if settings.DEBUG:
            return 'pipeline.storage.PipelineStorage'
        else:
            return settings.PIPELINE_CACHED_STORAGE
    return lazy(getter, str)()
