from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from . import models
from . import enums


class HtmlEmailSender(object):
    """
    HtmlEmailSender(subject='Welcome to ReturnHound',html_template='email/welcome_email.html',
                    extra_context={'user': user}).send(to=[user.email])

    """

    def __init__(self, subject, html_template, extra_context=None):
        extra_context = extra_context or {}
        # Email headers
        self.subject = subject
        self.sender = getattr(settings, 'DEFAULT_FROM_EMAIL', 'noreply@returnHound.com')

        # Email body
        self.html_template = html_template
        self.extra_context = extra_context

    def get_context(self):
        context = {
            'protocol': getattr(settings, 'SITE_PROTOCOL', 'https://'),
            'domain': Site.objects.get_current().domain
        }
        context.update(self.extra_context)
        return context

    def render_html_body(self):
        return render_to_string(self.html_template, self.get_context())

    def send(self, to, cc=None, bcc=None):
        msg = EmailMessage(
            subject=self.subject,
            body=self.render_html_body(),
            from_email=self.sender,
            to=to, cc=cc, bcc=bcc,
        )
        msg.content_subtype = "html"
        msg.send()


def get_subject(template, subj):
    is_base = template and template.template == enums.EmailTemplates.email_base
    return template.subject if template and template.subject and not is_base else subj


def send_password_reset_mail(user):
    email = models.EmailTemplate.objects.get_outgoing_email('password_reset_email')
    HtmlEmailSender(
        subject=get_subject(email, 'ReturnHound password reset.'),
        html_template='registration/password_reset_email.html',
        extra_context={
            'user': user,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user),
            'email_obj': email,
            'social_links': models.SocialLink.objects.system_default()
        }
    ).send(to=[user.email])


def send_report_user_registration_mail(user, password):
    hotel = user.get_hotel
    email = models.EmailTemplate.objects.get_outgoing_email('welcome_client_user', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Welcome to ReturnHound'),
                    html_template='emails/welcome_client_user.html',
                    extra_context={
                        'user': user,
                        'password': password,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[user.email])


def send_client_item_report_mail(item):
    hotel = item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('report_details', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Your report details - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/report_details.html',
                    extra_context={
                        'client': item.created_by,
                        'item': item,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[item.created_by.email])


def send_hotel_item_report_mail(item):
    kw = dict(employer=item.hotel, is_mail_recipient=True)
    owner = item.hotel.get_hotel_owner
    recipients = [x.employee.email for x in models.HotelUser.objects.filter(**kw)]
    email = models.EmailTemplate.objects.get_outgoing_email('new_report_received_hotel', item.hotel)
    HtmlEmailSender(
        subject=get_subject(email, 'New Report received'),
        html_template='emails/new_report_received_hotel.html',
        extra_context={
            'hotel': item.hotel,
            'item': item,
            'email_obj': email,
            'social_links': models.SocialLink.objects.system_default()
        }).send(to=recipients + [owner.employee.email] if owner else recipients)


def send_hotel_item_found_mail(matched_item):
    hotel = matched_item.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('item_found', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Your lost item on ReturnHound has been found - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/item_found.html',
                    extra_context={
                        'lost_item': matched_item.lost_item,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[matched_item.lost_item.created_by.email])


def send_known_owner_match_mail(matched_item, user_created, password):
    hotel = matched_item.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('known_owner_match_item', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Your lost item on ReturnHound has been found - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/known_owner_match_item.html',
                    extra_context={
                        'user_created': user_created,
                        'matched_item': matched_item,
                        'password': password,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[matched_item.lost_item.created_by.email])


def send_found_item_reminder_mail(matched_item):
    hotel = matched_item.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('item_found_reminder', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Reminder: Please confirm your lost item on ReturnHound - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/item_found_reminder.html',
                    extra_context={
                        'lost_item': matched_item.lost_item,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[matched_item.lost_item.created_by.email])


def send_hotel_signup_mail(hotel_user):
    email = models.EmailTemplate.objects.get_outgoing_email('welcome_hotel_user', hotel_user.employer)
    HtmlEmailSender(subject=get_subject(email, 'Welcome to ReturnHound'),
                    html_template='emails/welcome_hotel_user.html',
                    extra_context={
                        'hotel': hotel_user.employer,
                        'user': hotel_user.employee,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[hotel_user.employee.email])


def send_hotel_signup_admin_mail(hotel_user, password):
    email = models.EmailTemplate.objects.get_outgoing_email('welcome_hotel_user_admin', hotel_user.employer)
    HtmlEmailSender(subject=get_subject(email, 'New hotel signup on ReturnHound'),
                    html_template='emails/welcome_hotel_user_admin.html',
                    extra_context={
                        'hotel': hotel_user.employer,
                        'user': hotel_user.employee,
                        'password': password,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[getattr(settings, 'DEFAULT_ADMIN_EMAIL', '')])


def send_hotel_user_created_mail(hotel_user, password):
    email = models.EmailTemplate.objects.get_outgoing_email('new_hotel_user', hotel_user.employer)
    HtmlEmailSender(subject=get_subject(email, 'Welcome to your ReturnHound account.'),
                    html_template='emails/new_hotel_user.html',
                    extra_context={
                        'hotel': hotel_user.employer,
                        'user': hotel_user.employee,
                        'hotel_user': hotel_user,
                        'password': password,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[hotel_user.employee.email])


def send_hotel_user_permissions_mail(hotel_user):
    email = models.EmailTemplate.objects.get_outgoing_email('hotel_account_user', hotel_user.employer)
    HtmlEmailSender(subject=get_subject(email, 'Your ReturnHound account permissions have been updated.'),
                    html_template='emails/hotel_account_user.html',
                    extra_context={
                        'user': hotel_user.employee,
                        'hotel_user': hotel_user,
                        'hotel': hotel_user.employer,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[hotel_user.employee.email])


def send_contact_mail(contact):
    email = models.EmailTemplate.objects.get_outgoing_email('new_message_received', None)
    HtmlEmailSender(subject=get_subject(email, 'Contact message from ReturnHound'),
                    html_template='emails/new_message_received.html',
                    extra_context={
                        'full_name': contact['full_name'],
                        'email': contact['email'],
                        'contact_number': contact['contact_number'],
                        'message': contact['message'],
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[getattr(settings, 'DEFAULT_CONTACT_US_EMAIL', '')], bcc=[contact['email']])


def send_hotel_demo_mail(details):
    email = models.EmailTemplate.objects.get_outgoing_email('new_demo_request', None)
    HtmlEmailSender(subject=get_subject(email, 'Demo request for ReturnHound'),
                    html_template='emails/new_demo_request.html',
                    extra_context={
                        'full_name': details['full_name'],
                        'email': details['email'],
                        'contact_number': details['contact_number'],
                        'hotel_name': details['hotel_name'],
                        'demo_date': details.get('demo_date', ''),
                        'demo_time': details.get('demo_time', ''),
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[getattr(settings, 'DEFAULT_ADMIN_EMAIL', '')], bcc=[details['email']])


def send_user_shipment_created_mail(shipping_item):
    hotel = shipping_item.parcel.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('shipment_created', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Item collection / shipment created - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/shipment_created.html',
                    extra_context={
                        'lost_item': shipping_item.parcel.lost_item,
                        'shipping': shipping_item.details,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[shipping_item.parcel.lost_item.created_by.email])


def send_hotel_shipment_created_mail(shipping_item):
    hotel = shipping_item.parcel.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('hotel_shipment_created', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Item collection / shipment created'),
                    html_template='emails/hotel_shipment_created.html',
                    extra_context={
                        'lost_item': shipping_item.parcel.lost_item,
                        'found_item': shipping_item.parcel.found_item,
                        'shipping': shipping_item.details,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[shipping_item.parcel.found_item.hotel.get_hotel_owner.employee.email])


def send_shipment_scheduled_mail(shipping_item):
    hotel = shipping_item.parcel.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('shipment_scheduled', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Shipment scheduled - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/shipment_scheduled.html',
                    extra_context={
                        'lost_item': shipping_item.parcel.lost_item,
                        'shipping': shipping_item.details,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[shipping_item.parcel.lost_item.created_by.email])


def send_shipment_collected_mail(shipping_item):
    hotel = shipping_item.parcel.lost_item.hotel
    email = models.EmailTemplate.objects.get_outgoing_email('shipment_collected', hotel)
    HtmlEmailSender(subject=get_subject(email, 'Item collected - {} ({})'.format(hotel.name, hotel.address.city)),
                    html_template='emails/shipment_collected.html',
                    extra_context={
                        'lost_item': shipping_item.parcel.lost_item,
                        'shipping': shipping_item.details,
                        'hotel': hotel,
                        'email_obj': email,
                        'social_links': models.SocialLink.objects.system_default()
                    }).send(to=[shipping_item.parcel.lost_item.created_by.email])
