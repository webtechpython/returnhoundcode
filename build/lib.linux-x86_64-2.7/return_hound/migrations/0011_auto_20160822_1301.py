# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0010_auto_20160520_1549'),
    ]

    operations = [
        migrations.AlterField(
            model_name='founditem',
            name='category',
            field=models.ForeignKey(to='return_hound.Category'),
        ),
        migrations.AlterField(
            model_name='lostitem',
            name='category',
            field=models.ForeignKey(to='return_hound.Category'),
        ),
    ]
