# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0011_auto_20160822_1301'),
    ]

    operations = [
        migrations.AddField(
            model_name='founditem',
            name='status',
            field=models.SmallIntegerField(null=True, choices=[(1, 'Shipped'), (2, 'Collected'), (3, 'Archived')], blank=True),
        ),
        migrations.AddField(
            model_name='founditem',
            name='status_text',
            field=models.TextField(null=True, blank=True),
        ),
    ]
