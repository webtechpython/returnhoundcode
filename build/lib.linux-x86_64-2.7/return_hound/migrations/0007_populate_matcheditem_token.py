# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from uuid import uuid4


def gen_uuid(apps, schema_editor):
    matched_items = apps.get_model('return_hound', 'MatchedItem')
    for row in matched_items.objects.all():
        row.token = uuid4()
        row.save()


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0006_matcheditem_token'),
    ]

    operations = [
        # omit reverse_code=... if you don't want the migration to be reversible.
        migrations.RunPython(gen_uuid, reverse_code=migrations.RunPython.noop),
    ]
