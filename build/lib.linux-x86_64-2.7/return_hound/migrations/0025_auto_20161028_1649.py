# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import enumfields.fields
import return_hound.enums


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0024_remove_emailtemplate_is_base_style'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailtemplate',
            name='template',
            field=enumfields.fields.EnumIntegerField(default=1, enum=return_hound.enums.EmailTemplates),
            preserve_default=False,
        ),
    ]
