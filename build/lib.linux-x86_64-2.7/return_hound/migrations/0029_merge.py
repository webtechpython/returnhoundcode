# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0017_invoice_invoiceitem'),
        ('return_hound', '0028_merge'),
    ]

    operations = [
    ]
