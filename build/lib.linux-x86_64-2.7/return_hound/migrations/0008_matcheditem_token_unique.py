# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0007_populate_matcheditem_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matcheditem',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, unique=True),
        ),
    ]
