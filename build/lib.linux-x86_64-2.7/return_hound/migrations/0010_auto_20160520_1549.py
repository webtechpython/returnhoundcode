# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0009_auto_20160513_0837'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hoteluser',
            options={'ordering': ['id']},
        ),
    ]
