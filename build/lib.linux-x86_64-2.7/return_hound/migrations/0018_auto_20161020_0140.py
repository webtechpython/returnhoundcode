# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0017_auto_20161019_2045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sociallink',
            name='hotel',
            field=models.ForeignKey(null=True, to='return_hound.Hotel', blank=True),
        ),
    ]
