# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import model_utils.fields
import django.utils.timezone
from django.conf import settings
import colorful.fields
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cities_light', '0006_compensate_for_0003_bytestring_bug'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(verbose_name='email address', db_index=True, unique=True, max_length=255)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active.  Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(blank=True, max_length=50)),
                ('contact_number', models.CharField(blank=True, max_length=50)),
                ('terms', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', to='auth.Group', related_query_name='user', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', to='auth.Permission', related_query_name='user', verbose_name='user permissions')),
            ],
            options={
                'ordering': ['email'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('line_1', models.TextField()),
                ('line_2', models.TextField(blank=True)),
                ('zip_code', models.CharField(blank=True, max_length=15)),
                ('city', models.CharField(max_length=100)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('country', models.ForeignKey(to='cities_light.Country')),
                ('region', models.ForeignKey(to='cities_light.Region')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='CategoryItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('category', models.ForeignKey(to='return_hound.Category')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='CountryCurrency',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('country', models.ForeignKey(to='cities_light.Country')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=20)),
                ('code', models.CharField(unique=True, max_length=10)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FoundItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('date', models.DateField()),
                ('location', models.TextField(blank=True)),
                ('room_number', models.CharField(blank=True, max_length=10)),
                ('description', models.TextField(blank=True)),
                ('serial_number', models.CharField(blank=True, max_length=100)),
                ('specification', models.CharField(blank=True, max_length=100)),
                ('found_by', models.CharField(max_length=100)),
                ('owner_name', models.CharField(blank=True, max_length=50)),
                ('owner_email', models.EmailField(blank=True, max_length=50)),
                ('owner_surname', models.CharField(blank=True, max_length=50)),
                ('owner_contact_number', models.CharField(blank=True, max_length=50)),
                ('width', models.DecimalField(max_digits=10, decimal_places=2)),
                ('height', models.DecimalField(max_digits=10, decimal_places=2)),
                ('length', models.DecimalField(max_digits=10, decimal_places=2)),
                ('weight', models.DecimalField(max_digits=10, decimal_places=2)),
                ('category', models.ForeignKey(blank=True, to='return_hound.Category', null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=50)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='name')),
                ('address', models.CharField(max_length=200)),
                ('zip_code', models.CharField(blank=True, max_length=15)),
                ('city', models.CharField(max_length=100)),
                ('is_verified', models.BooleanField(default=False)),
                ('country', models.ForeignKey(to='cities_light.Country')),
                ('region', models.ForeignKey(to='cities_light.Region')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HotelUser',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False)),
                ('is_creator', models.BooleanField(default=False)),
                ('employee', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('employer', models.ForeignKey(to='return_hound.Hotel')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('path', models.TextField()),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LostItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('date', models.DateField()),
                ('location', models.TextField(blank=True)),
                ('room_number', models.CharField(blank=True, max_length=10)),
                ('description', models.TextField(blank=True)),
                ('serial_number', models.CharField(blank=True, max_length=100)),
                ('specification', models.CharField(blank=True, max_length=100)),
                ('arrival_date', models.DateField(blank=True, null=True)),
                ('departure_date', models.DateField(blank=True, null=True)),
                ('category', models.ForeignKey(blank=True, to='return_hound.Category', null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('hotel', models.ForeignKey(to='return_hound.Hotel')),
                ('item_type', models.ForeignKey(blank=True, to='return_hound.CategoryItem', null=True)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='MatchedItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('status', models.SmallIntegerField(default=2, choices=[(1, 'Accepted'), (2, 'Pending'), (3, 'Declined')])),
                ('found_item', models.ForeignKey(to='return_hound.FoundItem')),
                ('lost_item', models.ForeignKey(to='return_hound.LostItem')),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='ReportStyle',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('icon_color', colorful.fields.RGBColorField(default='#027fba')),
                ('form_label_color', colorful.fields.RGBColorField(default='#2e2e2e')),
                ('form_header_color', colorful.fields.RGBColorField(default='#666666')),
                ('form_background_color', colorful.fields.RGBColorField(default='#FFFFFF')),
                ('form_field_color', colorful.fields.RGBColorField(default='#2e2e2e')),
                ('form_field_border_color', colorful.fields.RGBColorField(default='#cccccc')),
                ('form_field_border_radius', models.IntegerField(default=6)),
                ('button_border_radius', models.IntegerField(default=6)),
                ('button_primary_color', colorful.fields.RGBColorField(default='#027fba')),
                ('button_primary_text_color', colorful.fields.RGBColorField(default='#FFFFFF')),
                ('button_secondary_color', colorful.fields.RGBColorField(default='#484848')),
                ('button_secondary_text_color', colorful.fields.RGBColorField(default='#FFFFFF')),
                ('header_color', colorful.fields.RGBColorField(default='#027fba')),
                ('header_active_color', colorful.fields.RGBColorField(default='#2e2e2e')),
                ('header_active_text_color', colorful.fields.RGBColorField(default='#FFFFFF')),
                ('header_inactive_color', colorful.fields.RGBColorField(default='#027fba')),
                ('header_inactive_text_color', colorful.fields.RGBColorField(default='#027fba')),
                ('show_hotel_logo', models.BooleanField(default=False)),
                ('show_cover_image', models.BooleanField(default=False)),
                ('hotel', models.ForeignKey(to='return_hound.Hotel')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Shipment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('label_url', models.TextField(blank=True)),
                ('shipping_date', models.DateTimeField(blank=True, null=True)),
                ('collected_date', models.DateTimeField(blank=True, null=True)),
                ('tracking_number', models.CharField(blank=True, max_length=100)),
                ('collector_name', models.CharField(blank=True, max_length=100)),
                ('collector_last_name', models.CharField(blank=True, max_length=100)),
                ('collector_contact_number', models.CharField(blank=True, max_length=100)),
                ('collector_email', models.EmailField(blank=True, max_length=254)),
                ('is_collection', models.BooleanField(default=False)),
                ('is_complete', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ShippingItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('is_deleted', models.BooleanField(default=False)),
                ('details', models.ForeignKey(to='return_hound.Shipment')),
                ('parcel', models.ForeignKey(to='return_hound.MatchedItem')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('payment_date', models.DateTimeField(blank=True, null=True)),
                ('charged_amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('surcharge_amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('reference_number', models.CharField(blank=True, max_length=100)),
                ('is_paid', models.BooleanField()),
                ('shipping_details', models.OneToOneField(to='return_hound.Shipment')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='founditem',
            name='hotel',
            field=models.ForeignKey(to='return_hound.Hotel'),
        ),
        migrations.AddField(
            model_name='founditem',
            name='item_type',
            field=models.ForeignKey(blank=True, to='return_hound.CategoryItem', null=True),
        ),
        migrations.AddField(
            model_name='countrycurrency',
            name='currency',
            field=models.ForeignKey(to='return_hound.Currency'),
        ),
    ]
