# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.db import migrations, models


def migrate_data(apps, schema):
    Address = apps.get_model('return_hound.Address')
    Hotel = apps.get_model('return_hound.Hotel')
    # populate Hotel.address FK:
    for hotel in Hotel.objects.all():
        hotel.address = Address.objects.create(
            line1=hotel.line1,
            line2=hotel.line2,
            city=hotel.city,
            post_code=hotel.post_code,
            aramex_country=hotel.aramex_country,
            aramex_state=hotel.aramex_state,
            content_type_id=ContentType.objects.get_for_model(Hotel).pk,
            object_id=hotel.pk,
        )
        hotel.save()


class Migration(migrations.Migration):
    dependencies = [
        ('return_hound', '0039_hotel_address'),
    ]
    operations = [
        migrations.RunPython(migrate_data, reverse_code=migrations.RunPython.noop)
    ]
