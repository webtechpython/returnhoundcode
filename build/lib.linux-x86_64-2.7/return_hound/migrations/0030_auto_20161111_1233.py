# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0029_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='attention',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='contact_number',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='invoice',
            name='hotel',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='invoice',
            name='tracking_number',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
