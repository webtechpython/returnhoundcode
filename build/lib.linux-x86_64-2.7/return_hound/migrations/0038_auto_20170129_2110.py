# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0037_address_hotel_aramex_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='old_address',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
