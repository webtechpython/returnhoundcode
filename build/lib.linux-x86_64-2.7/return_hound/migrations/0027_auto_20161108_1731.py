# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0026_merge'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='emailtemplate',
            unique_together=set([('hotel', 'template')]),
        ),
    ]
