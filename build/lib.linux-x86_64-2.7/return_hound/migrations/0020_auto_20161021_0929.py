# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import enumfields.fields
import colorful.fields
import return_hound.enums


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0019_auto_20161021_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailtemplate',
            name='is_base_style',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='banner_border_colour',
            field=colorful.fields.RGBColorField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='banner_colour',
            field=colorful.fields.RGBColorField(default='#fff'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_colour',
            field=colorful.fields.RGBColorField(default='#fff'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text',
            field=models.TextField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text_colour',
            field=colorful.fields.RGBColorField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text_colour_2',
            field=colorful.fields.RGBColorField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_border_colour',
            field=colorful.fields.RGBColorField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_colour',
            field=colorful.fields.RGBColorField(default='#fff'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_text_colour',
            field=colorful.fields.RGBColorField(default='#000'),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='template',
            field=enumfields.fields.EnumIntegerField(blank=True, null=True, enum=return_hound.enums.EmailTemplates),
        ),
    ]
