# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0021_auto_20161022_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailtemplate',
            name='subject',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
