# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0016_auto_20161028_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='domain_name',
            field=models.CharField(max_length=50, blank=True),
        ),
    ]
