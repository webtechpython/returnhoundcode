# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0015_founditem_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shipment',
            name='label_url',
        ),
        migrations.AddField(
            model_name='shipment',
            name='label_file',
            field=models.FileField(null=True, blank=True, upload_to='shipment/labels'),
        ),
    ]
