# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0011_auto_20160822_1301'),
    ]

    operations = [
        migrations.AddField(
            model_name='hoteluser',
            name='correspondence_chain',
            field=models.BooleanField(default=False),
        ),
    ]
