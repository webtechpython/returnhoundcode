# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='categoryitem',
            name='name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='founditem',
            name='room_number',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='lostitem',
            name='room_number',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
