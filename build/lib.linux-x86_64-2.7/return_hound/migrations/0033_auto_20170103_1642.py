# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0032_auto_20161213_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='fob',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='rep',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='tax_rate',
            field=models.DecimalField(max_digits=5, null=True, decimal_places=2),
        ),
    ]
