# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0012_auto_20160907_1814'),
        ('return_hound', '0012_hoteluser_correspondence_chain'),
    ]

    operations = [
    ]
