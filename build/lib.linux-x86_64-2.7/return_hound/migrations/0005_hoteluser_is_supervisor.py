# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0004_transaction_service_charge'),
    ]

    operations = [
        migrations.AddField(
            model_name='hoteluser',
            name='is_supervisor',
            field=models.BooleanField(default=False),
        ),
    ]
