# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import enumfields.fields
import return_hound.enums


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0014_auto_20160913_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='founditem',
            name='status',
            field=enumfields.fields.EnumIntegerField(enum=return_hound.enums.FoundItemStatus, null=True, blank=True),
        ),
    ]
