# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0023_auto_20161024_1513'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emailtemplate',
            name='is_base_style',
        ),
    ]
