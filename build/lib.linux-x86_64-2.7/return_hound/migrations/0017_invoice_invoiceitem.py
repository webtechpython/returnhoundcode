# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0016_auto_20161028_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('rep', models.CharField(max_length=255)),
                ('awb', models.CharField(max_length=255)),
                ('fob', models.CharField(max_length=255)),
                ('tax_rate', models.DecimalField(decimal_places=2, max_digits=5)),
                ('shipping_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('company', models.CharField(max_length=255, null=True, blank=True)),
                ('shipment', models.OneToOneField(to='return_hound.Shipment')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='InvoiceItem',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('name', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=255)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantity', models.PositiveSmallIntegerField(default=1)),
                ('invoice', models.ForeignKey(related_name='items', to='return_hound.Invoice')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
