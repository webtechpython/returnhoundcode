from zipfile import is_zipfile, ZipFile
import json
import os


def read_file(filepath):
    if is_zipfile(filepath):
        filename_in_zip = os.path.basename(filepath.replace('.zip', ''))
        with ZipFile(filepath, 'r') as zipped:
            content = zipped.read(filename_in_zip).decode()
    else:
        with open(filepath, 'r') as fh:
            content = fh.read()
    return content


def load_fixture(app_path, filename):
    """
    :param filename: eg: appname.ModelName.json
    """
    app_module = __import__(app_path)
    appdir = os.path.dirname(app_module.__file__)
    filepath = os.path.join(appdir, 'fixtures', filename)
    return json.loads(read_file(filepath))
