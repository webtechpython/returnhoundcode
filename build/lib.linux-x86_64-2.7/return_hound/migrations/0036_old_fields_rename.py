# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0035_address_fields'),
    ]

    operations = [
        migrations.RenameField(
            model_name='address',
            old_name='country',
            new_name='old_country',
        ),
        migrations.RenameField(
            model_name='address',
            old_name='region',
            new_name='old_region',
        ),
        migrations.RenameField(
            model_name='hotel',
            old_name='country',
            new_name='old_country',
        ),
        migrations.RenameField(
            model_name='hotel',
            old_name='region',
            new_name='old_region',
        ),
        migrations.RenameField(
            model_name='hotel',
            old_name='address',
            new_name='old_address',
        ),
    ]
