# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0029_founditem_owner_send_email'),
        ('return_hound', '0030_auto_20161111_1233'),
    ]

    operations = [
    ]
