# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.db import migrations, models


def migrate_data(apps, schema):
    User = apps.get_model('return_hound.User')
    Address = apps.get_model('return_hound.Address')
    user_ctype_id = ContentType.objects.get_for_model(User).pk
    for user in User.objects.all():
        address = Address.objects.filter(content_type_id=user_ctype_id, object_id=user.pk).first()
        if address:
            user.address = address
            user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0041_auto_20170207_1207'),
    ]

    operations = [
        migrations.RunPython(migrate_data, reverse_code=migrations.RunPython.noop)
    ]
