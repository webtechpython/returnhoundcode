# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-08-16 15:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import return_hound.enums


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0054_auto_20170713_1705'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='item_value_currency',
            field=enumfields.fields.EnumIntegerField(enum=return_hound.enums.ItemValueCurrencies, null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='shipping_details',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='transaction', to='return_hound.Shipment'),
        ),
    ]
