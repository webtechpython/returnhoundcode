# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0040_hotel_address_data'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hotel',
            name='aramex_country',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='aramex_state',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='city',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='line1',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='line2',
        ),
        migrations.RemoveField(
            model_name='hotel',
            name='post_code',
        ),
        migrations.AddField(
            model_name='user',
            name='address',
            field=models.ForeignKey(blank=True, to='return_hound.Address', null=True),
        ),
    ]
