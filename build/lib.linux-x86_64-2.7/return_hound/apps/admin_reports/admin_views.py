from braces.views import StaffuserRequiredMixin, PermissionRequiredMixin
from django.views import generic


class ReportAdminView(PermissionRequiredMixin, StaffuserRequiredMixin, generic.TemplateView):
    """
    Base view of all reports. Staff user is required to view any reports.
    """
    permission_required = 'return_hound.reports'


class HotelsReport(ReportAdminView):
    template_name = 'reports/hotels_report.html'


class OverviewReport(ReportAdminView):
    template_name = 'reports/overview_report.html'


class CollectionReadyReport(ReportAdminView):
    """
    Generate the collection ready report in the admin.
    """
    template_name = 'reports/collection_ready_report.html'


class FoundItemReport(ReportAdminView):
    """
    Generate the found found item report in the admin.
    """
    template_name = 'reports/found_item_report.html'


class MatchedItemReport(ReportAdminView):
    """
    Generate the found found item report in the admin.
    """
    template_name = 'reports/matched_item_report.html'


class HistoryItemReport(ReportAdminView):
    """
    Generate history report in the admin.
    """
    template_name = 'reports/history_report.html'


class PaymentItemReport(ReportAdminView):
    """
    Generate payment report history in admin
    """
    template_name = 'reports/payment_item_report.html'


class DashboardOverviewReport(ReportAdminView):
    """
    Generate an overview of based on a date range in the admin
    """
    template_name = 'reports/dashboard_overview_report.html'

