from django.core.exceptions import ValidationError
from django.db import models

from s3direct.fields import S3DirectField
from colorful.fields import RGBColorField
from model_utils.models import TimeStampedModel
from tinymce.models import HTMLField
from adminsortable.models import SortableMixin, SortableForeignKey

from return_hound.models import SocialLink


class SingleInstanceMixin(object):
    """
    Allow the creation of only one instance per model.
    """
    def clean(self):
        model = self.__class__
        first_instance = model.objects.first()
        if first_instance and self.id != first_instance.id:
            raise ValidationError('Can only create 1 {} instance'.format(model.__name__))


class OrderedModel(SortableMixin):
    number = models.PositiveSmallIntegerField(default=0, editable=False, db_index=True)

    class Meta:
        abstract = True


class Address(TimeStampedModel):
    line_1 = models.CharField(max_length=225, blank=True)
    line_2 = models.CharField(max_length=225, blank=True)
    line_3 = models.CharField(max_length=225, blank=True)
    po_box = models.CharField(max_length=225, blank=True)
    country = models.CharField(max_length=225, blank=True)
    city = models.CharField(max_length=225, blank=True)

    def __str__(self):
        return "{}, {}, {}".format(self.line_1, self.country, self.city)

    class Meta:
        verbose_name_plural = 'Address'


class PressRelease(TimeStampedModel, OrderedModel):
    logo = S3DirectField(dest='images', blank=True, help_text='Logo Dimensions 476x48, 242x194')
    short_description = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=225, blank=True)

    class Meta:
        ordering = ['number']

    def __str__(self):
        return self.link


class Header(SingleInstanceMixin, TimeStampedModel):
    header_logo = S3DirectField(dest='images', blank=True, help_text='Logo Dimensions 333x128')
    sticky_header_logo = S3DirectField(dest='images', blank=True, help_text='Logo Dimensions 350x45')
    background_colour = RGBColorField(default='#027fba', blank=True)
    # link texts
    report_lost_item_link_text = models.CharField(max_length=225, blank=True)
    login_link_text = models.CharField(max_length=225, blank=True)
    hotel_zone_link_text = models.CharField(max_length=255, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Header'


class Footer(SingleInstanceMixin, TimeStampedModel):
    background_colour = RGBColorField(default='#363636', blank=True)
    address = models.OneToOneField(Address, null=True, blank=True)
    social_links = models.ManyToManyField(SocialLink, limit_choices_to={'hotel__isnull': True}, blank=True)
    # link texts
    about_us_link_text = models.CharField(max_length=255, blank=True)
    how_it_works_link_text = models.CharField(max_length=255, blank=True)
    press_releases_link_text = models.CharField(max_length=255, blank=True)
    faq_link_text = models.CharField(max_length=255, blank=True)
    report_lost_item_link_text = models.CharField(max_length=255, blank=True)
    log_in_link_text = models.CharField(max_length=225, blank=True)
    sign_up_link_text = models.CharField(max_length=225, blank=True)
    contact_us_link_text = models.CharField(max_length=255, blank=True)
    terms_and_conditions_link_text = models.CharField(max_length=255, blank=True)
    # static text
    site_name_text = models.CharField(max_length=225, blank=True)
    connect_with_us_text = models.CharField(max_length=225, blank=True)
    we_accept_in_aed_text = models.CharField(max_length=225, blank=True)
    copyright_text = models.CharField(max_length=225, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Footer'


class Testimonial(TimeStampedModel, OrderedModel):
    title = models.CharField(max_length=225, blank=True)
    comment = models.TextField(blank=True, null=True)
    author = models.CharField(max_length=225, blank=True)

    class Meta:
        ordering = ['number']

    def __str__(self):
        return self.title


class BaseHomePage(models.Model):
    background_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1920x1080')
    main_header_text = models.CharField(max_length=255, blank=True)
    sub_header_text = models.CharField(max_length=225, blank=True)
    # Video section
    video_header_text = models.CharField(max_length=255, blank=True)
    video_link = models.URLField(blank=True)
    video_background_colour = RGBColorField(default='#027fba', blank=True)
    video_shadow_colour = RGBColorField(default='#015c87', blank=True)
    # Testimonials section
    testimonials = models.ManyToManyField(Testimonial, blank=True)
    testimonials_background_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1920x1080')
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class PressReleaseLogo(models.Model):
    image = S3DirectField(dest='images', blank=True, help_text='Logo Dimensions 476x48, 242x194')

    def __str__(self):
        return self.image


class HomePage(SingleInstanceMixin, TimeStampedModel, BaseHomePage):
    # How it works section
    how_it_works_header_text = models.CharField(max_length=255, blank=True)
    how_it_works_sub_header_text = models.CharField(max_length=225, blank=True)
    how_it_works_background_colour = RGBColorField(default='#ffffff', blank=True)
    # What the press says section
    press_header_text = models.CharField(max_length=255, blank=True)
    press_info_text = models.CharField(max_length=225, blank=True)
    press_comment_text = models.CharField(max_length=225, blank=True)
    press_background_colour = RGBColorField(default='#ffffff', blank=True)
    press_releases = models.ManyToManyField(PressReleaseLogo, blank=True)

    class Meta:
        verbose_name_plural = 'Home page'


class HomePageHowItWorksStep(TimeStampedModel, OrderedModel):
    home_page = SortableForeignKey(HomePage, null=True, blank=True)
    image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 374x520, 530x512, 332x528')
    main_text = models.CharField(max_length=225, blank=True)
    info_text = models.CharField(max_length=225, blank=True)

    class Meta:
        verbose_name_plural = 'How it works steps'
        ordering = ['number']


class HotelZonePage(SingleInstanceMixin, TimeStampedModel, BaseHomePage):
    question = models.CharField(max_length=225, blank=True)
    # Lost & found section
    lost_found_header_text = models.CharField(max_length=255, blank=True)
    lost_found_no_cost_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1894x334')
    lost_found_sub_header_text = models.CharField(max_length=255, blank=True)
    lost_found_description = models.TextField(blank=True, null=True)
    lost_found_background_colour = RGBColorField(default='#ffffff', blank=True)
    # Automated process section
    process_header_text = models.CharField(max_length=255, blank=True)
    process_description = models.TextField(blank=True, null=True)
    process_background_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1920x1080')
    # Get started section
    get_started_header_text = models.CharField(max_length=255, blank=True)
    get_started_description = models.TextField(blank=True, null=True)
    get_started_background_colour = RGBColorField(default='#027fba', blank=True)

    class Meta:
        verbose_name_plural = 'Hotel zone page'


class AboutUsPage(SingleInstanceMixin, TimeStampedModel):
    background_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1920x1080')
    background_colour = RGBColorField(default='#ffffff', blank=True)
    main_header = models.CharField(max_length=225, blank=True)
    main_text = models.CharField(max_length=225, blank=True)
    section_header = models.CharField(max_length=225, blank=True)
    section_text = models.TextField(blank=True, null=True)
    section_footer_text = models.CharField(max_length=225, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'About us page'


class HowItWorksPage(SingleInstanceMixin, TimeStampedModel):
    header_text = models.CharField(max_length=225, blank=True)
    footer_text = models.CharField(max_length=225, blank=True)
    footer_background_color = RGBColorField(default='#cbcbcb', blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'How it works page'


class HowItWorksStep(TimeStampedModel, OrderedModel):
    header_text = models.CharField(max_length=225, blank=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['number']

    def __str__(self):
        return self.header_text


class HowItWorksSection(TimeStampedModel, OrderedModel):
    how_it_works_page = SortableForeignKey(HowItWorksPage, null=True, blank=True)
    image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 530x330, 332x392, 374x356')
    background_color = RGBColorField(default='#ffffff', blank=True)
    steps = models.ManyToManyField(HowItWorksStep)

    class Meta:
        ordering = ['number']


class PressReleasePage(SingleInstanceMixin, TimeStampedModel):
    background_colour = RGBColorField(default='#ffffff', blank=True)
    header_text = models.CharField(max_length=225, blank=True)
    press_releases = models.ManyToManyField(PressRelease, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Press release page'


class FaqPage(SingleInstanceMixin, TimeStampedModel):
    background_colour = RGBColorField(default='#ffffff', blank=True)
    header_text = models.CharField(max_length=225, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Faq page'


class Faq(TimeStampedModel, OrderedModel):
    faq_page = SortableForeignKey(FaqPage, null=True, blank=True)
    question = models.CharField(max_length=225, blank=True)
    answer = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['number']


class SimplePage(SingleInstanceMixin, TimeStampedModel):
    header_text = models.CharField(max_length=225, blank=True)
    background_image = S3DirectField(dest='images', blank=True, help_text='Image Dimensions 1920x1080')
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class ReportLostItemPage(SimplePage):
    pass

    class Meta:
        verbose_name_plural = 'Report lost item page'


class LogInPage(SimplePage):
    pass

    class Meta:
        verbose_name_plural = 'Log in page'


class SignUpPage(SimplePage):
    pass

    class Meta:
        verbose_name_plural = 'Sign up page'


class Tab(OrderedModel, TimeStampedModel):
    header_text = models.CharField(max_length=225)
    content = HTMLField()

    class Meta:
        ordering = ['number']

    def __str__(self):
        return self.header_text


class TermsPage(SingleInstanceMixin, TimeStampedModel):
    background_colour = RGBColorField(default='#ffffff', blank=True)
    tabs = models.ManyToManyField(Tab, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Terms and conditions page'


class ForgotPasswordPage(SimplePage):
    info_text = models.CharField(max_length=225, blank=True)

    class Meta:
        verbose_name_plural = 'Forgot password page'


class RequestDemoPage(SimplePage):
    info_text = models.CharField(max_length=225, blank=True)

    class Meta:
        verbose_name_plural = 'Request demo page'


class ContactUsPage(SingleInstanceMixin, TimeStampedModel):
    background_colour = RGBColorField(default='#ffffff', blank=True)
    address = models.OneToOneField(Address, null=True, blank=True)
    header_text = models.CharField(max_length=225, blank=True)
    header_info_text = models.CharField(max_length=225, blank=True)
    form_header_text = models.CharField(max_length=225, blank=True)
    contact_number = models.CharField(max_length=225, blank=True)
    email = models.EmailField(blank=True)
    # Icon Text
    address_icon_text = models.CharField(max_length=225, blank=True)
    phone_icon_text = models.CharField(max_length=225, blank=True)
    email_icon_text = models.CharField(max_length=225, blank=True)
    # field that checks if the page has been published
    is_active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Contact us page'
