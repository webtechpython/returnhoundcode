from django.contrib import admin
from .models import (
    Address, Header, Footer, Testimonial, HomePageHowItWorksStep, HomePage, HotelZonePage, AboutUsPage,
    HowItWorksSection, HowItWorksPage, PressReleasePage, Faq, FaqPage, ReportLostItemPage, LogInPage,
    SignUpPage, ContactUsPage, RequestDemoPage, PressRelease, TermsPage, Tab, PressReleaseLogo, HowItWorksStep,
    ForgotPasswordPage
)

from adminsortable.admin import SortableTabularInline, NonSortableParentAdmin


class AddressAdmin(admin.ModelAdmin):
    list_display = ['id', 'line_1', 'country', 'city']
    fieldsets = (
        ('Address Information', {
            'fields': ('line_1', 'line_2', 'line_3', 'po_box', 'country', 'city')
        }),
    )


class HeaderAdmin(admin.ModelAdmin):
    list_display = ['id', 'background_colour', 'report_lost_item_link_text', 'login_link_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('background_colour', 'header_logo', 'sticky_header_logo')
        }),
        ('Link Text', {
            'fields': ('report_lost_item_link_text', 'login_link_text', 'hotel_zone_link_text')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class FooterAdmin(admin.ModelAdmin):
    list_display = ['id', 'background_colour', 'site_name_text', 'connect_with_us_text', 'is_active']
    list_editable = ['is_active']
    filter_horizontal = ['social_links']
    fieldsets = (
        ('Main Section', {
            'fields': ('background_colour', )
        }),
        ('Link Text', {
            'fields': ('about_us_link_text', 'how_it_works_link_text', 'press_releases_link_text', 'faq_link_text',
                       'report_lost_item_link_text', 'log_in_link_text', 'sign_up_link_text', 'contact_us_link_text',
                       'terms_and_conditions_link_text')
        }),
        ('Static Text', {
            'fields': ('site_name_text', 'connect_with_us_text', 'we_accept_in_aed_text', 'copyright_text')
        }),
        ('Returnhound Information Section', {
            'fields': ('address', 'social_links')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class ReportLostItemPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'header_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('header_text', 'background_image')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class LogInPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'header_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('header_text', 'background_image')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class SignUpPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'header_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('header_text', 'background_image')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class FaqInline(SortableTabularInline):
    model = Faq


class FaqPageAdmin(NonSortableParentAdmin):
    list_display = ['id', 'header_text', 'background_colour', 'is_active']
    list_editable = ['is_active']
    display_inline = ['background_colour', 'header_text', 'is_active']
    inlines = [FaqInline]

    fieldsets = (
        ('Main Section', {
            'fields': ('background_colour', 'header_text')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class AboutUsPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'background_colour', 'main_header', 'section_header', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('main_header', 'main_text', 'background_image')
        }),
        ('About Us Section', {
            'fields': ('background_colour', 'section_header', 'section_text', 'section_footer_text')
        }),
        ('Activate changes', {
            'fields': ('is_active', )
        })
    )


class PressReleaseInline(admin.TabularInline):
    model = PressRelease


class PressReleasePageAdmin(admin.ModelAdmin):
    list_display = ['id', 'background_colour', 'header_text', 'is_active']
    list_editable = ['is_active']
    filter_horizontal = ['press_releases']

    fieldsets = (
        ('Main Section', {
            'fields': ('background_colour', 'header_text', 'press_releases')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class ContactUsPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'header_text', 'header_info_text', 'form_header_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('background_colour', 'header_text', 'header_info_text', 'form_header_text')
        }),
        ('Returnhound Information Section', {
            'fields': ('address', 'address_icon_text', 'email', 'email_icon_text', 'contact_number', 'phone_icon_text')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class HowItWorksSectionInline(SortableTabularInline):
    model = HowItWorksSection
    filter_horizontal = ['steps']


class HowItWorksPageAdmin(NonSortableParentAdmin):
    list_display = ['id', 'footer_background_color', 'header_text', 'footer_text', 'is_active']
    list_editable = ['is_active']
    inlines = [HowItWorksSectionInline]

    fieldsets = (
        ('Header Section', {
            'fields': ('header_text', )
        }),
        ('Footer Section', {
            'fields': ('footer_text', 'footer_background_color')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class HomePageHowItWorksStepInline(SortableTabularInline):
    model = HomePageHowItWorksStep


class HomePageAdmin(NonSortableParentAdmin):
    list_display = ['id', 'main_header_text', 'sub_header_text', 'is_active']
    list_editable = ['is_active']
    filter_horizontal = ['testimonials', 'press_releases']
    inlines = [HomePageHowItWorksStepInline]

    fieldsets = (
        ('Header Section', {
            'fields': ('main_header_text', 'sub_header_text', 'background_image')
        }),
        ('Video Section', {
            'fields': ('video_header_text', 'video_link', 'video_background_colour', 'video_shadow_colour')
        }),
        ('Press Section', {
           'fields': ('press_header_text', 'press_info_text', 'press_comment_text',
                      'press_background_colour', 'press_releases')
        }),
        ('Testimonials', {
            'fields': ('testimonials', 'testimonials_background_image')
        }),
        ('How It Works Section', {
           'fields': ('how_it_works_header_text', 'how_it_works_sub_header_text', 'how_it_works_background_colour')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
    })
    )


class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author']
    fieldsets = (
        ('Testimonial Information', {
            'fields': ('title', 'comment', 'author')
        }),
    )


class HotelZonePageAdmin(admin.ModelAdmin):
    list_display = ['id', 'main_header_text', 'sub_header_text', 'is_active']
    list_editable = ['is_active']
    filter_horizontal = ['testimonials']

    fieldsets = (
        ('Header Section', {
            'fields': ('main_header_text', 'sub_header_text', 'background_image')
        }),
        ('Question Text', {
           'fields': ('question', )
        }),
        ('Lost & Found Section', {
            'fields': ('lost_found_header_text', 'lost_found_sub_header_text', 'lost_found_description',
                       'lost_found_background_colour', 'lost_found_no_cost_image')
        }),
        ('Automated Process Section', {
           'fields': ('process_header_text', 'process_description', 'process_background_image')
        }),
        ('Video Section', {
            'fields': ('video_header_text', 'video_link', 'video_background_colour', 'video_shadow_colour')
        }),
        ('Testimonials', {
            'fields': ('testimonials', 'testimonials_background_image')
        }),
        ('Get Started Section', {
           'fields': ('get_started_header_text', 'get_started_description', 'get_started_background_colour')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class RequestDemoPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'header_text', 'info_text', 'is_active']
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('header_text', 'info_text', 'background_image')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class ForgotPasswordPageAdmin(admin.ModelAdmin):
    list_display = ('id', 'header_text', 'info_text', 'is_active')
    list_editable = ['is_active']
    fieldsets = (
        ('Main Section', {
            'fields': ('header_text', 'info_text', 'background_image')
        }),
        ('Activate Changes', {
            'fields': ('is_active', )
        })
    )


class TermsPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'is_active']
    list_editable = ['is_active']
    filter_horizontal = ['tabs']


admin.site.register(HowItWorksStep)
admin.site.register(Tab)
admin.site.register(PressRelease)
admin.site.register(PressReleaseLogo)
admin.site.register(ForgotPasswordPage, ForgotPasswordPageAdmin)
admin.site.register(TermsPage, TermsPageAdmin)
admin.site.register(RequestDemoPage, RequestDemoPageAdmin)
admin.site.register(HotelZonePage, HotelZonePageAdmin)
admin.site.register(Testimonial, TestimonialAdmin)
admin.site.register(HomePage, HomePageAdmin)
admin.site.register(HowItWorksPage, HowItWorksPageAdmin)
admin.site.register(ContactUsPage, ContactUsPageAdmin)
admin.site.register(PressReleasePage, PressReleasePageAdmin)
admin.site.register(AboutUsPage, AboutUsPageAdmin)
admin.site.register(FaqPage, FaqPageAdmin)
admin.site.register(SignUpPage, SignUpPageAdmin)
admin.site.register(LogInPage, LogInPageAdmin)
admin.site.register(Header, HeaderAdmin)
admin.site.register(Footer, FooterAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(ReportLostItemPage, ReportLostItemPageAdmin)
