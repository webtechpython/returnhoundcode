# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models
import s3direct.fields
import adminsortable.fields
import django.utils.timezone
import return_hound.apps.cms.models
import colorful.fields
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0031_merge'),
        ('cms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForgotPasswordPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('background_image', s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('info_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Forgot password page',
            },
            bases=(return_hound.apps.cms.models.SingleInstanceMixin, models.Model),
        ),
        migrations.CreateModel(
            name='PressReleaseLogo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('image', s3direct.fields.S3DirectField(help_text='Logo Dimensions 476x48, 242x194', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('number', models.PositiveSmallIntegerField(default=0, editable=False, db_index=True)),
                ('header_text', models.CharField(max_length=225)),
                ('content', tinymce.models.HTMLField()),
            ],
            options={
                'ordering': ['number'],
            },
        ),
        migrations.CreateModel(
            name='TermsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('background_colour', colorful.fields.RGBColorField(default='#ffffff', blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('tabs', models.ManyToManyField(to='cms.Tab', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Terms and conditions page',
            },
            bases=(return_hound.apps.cms.models.SingleInstanceMixin, models.Model),
        ),
        migrations.AlterModelOptions(
            name='aboutuspage',
            options={'verbose_name_plural': 'About us page'},
        ),
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name_plural': 'Address'},
        ),
        migrations.AlterModelOptions(
            name='contactuspage',
            options={'verbose_name_plural': 'Contact us page'},
        ),
        migrations.AlterModelOptions(
            name='faq',
            options={'ordering': ['number']},
        ),
        migrations.AlterModelOptions(
            name='faqpage',
            options={'verbose_name_plural': 'Faq page'},
        ),
        migrations.AlterModelOptions(
            name='footer',
            options={'verbose_name_plural': 'Footer'},
        ),
        migrations.AlterModelOptions(
            name='header',
            options={'verbose_name_plural': 'Header'},
        ),
        migrations.AlterModelOptions(
            name='homepage',
            options={'verbose_name_plural': 'Home page'},
        ),
        migrations.AlterModelOptions(
            name='homepagehowitworksstep',
            options={'ordering': ['number'], 'verbose_name_plural': 'How it works steps'},
        ),
        migrations.AlterModelOptions(
            name='hotelzonepage',
            options={'verbose_name_plural': 'Hotel zone page'},
        ),
        migrations.AlterModelOptions(
            name='howitworkspage',
            options={'verbose_name_plural': 'How it works page'},
        ),
        migrations.AlterModelOptions(
            name='howitworkssection',
            options={'ordering': ['number']},
        ),
        migrations.AlterModelOptions(
            name='howitworksstep',
            options={'ordering': ['number']},
        ),
        migrations.AlterModelOptions(
            name='loginpage',
            options={'verbose_name_plural': 'Log in page'},
        ),
        migrations.AlterModelOptions(
            name='pressrelease',
            options={'ordering': ['number']},
        ),
        migrations.AlterModelOptions(
            name='pressreleasepage',
            options={'verbose_name_plural': 'Press release page'},
        ),
        migrations.AlterModelOptions(
            name='reportlostitempage',
            options={'verbose_name_plural': 'Report lost item page'},
        ),
        migrations.AlterModelOptions(
            name='requestdemopage',
            options={'verbose_name_plural': 'Request demo page'},
        ),
        migrations.AlterModelOptions(
            name='signuppage',
            options={'verbose_name_plural': 'Sign up page'},
        ),
        migrations.AlterModelOptions(
            name='testimonial',
            options={'ordering': ['number']},
        ),
        migrations.RemoveField(
            model_name='faqpage',
            name='faqs',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='how_it_works_steps',
        ),
        migrations.RemoveField(
            model_name='howitworkspage',
            name='sections',
        ),
        migrations.AddField(
            model_name='aboutuspage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='aboutuspage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='faq',
            name='faq_page',
            field=adminsortable.fields.SortableForeignKey(null=True, blank=True, to='cms.FaqPage'),
        ),
        migrations.AddField(
            model_name='faqpage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='footer',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='header',
            name='header_logo',
            field=s3direct.fields.S3DirectField(help_text='Logo Dimensions 333x128', blank=True),
        ),
        migrations.AddField(
            model_name='header',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='header',
            name='sticky_header_logo',
            field=s3direct.fields.S3DirectField(help_text='Logo Dimensions 350x45', blank=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='homepage',
            name='testimonials_background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='homepagehowitworksstep',
            name='home_page',
            field=adminsortable.fields.SortableForeignKey(null=True, blank=True, to='cms.HomePage'),
        ),
        migrations.AddField(
            model_name='homepagehowitworksstep',
            name='image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 374x520, 530x512, 332x528', blank=True),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='lost_found_no_cost_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1894x334', blank=True),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='process_background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='testimonials_background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='howitworkspage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='howitworkssection',
            name='how_it_works_page',
            field=adminsortable.fields.SortableForeignKey(null=True, blank=True, to='cms.HowItWorksPage'),
        ),
        migrations.AddField(
            model_name='howitworkssection',
            name='image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 530x330, 332x392, 374x356', blank=True),
        ),
        migrations.AddField(
            model_name='loginpage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='loginpage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='pressrelease',
            name='logo',
            field=s3direct.fields.S3DirectField(help_text='Logo Dimensions 476x48, 242x194', blank=True),
        ),
        migrations.AddField(
            model_name='pressreleasepage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='reportlostitempage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='reportlostitempage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='requestdemopage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='requestdemopage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='signuppage',
            name='background_image',
            field=s3direct.fields.S3DirectField(help_text='Image Dimensions 1920x1080', blank=True),
        ),
        migrations.AddField(
            model_name='signuppage',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='aboutuspage',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.AlterField(
            model_name='contactuspage',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.AlterField(
            model_name='faq',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='faqpage',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.AlterField(
            model_name='footer',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#363636', blank=True),
        ),
        migrations.RemoveField(
            model_name='footer',
            name='social_links',
        ),
        migrations.AddField(
            model_name='footer',
            name='social_links',
            field=models.ManyToManyField(to='return_hound.SocialLink', blank=True),
        ),
        migrations.AlterField(
            model_name='header',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#027fba', blank=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='how_it_works_background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='press_background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='testimonials',
        ),
        migrations.AddField(
            model_name='homepage',
            name='testimonials',
            field=models.ManyToManyField(to='cms.Testimonial', blank=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='video_background_colour',
            field=colorful.fields.RGBColorField(default='#027fba', blank=True),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='video_shadow_colour',
            field=colorful.fields.RGBColorField(default='#015c87', blank=True),
        ),
        migrations.AlterField(
            model_name='homepagehowitworksstep',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='hotelzonepage',
            name='get_started_background_colour',
            field=colorful.fields.RGBColorField(default='#027fba', blank=True),
        ),
        migrations.AlterField(
            model_name='hotelzonepage',
            name='lost_found_background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.RemoveField(
            model_name='hotelzonepage',
            name='testimonials',
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='testimonials',
            field=models.ManyToManyField(to='cms.Testimonial', blank=True),
        ),
        migrations.AlterField(
            model_name='hotelzonepage',
            name='video_background_colour',
            field=colorful.fields.RGBColorField(default='#027fba', blank=True),
        ),
        migrations.AlterField(
            model_name='hotelzonepage',
            name='video_shadow_colour',
            field=colorful.fields.RGBColorField(default='#015c87', blank=True),
        ),
        migrations.AlterField(
            model_name='howitworkspage',
            name='footer_background_color',
            field=colorful.fields.RGBColorField(default='#cbcbcb', blank=True),
        ),
        migrations.AlterField(
            model_name='howitworkssection',
            name='background_color',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.AlterField(
            model_name='howitworkssection',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.RemoveField(
            model_name='howitworkssection',
            name='steps',
        ),
        migrations.AddField(
            model_name='howitworkssection',
            name='steps',
            field=models.ManyToManyField(to='cms.HowItWorksStep'),
        ),
        migrations.AlterField(
            model_name='howitworksstep',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='pressrelease',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='pressreleasepage',
            name='background_colour',
            field=colorful.fields.RGBColorField(default='#ffffff', blank=True),
        ),
        migrations.RemoveField(
            model_name='pressreleasepage',
            name='press_releases',
        ),
        migrations.AddField(
            model_name='pressreleasepage',
            name='press_releases',
            field=models.ManyToManyField(to='cms.PressRelease', blank=True),
        ),
        migrations.AlterField(
            model_name='testimonial',
            name='number',
            field=models.PositiveSmallIntegerField(default=0, editable=False, db_index=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='press_releases',
            field=models.ManyToManyField(to='cms.PressReleaseLogo', blank=True),
        ),
    ]
