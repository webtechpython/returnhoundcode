
def find_repr(obj):
    from return_hound.models import LostItem, FoundItem, Hotel, User

    if isinstance(obj, Hotel):
        return '<Hotel:{}>'.format(obj.name)

    if isinstance(obj, User):
        return '<User:{}>'.format(obj.email)

    if isinstance(obj, LostItem):
        return '<Lost: {} ({})>'.format(obj.item_type.name, obj.created_by.email)

    if isinstance(obj, FoundItem):
        return '<Found: {} ({})>'.format(obj.item_type.name, obj.created_by.email)

    return repr(obj)


def action_description(action):
    return ' '.join([
        find_repr(action.actor),
        action.verb,
        find_repr(action.target)
    ])
