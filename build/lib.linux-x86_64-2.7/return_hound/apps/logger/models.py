import logging
from django.db import models
from jsonfield_compat import JSONField
from model_utils.fields import AutoCreatedField


class SystemLog(models.Model):
    LEVELS = (
        (logging.DEBUG, 'DEBUG'),
        (logging.INFO, 'INFO'),
        (logging.WARN, 'WARN'),
        (logging.ERROR, 'ERROR'),
        (logging.FATAL, 'FATAL'),
    )
    level = models.IntegerField(choices=LEVELS)
    logger = models.CharField(max_length=200, null=True)
    timestamp = AutoCreatedField()
    message = models.TextField()
    tag = models.CharField(max_length=50, blank=True, null=True)
    data = JSONField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
