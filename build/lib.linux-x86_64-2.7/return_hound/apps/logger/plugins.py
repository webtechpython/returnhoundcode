from lxml import etree
from logging import getLogger
from suds.plugin import MessagePlugin


class TaggedLogPlugin(MessagePlugin):
    def __init__(self, logger, tag=None):
        self.tag = tag or ''
        if isinstance(logger, str):
            self.log = getLogger(logger)
        else:
            self.log = logger

    def sending(self, context):
        envelope = context.envelope
        xml = etree.tostring(etree.XML(envelope), pretty_print=True)
        self.log.debug('SOAP REQUEST', extra={'text': xml})

    def received(self, context):
        reply = context.reply
        xml = etree.tostring(etree.XML(reply), pretty_print=True)
        self.log.debug('SOAP RESPONSE', extra={'text': xml})
