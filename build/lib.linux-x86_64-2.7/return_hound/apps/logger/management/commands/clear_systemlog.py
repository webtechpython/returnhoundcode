from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils.timezone import now
from return_hound.apps.logger.models import SystemLog


class Command(BaseCommand):
    help = 'Delete database logs (logger.SystemLog) [default: older than 90 days]'

    def add_arguments(self, parser):
        parser.add_argument(
            '--days',
            type=int,
            default=90,
            help='Delete logs older than X days.')

        parser.add_argument(
            '--all',
            action='store_true',
            default=False,
            help='Delete all logs.')

        parser.add_argument(
            '--tag',
            type=str,
            default='',
            help='Delete logs by tag.')

        parser.add_argument(
            '--message',
            type=str,
            default='',
            help='Delete logs by message.')

    def handle(self, *args, **options):
        if options['all']:
            SystemLog.objects.all().delete()
        elif options['tag']:
            SystemLog.objects.filter(tag=options['tag']).delete()
        elif options['message']:
            SystemLog.objects.filter(message__exact=options['message']).delete()
        else:
            cutoff = now() - timedelta(days=options['days'])
            SystemLog.objects.filter(timestamp__lt=cutoff).delete()
