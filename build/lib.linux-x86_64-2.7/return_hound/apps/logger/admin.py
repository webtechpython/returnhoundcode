from django.contrib import admin
from return_hound.admin_tools.readonly import ReadOnlyAdmin

from .models import SystemLog


@admin.register(SystemLog)
class SystemLogAdmin(ReadOnlyAdmin):
    search_fields = ('message', 'text')
    list_display = ('timestamp', 'level', 'message')
    list_filter = ('timestamp', 'level', 'logger')
    fields = ('level', 'message', 'timestamp', 'data', 'text')
    change_form_template = 'logger/log.html'
