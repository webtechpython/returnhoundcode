import logging
import traceback


class DBFormatter(logging.Formatter):

    DEFAULT_PROPERTIES = logging.LogRecord('', '', '', '', '', '', '', '').__dict__.keys()

    def format(self, record):
        """Formats LogRecord into python dictionary."""

        if record.exc_info:
            exc_name = record.exc_info[0].__name__
            msg = record.getMessage()
            msg = msg if exc_name in msg else '{}: {}'.format(exc_name, msg)
            text = traceback.format_exc()
        else:
            msg = record.getMessage()
            text = None

        # Standard document
        document = {
            'level': record.levelno,
            'logger': record.name,
            'message': msg,
            'data': getattr(record, 'data', None),
            'text': getattr(record, 'text', text),
        }
        return document


class DatabaseLogHandler(logging.Handler):
    def __init__(self, using=None):
        """
        :param using: The database connection to use. (key inside settings.DATABASES)
        :type using: str
        """
        self.using = using or 'default'
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        self.formatter = DBFormatter()

    def emit(self, record):
        # noinspection PyBroadException
        try:
            # NB: avoid circular import
            from .models import SystemLog
            log = SystemLog(**self.format(record))
            log.save(using=self.using)
        except:  # Gotta catch'em ALL
            self.handleError(record)
