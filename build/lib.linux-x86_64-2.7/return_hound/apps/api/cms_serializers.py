from rest_framework import serializers
from return_hound.apps.cms import models

from .serializers import SocialLinkSerializer


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        exclude = ('created', 'modified')


class FooterSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    social_links = SocialLinkSerializer(many=True)

    class Meta:
        model = models.Footer
        exclude = ('created', 'modified')


class HeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Header
        exclude = ('created', 'modified')


class TestimonialSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Testimonial
        exclude = ('created', 'modified')


class HowItWorksStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.HomePageHowItWorksStep
        exclude = ('created', 'modified', 'home_page')


class HowItWorksPageStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.HowItWorksStep
        exclude = ('created', 'modified')


class HowItWorksSectionSerializer(serializers.ModelSerializer):
    steps = HowItWorksPageStepSerializer(many=True)

    class Meta:
        model = models.HowItWorksSection
        exclude = ('created', 'modified', 'how_it_works_page')


class AboutUsPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AboutUsPage
        exclude = ('created', 'modified')


class ContactUsPageSerializer(serializers.ModelSerializer):
    address = AddressSerializer()

    class Meta:
        model = models.ContactUsPage
        exclude = ('created', 'modified')


class FaqSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Faq
        exclude = ('created', 'modified', 'faq_page')


class FaqPageSerializer(serializers.ModelSerializer):
    faq = FaqSerializer(source='faq_set.all', many=True)

    class Meta:
        model = models.FaqPage
        exclude = ('created', 'modified')


class PressReleaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PressRelease
        exclude = ('created', 'modified')


class PressReleaseLogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PressReleaseLogo
        exclude = ()


class HomePageSerializer(serializers.ModelSerializer):
    testimonials = TestimonialSerializer(many=True)
    press_releases = PressReleaseLogoSerializer(many=True)
    how_it_works_steps = HowItWorksStepSerializer(source='homepagehowitworksstep_set.all', many=True)

    class Meta:
        model = models.HomePage
        exclude = ('created', 'modified')


class HotelZonePageSerializer(serializers.ModelSerializer):
    testimonials = TestimonialSerializer(many=True)

    class Meta:
        model = models.HotelZonePage
        exclude = ('created', 'modified')


class PressReleasePageSerializer(serializers.ModelSerializer):
    press_releases = PressReleaseSerializer(many=True)

    class Meta:
        model = models.PressReleasePage
        exclude = ('created', 'modified')


class LogInPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LogInPage
        exclude = ('created', 'modified')


class HowItWorksPageSerializer(serializers.ModelSerializer):
    how_it_works = HowItWorksSectionSerializer(source='howitworkssection_set.all', many=True)

    class Meta:
        model = models.HowItWorksPage
        exclude = ('created', 'modified')


class ReportLostItemPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ReportLostItemPage
        exclude = ('created', 'modified')


class RequestDemoPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequestDemoPage
        exclude = ('created', 'modified')


class ForgotPasswordPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ForgotPasswordPage
        exclude = ('created', 'modified')


class SignUpPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SignUpPage
        exclude = ('created', 'modified')


class TabSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Tab
        exclude = ()


class TermsSerializer(serializers.ModelSerializer):
    tabs = TabSerializer(many=True)

    class Meta:
        model = models.TermsPage
