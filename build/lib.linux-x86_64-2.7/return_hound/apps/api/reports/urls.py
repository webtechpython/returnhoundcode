from bo_drf.routers import FlexiRouter
from . import views

router = FlexiRouter(namespace='reports', root_view_api_name='Reports')

router.register(r'hotels-report', views.HotelsReportAPI)
router.register(r'overview-report', views.OverviewReportAPI)
router.register(r'found-items', views.FoundItems, base_name='found_items')
router.register(r'matched-items', views.MatchedItems, base_name='matched_items')
router.register(r'history-items', views.HistoryItems, base_name='history_items')
router.register(r'payment-items', views.PaymentReportItems, base_name='payment_items')
router.register(r'collection-ready-items', views.CollectionReadyItem, base_name='collection_ready_items')
router.register(r'hotels', views.Hotels)
router.register(r'chains', views.HotelChains)
router.register(r'brands', views.HotelBrands)
