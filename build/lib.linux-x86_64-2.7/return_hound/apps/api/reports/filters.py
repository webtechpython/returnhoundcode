import datetime
import django_filters

from return_hound import enums
from return_hound.models import FoundItem, MatchedItem, Hotel, ShippingItem, Transaction, LostItem


class EndDateFilter(django_filters.DateFilter):
    """
    Include whole day in filter
    """
    def filter(self, qs, end_date):
        if end_date:
            end = datetime.datetime.combine(end_date, datetime.time.max)
            return qs.filter(**{'{}__{}'.format(self.name, self.lookup_type): end})
        return qs


class LostItemFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='hotel', queryset=Hotel.objects.all())

    class Meta:
        model = LostItem
        fields = ('start_date', 'end_date', 'hotel')


class FoundItemFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='hotel', queryset=Hotel.objects.all())

    class Meta:
        model = FoundItem
        fields = ('start_date', 'end_date', 'hotel')


class MatchedItemFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='found_item__hotel', queryset=Hotel.objects.all())

    class Meta:
        model = MatchedItem
        fields = ('start_date', 'end_date', 'hotel')


class ShippingItemFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='details__shipping_date', lookup_type='gte')
    end_date = EndDateFilter(name='details__shipping_date', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='parcel__found_item__hotel', queryset=Hotel.objects.all())

    class Meta:
        model = ShippingItem
        fields = ('start_date', 'end_date', 'hotel')


class CollectedDateFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='details__collected_date', lookup_type='gte')
    end_date = EndDateFilter(name='details__collected_date', lookup_type='lte')

    class Meta:
        model = ShippingItem
        fields = ('start_date', 'end_date')


class HistoryFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='parcel__found_item__hotel', queryset=Hotel.objects.all())
    status = django_filters.MethodFilter()

    def filter_status(self, queryset, value):
        if value:
            return queryset.filter(parcel__status=enums.FoundItemStatus(int(value)).value)
        return queryset

    class Meta:
        model = ShippingItem
        fields = ('start_date', 'end_date', 'hotel', 'status')


class PaymentReportFilterSet(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')

    class Meta:
        model = Transaction
        fields = ('start_date', 'end_date')


class CollectionReadyReportFilter(django_filters.FilterSet):
    start_date = django_filters.DateFilter(name='created', lookup_type='gte')
    end_date = EndDateFilter(name='created', lookup_type='lte')
    hotel = django_filters.ModelChoiceFilter(name='parcel__found_item__hotel', queryset=Hotel.objects.all())

    class Meta:
        model = ShippingItem
        fields = ('start_date', 'end_date', 'hotel')
