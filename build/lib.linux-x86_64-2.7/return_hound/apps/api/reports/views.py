import csv
from datetime import timedelta, datetime, date
from decimal import Decimal
from io import StringIO

from django.db.models import Sum, Q, Count
from django.utils.timezone import now
from enumfields import Enum
from rest_framework.authentication import SessionAuthentication
from rest_framework.filters import DjangoFilterBackend
from rest_framework.mixins import ListModelMixin
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAdminUser
from rest_framework.renderers import BaseRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework import serializers

from bo_drf.auth.permissions import HasPermissions
from bo_drf.serializers import serializer

from return_hound.apps.api.reports.filters import FoundItemFilterSet, MatchedItemFilterSet, HistoryFilterSet, \
    ShippingItemFilterSet, PaymentReportFilterSet, CollectionReadyReportFilter, LostItemFilterSet
from return_hound.apps.api.reports.serializers import HistoryItemsSerializer, CollectionReadySerializer, \
    MatchedItemSerializer, FoundItemSerializer
from return_hound.models import FoundItem, MatchedItem, ShippingItem, Hotel, LostItem, Transaction, Category, \
    HotelChain, HotelBrand
from return_hound.enums import FoundItemStatus, ItemStatus, LostItemStatus, MatchedItemStatus
from return_hound.apps.api.views import ExportCSVMixin


class StandardPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ReportsViewSet(GenericViewSet):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAdminUser, HasPermissions]
    permissions_required = ['return_hound.reports']


class Hotels(ListModelMixin, ReportsViewSet):
    serializer_class = serializer(model=Hotel)
    queryset = Hotel.objects.all()
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('brand', 'brand__chain',)


class HotelChains(ListModelMixin, ReportsViewSet):
    serializer_class = serializer('id', 'name', model=HotelChain)
    queryset = HotelChain.objects.all()
    pagination_class = None


class HotelBrands(ListModelMixin, ReportsViewSet):
    serializer_class = serializer('id', 'chain', 'name', model=HotelBrand)
    queryset = HotelBrand.objects.all()
    pagination_class = None
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('chain',)


class HotelReportSerializer(serializers.ModelSerializer):
    city = serializers.ReadOnlyField(source='address.city')
    country = serializers.ReadOnlyField(source='address.aramex_country.code2')
    brand = serializers.ReadOnlyField(source='brand.name')
    chain = serializers.ReadOnlyField(source='chain.name')
    lost_items_count = serializers.SerializerMethodField()
    found_items_count = serializers.SerializerMethodField()
    found_items_known_count = serializers.SerializerMethodField()
    found_items_not_known_count = serializers.SerializerMethodField()
    shipped_count = serializers.SerializerMethodField()
    collected_count = serializers.SerializerMethodField()
    returned_count = serializers.SerializerMethodField()
    returned_rate = serializers.SerializerMethodField()
    shipped_rate = serializers.SerializerMethodField()
    found_per_room = serializers.SerializerMethodField()
    lost_per_room = serializers.SerializerMethodField()
    returned_per_room = serializers.SerializerMethodField()

    def _items_filter(self, qs):
        params = self.context['request'].GET
        cat = params.get('cat')
        subcat = params.get('subcat')
        end_date = params.get('endDate')
        start_date = params.get('startDate')
        if cat:
            qs = qs.filter(category=cat)
        if subcat:
            qs = qs.filter(item_type=subcat)
        if end_date:
            qs = qs.filter(created__lte=end_date)
        if start_date:
            qs = qs.filter(created__gte=start_date)
        return qs

    def _shipment_filter(self, qs):
        params = self.context['request'].GET
        end_date = params.get('endDate')
        start_date = params.get('startDate')
        if end_date:
            qs = qs.filter(created__lte=end_date)
        if start_date:
            qs = qs.filter(created__gte=start_date)
        return qs

    def get_lost_items_count(self, obj):
        known_owner = Q(matcheditem__isnull=False) & ~Q(matcheditem__found_item__owner_email='')
        qs = LostItem.objects.filter(hotel=obj).exclude(known_owner)
        return self._items_filter(qs).count()

    def get_found_items_count(self, obj):
        qs = FoundItem.objects.filter(hotel=obj)
        return self._items_filter(qs).count()

    def get_found_items_known_count(self, obj):
        known_owner = ~Q(owner_email='')
        qs = FoundItem.objects.filter(known_owner, hotel=obj)
        return self._items_filter(qs).count()

    def get_found_items_not_known_count(self, obj):
        return self.get_found_items_count(obj) - self.get_found_items_known_count(obj)

    def _returned(self, obj):
        """Completed/Returned Shipments or Collected"""
        qs = ShippingItem.objects.filter(parcel__found_item__hotel=obj, details__is_complete=True)
        return self._shipment_filter(qs)

    def _div(self, top, bottom):
        if bottom:
            rate = top / Decimal(str(bottom))
            return '%.2f' % rate
        else:
            return '0.00'

    def get_returned_count(self, obj):
        return self._returned(obj).count()

    def get_shipped_count(self, obj):
        return self._returned(obj).filter(details__is_collection=False).count()

    def get_collected_count(self, obj):
        return self._returned(obj).filter(details__is_collection=True).count()

    def get_returned_rate(self, obj):
        return self._div(self.get_returned_count(obj), self.get_found_items_count(obj))

    def get_shipped_rate(self, obj):
        return self._div(self.get_shipped_count(obj), self.get_found_items_count(obj))

    def get_found_per_room(self, obj):
        return self._div(self.get_found_items_count(obj), obj.number_of_rooms)

    def get_lost_per_room(self, obj):
        return self._div(self.get_lost_items_count(obj), obj.number_of_rooms)

    def get_returned_per_room(self, obj):
        return self._div(self.get_returned_count(obj), obj.number_of_rooms)

    class Meta:
        model = Hotel
        fields = (
            'id', 'name', 'number_of_rooms', 'city', 'country', 'chain', 'brand',
            'found_items_count', 'found_items_known_count', 'found_items_not_known_count',
            'lost_items_count', 'shipped_count', 'collected_count', 'returned_count',
            'returned_rate', 'shipped_rate',
            'found_per_room', 'lost_per_room', 'returned_per_room',
        )


class HotelsReportAPI(ExportCSVMixin, ListModelMixin, ReportsViewSet):
    pagination_class = None
    serializer_class = HotelReportSerializer

    def get_queryset(self):
        return Hotel.objects.all()


class ReportDateRanges(object):
    def __init__(self, dt):
        dt_end = dt + timedelta(days=1)
        year = dt.year
        month = dt.month
        day = dt.day
        year_prev = year - 1
        dt_prev = date(year_prev, month, day)
        dt_prev_end = dt_prev + timedelta(days=1)
        # expose date ranges:
        self.DATE = dt
        # Current year
        self.YTD = [date(year, 1, 1), dt_end]
        self.MTD = [date(year, month, 1), dt_end]
        self.DAY = [dt, dt_end]
        # Previous year
        self.PREV_YTD = [date(year_prev, 1, 1), dt_prev_end]
        self.PREV_MTD = [date(year_prev, month, 1), dt_prev_end]
        self.PREV_DAY = [dt_prev, dt_prev_end]

    def range_str(self, date_range):
        return '-'.join([d.strftime('%Y%m%d') for d in date_range])

    def __str__(self):
        dates = [self.YTD, self.MTD, self.DAY, self.DATE, self.PREV_DAY, self.PREV_MTD, self.PREV_YTD]
        dates_str = [self.range_str(dt) if isinstance(dt, list) else dt.strftime('%Y%m%d') for dt in dates]
        return 'YTD:{} | MTD:{} | DAY:{} | {} | PREV_DAY:{} | PREV_MTD:{} | PREV_YTD:{}'.format(*dates_str)

    def __repr__(self):
        return '<ReportDateRanges:: %s>' % self


class ProActivityLevels(Enum):
    NO_ROOM = 1
    NO_MAIL = 2
    MAIL_NOT_SENT = 3
    MAIL_SENT = 4

    class Labels:
        NO_ROOM = 'no room number'
        NO_MAIL = 'room number, no mail address'


class OverviewCSVRenderer(BaseRenderer):
    format = 'csv'
    media_type = 'text/csv'
    render_style = 'binary'

    def name(self, obj):
        return obj.name if obj else ''

    def render(self, data, media_type=None, renderer_context=None):
        if data is None:
            return ''
        view = renderer_context['view']  # type: OverviewReportAPI
        report_date = view.report_date
        cols = ('YTD', 'MTD', 'DAY', 'description', 'PREV_DAY', 'PREV_MTD', 'PREV_YTD')
        buffer = StringIO()
        csv_writer = csv.writer(buffer)
        csv_writer.writerow(('', '', 'Chain', self.name(view.chain), '', 'Year', report_date.year))
        csv_writer.writerow(('', '', 'Brand', self.name(view.brand), '', 'Month', report_date.month))
        csv_writer.writerow(('', '', 'Hotel', self.name(view.hotel), '', 'Day', report_date.day))
        csv_writer.writerow(cols)
        for section in data:
            if not section.get('noHead'):
                csv_writer.writerow(('', '', '', section['name'], '', '', ''))
            for row in section['rows']:
                csv_writer.writerow([row.get('name' if c == 'description' else c) for c in cols])
        return buffer.getvalue()


# noinspection PyAttributeOutsideInit
class OverviewReportAPI(ListModelMixin, ReportsViewSet):
    renderer_classes = (JSONRenderer, OverviewCSVRenderer)
    pagination_class = None

    def count(self, qs):
        return qs.aggregate(cnt=Count('*'))['cnt']

    def lost(self, category=None, date_range=None):
        known_owner = Q(matcheditem__isnull=False) & ~Q(matcheditem__found_item__owner_email='')
        qs = LostItem.objects.exclude(known_owner)
        filters = {}
        if category:
            filters['category'] = category
        if date_range:
            filters['created__range'] = date_range
        if self.hotel_filters:
            filters['hotel__in'] = self.hotels
        return qs.filter(**filters)

    def found(self, category=None, date_range=None):
        filters = {}
        if category:
            filters['category'] = category
        if date_range:
            filters['created__range'] = date_range
        if self.hotel_filters:
            filters['hotel__in'] = self.hotels
        return FoundItem.objects.filter(**filters)

    def pro_activity(self, category, date_range):
        qs = self.found(date_range=date_range)
        no_room = Q(room_number='')
        no_mail = Q(owner_email='')
        # Found with no room number
        if category == ProActivityLevels.NO_ROOM:
            return qs.filter(no_room)
        # Found with room number but no email
        if category == ProActivityLevels.NO_MAIL:
            return qs.filter(~no_room & no_mail)
        # Found with room number and email AND guest was not informed
        if category == ProActivityLevels.MAIL_NOT_SENT:
            return qs.filter(~no_room & ~no_mail, owner_send_email=False)
        # Found with room number and email AND guest informed
        if category == ProActivityLevels.MAIL_SENT:
            return qs.filter(~no_room & ~no_mail, owner_send_email=True)

    def matched(self, category=None, date_range=None):
        filters = {}
        if category:
            filters['lost_item__category'] = category
        if date_range:
            filters['lost_item__created__range'] = date_range
        if self.hotel_filters:
            filters['lost_item__hotel__in'] = self.hotels
        return MatchedItem.objects.filter(**filters)

    def shipped(self, category=None, date_range=None):
        qs = ShippingItem.objects.filter(details__is_complete=True, details__is_collection=False)
        filters = {}
        if category:
            filters['parcel__lost_item__category'] = category
        if date_range:
            filters['created__range'] = date_range
        if self.hotel_filters:
            filters['parcel__lost_item__hotel__in'] = self.hotels
        return qs.filter(**filters)

    def archive(self, status, date_range):
        if status == ItemStatus.NOT_FOUND:
            not_found = (
                Q(status=LostItemStatus.LOST_NOT_FOUND.value) |
                Q(matcheditem__isnull=True)
            )
            return self.lost(date_range=date_range).filter(not_found)

        if status == ItemStatus.UNCLAIMED:
            unclaimed = (
                Q(status=MatchedItem.STATUS.pending) |
                Q(assigned_status=MatchedItemStatus.UNCLAIMED.value)
            )
            return self.matched(date_range=date_range).filter(unclaimed)

        if status == ItemStatus.COLLECTED:
            collected = (
                Q(found_item__status=FoundItemStatus.COLLECTED.value) |
                Q(assigned_status=MatchedItemStatus.COLLECTED.value)
            )
            return self.matched(date_range=date_range).filter(collected)

        if status == ItemStatus.SHIPPED:
            shipped = (
                Q(found_item__status=FoundItemStatus.SHIPPED.value) |
                Q(assigned_status=MatchedItemStatus.SHIPPED.value)
            )
            return self.matched(date_range=date_range).filter(shipped)

        if status == ItemStatus.GUEST_REJECTED:
            rejected = (
                Q(status=MatchedItem.STATUS.declined) |
                Q(assigned_status=MatchedItemStatus.GUEST_REJECTED.value)
            )
            return self.matched(date_range=date_range).filter(rejected)

        if status == ItemStatus.REGULATORY_HANDOVER:
            return self.matched(date_range=date_range).filter(assigned_status=MatchedItemStatus.REGULATORY_HANDOVER.value)

        if status == ItemStatus.OVERDUE:
            return self.found(date_range=date_range).filter(status=FoundItemStatus.OVERDUE.value)

    def archived_count(self, date_range):
        count = 0
        for status in ItemStatus:
            count += self.count(self.archive(status, date_range))
        return count

    turnover_trn_fields = {
        'shipping': 'charged_amount',
        'discretionary': 'service_charge',
    }

    def turnover(self, category, date_range=None):
        filters = {'is_paid': True}
        if date_range:
            filters['created__range'] = date_range
        if self.hotel_filters:
            filters['shipping_details__shippingitem__parcel__lost_item__hotel__in'] = self.hotels
        qs = Transaction.objects.filter(**filters)
        return qs.aggregate(sum=Sum(self.turnover_trn_fields[category]))['sum'] or 0

    def turnover_total(self, date_range):
        return sum((self.turnover(f, date_range) for f in self.turnover_trn_fields.keys()))

    def hotels_in_report_count(self):
        return self.hotels.aggregate(cnt=Count('*'))['cnt']

    def rooms_in_report_count(self):
        return self.hotels.aggregate(sum=Sum('number_of_rooms'))['sum'] or 0

    def _div(self, top, bottom):
        if bottom:
            rate = top / Decimal(str(bottom))
            return '%.2f' % rate
        else:
            return '0.00'

    def count_per_room(self, qs):
        return self._div(self.count(qs), self.rooms_in_report_count)

    def get_queryset(self):
        return []  # API router requires this

    def get_report_date(self, request):
        try:
            report_date_str = request.GET.get('date')
            return datetime.strptime(report_date_str, '%Y-%m-%d').date()
        except (TypeError, ValueError):
            return now().date()

    def get_hotels(self, request):
        chain = request.GET.get('chain')
        brand = request.GET.get('brand')
        hotel = request.GET.get('hotel')

        # Set Chain,Brand,Hotel context for use in CSV
        self.chain = HotelChain.objects.get(pk=chain) if chain else None
        self.brand = HotelBrand.objects.get(pk=brand) if brand else None
        self.hotel = Hotel.objects.get(pk=hotel) if hotel else None

        filters = {}
        if hotel:
            filters['pk'] = hotel
        else:
            if brand:
                filters['brand'] = brand
            else:
                if chain:
                    filters['brand__chain'] = chain

        self.hotel_filters = bool(filters)
        return Hotel.objects.filter(**filters)

    def list(self, request, *args, **kwargs):
        self.hotels = self.get_hotels(request)
        self.hotels_in_report_count = self.hotels_in_report_count()
        self.rooms_in_report_count = self.rooms_in_report_count()
        self.report_date = self.get_report_date(request)
        report_date_prev = date(self.report_date.year - 1, self.report_date.month, self.report_date.day)
        ranges = ReportDateRanges(self.report_date)
        categories = Category.objects.all()
        sections = [
            {
                'name': '',
                'noHead': True,
                'rows': [
                    {
                        'name': '# days',
                        'YTD': (self.report_date - date(self.report_date.year, 1, 1)).days + 1,
                        'MTD': self.report_date.day,
                        'DAY': 1,
                        'PREV_YTD': (report_date_prev - date(report_date_prev.year, 1, 1)).days + 1,
                        'PREV_MTD': report_date_prev.day,
                        'PREV_DAY': 1,
                    },
                    {
                        'name': '# hotels',
                        'DAY': self.hotels_in_report_count,
                    },
                    {
                        'name': '# rooms',
                        'DAY': self.rooms_in_report_count,
                    }
                ]
            },
            {
                'name': 'Turnover',
                'rows': [
                    {
                        'name': cat.title(),
                        'YTD': self.turnover(date_range=ranges.YTD, category=cat),
                        'MTD': self.turnover(date_range=ranges.MTD, category=cat),
                        'DAY': self.turnover(date_range=ranges.DAY, category=cat),
                        'PREV_YTD': self.turnover(date_range=ranges.PREV_YTD, category=cat),
                        'PREV_MTD': self.turnover(date_range=ranges.PREV_MTD, category=cat),
                        'PREV_DAY': self.turnover(date_range=ranges.PREV_DAY, category=cat)
                    }
                    for cat in self.turnover_trn_fields.keys()
                ]
            },
            {
                'name': 'Lost',
                'rows': [
                    {
                        'name': cat.name,
                        'YTD': self.count(self.lost(date_range=ranges.YTD, category=cat)),
                        'MTD': self.count(self.lost(date_range=ranges.MTD, category=cat)),
                        'DAY': self.count(self.lost(date_range=ranges.DAY, category=cat)),
                        'PREV_YTD': self.count(self.lost(date_range=ranges.PREV_YTD, category=cat)),
                        'PREV_MTD': self.count(self.lost(date_range=ranges.PREV_MTD, category=cat)),
                        'PREV_DAY': self.count(self.lost(date_range=ranges.PREV_DAY, category=cat))
                    }
                    for cat in categories
                ]
            },
            {
                'name': 'Found',
                'rows': [
                    {
                        'name': cat.name,
                        'YTD': self.count(self.found(date_range=ranges.YTD, category=cat)),
                        'MTD': self.count(self.found(date_range=ranges.MTD, category=cat)),
                        'DAY': self.count(self.found(date_range=ranges.DAY, category=cat)),
                        'PREV_YTD': self.count(self.found(date_range=ranges.PREV_YTD, category=cat)),
                        'PREV_MTD': self.count(self.found(date_range=ranges.PREV_MTD, category=cat)),
                        'PREV_DAY': self.count(self.found(date_range=ranges.PREV_DAY, category=cat))
                    }
                    for cat in categories
                ]
            },
            {
                'name': 'Found (pro-activity)',
                'rows': [
                    {
                        'name': cat.label.lower(),
                        'YTD': self.count(self.pro_activity(cat, ranges.YTD)),
                        'MTD': self.count(self.pro_activity(cat, ranges.MTD)),
                        'DAY': self.count(self.pro_activity(cat, ranges.DAY)),
                        'PREV_YTD': self.count(self.pro_activity(cat, ranges.PREV_YTD)),
                        'PREV_MTD': self.count(self.pro_activity(cat, ranges.PREV_MTD)),
                        'PREV_DAY': self.count(self.pro_activity(cat, ranges.PREV_DAY))
                    }
                    for cat in ProActivityLevels
                ]
            },
            {
                'name': 'Matched',
                'rows': [
                    {
                        'name': cat.name,
                        'YTD': self.count(self.matched(date_range=ranges.YTD, category=cat)),
                        'MTD': self.count(self.matched(date_range=ranges.MTD, category=cat)),
                        'DAY': self.count(self.matched(date_range=ranges.DAY, category=cat)),
                        'PREV_YTD': self.count(self.matched(date_range=ranges.PREV_YTD, category=cat)),
                        'PREV_MTD': self.count(self.matched(date_range=ranges.PREV_MTD, category=cat)),
                        'PREV_DAY': self.count(self.matched(date_range=ranges.PREV_DAY, category=cat))
                    }
                    for cat in categories
                ]
            },
            {
                'name': 'Shipped',
                'rows': [
                    {
                        'name': cat.name,
                        'YTD': self.count(self.shipped(date_range=ranges.YTD, category=cat)),
                        'MTD': self.count(self.shipped(date_range=ranges.MTD, category=cat)),
                        'DAY': self.count(self.shipped(date_range=ranges.DAY, category=cat)),
                        'PREV_YTD': self.count(self.shipped(date_range=ranges.PREV_YTD, category=cat)),
                        'PREV_MTD': self.count(self.shipped(date_range=ranges.PREV_MTD, category=cat)),
                        'PREV_DAY': self.count(self.shipped(date_range=ranges.PREV_DAY, category=cat))
                    }
                    for cat in categories
                ]
            },
            {
                'name': 'Archived',
                'rows': [
                    {
                        'name': status.label,
                        'YTD': self.count(self.archive(status, ranges.YTD)),
                        'MTD': self.count(self.archive(status, ranges.MTD)),
                        'DAY': self.count(self.archive(status, ranges.DAY)),
                        'PREV_YTD': self.count(self.archive(status, ranges.PREV_YTD)),
                        'PREV_MTD': self.count(self.archive(status, ranges.PREV_MTD)),
                        'PREV_DAY': self.count(self.archive(status, ranges.PREV_DAY))
                    }
                    for status in ItemStatus
                ]
            },
            {
                'name': 'Statistics',
                'noTotals': True,
                'rows': [
                    {
                        'name': 'revenue / hotel',
                        'YTD': self._div(self.turnover_total(ranges.YTD), self.hotels_in_report_count),
                        'MTD': self._div(self.turnover_total(ranges.MTD), self.hotels_in_report_count),
                        'DAY': self._div(self.turnover_total(ranges.DAY), self.hotels_in_report_count),
                        'PREV_YTD': self._div(self.turnover_total(ranges.PREV_YTD), self.hotels_in_report_count),
                        'PREV_MTD': self._div(self.turnover_total(ranges.PREV_MTD), self.hotels_in_report_count),
                        'PREV_DAY': self._div(self.turnover_total(ranges.PREV_DAY), self.hotels_in_report_count),
                    },
                    {
                        'name': 'revenue / room',
                        'YTD': self._div(self.turnover_total(ranges.YTD), self.rooms_in_report_count),
                        'MTD': self._div(self.turnover_total(ranges.MTD), self.rooms_in_report_count),
                        'DAY': self._div(self.turnover_total(ranges.DAY), self.rooms_in_report_count),
                        'PREV_YTD': self._div(self.turnover_total(ranges.PREV_YTD), self.rooms_in_report_count),
                        'PREV_MTD': self._div(self.turnover_total(ranges.PREV_MTD), self.rooms_in_report_count),
                        'PREV_DAY': self._div(self.turnover_total(ranges.PREV_DAY), self.rooms_in_report_count),
                    },
                    {
                        'name': 'revenue / shipped item',
                        'YTD': self._div(self.turnover_total(ranges.YTD), self.count(self.shipped(date_range=ranges.YTD))),
                        'MTD': self._div(self.turnover_total(ranges.MTD), self.count(self.shipped(date_range=ranges.MTD))),
                        'DAY': self._div(self.turnover_total(ranges.DAY), self.count(self.shipped(date_range=ranges.DAY))),
                        'PREV_YTD': self._div(self.turnover_total(ranges.PREV_YTD), self.count(self.shipped(date_range=ranges.PREV_YTD))),
                        'PREV_MTD': self._div(self.turnover_total(ranges.PREV_MTD), self.count(self.shipped(date_range=ranges.PREV_MTD))),
                        'PREV_DAY': self._div(self.turnover_total(ranges.PREV_DAY), self.count(self.shipped(date_range=ranges.PREV_DAY))),
                    },
                    {
                        'name': '# lost / room',
                        'YTD': self.count_per_room(self.lost(date_range=ranges.YTD)),
                        'MTD': self.count_per_room(self.lost(date_range=ranges.MTD)),
                        'DAY': self.count_per_room(self.lost(date_range=ranges.DAY)),
                        'PREV_YTD': self.count_per_room(self.lost(date_range=ranges.PREV_YTD)),
                        'PREV_MTD': self.count_per_room(self.lost(date_range=ranges.PREV_MTD)),
                        'PREV_DAY': self.count_per_room(self.lost(date_range=ranges.PREV_DAY)),
                    },
                    {
                        'name': '# found / room',
                        'YTD': self.count_per_room(self.found(date_range=ranges.YTD)),
                        'MTD': self.count_per_room(self.found(date_range=ranges.MTD)),
                        'DAY': self.count_per_room(self.found(date_range=ranges.DAY)),
                        'PREV_YTD': self.count_per_room(self.found(date_range=ranges.PREV_YTD)),
                        'PREV_MTD': self.count_per_room(self.found(date_range=ranges.PREV_MTD)),
                        'PREV_DAY': self.count_per_room(self.found(date_range=ranges.PREV_DAY)),
                    },
                    {
                        'name': '# matched / room',
                        'YTD': self.count_per_room(self.matched(date_range=ranges.YTD)),
                        'MTD': self.count_per_room(self.matched(date_range=ranges.MTD)),
                        'DAY': self.count_per_room(self.matched(date_range=ranges.DAY)),
                        'PREV_YTD': self.count_per_room(self.matched(date_range=ranges.PREV_YTD)),
                        'PREV_MTD': self.count_per_room(self.matched(date_range=ranges.PREV_MTD)),
                        'PREV_DAY': self.count_per_room(self.matched(date_range=ranges.PREV_DAY)),
                    },
                    {
                        'name': '# shipped / room',
                        'YTD': self.count_per_room(self.shipped(date_range=ranges.YTD)),
                        'MTD': self.count_per_room(self.shipped(date_range=ranges.MTD)),
                        'DAY': self.count_per_room(self.shipped(date_range=ranges.DAY)),
                        'PREV_YTD': self.count_per_room(self.shipped(date_range=ranges.PREV_YTD)),
                        'PREV_MTD': self.count_per_room(self.shipped(date_range=ranges.PREV_MTD)),
                        'PREV_DAY': self.count_per_room(self.shipped(date_range=ranges.PREV_DAY)),
                    },
                    {
                        'name': '# archived / room',
                        'YTD': self._div(self.archived_count(ranges.YTD), self.rooms_in_report_count),
                        'MTD': self._div(self.archived_count(ranges.MTD), self.rooms_in_report_count),
                        'DAY': self._div(self.archived_count(ranges.DAY), self.rooms_in_report_count),
                        'PREV_YTD': self._div(self.archived_count(ranges.PREV_YTD), self.rooms_in_report_count),
                        'PREV_MTD': self._div(self.archived_count(ranges.PREV_MTD), self.rooms_in_report_count),
                        'PREV_DAY': self._div(self.archived_count(ranges.PREV_DAY), self.rooms_in_report_count),
                    },
                    {
                        'name': '# archived / found',
                        'YTD': self._div(self.archived_count(ranges.YTD), self.count(self.found(date_range=ranges.YTD))),
                        'MTD': self._div(self.archived_count(ranges.MTD), self.count(self.found(date_range=ranges.MTD))),
                        'DAY': self._div(self.archived_count(ranges.DAY), self.count(self.found(date_range=ranges.DAY))),
                        'PREV_YTD': self._div(self.archived_count(ranges.PREV_YTD), self.count(self.found(date_range=ranges.PREV_YTD))),
                        'PREV_MTD': self._div(self.archived_count(ranges.PREV_MTD), self.count(self.found(date_range=ranges.PREV_MTD))),
                        'PREV_DAY': self._div(self.archived_count(ranges.PREV_DAY), self.count(self.found(date_range=ranges.PREV_DAY))),
                    },
                ]
            },
        ]
        return Response(sections)


class FoundItems(ListModelMixin, ReportsViewSet):
    basename = 'found-items'
    serializer_class = FoundItemSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = FoundItemFilterSet
    pagination_class = StandardPagination

    def get_queryset(self):
        base_qs = FoundItem.objects.all()
        return FoundItemFilterSet(self.request.query_params, queryset=base_qs).qs.order_by('id')


class MatchedItems(ListModelMixin, ReportsViewSet):
    basename = 'matched-items'
    serializer_class = MatchedItemSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = MatchedItemFilterSet
    pagination_class = StandardPagination

    def get_queryset(self):
        base_qs = MatchedItem.objects.all()
        return MatchedItemFilterSet(self.request.query_params, queryset=base_qs).qs.order_by('id')


class HistoryItems(ListModelMixin, ReportsViewSet):
    basename = 'history-items'
    serializer_class = HistoryItemsSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = HistoryFilterSet
    pagination_class = StandardPagination

    def get_queryset(self):
        base_qs = ShippingItem.objects.all()
        return HistoryFilterSet(self.request.query_params, queryset=base_qs).qs.order_by('id')


class PaymentReportItems(ExportCSVMixin, ListModelMixin, ReportsViewSet):
    basename = 'report-items'
    serializer_class = serializer(
        'created',
        'id',
        'user',
        'email',
        'reference_number',
        'currency',
        'service_charge',
        'insurance_fee',
        'charged_amount',
        ('payment_date', serializers.DateTimeField()),
        'tracking_number',
        'hotel',
    )
    filter_backends = (DjangoFilterBackend, )
    pagination_class = StandardPagination

    def get_hotel(self, obj):
        try:
            return obj.shipping_details.shippingitem_set.first().parcel.lost_item.hotel
        except AttributeError:
            return None

    def get_user(self, obj):
        try:
            return obj.shipping_details.shippingitem_set.first().parcel.lost_item.created_by
        except AttributeError:
            return None

    def get_queryset(self):
        results = []
        transaction_qs = Transaction.objects.all()
        transactions = PaymentReportFilterSet(self.request.query_params, queryset=transaction_qs).qs.order_by('id')
        for t in transactions:
            transaction = {
                'id': t.id,
                'tracking_number': t.shipping_details.tracking_number,
                'charged_amount': t.charged_amount,
                'payment_date': t.payment_date,
                'service_charge': t.service_charge,
                'reference_number': t.reference_number,
                'insurance_fee': t.insurance_fee
            }
            hotel = self.get_hotel(t)
            user = self.get_user(t)
            try:
                transaction['user'] = '{} {}'.format(user.first_name, user.last_name)
                transaction['email'] = user.email
                transaction['hotel'] = hotel.name
                transaction['currency'] = hotel.address.get_currency_code
            except AttributeError:
                transaction['user'] = 'Not Available'
                transaction['email'] = 'Not Available'
                transaction['hotel'] = 'Not Available'
                transaction['currency'] = 'Not Available'
            if 'hotel' not in self.request.query_params.keys():
                results.append(transaction)
            else:
                if self.request.query_params['hotel'] is not '':
                    if hotel and hotel.pk == int(self.request.query_params['hotel']):
                        results.append(transaction)
                else:
                    results.append(transaction)
        return results


class CollectionReadyItem(ListModelMixin, ReportsViewSet):
    basename = 'collection-ready-item'
    serializer_class = CollectionReadySerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = CollectionReadyReportFilter
    pagination_class = StandardPagination

    def get_queryset(self):
        base_qs = ShippingItem.objects.all()
        return CollectionReadyReportFilter(self.request.query_params, queryset=base_qs).qs.order_by('id')
