from django.db.models import Model
from rest_framework.utils.encoders import JSONEncoder
from enumfields import Enum


class SmarterJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return obj.name

        if isinstance(obj, Model):
            clsname = obj.__class__.__name__
            return '%s:%s' % (clsname, obj.pk)

        return super(SmarterJSONEncoder, self).default(obj)
