from django.core.urlresolvers import reverse
from django.http import QueryDict
from django.template import Library

register = Library()


AUTH_PARAMS = ['key', 'sig', 'token', 'v']


@register.simple_tag(takes_context=True)
def authparams(context):
    request = context['request']
    response = context.get('response')

    if not request.user.is_authenticated():
        return ''

    query = QueryDict(mutable=True)
    # only auth params should be passed on
    for k, v in request.GET.items():
        if k in AUTH_PARAMS:
            query[k] = v

    # add the user token on successful login
    if response and 'token' in response.data:
        query['token'] = response.data['token']

    return '?' + query.urlencode()


@register.simple_tag
def api_url(url):
    urlname = '{}'.format(url.name)
    if '?P<pk' in url.regex.pattern:
        return reverse(urlname, kwargs=dict(pk='ID'))
    return reverse(urlname)
