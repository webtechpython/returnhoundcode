from rest_framework import viewsets
from rest_framework.response import Response
from .cms_serializers import *


SERIALIZERS = [
    # layout blocks...
    HeaderSerializer,
    FooterSerializer,
    # pages...
    HomePageSerializer,
    LogInPageSerializer,
    SignUpPageSerializer,
    FaqPageSerializer,
    AboutUsPageSerializer,
    ContactUsPageSerializer,
    HotelZonePageSerializer,
    HowItWorksPageSerializer,
    RequestDemoPageSerializer,
    PressReleasePageSerializer,
    ReportLostItemPageSerializer,
    ForgotPasswordPageSerializer,
    # other...
    TermsSerializer,
]


class ContentViewSet(viewsets.ViewSet):
    permission_classes = ()

    def content(self, ser: serializers.ModelSerializer):
        obj = ser.Meta.model.objects.filter(is_active=True).first()
        return ser(instance=obj).data

    def key(self, serializer_class):
        return str(serializer_class.__name__).replace('Serializer', '')

    def list(self, request, *args, **kwargs):
        return Response(data={self.key(s): self.content(s) for s in SERIALIZERS})
