import requests
from logging import getLogger
from requests.auth import HTTPBasicAuth
from django.conf import settings


log = getLogger('default')


class UrbanBuz:
    """
    Client to interact with UrbanBuz
    """
    def __init__(self):
        self.api_key = settings.URBAN_BUZ_API_KEY
        self.base_url = settings.URBAN_BUZ_BASE_URL

    def _request(self, url, data):
        log.info('UrbanBuz request: {}'.format(url), extra=data)
        r = requests.post(url, json=data, auth=HTTPBasicAuth(self.api_key, ''))
        response = r.json()
        log.info('UrbanBuz response: {}'.format(data))
        return response

    def signup(self, rh_id, hotel, email, password, first, **kwargs):
        """
        Signup method used to register hotel user with Urban Buz(reward system)
        :param rh_id: User’s ReturnHound account ID
        :param hotel: User’s ReturnHound account hotel ID
        :param email: User’s ReturnHound account email address
        :param password: User’s ReturnHound account password (hashed password saved in returnhound backend)
        :param first: User’s first name
        :return: Response from signup endpoint
        """
        url = self.base_url + '/api/signup'
        data = {
            'rh_id': str(rh_id),
            'first': first,
            'hotel': str(hotel),
            'email': email,
            'password': password,
        }
        # **kwargs are used for field that are not required (as seen below)
        # phone: User’s phone number (Note this field is not required)
        # avatar: URL of user’s avatar (Note this field is not required)
        data.update(kwargs)
        return self._request(url, data)

    def user_update(self, rh_id, **kwargs):
        """
        User update method, when user details are updated on returnhound so should
        Urban Buz user instance be updated
        :param rh_id: User’s ReturnHound account ID
        :return: Response from update endpoint
        """
        url = self.base_url + '/api/update?id={}'.format(rh_id)
        data = {
            'rh_id': str(rh_id),
        }
        # **kwargs are used for field that are not required (as seen below)
        # hotel: User’s ReturnHound account hotel ID (Note this field is not required)
        # email: User’s ReturnHound account email address (Note this field is not required)
        # password: User’s ReturnHound account password (Note this field is not required)
        # first: User’s first name (Note this field is not required)
        # last: User’s last name (Note this field is not required)
        # phone: User’s phone number (Note this field is not required)
        # avatar: URL of user’s avatar (Note this field is not required)
        data.update(kwargs)
        return self._request(url, data)

    def award_point(self, rh_id, item_id, item_description, submission_date):
        """
        Endpoint used to award points to hotel users who successfully shipped items back to
        owners.
        :param rh_id: User’s ReturnHound account ID
        :param item_id: Lost item’s ID
        :param item_description: Lost item’s description
        :param submission_date: Lost item's submission time (yyyy-mm-dd hh:mm:ss)
        :return: Response from award endpoint
        """
        url = self.base_url + '/api/award'
        data = {
            'rh_id': str(rh_id),
            'item_id': str(item_id),
            'item_description': item_description,
            'submission_date': submission_date,
        }
        return self._request(url, data)
