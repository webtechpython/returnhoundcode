from django.contrib import admin
from django.db.models import Count
from salmonella.admin import SalmonellaMixin
from return_hound.admin_tools.readonly import ReadOnlyAdmin
from .models import AramexCountry, AramexState, PostalCodeFormat


class CountryPostalCodeFormatInline(admin.TabularInline):
    model = PostalCodeFormat


@admin.register(AramexCountry)
class CountryAdmin(SalmonellaMixin, ReadOnlyAdmin):
    list_display = ['name', 'state_required', 'postcode_required', 'state_count']
    list_filter = ['state_required', 'postcode_required']
    search_fields = ['name']
    inlines = [CountryPostalCodeFormatInline]

    def get_queryset(self, request):
        qs = super(CountryAdmin, self).get_queryset(request)
        return qs.annotate(state_count=Count('aramexstate'))

    def state_count(self, obj: AramexCountry):
        return obj.state_count
    state_count.admin_order_field = 'state_count'


@admin.register(AramexState)
class StateAdmin(SalmonellaMixin, ReadOnlyAdmin):
    list_display = ['name']
    list_filter = ['aramex_country']
    salmonella_fields = ['aramex_country']
    search_fields = ['name']
