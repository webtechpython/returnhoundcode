from types import FunctionType
from enum import Enum
from decimal import Decimal
from django.core.cache import caches


class DjangoCache(object):
    def __init__(self, backend, timeout=300):
        self.cache = caches[backend]
        self.timeout = timeout

    def __getitem__(self, key):
        val = self.cache.get(self.format(key))
        if val:
            return val
        else:
            raise KeyError

    def __setitem__(self, key, value):
        self.cache.set(self.format(key), value, self.timeout)

    def __delitem__(self, key):
        self.cache.delete(self.format(key))

    def __contains__(self, key):
        return self.format(key) in self.cache

    def format(self, rawkey):
        """
        Format the final key string
        """
        assert isinstance(rawkey, tuple)
        key = []
        for k in rawkey:
            if isinstance(k, str):
                key.append(k)
            elif isinstance(k, (int, float, Decimal)):
                key.append(str(k))
            elif isinstance(k, Enum):
                key.append(k.name)
            elif isinstance(k, FunctionType):
                key.append(k.__name__)
            elif hasattr(k, '__dict__'):
                key.append(k.__class__.__name__)
            elif k.__class__ == object:
                key.append('(object)')  # weird blank object instance from cachetools
            else:
                raise Exception("Unsupported cache key arg")
        return '|'.join(key)
