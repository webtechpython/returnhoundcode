# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AramexCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('code2', models.CharField(max_length=2)),
                ('code3', models.CharField(max_length=3)),
                ('state_required', models.BooleanField()),
                ('postcode_required', models.BooleanField()),
            ],
            options={
                'verbose_name': 'Aramex Country',
                'verbose_name_plural': 'Aramex Countries',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='AramexState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('code', models.CharField(max_length=20, blank=True)),
                ('aramex_country', models.ForeignKey(to='aramex.AramexCountry')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
    ]
