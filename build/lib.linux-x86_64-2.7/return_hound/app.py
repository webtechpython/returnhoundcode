from django.apps import AppConfig


class ReturnHoundConfig(AppConfig):
    name = 'return_hound'
    verbose_name = 'Return Hound'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('User'))
        registry.register(self.get_model('Hotel'))
        registry.register(self.get_model('LostItem'))
        registry.register(self.get_model('FoundItem'))
        registry.register(self.get_model('MatchedItem'))
        registry.register(self.get_model('Shipment'))
        registry.register(self.get_model('ShippingItem'))
        registry.register(self.get_model('Transaction'))
