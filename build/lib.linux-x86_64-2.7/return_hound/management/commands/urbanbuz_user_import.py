from django.core.management.base import BaseCommand

from return_hound.models import HotelUser
from return_hound.apps.urbanbuz.service import UrbanBuz


class Command(BaseCommand):
    help = 'Import Returnhound users to Urbanbuz'

    def handle(self, *args, **options):
        client = UrbanBuz()
        for user in HotelUser.objects.all():
            client.signup(
                rh_id=user.employee.id,
                hotel=user.employer.id,
                email=user.employee.email,
                password=user.employee.password,
                first=user.employee.first_name,
                last=user.employee.last_name,
                phone=user.employee.contact_number
            )
