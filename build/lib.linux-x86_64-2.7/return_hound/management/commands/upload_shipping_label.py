from django.conf import settings
from django.core.management.base import BaseCommand

from urllib.request import urlretrieve
import boto
from boto.s3.connection import OrdinaryCallingFormat
from boto.s3.key import Key

from return_hound.models import Shipment


class Command(BaseCommand):
    help = 'Download the shipping label pdf from Aramex and store this on S3'

    def handle(self, *args, **options):
        conn = boto.s3.connect_to_region('eu-west-1',
                                         aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                         aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                                         calling_format=OrdinaryCallingFormat())
        bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
        bucket_url = 'https://{}/{}/'.format(settings.AWS_S3_HOST, settings.AWS_STORAGE_BUCKET_NAME)

        for shipment in Shipment.objects.filter(is_complete=False).exclude(label_url=''):
            file_name = '{}{}'.format(shipment.created.strftime("%Y%m%d%H%M%S"), shipment.pk) + '.pdf'
            bucket_path = 'uploads/' + file_name
            if not bucket.get_key(bucket_path):
                tempfile_path = urlretrieve(shipment.label_url)[0]
                k = Key(bucket=bucket, name=bucket_path)
                k.set_acl('public-read')
                k.set_contents_from_filename(tempfile_path)
                shipment.label_url = bucket_url + bucket_path
                shipment.save()
