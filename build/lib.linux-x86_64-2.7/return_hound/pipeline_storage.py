from django.contrib.staticfiles.storage import ManifestStaticFilesStorage
from pipeline.storage import GZIPMixin, PipelineMixin


class ForgivingMixin(object):
    """
    A forgiving storage mixin that ignores missing image references in css
    """
    def hashed_name(self, name, content=None):
        try:
            out = super(ForgivingMixin, self).hashed_name(name, content)
        except ValueError:
            # This means that a file could not be found, and normally this would
            # cause a fatal error, which seems rather excessive given that
            # some packages have missing files in their css all the time.
            out = name
        return out


class CompressedStorage(ForgivingMixin, GZIPMixin, PipelineMixin, ManifestStaticFilesStorage):
    """
    Manifest storage that also produces compresses (.gz) assets
    """
