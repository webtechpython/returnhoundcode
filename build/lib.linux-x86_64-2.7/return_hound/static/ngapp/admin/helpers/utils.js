'use strict';

angular.module('helpers.utils', [])

    .factory('Res', function ($resource) {
        /**
         * Default API Res(ource)
         */
        return function Res(url_pattern, params, actions) {
            var url = '/api/reports/' + url_pattern;
            // store resource baseURL for inspection (eg: CSV.csvFromAPI)
            var resource = $resource(url, params || {}, _.extend({}, {
                query: {method: 'GET'},  // default pagination is object body (isArray:False)
                // generic actions...
                GET: {method: 'GET'},
                GET_LIST: {method: 'GET', isArray: true},
                PATCH: {method: 'PATCH'},
                PUT: {method: 'PUT'},
                POST: {method: 'POST'}
            }, actions || {}));
            resource.baseURL = url_pattern.split('/')[0];
            return resource
        }
    })

    .factory('CSV', function($http, Util) {
        return {
            // no front end auth token needed as this csv export is for the django backend
            exportCSV: function (fileName, url){
                return $http({url: url}).success(function(res){
                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([res]);  //csv data string as an array.
                        window.navigator.msSaveBlob(blob, fileName); // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
                    } else {
                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document for FireFox
                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(res),
                            target: '_blank', download: fileName
                        })[0].click();
                        anchor.remove();
                    }
                });
            },
            /**
             * Generic CSV export for API resources.
             * @param resource: ngResource
             * @param filterParams: filter/search params object
             */
            csvFromAPI: function (resource, filterParams) {
                var url = '/api/reports/' + resource.baseURL + '/?format=csv';
                var fileName = resource.baseURL + '.csv';
                _.each(Util.cleanParams(filterParams), function (val, key) {
                    url += '&' + key + '=' + val
                });

                return this.exportCSV(fileName, url);
            }
        }
    })

    .factory('Util', function () {
        return {
            /**
             * Clean a filter object for use as URL search params
             */
            cleanParams: function (filter) {
                // convert Date to string and omit blanks
                var params = {};
                _.each(filter, function (v, k) {
                    if (_.isDate(v)){
                        params[k] = moment(v).format('YYYY-MM-DD')
                    } else {
                        params[k] = v;
                    }
                });
                return _.omit(params, function (val) {
                    // _.isEmpty(123) ==> true
                    // _.isEmpty('123') ==> false
                    return _.isString(val) && _.isEmpty(val);
                })
            }
        }
    })
;

