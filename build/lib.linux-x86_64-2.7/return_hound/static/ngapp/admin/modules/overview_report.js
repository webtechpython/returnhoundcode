'use strict';

angular.module('overviewReport', [
        'ngResource',
        'ui.bootstrap',
        'helpers.pagination',
        'helpers.utils'
    ])

    .config(['$resourceProvider', function ($resourceProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }])

    .factory('API', function (Res) {
        return {
            OverviewReport: Res('overview-report/'),
            Hotel: Res('hotels/'),
            Brand: Res('brands/'),
            Chain: Res('chains/')
        }
    })

    .controller('OverviewCtrl', function ($scope, API, Util, CSV){
        var vm = this, now = moment();

        vm.chains = API.Chain.GET_LIST();

        vm.datepicker = {
            isOpen: false,
            open: function () { this.isOpen = true },
            maxDate: now.toDate()
        };
        vm.resetFilter = function () {
            var defaults = {
                hotel: '',
                brand: '',
                chain: '',
                date: now.toDate()
            };
            // noinspection JSCheckFunctionSignatures
            if (!_.isEqual(vm.filter, defaults)) {
                vm.filter = angular.copy(defaults);
            }
        };
        vm.resetFilter(); // set defaults on init

        vm.total = function(rows, dateRange) {
            return _.reduce(rows, function(sum, row){ return sum + (row[dateRange] || 0) }, 0)
        };

        vm.reportSections = [];
        vm.loadData = _.debounce(function () {
            vm.reportSections = API.OverviewReport.GET_LIST(Util.cleanParams(vm.filter))
        }, 150);

        vm.exportCSV = function() {
            CSV.csvFromAPI(API.OverviewReport, vm.filter);
        };

        // load Brands for selectbox when Chain selected...
        $scope.$watch('vm.filter.chain', function (selectedChain) {
            vm.filter.brand = ''; // reset previosly selected
            if (selectedChain) {
                vm.brands = API.Brand.GET_LIST(Util.cleanParams({chain: selectedChain}));
            }
        });

        // load Hotels for selectbox when Brand or Chain is selected...
        $scope.$watchGroup(['vm.filter.brand', 'vm.filter.chain'], function () {
            vm.filter.hotel = '';  // reset previosly selected
            vm.hotels = API.Hotel.GET_LIST(Util.cleanParams({
                brand: vm.filter['brand'],
                brand__chain: vm.filter['chain']
            }));
        });

        // reload data when changing filter
        $scope.$watchCollection('vm.filter', vm.loadData);
    });
