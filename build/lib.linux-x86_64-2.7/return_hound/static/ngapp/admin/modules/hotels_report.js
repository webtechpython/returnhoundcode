'use strict';

angular.module('AdminHotelsReport', [
        'ngResource',
        'ui.bootstrap',
        'helpers.utils'
    ])

    .config(['$resourceProvider', function ($resourceProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }])

    .factory('API', function (Res, $resource) {
        return {
            HotelReport: Res('hotels-report/'),
            Category: $resource('/api/category', {page_size:1000}, {GET: {method: 'GET'}}),
            SubCategory: $resource('/api/category-item', {page_size:1000}, {GET: {method: 'GET'}})
        }
    })

    .controller('AdminHotelsReportCtrl', function ($scope, API, CSV, Util){
        var ctrl = this;

        ctrl.exportCSV = function() {
            CSV.csvFromAPI(API.HotelReport, ctrl.filter);
        };

        ctrl.filterBlank = {
            startDate: '',
            endDate: '',
            cat: '',
            subcat: ''
        };
        ctrl.filter = angular.copy(ctrl.filterBlank);
        ctrl.resetFilter = function () { ctrl.filter = angular.copy(ctrl.filterBlank) };

        ctrl.datepicker = {
            isOpen: {},
            maxDate: {
                endDate: moment().toDate()
            },
            open: function (name) {
                ctrl.datepicker.isOpen[name] = true;
            }
        };

        ctrl.cats = API.Category.GET();

        /**
         * Calculate field totals from Hotel report rows
         */
        function total(rows, fieldName) {
            return _.reduce(rows, function (memo, row) {return memo + row[fieldName]}, 0)
        }

        function round(val, digits) {
            return Number(val.toFixed(digits || 2))
        }

        ctrl.loadData = _.debounce(function () {
            ctrl.rows = [];
            API.HotelReport.GET_LIST(Util.cleanParams(ctrl.filter)).$promise.then(function (rows) {
                var totals = {
                    hotels: rows.length,
                    rooms: total(rows, 'number_of_rooms'),
                    found: total(rows, 'found_items_count'),
                    found_known: total(rows, 'found_items_known_count'),
                    found_not_known: total(rows, 'found_items_not_known_count'),
                    lost: total(rows, 'lost_items_count'),
                    shipped: total(rows, 'shipped_count'),
                    collected: total(rows, 'collected_count'),
                    returned: total(rows, 'returned_count')
                };
                angular.extend(totals, {
                    returned_rate: round(totals.returned / totals.found),
                    shipped_rate: round(totals.shipped / totals.found),
                    found_per_room: round(totals.found / totals.rooms),
                    lost_per_room: round(totals.lost / totals.rooms),
                    returned_per_room: round(totals.returned / totals.rooms)
                });
                ctrl.rows = rows;
                ctrl.totals = totals;
            });
        }, 150);

        // Load SubCategory data for dropdown on Category change
        $scope.$watch('ctrl.filter.cat', function(category){
            ctrl.filter.subcat = '';
            if (category) {
                ctrl.subcats = API.SubCategory.GET({category: category});
            }
        });

        $scope.$watchCollection('ctrl.filter', ctrl.loadData);
    });
