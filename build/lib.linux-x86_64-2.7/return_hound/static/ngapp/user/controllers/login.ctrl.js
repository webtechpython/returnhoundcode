angular
    .module('returnHound.login', ['returnHound.user.session'])
    .controller('LoginCtrl', function ($location, UserSession) {
        var vm = this;
         vm.defaults = {
            email: '',
            password: '',
            errors: {}
        };

        vm.doLogin = doLogin;
        vm.doYammerLogin = doYammerLogin;

        function doLogin () {
            vm.defaults.errors = {}; // clear any non_field_errors
            vm.defaults.errors.email = !vm.defaults.email;
            vm.defaults.errors.password = !vm.defaults.password;

            if (!_.any(_.values(vm.defaults.errors))) {
                UserSession.login(vm.defaults).then(
                    function (resp) {
                        if (resp.is_hotel_user){
                            return $location.path('/hotel-dashboard/log-item');
                        }
                        $location.path('/dashboard/my-reports');
                    },
                    function (error) {
                        vm.defaults.errors = error.data;
                    }
                );
            }
        }

        function doYammerLogin(data) {
            UserSession.yammerLogin(data).then(
                function (resp) {
                    if (resp.is_hotel_user) {
                        return $location.path('/hotel-dashboard/log-item')
                    }
                },
                function (error) {
                    vm.defaults.errors = error.data;
                }
            )
        }
    })

    .controller('LoginWithTokenCtrl', function (UserSession, $routeParams, $location) {
        var token = $routeParams.token;
        if (!token) $location.path('/');
        UserSession.autoLogin(token)
            .then(function () {
                // go to dashboard
                $location.path('/hotel-dashboard/log-item')
            })
            .catch(function () {
                $location.path('/')
            })
    })

;