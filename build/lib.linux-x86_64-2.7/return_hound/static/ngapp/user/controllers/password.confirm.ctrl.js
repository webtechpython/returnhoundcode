angular
    .module('returnHound.password.confirm', ['returnHound.user.service'])
    .controller('PasswordConfirmCtrl', PasswordConfirmCtrl);

    PasswordConfirmCtrl.$inject = ["$scope", "$routeParams", "UserFactory"];

    function PasswordConfirmCtrl($scope, $routeParams, UserFactory){
        var vm = this;
        vm.defaults = {
            formData: {},
            errors: {}
        };

        vm.resetPasswordConfirm = resetPasswordConfirm;

        function resetPasswordConfirm(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid){
                UserFactory.resetPasswordConfirm(vm, $routeParams['uid'], $routeParams['token']);
            }
        }
    }
