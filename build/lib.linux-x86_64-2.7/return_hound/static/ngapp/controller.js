angular
    .module('returnHound.controller', ['returnHound.alert', 'returnHound.cms'])
    .controller('AppCtrl', AppCtrl);

    function AppCtrl(AlertFactory, gettextCatalog, ContentService,
                     $cookies, $sce, $scope, $rootScope, RECAPTCHA_SITE_KEY){

        $scope.cms = ContentService;

        $scope.closeAlert = AlertFactory.close;

        $scope.recaptchaKey = RECAPTCHA_SITE_KEY;

        $scope.languages = [
            //{name: 'Arabic', key: 'ara'},
            {name: 'English', key: 'en'},
            //{name: 'French', key: 'fr'}
            //{name: 'Spanish', key: 'es'}
        ];

        $scope.changeLanguage = function (langKey) {
            $scope.selectedLanguage = langKey || 'en';
            $cookies.put('LANGUAGE_SELECTION', $scope.selectedLanguage);
            gettextCatalog.setCurrentLanguage($scope.selectedLanguage);
            if($scope.selectedLanguage == 'ara'){
                $rootScope.pageDirection = 'rtl';
                $rootScope.isArabic = true;
                $rootScope.isFrench = false;
            }
            else{
                $rootScope.pageDirection = 'ltr';
                $rootScope.isArabic = false;
                $rootScope.isFrench = $scope.selectedLanguage == 'fr';
            }
        };

        $scope.changeLanguage($cookies.get('LANGUAGE_SELECTION'));

        $scope.openNav = false;

        $scope.trustedURL = function(url){
            return $sce.trustAsResourceUrl(url);
        };

        $scope.renderHtml = function(html_code){
            return $sce.trustAsHtml(html_code);
        };
    }

