angular
    .module('returnHound.contact', ['returnHound.api'])
    .controller('ContactCtrl', ContactCtrl);

    ContactCtrl.$inject = ['$scope', 'AlertFactory', 'API', 'FormErrors', 'vcRecaptchaService'];

    function ContactCtrl($scope, AlertFactory, API, FormErrors, vcRecaptchaService) {
        var vm = this;
        vm.formData = {};
        vm.sendMail = sendMail;
        vm.setCaptchaFieldValidity = setCaptchaFieldValidity;

        function sendMail(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid && vm.formData.recaptcha){
                API.Contact.save({
                    email: vm.formData.email,
                    full_name: vm.formData.fullName,
                    contact_number: vm.formData.contactNumber,
                    message: vm.formData.message,
                    recaptcha: vm.formData.recaptcha
                }, function(resp){
                    vm.formData = {};
                    form.$submitted = false;
                    AlertFactory.add('success', 'Message sent.');
                    $scope.$broadcast('show-errors-reset');
                    vcRecaptchaService.reload();
                }, function(errors){
                    vm.formData.recaptcha = null;
                    FormErrors.show(form, errors.data);
                    vcRecaptchaService.reload();
                })
            }
        }

        function setCaptchaFieldValidity(form, isValid){
            form['recaptcha'].$setValidity('server', isValid);
        }
    }
