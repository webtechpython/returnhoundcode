angular
    .module('returnHound.item.service', ['returnHound.api', 'returnHound.modal'])
    .factory('ItemFactory', ItemFactory);

    ItemFactory.$inject = ['$rootScope', '$routeParams', '$location', 'API', 'PaginatedRes', 'ModalFactory', 'AlertFactory', 'FormErrors'];

    function ItemFactory($rootScope, $routeParams, $location, API, PaginatedRes, ModalFactory, AlertFactory, FormErrors){
        return {
            addItem: addItem,
            logItem: logItem,
            getMyItem: getMyItem,
            updateMyItem: updateMyItem,
            removeItem: removeItem,
            deleteItem: deleteItem,
            updateItem: updateItem,
            getMyLostItem: getMyLostItem,
            getMyLostItems: getMyLostItems,
            getFoundItem: getFoundItem,
            getMatchedItems: getMatchedItems,
            updateFoundItem: updateFoundItem,
            updateMultipleFoundItemsStatus: updateMultipleFoundItemsStatus,
            updateMultipleMatchedItemsStatus: updateMultipleMatchedItemsStatus,
            updateMultipleLostItemsStatus: updateMultipleLostItemsStatus,
            deleteFoundItem: deleteFoundItem,
            createItemImage: createItemImage,
            removeItemImage: removeItemImage,
            deleteItemImage: deleteItemImage,
            deleteFoundItemImage: deleteFoundItemImage,
            getMatchedFoundItems: getMatchedFoundItems,
            getMatchedLostItems: getMatchedLostItems,
            getMatchedLostItem: getMatchedLostItem,
            getLostItemAddress: getLostItemAddress,
            selectLostItemMatch: selectLostItemMatch,
            selectFoundItemMatch: selectFoundItemMatch,
            getHotelMatchedItem: getHotelMatchedItem,
            getCollectionItem: getCollectionItem,
            getHistoryItem: getHistoryItem,
            matchedItemFeedback: matchedItemFeedback,
            getInvoice: getInvoice
        };

        function addItem(formData, step){
            if (!formData[step].items){
                formData[step].items = [formData[step].lostItem];
                formData[step].lostItem = {};
            }else if (!$.isEmptyObject(formData[step].lostItem) && formData[step].lostItem.category){
                formData[step].items.push(formData[step].lostItem);
                formData[step].lostItem = {};
            }
            return formData[step];
        }

        function logItem(formData, form){
            FormErrors.clearErrors(form);
            return API.HotelFoundItems.save({
                created_by: $rootScope.user.id,
                category: formData.category.id,
                item_type: formData.categoryItem.id,
                description: formData.description,
                serial_number: formData.serial || '',
                specification: formData.specification || '',
                hotel: $rootScope.user.hotel.hotel_id,
                found_by: formData.founder,
                date:  moment(formData.date).format('YYYY-MM-DD'),
                room_number: formData.room || '',
                location: formData.location,
                owner_name: formData.owner_name || '',
                owner_email: formData.owner_email || '',
                owner_send_email: formData.owner_send_email || false,
                owner_surname: formData.owner_surname ||'',
                owner_contact_number: formData.owner_contact_number ||'',
                width: formData.width,
                height: formData.height,
                length: formData.length,
                weight: formData.weight,
                stored_at: formData.stored_at || ''
            }, function(resp){
                AlertFactory.add('success', 'Item #' + resp.id +' was logged.');
                _.each(formData.images, function(image){
                    API.FoundItemImage.save({
                        object_id: resp.id,
                        path: image.url
                    })
                });
            }, function(errors){
                FormErrors.show(form, errors.data)
            });
        }

        function getInvoice(data){
            return API.Invoice.get(data)
        }

        function getMyItem(){
            return API.KnownOwnerMatch.get({id: $routeParams['id']})
        }

        function updateMyItem(status){
            return API.KnownOwnerMatch.update({id: $routeParams['id']}, {
                status: status
            })
        }

        function removeItem(items, index){
            items.splice(index, 1)
        }

        function deleteItem(item){
            var modal = ModalFactory({
                text: 'Are you sure you want to cancel this item?'
            });
            modal.result.then(function () {
                AlertFactory.add('info', 'Item #' + item.id +' was cancelled.');
                API.MyLostItems.delete({id: item.id}, function () {
                    $location.path('dashboard/my-reports')
                });
            })
        }

        function updateItem(formData, form){
            FormErrors.clearErrors(form);
            API.MyLostItems.partial({id: formData.id}, {
                category: formData.category.id,
                item_type: formData.item_type.id,
                description: formData.description,
                serial_number: formData.serial_number || '',
                specification: (formData.item_type.name == 'Other') ? formData.specification : '',
                hotel: formData.hotel.id,
                arrival_date:  formData.arrival_date ? moment(formData.arrival_date).format('YYYY-MM-DD') : null,
                departure_date: formData.departure_date ? moment(formData.departure_date).format('YYYY-MM-DD') : null,
                date: moment(formData.date).format('YYYY-MM-DD'),
                location: formData.location || '',
                room_number: formData.room_number || ''
            }, function(resp){
                $location.path('dashboard/my-reports/'+ formData.id)
            }, function(errors){
                FormErrors.show(form, errors.data)
            });
        }

        function getMyLostItem(id){
            return API.MyLostItems.get({id: id}).$promise
        }

        function getMyLostItems(){
            return PaginatedRes(API.MyLostItems, {page_start: $routeParams['page'] || 1})
        }

        function getFoundItem(id){
            return API.HotelFoundItems.get({id: id}, function(response){})
        }

        function getMatchedItems(item){
            return API.MyLostItemMatch.query({id: item})
        }

        function updateFoundItem(formData, form){
            FormErrors.clearErrors(form);
            API.HotelFoundItems.partial({id:formData.id}, {
                category: formData.category.id,
                item_type: formData.item_type.id,
                status: formData.status? formData.status.value : '',
                status_text: formData.status_text || '',
                serial_number: formData.serial_number || '',
                description: formData.description,
                date:  moment(formData.date).format('YYYY-MM-DD'),
                room_number: formData.room_number || '',
                specification: (formData.item_type.name == 'Other') ? formData.specification : '',
                location: formData.location,
                found_by: formData.found_by,
                width: formData.width,
                height: formData.height,
                length: formData.length,
                weight: formData.weight,
                stored_at: formData.stored_at || '',
                owner_name: formData.owner_name || '',
                owner_email: formData.owner_email || '',
                owner_send_email: formData.owner_send_email || false,
                owner_surname: formData.owner_surname ||'',
                owner_contact_number: formData.owner_contact_number ||''
            }, function(resp){
                AlertFactory.add('success', 'Item #' + formData.id +' was changed.');
                _.each(formData.images, function(image){
                    if (!image.id){
                        API.FoundItemImage.save({
                            object_id: formData.id,
                            path: image.path
                        })
                    }
                });
                if (resp.owner_email){
                    $location.path('hotel-dashboard/matched-items')
                }else{
                    $location.path('hotel-dashboard/inventory/'+formData.id)
                }
            }, function(errors){
                FormErrors.show(form, errors.data)
            });
        }

        function updateMultipleFoundItemsStatus(objects, status, status_text){
            return API.HotelFoundItemsMultipleStatus.partial({}, {objects: objects, status: status, status_text: status_text})
        }

        function updateMultipleMatchedItemsStatus(objects, assigned_status, status_text){
            return API.HotelMatchedItemsMultipleStatus.partial({}, {objects: objects, assigned_status: assigned_status, status_text: status_text})
        }

        function updateMultipleLostItemsStatus(objects, status, status_text){
            return API.HotelLostItemsMultipleStatus.partial({}, {objects: objects, status: status, status_text: status_text})
        }

        function deleteFoundItem(foundItem){
            var modal = ModalFactory({
                text: 'Are you sure you want to remove this found item?'
            });
            modal.result.then(function () {
                AlertFactory.add('info', 'Found item #' + foundItem.id +' was removed.');
                API.HotelFoundItems.delete({id: foundItem.id}, function () {
                    $location.path('hotel-dashboard/inventory')
                });
            })
        }

        function createItemImage(item, path){
            API.LostItemImage.save({
                object_id: item.id,
                path: path
            }, function(resp){
                if (!item.images){
                    item.images = [{path: path, id: resp.id}];
                }else{
                    item.images.push({path: path, id: resp.id});
                }
            })
		}

        function removeItemImage(items, itemIndex, imageIndex){
            items[itemIndex].images.splice(imageIndex, 1)
        }

        function deleteItemImage(images, image, index){
            var modal = ModalFactory({
                text: 'Are you sure you want to remove this image?'
            });
            modal.result.then(function () {
                AlertFactory.add('info', 'Image was removed.');
                API.ReportImage.delete({id: image.id}, function(resp){
                    images.splice(index, 1)
                })
            })
		}

        function deleteFoundItemImage(images, image, index){
            API.FoundItemImage.delete({id: image.id}, function(resp){
                images.splice(index, 1)
            })
        }

        function getMatchedFoundItems(lostItem){
            return API.LostItemMatches.query({id:lostItem.id});
        }

        function getMatchedLostItems(foundItem){
            return API.FoundItemMatches.query({id:foundItem.id});
        }

        function getMatchedLostItem(id){
            return API.HotelLostItems.get({id: id})
        }

        function getLostItemAddress(id){
            return API.LostItemAddress.get({id:id});
        }

        function selectLostItemMatch(lostItem, matchedItem){
            var modal = ModalFactory({
                text: 'Are you sure you want to select this item as a match?'
            });
            modal.result.then(function () {
                API.HotelMatchedItems.save({found_item: matchedItem.pk, lost_item: lostItem.id}, function(resp){
                    $location.path('hotel-dashboard/lost-items')
                });
            });
        }

        function selectFoundItemMatch(foundItem, matchedItem){
            var modal = ModalFactory({
                text: 'Are you sure you want to select this item as a match?'
            });
            modal.result.then(function () {
                API.HotelMatchedItems.save({found_item: foundItem.id, lost_item:matchedItem.pk}, function(resp){
                    $location.path('hotel-dashboard/inventory')
                });
            });
        }

        function getHotelMatchedItem(id){
            return API.HotelMatchedItems.get({id: id})
        }

        function getCollectionItem(id){
            return API.HotelCollectItem.get({id: id})
        }

        function getHistoryItem(id){
            return API.HotelItemHistory.get({id: id})
        }

        function matchedItemFeedback(matchedItem, status){
            return API.MatchedItems.partial({id: matchedItem.id}, {status: status}, function(resp){
                if (status === 1){
                    matchedItem.status_display = 'Accepted';
                }else{
                    matchedItem.status_display = 'Declined';
                }
            })
        }
    }
