angular
    .module('returnHound.history.detail', ['returnHound.report.service'])
    .controller('HistoryDetailCtrl', HistoryDetailCtrl);

    HistoryDetailCtrl.$inject = ['$routeParams', 'STATIC_VIEWS_URL', 'ReportFactory'];

    function HistoryDetailCtrl($routeParams, STATIC_VIEWS_URL, ReportFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/history_detail.html'
        };

        vm.historyItem = ReportFactory.getMyHistoryItem($routeParams['id']);
    }
