angular
    .module('returnHound.my.profile', ['returnHound.user.service', 'returnHound.utils', 'formEditRevert'])

    .controller('MyProfileCtrl', function(UserSession, UserFactory, AlertFactory, AppUtils, Settings, API,
                                          FormErrors, EditRevert){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'user/views/user_nav.html',
            content: Settings.ROOT + 'report/views/account_settings.html'
        };

        vm.userProfile = API.UserProfile.get();
        vm.passwordData = {};  // password form data

        // Init components
        EditRevert.init(vm);

        /**
         * Save changed password
         * @param form: changePasswordForm
         */
        vm.changePassword = function(form){
            if (form.$valid || form.$error.server){
                UserFactory.changePassword(vm.passwordData, form).$promise
                    .then(function () {
                        vm.doEditDone(form);
                        vm.passwordData = {}; // clear model, form inputs
                    })
            }
        };

        /***
         * Save User profile details
         * @param form: userForm
         */
        vm.updateUserDetails = function(form){
            FormErrors.clearErrors(form);
            if (form.$valid) {
                UserFactory.updateUserProfile(vm.userProfile)
                    .then(function () {
                        vm.doEditDone(form);
                        UserSession.reloadUser();
                        AlertFactory.add('info', 'User profile updated.');
                    })
                    .catch(function (errors) {
                        FormErrors.show(form, errors.data)
                    })
            }
        };

        /**
         * Save Address
         * @param form: addressForm
         */
        vm.updateAddress = function (form){
            FormErrors.clearErrors(form);
            if (form.$valid) {
                API.UserProfile.partial({}, {address: vm.userProfile.address}).$promise
                    .then(function () {
                        vm.doEditDone(form);
                        UserSession.reloadUser();
                        AlertFactory.add('info', 'Address updated.');
                    })
                    .catch(function (errors) {
                        FormErrors.show(form, errors.data)
                    })
            }
        };
    })

    .controller('MyAddressCtrl', function(API, UserSession, FormErrors, $routeParams, $location){
        var vm = this;

        vm.address = {}; // populated by `shipping-address` directive
        vm.createAddress = function(form){
            FormErrors.clearErrors(form);
            if (form.$valid) {
                API.UserProfile.partial({}, {address: vm.address}).$promise
                    .then(function () {
                        // refresh user data and redirect...
                        UserSession.reloadUser().then(function () {
                            if ($routeParams['next']) {
                                $location.path('/dashboard/my-reports/' + $routeParams['next'])
                            } else {
                                $location.path('/dashboard/my-reports')
                            }
                        })
                    })
                    .catch(function (errors) {
                        FormErrors.show(form, errors.data)
                    })
            }
        };

    });
