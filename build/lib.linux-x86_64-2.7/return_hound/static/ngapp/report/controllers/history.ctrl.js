angular
    .module('returnHound.history', ['returnHound.report.service'])
    .controller('HistoryCtrl', HistoryCtrl);

    HistoryCtrl.$inject = ['STATIC_VIEWS_URL', 'ReportFactory'];

    function HistoryCtrl(STATIC_VIEWS_URL, ReportFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/history.html'
        };

        vm.historyItems = ReportFactory.getMyHistoryItems();
    }
