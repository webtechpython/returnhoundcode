angular
    .module('returnHound.my.report.detail', ['returnHound.item.service', 'returnHound.shipping.service', 'returnHound.payment.service', 'returnHound.paymentGateway'])
    .controller('MyReportDetailCtrl', MyReportDetailCtrl);

    function MyReportDetailCtrl($scope, $location, $routeParams, Settings, ModalFactory, ItemFactory, ShippingFactory, SERVICE_CHARGE_PERCENTAGE, INSURANCE_FEE_PERCENTAGE, CURRENCY, API){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'user/views/user_nav.html',
            content: Settings.ROOT + 'report/views/my_report_detail.html'
        };

        vm.currency = CURRENCY;

        vm.redirect = false;
        vm.terms = false;
        vm.serviceChargePercentage = SERVICE_CHARGE_PERCENTAGE;
        vm.insuranceChargePercentage = INSURANCE_FEE_PERCENTAGE;

        // Amount split for discretionary charge
        vm.staffAmount = 7;
        vm.rhFoundationAmount = 4;

        vm.payNow = payNow;
        vm.deleteItem = deleteItem;
        vm.giveFeedback = giveFeedback;
        vm.processShipment = processShipment;
        vm.cancelShipment = cancelShipment;
        vm.getShippingRate = getShippingRates;
        vm.amountPaid = amountPaid;

        vm.terms_alert = _.debounce(function() {
            AlertFactory.add('warning', 'Please accept the terms and conditions');
        }, 3000, true);

        vm.confirmPayment = function(){
            if(vm.terms){
                var opts = {
                    title: 'Payment Confirmation',
                    text: 'Once a payment is processed we will not be able to cancel the shipment.',
                    modal_ok_button: 'Proceed'
                }
                var modal = ModalFactory(opts);
                modal.result.then(function(results){vm.payNow();});
            }
            else{
                vm.terms_alert();
            }
        };

        function getShippingRates(id){
            ShippingFactory.getShippingRate(id).$promise.then(function(data){
                vm.shipping = data;
            }).catch(function(data) {
                vm.shipping = {
                    shipping_rate: {
                        has_error: true,
                        errors: [{
                            Code: '',
                            Message: data.data.server_error
                        }]
                    }
                };
            })
        }

        vm.getLostItem = ItemFactory.getMyLostItem($routeParams['id']).then(function(data){
            vm.item = data;
            ItemFactory.getMatchedItems(data.id).$promise.then(function(data){
                vm.matchedItem = data[0];
                if (vm.matchedItem && vm.matchedItem.status_display === 'Accepted' && !vm.matchedItem.shipment_status){
                    getShippingRates(vm.matchedItem.id)
                }else if(vm.matchedItem && vm.matchedItem.status_display === 'Accepted' && vm.matchedItem.shipment_status){
                    ShippingFactory.getShippingDetails(vm.matchedItem.id).$promise.then(function(data){
                        vm.shipment = data[0];
                        vm.serviceCharge = vm.shipment.transaction.calculated_service_charge;
                        vm.insuranceFee = vm.shipment.transaction.calculated_insurance_fee
                    })
                }
            });
        });

        function deleteItem(){
            ItemFactory.deleteItem(vm.item)
        }

        function giveFeedback(status){
            ItemFactory.matchedItemFeedback(vm.matchedItem, status).$promise.then(function(data){
                if (status === 1){
                    getShippingRates(vm.matchedItem.id)
                }
            })
        }

        function processShipment(isCollection){
            ShippingFactory.processShipment(isCollection, vm)
        }

        /**
         * Track API calls that are busy (in progress)
         */
        var busy = {};
        vm.isBusy = function(name) {
            var resName = name || 'api';
            return busy[resName]
        };

        function setBusy(apiCall, resName) {
            var name = resName || 'api';
            busy[name] = true;
            apiCall.$promise.then(function() { busy[name] = false })
        }

        function amountPaid() {
            var amount = parseFloat(vm.shipment.transaction.charged_amount);
            var service_charge = parseFloat(vm.shipment.transaction.service_charge);
            var insurance_fee = parseFloat(vm.shipment.transaction.insurance_fee);
            return amount + service_charge + insurance_fee;
        }

		function addInsuranceServiceCharge(){
            setBusy(API.Charges.update({id: vm.shipment.transaction.id}, {
                service_charge: vm.shipment.transaction.service_charge,
                insurance_fee: vm.shipment.transaction.insurance_fee
            }));
        }

        function cancelShipment(){
            ShippingFactory.cancelShipment(vm)
        }
        function payNow(){
            if (vm.terms) {
                addInsuranceServiceCharge();
                $location.url('/dashboard/my-reports/'+ vm.item.id + '/payment/' + vm.shipment.transaction.id + '?terms=' + vm.terms);
            }
        }
    }
