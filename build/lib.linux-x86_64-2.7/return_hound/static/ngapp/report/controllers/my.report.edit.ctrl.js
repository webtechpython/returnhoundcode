angular
    .module('returnHound.my.report.edit', ['returnHound.item.service', 'returnHound.utils'])
    .controller('MyReportEditCtrl', function($scope, $routeParams, Settings, ItemFactory, API, AppUtils, Upload){
        var vm = this;

        vm.templates = {
            nav: Settings.ROOT + 'user/views/user_nav.html',
            content: Settings.ROOT + 'report/views/my_report_edit.html'
        };

        vm.categories = AppUtils.getItemCategory();
        vm.s3Settings = AppUtils.getS3Settings();

        // Init the form data. Get lost item report detail:
        ItemFactory.getMyLostItem($routeParams['id']).then(function(data){
            vm.formData = angular.extend(data, {
                // parse server dates for datepicker widgets... FIXME: can the widget not do this?
                arrival_date: AppUtils.strToDate(data.arrival_date),
                departure_date: AppUtils.strToDate(data.departure_date),
                date: AppUtils.strToDate(data.date)
            });
        });

        vm.deleteItem = function deleteItem(){
            ItemFactory.deleteItem(vm.formData)
        };

        // form submit action:
        vm.updateItem = function updateItem(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid || form.$error.server){
                ItemFactory.updateItem(vm.formData, form)
            }
        };

		vm.imageUpload = function imageUpload(files, errFiles){
            vm.files = files;
            angular.forEach(files, function(file, index) {
                if(file !== null && vm.files.length){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = vm.s3Settings.url + path;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            vm.imagePath = fileURL;
                            ItemFactory.createItemImage(vm.formData, vm.imagePath);
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                   }
                }
            })
        };

        vm.deleteItemImage = function deleteItemImage(image, index){
            ItemFactory.deleteItemImage(vm.formData.images, image, index)
		};

        /***
         * Load sub categories on `formData.category` change
         */
        $scope.$watch('vm.formData.category', function(category) {
            if (category && category['id']) {
                vm.categoryItems = API.Item.query(
                    {category: category['id'], page_size: 100})  // FIXME: pagination ???
            }
        })

    });