angular
    .module('returnHound.my.report', ['returnHound.user.session', 'returnHound.item.service'])

    .controller('MyReportCtrl', function MyReportCtrl(Settings, ItemFactory) {
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'user/views/user_nav.html',
            content: Settings.ROOT + 'report/views/my_reports.html'
        };
        vm.items = ItemFactory.getMyLostItems()
    })

    .controller('MyItemCtrl', function MyItemCtrl($routeParams, $location, ItemFactory, UserSession){
        var vm = this;
        ItemFactory.getMyItem().$promise.then(function(data){
            vm.item = data;
            ItemFactory.updateMyItem(($routeParams['status']==='accepted') ? 1 : 3).$promise.then(function(data){
                if (vm.item.user_token && $routeParams['status']==='accepted'){
                    UserSession.autoLogin(vm.item.user_token).then(function(data){
                        if (data.address){
                            $location.path('/dashboard/my-reports/'+vm.item.lost_item)
                        }else{
                            $location.path('/my-address/'+vm.item.lost_item)
                        }
                    });
                }else if($routeParams['status']==='declined'){
                    vm.item.status = data.status;
                    vm.item.status_display = data.status_display
                }else{
                    $location.path('/')
                }
            });
        });
    });
