angular
    .module('returnHound.CategoryItemFilter', ['returnHound.api'])

    .controller('CategoryItemFilterController', function(API, $scope) {
        var vm = this;
        vm.multiSelectEventHandlers = {onItemSelect: setModel, onItemDeselect: setModel};
        vm.multiselectSettings = {
            enableSearch: false,
            displayProp: 'name',
            showCheckAll: false,
            showUncheckAll: false,
            buttonClasses: 'form-control multiselect',
            scrollableHeight: 'auto'
        };
        vm.translationTexts = {
            buttonDefaultText: 'Select item type',
            dynamicButtonTextSuffix: 'selected'
        };

        vm.subCategories = [];
        vm.selectedSubCategories = [];
        loadSubCategories();

        // watch the parent scope
        $scope.$watch('categories', loadSubCategories);
        $scope.$watch('model', setSelected);

        function loadSubCategories() {
            if (!$scope.categories) return null;
            var q = {
                category: $scope.categories,
                page_size: 100
            };
            API.CategoryItem.query(q, function(response){
                vm.subCategories = response.results;
                setSelected()
            });
        }

        function setModel() {
            $scope.model = toIDs(vm.selectedSubCategories);
        }

        function setSelected() {
            vm.selectedSubCategories = $scope.model ? fromIDs($scope.model) : [];
        }

        function toIDs() {
            var ids = [];
            angular.forEach(vm.selectedSubCategories, function (item) {
                ids.push(item.id);
            });
            return ids.join();
        }

        function fromIDs(modelStr) {
            var ids = modelStr ? modelStr.split(',') : [];
            return _.filter(vm.subCategories || [], function (cat) {
                return _.contains(ids, cat.id.toString())
            })
        }
    })

    .directive('categoryItemFilter', function (Settings) {
        return {
            scope: {
                categories: '=',
                model: '='
            },
            templateUrl: Settings.ROOT + 'common/categoryItemFilter/categoryItemFilter.html',
            controller: 'CategoryItemFilterController',
            controllerAs: 'vm'
        }
    })
;