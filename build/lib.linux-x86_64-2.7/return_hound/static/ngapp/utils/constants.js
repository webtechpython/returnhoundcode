angular
    .module('returnHound.constants', [])
    .constant('USER_NAV_TABS', [
        {label: "My Reports",   href: "/dashboard/my-reports", class: "dash-link"},
        {label: "Settings",     href: "/dashboard/settings", class: "dash-link"}
    ])
    .constant('HOTEL_ADMIN_TABS', [
        {label: "Log found item",   href: "/hotel-dashboard/log-item", class: "dash-link"},
        {label: "Found items",      href: "/hotel-dashboard/inventory", class: "dash-link"},
        {label: "Lost items",       href: "/hotel-dashboard/lost-items", class: "dash-link"},
        {label: "Matched items",    href: "/hotel-dashboard/matched-items", class: "dash-link"},
        {label: "Collection ready", href: "/hotel-dashboard/collection-ready", class: "dash-link"},
        {label: "History",          href: "/hotel-dashboard/history", class: "dash-link"},
        {label: "Archived",         href: "/hotel-dashboard/archived", class: "dash-link"},
        {label: "Settings",         href: "/hotel-dashboard/settings", class: "dash-link"}
    ])
    .constant('HOTEL_CREATOR_TABS', [
        {label: "Log found item",   href: "/hotel-dashboard/log-item", class: "dash-link"},
        {label: "Found items",      href: "/hotel-dashboard/inventory", class: "dash-link"},
        {label: "Lost items",       href: "/hotel-dashboard/lost-items", class: "dash-link"},
        {label: "Matched items",    href: "/hotel-dashboard/matched-items", class: "dash-link"},
        {label: "Collection ready", href: "/hotel-dashboard/collection-ready", class: "dash-link"},
        {label: "History",          href: "/hotel-dashboard/history", class: "dash-link"},
        {label: "Archived",         href: "/hotel-dashboard/archived", class: "dash-link"},
        {label: "Reports",          href: "/hotel-dashboard/reports", class: "dash-link"},
        {label: "Settings",         href: "/hotel-dashboard/settings", class: "dash-link"}
    ])
    .constant('HOTEL_USER_TABS', [
        {label: "Log found item",   href: "/hotel-dashboard/log-item", class: "dash-link"},
        {label: "Found items",      href: "/hotel-dashboard/inventory", class: "dash-link"},
        {label: "History",          href: "/hotel-dashboard/history", class: "dash-link"},
        {label: "Settings",         href: "/hotel-dashboard/settings", class: "dash-link"}
    ]);
