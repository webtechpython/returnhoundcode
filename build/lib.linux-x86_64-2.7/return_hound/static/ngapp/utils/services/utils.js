angular
    .module('returnHound.utils', ['returnHound.api'])
    .factory('AppUtils', AppUtils);

    function AppUtils($http, Settings, API, AuthService, Upload){
        var fileNameGenerator = function(file){
            var re = /(.+?)(\.[^.]*$|$)/;
            var ext = re.exec(file.name)[2];  // 1==filename, 2==ext (eg: '.jpg')
            var uuid = UUID.generate();
            var now = Date.now();
            return now + '_' + uuid + ext;
        };

        return {
            viewUrl: function(path){
                return Settings.ROOT + path
            },
            getS3Settings: function(){
                return API.S3Settings.save({})
            },
            fileNameGenerator: fileNameGenerator,
            getItemCategory: function(){
                return API.Category.query({page_size: 100})
            },
            getItem: function(category) {
                return API.CategoryItem.query({category: category, page_size: 100})
            },
            imageUpload: function(files, s3Settings, success){
                angular.forEach(files, function(file, index) {
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = s3Settings.url + path;
                        Upload.upload({
                            url: s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: s3Settings.key,
                                acl: 'public-read',
                                policy: s3Settings.policy,
                                signature: s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            success(resp, fileURL);
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                   }

                });
            },
            getMultiselectSettings: function (updateItemTypes, selectedCategories) {
                return {
                    settings: {
                        enableSearch: false,
                        displayProp: 'name',
                        showCheckAll: false,
                        showUncheckAll: false,
                        buttonClasses: 'form-control multiselect',
                        scrollableHeight: 'auto'
                    },
                    translationTexts: {
                        categories: {
                            buttonDefaultText: 'Select category',
                            dynamicButtonTextSuffix: 'selected'
                        },
                        itemTypes: {
                            buttonDefaultText: 'Select item type',
                            dynamicButtonTextSuffix: 'selected'
                        }
                    },
                    events: {
                        onItemSelect: function() {updateItemTypes(selectedCategories)},
                        onItemDeselect: function() {updateItemTypes(selectedCategories)}
                    }
                }
            },
            exportCSV: function (fileName, url, error_callback){
                return $http({url: url}).success(function(res){
                    if (window.navigator.msSaveOrOpenBlob) {
                        var blob = new Blob([res]);  //csv data string as an array.
                        window.navigator.msSaveBlob(blob, fileName); // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
                    } else {
                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document for FireFox
                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(res),
                            target: '_blank', download: fileName
                        })[0].click();
                        anchor.remove();
                    }
                });
            },
            /**
             * Generic CSV export for API resources.
             * @param resource: ngResource
             * @param filterParams: filter/search params object
             */
            csvFromAPI: function (resource, filterParams) {
                var token = AuthService.getToken();
                var url = Settings.API_ROOT + resource.baseURL + '/?format=csv';
                var fileName = resource.baseURL + '.csv';
                _.each(this.cleanParams(filterParams), function (val, key) {
                    url += '&' + key + '=' + val
                });
                return this.exportCSV(fileName, url + '&token=' + token);
            },

            /**
             * Clean a filter object for use as URL search params
             */
            cleanParams: function (filter) {
                // convert Date to string and omit blanks
                var params = {};
                _.each(filter, function (v, k) {
                    if (_.isDate(v)){
                        params[k] = moment(v).format('YYYY-MM-DD')
                    } else {
                        params[k] = v;
                    }
                });
                return _.omit(params, function (val) {
                    // _.isEmpty(123) ==> true
                    // _.isEmpty('123') ==> false
                    return _.isString(val) && _.isEmpty(val);
                });
            },

            /**
             * Convert a String date (from URL) to Date object
             * For use with datepicker that requires Date
             */
            strToDate: function (val) {
                return val ? moment(val, 'YYYY-MM-DD').toDate() : ''
            },

            range: function (min, max, step) {
                // parameters validation for method overloading
                if (max == undefined) {
                    max = min;
                    min = 0;
                }
                step = Math.abs(step) || 1;
                if (min > max) {
                    step = -step;
                }
                // building the array
                var output = [];
                for (var value = min; value < max; value += step) {
                    output.push(value);
                }
                // returning the generated array
                return output;
            }
        };
    }
