angular
    .module('returnHound.pagination', [])
    .value('PaginatedRes', function PaginatedRes(res, opts) {
        opts = opts || {};
        var self = {}, query_method = opts['query_method'] || 'query';
        self.query = null;
        self.results = [];
        self.count = 0;
        self.page_count = 0;
        self.page_size = opts['page_size'] || 15;
        self.page = opts['page_start'] || 1;
        self.filter = opts.filter || {};
        self.setFilter = function (filter) {
            self.filter = filter;
            self.setPage(1); // reset page number and reload
        };
        self.cleanFilter = function(filter) {
            // convert Date obejcts to string and omit blanks
            var cleaned = {};
            _.each(filter, function (v, k) {
                if (_.isDate(v)) {
                    cleaned[k] = moment(v).format('YYYY-MM-DD')
                } else {
                    cleaned[k] = v;
                }
            });
            return _.omit(cleaned, _.isEmpty);
        };
        self.params = function () {
            return angular.extend(
                {
                    page: self.page,
                    page_size: self.page_size
                },
                self.cleanFilter(self.filter)  // remove false/blank values
            )
        };
        self.loadPage = function (callback) {
            self.query = res[query_method](self.params(), function (resp) {
                self.results = resp.results;
                self.count = resp.count;
                self.page_count = Math.ceil(resp.count / self.page_size);
            });
            if (callback) self.query.$promise.then(callback)
        };
        self.hasNext = function () {
            return self.page < self.page_count
        };
        self.hasPrev = function () {
            return self.page > 1
        };
        self.pageNext = function () {
            if (self.hasNext()) {
                self.page++;
                self.loadPage()
            }
        };
        self.pagePrev = function () {
            if (self.hasPrev()) {
                self.page--;
                self.loadPage()
            }
        };
        self.setPage = function (pagenum) {
            self.page = pagenum;
            self.loadPage()
        };
        self.loadPage(opts.done);
        return self;
    });
