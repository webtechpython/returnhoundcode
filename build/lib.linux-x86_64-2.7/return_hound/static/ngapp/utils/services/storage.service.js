angular
    .module('returnHound.storage', [])
    .factory('SStorage', SStorage);

    SStorage.$inject = ['$window'];

    function SStorage ($window) {
        return {
            setData: function(key, val){
                $window.sessionStorage.setItem(key, val);
                return this;
            },
            getData: function(key){
                return $window.sessionStorage.getItem(key);
            }
        }
    }
