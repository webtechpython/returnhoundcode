angular
    .module('returnHound.loading.interceptor', ['returnHound.settings'])
    .factory('LoadingInterceptor', LoadingInterceptor);
    function LoadingInterceptor ($q, $rootScope, $location, Settings) {
        // With isDebug == true : There wont be a redirect to `something-went-wrong`
        var isDebug = Boolean(~Settings.DEBUG_HOSTS.indexOf($location.host()));
        var xhrCreations = 0;
        var xhrResolutions = 0;
        var excluded_urls  = [
            'amazonaws',
            'image',
            'hotel-list',
            'shipping-',
            'region',
            'email-check',
            'category-item',
            'service_charge',
            'insurance_fee'
        ];

        function excluded(config){
            return _.any(excluded_urls, function (url) {
                return ~config.url.indexOf(url)
            });
        }

        function handleErrorStatus(status){
            if (status === 404){
                $location.path('/page-not-found-404');
            }else if(status === 403){
                $location.path('/permission-denied-403');
            }else if(status === 500 && !isDebug){
                $location.path('/something-went-wrong');
            }
        }

        function isLoading() {
            return xhrResolutions < xhrCreations;
        }

        function updateStatus() {
            $rootScope.loading = isLoading();
        }
        return {
            request: function (config) {
                if (!excluded(config)){
                    xhrCreations++;
                    updateStatus();
                }
                return config || $q.when(config)
            },
            requestError: function (rejection) {
                if (!excluded(rejection.config)){
                    xhrResolutions++;
                    updateStatus();
                }
                return $q.reject(rejection);
            },
            response: function (response) {
                if (!excluded(response.config)) {
                    xhrResolutions++;
                    updateStatus();
                }
                return response;
            },
            responseError: function (error) {
                if (!excluded(error.config)){
                    xhrResolutions++;
                    updateStatus();
                }
                handleErrorStatus(error.status);
                return $q.reject(error)
            }
        };
    }
