angular
    .module('multiselectDisabled.directive', [])
    .directive('ngDropdownMultiselectDisabled', ngDropdownMultiselectDisabled);

    ngDropdownMultiselectDisabled.$inject = [];

    function ngDropdownMultiselectDisabled() {
        return function (scope, element, attrs) {
            var btn;
            btn = element.find('button');
            return scope.$watch(attrs.ngDropdownMultiselectDisabled, function (newVal) {
                return btn.attr('disabled', newVal);
            });
        };
    }