angular
    .module('img.load.directive', [])
    .directive('imgPreload', imgPreload);

    imgPreload.$inject = [];

    function imgPreload() {
        return {
            restrict: 'A',
            scope: {
                ngSrc: '@'
            },
            link: function (scope, element, attrs) {
                element.on('load', function () {
                    element.addClass('in');
                }).on('error', function () {
                    //
                });

                scope.$watch('ngSrc', function (newVal) {
                    element.removeClass('in');
                });
            }
        }
    }

