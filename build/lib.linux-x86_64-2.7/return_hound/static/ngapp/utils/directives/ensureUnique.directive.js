angular
    .module('ensureUnique.directive', [])
    .directive('ensureUnique', ensureUnique);

    ensureUnique.$inject = ['$http'];

    function ensureUnique($http){
        return {
            require: 'ngModel',
            link: function(scope, ele, attrs, c) {
                scope.$watch(attrs.ngModel, function(email) {
                    if (!email) return;
                    $http.post('/api/email-check/', {'email': email}).success(function (data) {
                        c.$setValidity('is_unique', data.is_unique);
                    }).error(function (data) {
                        c.$setValidity('is_unique', false);
                    });
                })
            }
        }
    }
