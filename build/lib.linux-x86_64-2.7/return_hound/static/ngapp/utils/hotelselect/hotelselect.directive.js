angular
    .module('hotelselect', ['returnHound.api'])

    .controller('HotelSelectController', function(API, $scope) {
        var vm = this;

        vm.countries = API.Country.query();

        function getRegions(country) {
            if (!country) return;
            vm.hotels = [];
            vm.regions = API.Region.query({country: country['code2']}, function (regions) {
                // reset selected region when regions are blank (trigger getHotels)
                if (regions.length === 0) {
                     $scope.model.address.region = null;
                     getHotels();  // force update (address.region might be null already)
                }
            })
        }

        function getHotels(region) {
            if (!$scope.model) return; // ignore blank model on init
            var country = $scope.model.address.country;
            var q = {country: country['code2']};
            if (region) q['region'] = region['id'];
            vm.hotels = API.Hotel.query(q, function (data) {
                // reset selected hotel when region is changed
                $scope.model.hotel = null;
                // set initial model.hotel from model.id (update form screen)
                if ($scope.model && $scope.model.id && !$scope.model.hotel){
                    $scope.model.hotel = _.find(data.results, function (hotel) {
                        return $scope.model.id === hotel.id
                    })
                }
            })
        }

        /**
         * We need to keep the initial value of model.id (hotel) in sync.
         * On Update forms the viewmodel can look like this:
         *
         $scope.model = {  // controller: vm.formData.hotel
            id: 13,  // sent to server on PATCH call
            hotel: {
                id: 13, // hotel selectbox input binds to this
                name: 'The Hotel'
            }
          }
         */
        function setHotelID(hotel) {
            if (hotel && $scope.model.id) $scope.model.id = hotel.id
        }

        // watch for changes...
        $scope.$watch('model.address.country', getRegions);
        $scope.$watch('model.address.region', getHotels);
        $scope.$watch('model.hotel', setHotelID);
    })

    .directive('hotelSelect', function (Settings) {
        return {
            scope: {model: '='},
            templateUrl: Settings.ROOT + 'utils/hotelselect/hotel_select.html',
            controller: 'HotelSelectController',
            controllerAs: 'vm'
        }
    })
;