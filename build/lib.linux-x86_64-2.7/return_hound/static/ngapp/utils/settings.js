angular
    .module('returnHound.settings', [])
    .constant('Settings', {
        ROOT: '/static/ngapp/',
        API_ROOT: '/api/',
        MEDIA_URL: '/media/',
        STATIC_URL: '/static/',
        DEBUG_HOSTS: [
            'localhost',
            'qa.returnhound.com'
        ]
    })
    // DEPRECATED... TODO: refactor. only use Settings
    .constant('MEDIA_URL', '/media/')
    .constant('STATIC_URL', '/static/')
    .constant('STATIC_VIEWS_URL', '/static/ngapp/')
    .constant('API_ROOT', '/api/')
;
