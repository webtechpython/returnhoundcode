// HAX: startswith polyfill
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
  };
}

function ContentService(API){
    return {
        data: API.Content.get(),
        getKey: function(dotted_path) {
            return dotted_path.split('.').reduce(function (prev, k) {
                return prev ? prev[k] : undefined
            }, this.data)
        }
    }
}

function cmsDirective($route) {
    return {
        link: function (scope, elem, attrs) {
            var key = attrs['cms'], defaultVal = attrs['default'];

            scope.$watch('cms.data.' + key, function (cmsVal) {
                if (cmsVal || defaultVal) {
                    elem.text(cmsVal);
                }
            })
        }
    }
}


function cmsAttrDirective($interpolate, $parse) {
    return {
        link: function (scope, elem, attrs) {
            var attrMap = $parse(attrs['cmsAttr'])(),
                defaultVal = attrs['default'],
                template = attrs['tmpl'];

            _.each(attrMap, function (key, attr) {
                scope.$watch('cms.data.' + key, function (cmsVal) {
                    var content = cmsVal || defaultVal;
                    var tmpl_raw = template || '[' + attr + ']';
                    var tmpl = $interpolate(tmpl_raw.replace('\[', '{{').replace('\]', '}}'));
                    var ctx = {};
                    var attrsToSet = {};
                    ctx[attr] = content;
                    attrsToSet[attr] = tmpl(ctx);
                    elem.attr(attrsToSet)
                });
            });
        }
    }
}

angular
    .module('returnHound.cms', ['returnHound.api'])
    .factory('ContentService', ContentService)
    .directive('cms', cmsDirective)
    .directive('cmsAttr', cmsAttrDirective)
;
