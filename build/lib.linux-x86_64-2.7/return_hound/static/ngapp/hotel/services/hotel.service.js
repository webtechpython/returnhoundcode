angular
    .module('returnHound.hotel.service', ['returnHound.api', 'returnHound.user.session', 'returnHound.modal'])
    .factory('HotelFactory', HotelFactory);

    function HotelFactory($routeParams, API, PaginatedRes, ModalFactory, AlertFactory, UserSession, FormErrors){
        return {
            createHotelUser: _createHotelUser,
            deleteUser: deleteUser,
            updateUser: updateUser,
            updateCorrespondenceChain: updateCorrespondenceChain,
            updateHotelUser:  updateHotelUser,
            registerHotel: registerHotel,
            getHotelUsers: _getHotelUsers,
            uploadHotelLogo: uploadHotelLogo,
            uploadReportImage: uploadReportImage,
            getHotelReportStyle: getHotelReportStyle,
            getHotelSocialLinks: getHotelSocialLinks,
            getEmailTemplates: getEmailTemplates,
            getEmailTemplateTypes: getEmailTemplateTypes,
            getHotelEmailTemplateStyle: getHotelEmailTemplateStyle,
            saveEmailTemplateStyle: saveEmailTemplateStyle,
            deleteEmailTemplate: deleteEmailTemplate,
            updateHotelReportStyle: updateHotelReportStyle,
            requestDemo: requestDemo,
            getSocialProviders: getSocialProviders,
            updateSocialLink: updateSocialLink,
            uploadEmailLogoImage: uploadEmailLogoImage
        };

        function _createHotelUser(newUserData, hotelUsers){
            return UserSession.getUser().then(function (currentUser) {
                return API.HotelUsers.save({
                    email: newUserData.email,
                    first_name: newUserData.firstName,
                    last_name: newUserData.lastName || '',
                    contact_number: newUserData.contact,
                    is_admin: newUserData.isAdmin,
                    is_supervisor: newUserData.isSupervisor,
                    is_mail_recipient: newUserData.receiveSystemEmail,
                    is_creator: newUserData.isCreator,
                    employer: currentUser.hotel.hotel_id  // FIXME
                }).$promise.then(function (response) {
                    // appends new user to list of users (vm.hotelUsers)
                    hotelUsers.results.push({
                        id: response.id,
                        user_first_name: newUserData.firstName,
                        user_last_name: newUserData.lastName,
                        user_contact_number: newUserData.contact,
                        is_admin: response.is_admin,
                        is_supervisor: response.is_supervisor,
                        is_mail_recipient: response.is_mail_recipient,
                        is_creator: response.is_creator
                    });
                    return response
                })
            });
        }

        function deleteUser(user, hotelUsers, index){
            var modal = ModalFactory({
                text: 'Are you sure you want to remove this user?'
            });
            modal.result.then(function () {
                API.HotelUsers.delete({id: user.id} ,function(resp){
                    AlertFactory.add('info', 'User removed.');
                    hotelUsers.results.splice(index, 1);
                })
            })
        }

        function updateUser(user, isSupervisor, isAdmin, isCreator){
            var modal = ModalFactory({
                text: isSupervisor ? 'Are you sure you want to change this user\'s supervisor status?' : 'Are you sure you want to change this user\'s admin status?'
            });
            modal.result.then(function () {
                UserSession.getUser().then(function (sessionUser) {
                // Only one of the three admin roles may be true at a time.
                // when one admin state is updated the rest will be reset to false.
                var data = {
                    is_admin: isAdmin ? !user.is_admin: false,
                    is_supervisor: isSupervisor ? !user.is_supervisor: false,
                    is_creator: isCreator ? !user.is_creator: false,
                    employer: sessionUser.hotel.hotel_id
                };
                API.HotelUsers.partial({id: user.id}, data, function(resp){
                    AlertFactory.add('info', isSupervisor ? 'User supervisor status updated.':isAdmin ?'User admin status updated.':isCreator ?'User owner status updated' : 'User email status updated.');
                    user.is_admin = resp.is_admin;
                    user.is_supervisor = resp.is_supervisor;
                    user.is_mail_recipient = resp.is_mail_recipient;
                    user.is_creator = resp.is_creator;
                    // Refresh the current(logged-in) user data when changed...
                    if (user.id === sessionUser.hotel.id) {
                        UserSession.reloadUser()
                    }
                }, function (error) {
                    if (error.data.is_creator) {
                        AlertFactory.add('danger', error.data.is_creator[0]);
                    }
                    if(error.data.is_mail_recipient){
                        AlertFactory.add('danger', error.data.is_mail_recipient[0]);
                    }
                });
                });
            })
        }

        function updateCorrespondenceChain(user, receiveSystemEmail){
            var modal = ModalFactory({
                text: 'Are you sure you want to change this user\'s email status?'
            });
            modal.result.then(function () {
                var data = {
                    is_mail_recipient: receiveSystemEmail ? !user.is_mail_recipient: false
                };
                API.HotelUsers.partial({id: user.id}, data, function(resp){
                    AlertFactory.add('info', 'User email status updated.');
                    user.is_admin = resp.is_admin;
                    user.is_supervisor = resp.is_supervisor;
                    user.is_mail_recipient = resp.is_mail_recipient;
                    user.is_creator = resp.is_creator
                }, function (error) {
                    if(error.data.is_mail_recipient){
                        AlertFactory.add('danger', error.data.is_mail_recipient[0]);
                    }
                });
            })
        }

        function updateHotelUser(user, form){
            FormErrors.clearErrors(form);
            API.HotelUsers.partial({id: user.id}, {
                first_name: user.user_first_name,
                last_name: user.user_last_name,
                contact_number: user.user_contact_number
            }, function(resp){
                AlertFactory.add('info', 'User details updated.');
                user.showEdit = false;
            }, function(errors){
                FormErrors.show(form, errors.data)
            })
        }

        function registerHotel(formData){
            return API.RegisterHotel.save({
                email: formData.stepTwo.email,
                first_name: formData.stepTwo.firstName,
                last_name: formData.stepTwo.lastName || '',
                contact_number: formData.stepTwo.contactNumber,
                terms: formData.stepTwo.terms,
                recaptcha: formData.stepTwo.recaptcha,
                name: formData.stepOne.name,
                domain_name: formData.stepOne.domain_name || '',
                number_of_rooms: formData.stepOne.number_of_rooms || 0,
                address: formData.stepOne.address
            }).$promise
        }

        function _getHotelUsers(){
            return UserSession.getUser().then(function (currentUser) {
                return PaginatedRes(API.HotelUsers, {
                    page_start: $routeParams['page'] || 1,
                    filter: {
                        employer: currentUser.hotel.hotel_id
                    }
                })
            })
        }

        function getHotelReportStyle(){
            return API.HotelReportStyle.get({})
        }

        function getHotelEmailTemplateStyle(data){
            return API.EmailTemplate.query(data);
        }

        function getHotelSocialLinks(data){
            return data? API.SocialLink.query(data): API.SocialLink.query();
        }

        function getSocialProviders(){
            return API.SocialProvider.query();
        }

        function updateSocialLink(params, data){
            return params && params.id ? API.SocialLink.update(params, data) : API.SocialLink.save(data);
        }

        function getEmailTemplateTypes(data){
            return API.EmailTemplateType.query(data);
        }

        function getEmailTemplates(data){
            return API.EmailTemplate.query(data) ;
        }

        function saveEmailTemplateStyle(data){
            return data.id ? API.EmailTemplate.update({id: data.id}, data) : API.EmailTemplate.save(data);
        }

        function deleteEmailTemplate(data){
            return API.EmailTemplate.delete({id: data.id})
        }

        function updateHotelReportStyle(newReportStyle, currentReportStyle){
            return API.HotelReportStyle.partial({}, {
                icon_color: newReportStyle.icon_color ? newReportStyle.icon_color : currentReportStyle.icon_color,
                form_label_color: newReportStyle.form_label_color ? newReportStyle.form_label_color : currentReportStyle.form_label_color,
                form_header_color: newReportStyle.form_header_color ? newReportStyle.form_header_color : currentReportStyle.form_header_color,
                form_background_color: newReportStyle.form_background_color ? newReportStyle.form_background_color : currentReportStyle.form_background_color,
                form_field_color: newReportStyle.form_field_color ? newReportStyle.form_field_color : currentReportStyle.form_field_color,
                form_field_border_color: newReportStyle.form_field_border_color ? newReportStyle.form_field_border_color : currentReportStyle.form_field_border_color,
                form_field_border_radius: newReportStyle.form_field_border_radius ? newReportStyle.form_field_border_radius : currentReportStyle.form_field_border_radius,
                button_border_radius: newReportStyle.button_border_radius ? newReportStyle.button_border_radius : currentReportStyle.button_border_radius,
                button_primary_color: newReportStyle.button_primary_color ? newReportStyle.button_primary_color : currentReportStyle.button_primary_color,
                button_primary_text_color: newReportStyle.button_primary_text_color ? newReportStyle.button_primary_text_color : currentReportStyle.button_primary_text_color,
                button_secondary_color: newReportStyle.button_secondary_color ? newReportStyle.button_secondary_color : currentReportStyle.button_secondary_color,
                button_secondary_text_color: newReportStyle.button_secondary_text_color ? newReportStyle.button_secondary_text_color : currentReportStyle.button_secondary_text_color,
                header_color: newReportStyle.header_color ? newReportStyle.header_color : currentReportStyle.header_color,
                header_active_color: newReportStyle.header_active_color ? newReportStyle.header_active_color : currentReportStyle.header_active_color,
                header_active_text_color: newReportStyle.header_active_text_color ? newReportStyle.header_active_text_color : currentReportStyle.header_active_text_color,
                header_inactive_color: newReportStyle.header_inactive_color ? newReportStyle.header_inactive_color : currentReportStyle.header_inactive_text_color,
                header_inactive_text_color: newReportStyle.header_inactive_text_color ? newReportStyle.header_inactive_text_color : currentReportStyle.header_inactive_text_color,
                show_hotel_logo: newReportStyle.show_hotel_logo ? newReportStyle.show_hotel_logo : currentReportStyle.show_hotel_logo,
                show_cover_image: newReportStyle.show_cover_image ? newReportStyle.show_cover_image : currentReportStyle.show_cover_image
            }, function(response){
                AlertFactory.add('info', 'Report template updated.');
            })
        }

        function uploadHotelLogo(imagePath, reportStyle){
            if (reportStyle.hotel_logo.length){
                return API.HotelLogo.partial({id: reportStyle.hotel_logo[0].id}, {
                    path: imagePath
                }, function(response){

                })
            }else{
                return API.HotelLogo.save({
                    object_id: reportStyle.hotel,
                    path: imagePath
                }, function(response){

                })
            }
        }

        function uploadReportImage(imagePath, reportStyle){
            if (reportStyle.cover_images.length){
                return API.ReportImage.partial({id: reportStyle.cover_images[0].id}, {
                    path: imagePath
                }, function(response){

                })
            }else{
                return API.ReportImage.save({
                    object_id: reportStyle.id,
                    path: imagePath
                }, function(response){

                })
            }
        }

        function uploadEmailLogoImage(imagePath, email_template){
            if (email_template.logo && email_template.logo.length){
                return API.EmailLogoImage.partial({id: email_template.logo[0].id}, {
                    path: imagePath
                }, function(response){

                })
            }else{
                return API.EmailLogoImage.save({
                    object_id: email_template.id,
                    path: imagePath
                })
            }
        }

        function requestDemo(formData, form){
            FormErrors.clearErrors(form);
            formData.demo_date = formData.date ? moment(formData.date).format('YYYY-MM-DD') : null;
            formData.demo_time = (formData.hour && formData.minutes) ? formData.hour + ':' + formData.minutes : '';
            return API.HotelDemo.save(formData, function(response){

            }, function(errors){
                FormErrors.show(form, errors.data)
            })

        }
    }
