angular
    .module('returnHound.hotel.register', ['returnHound.utils', 'returnHound.hotel.service'])
    .controller('HotelRegisterCtrl', HotelRegisterCtrl);
    function HotelRegisterCtrl($scope, $routeParams, $location, API, Data, HotelFactory, vcRecaptchaService, FormErrors){
        var vm = this;
        var storage = Data.storage('hotel-register');
        var saved = storage.getKey('hotel', {
            stepTwo: {} // FIXME: need this to set terms (false) below
        });

        vm.step = 1;
        vm.numberOfSteps = 3;
        vm.formData = saved;
        vm.formData.stepTwo.terms = false;

        function init() {
            vm.setStep($routeParams['id']);
        }

        function goStep(step) {
            // Navigate to the specified step
            return function () {
                vm.step = step;
                $location.update_path('/hotel-zone/register/step-' + step);
            }
        }

        vm.setStep = function(step, form){
            $scope.$broadcast('show-errors-check-validity');
            if(form) {
                if (form.$valid && step === 2) {
                    form.$setSubmitted(true);
                    // Do Address validation only to show form errors... :(
                    // If address validation fails on last step we cant show the errors
                    API.AddressValidation.POST(vm.formData.stepOne.address).$promise
                        .then(goStep(2))
                        .catch(function (errors) {
                            FormErrors.show(form, errors.data);
                        })
                }
                else {
                    if (form.$valid) goStep(step)();
                }
            }else if(step > vm.numberOfSteps || !vm.formData.stepOne){
                goStep(1)()
            }else if(!vm.formData.stepTwo && form){
                goStep(2)()
            }else{
                goStep(step)();
            }
        };

        vm.createHotel = function(form){
            $scope.$broadcast('show-errors-check-validity');
            form.$setSubmitted(true);
            if(form.$valid && vm.formData.stepTwo.terms && vm.formData.stepTwo.recaptcha) {
                HotelFactory.registerHotel(vm.formData)
                    .then(function () {
                        vm.setStep(3, form);
                    })
                    .catch(function (errors) {
                        FormErrors.show(form, errors.data);
                        vcRecaptchaService.reload();
                    })
            }
        };

        $scope.$watch("vm.formData", function(){
            storage.setKey('hotel', vm.formData);
        }, true);

        init()
    }
