angular
    .module('returnHound.lost.item', ['returnHound.item.service'])
    .controller('LostItemCtrl', LostItemCtrl);

    function LostItemCtrl($routeParams, $location, API, AppUtils, Settings, PaginatedRes, AlertFactory, ItemFactory){
        var vm = this;

        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/lost_items.html'
        };

        // Set the datePicker dates from routeParams, ui-datepicker only supports date objects.
        vm.dateObj = {
            created__gte: AppUtils.strToDate($routeParams['created__gte']),
            created__lte: AppUtils.strToDate($routeParams['created__lte'])
        };

        // Set the initial searchParams from routeParams, and convert date objects to strings for filtering.
        vm.searchParams = angular.extend({
            pk: $routeParams['pk'] || '',
            search: $routeParams['search'] || '',
            category: $routeParams['category'] || '',
            item_type: $routeParams['item_type'] || ''
        }, vm.dateObj);  // TODO: cleanup stupid Date handling, remove separate dateObj

        vm.selectedCategories = [];
        vm.selectedItemTypes = [];
        vm.selectedItems = [];

        vm.statuses = API.LostItemStatus.query();
        vm.itemStatus = '';
        vm.itemStatusText = '';

        // Paginated LostItem objects:
        vm.lostItems = PaginatedRes(API.HotelLostItems,  {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        AppUtils.getItemCategory().$promise.then(function(data) {
            vm.categories = data.results
        });

        vm.search = search;
        vm.reload = reload;
        vm.clearSearch = clearSearch;
        vm.clearField = clearField;
        vm.updateItemTypes = updateItemTypes;
        vm.selectItem = selectItem;
        vm.updateItemStatus = updateItemStatus;

        // ng-dropdown-multiselect
        var multiselect = AppUtils.getMultiselectSettings(vm.updateItemTypes, vm.selectedCategories);
        vm.multiselectSettings = multiselect.settings;
        vm.multiselectEvents = multiselect.events;
        vm.multiselectTranslationTexts = multiselect.translationTexts;

        vm.exportCSV = function() {
            AppUtils.csvFromAPI(API.HotelLostItems, vm.searchParams)
        };

        function updateItemTypes(categories){
            var categoryIds = [];
            vm.categoryItems = [];
            vm.selectedItemTypes = [];
            if (categories.length > 0) {
                angular.forEach(categories, function (category) {
                    categoryIds.push(category.id);
                });
                AppUtils.getItem(categoryIds.join()).$promise.then(function(data) {
                    vm.categoryItems = data.results
                });
            }
        }

        function selectItem(id) {
            var index = vm.selectedItems.indexOf(id);
            if (index !== -1) {
                vm.selectedItems.splice(index, 1);
            } else {
                vm.selectedItems.push(id);
            }
        }

        function updateItemStatus() {
            if (vm.itemStatus == null || vm.itemStatus == '' && vm.itemStatusText == null || vm.itemStatusText == '') {
                AlertFactory.add('danger', 'Select a status and add status description before updating');
            } else {
                if (vm.selectedItems.length > 0) {
                    ItemFactory.updateMultipleLostItemsStatus(vm.selectedItems, vm.itemStatus.value, vm.itemStatusText).$promise.then(function (data) {
                        AlertFactory.add('success', data.message);
                        vm.selectedItems = [];  // reset selected items
                        reload();
                    }).catch(function (error) {
                        AlertFactory.add('danger', error.message);
                    })
                } else {
                    AlertFactory.add('danger', 'No items selected');
                }
            }
        }

        function reload(){
            $location.search(AppUtils.cleanParams(vm.searchParams));
            vm.lostItems.setFilter(vm.searchParams);
        }

        function search(){
            var categories = [],
                itemTypes = [];
            if (vm.selectedCategories) {
                angular.forEach(vm.selectedCategories, function (category) {
                    categories.push(category.id);
                })
            }
            if (vm.selectedItemTypes) {
                angular.forEach(vm.selectedItemTypes, function (itemType) {
                    itemTypes.push(itemType.id);
                })
            }
            vm.searchParams.category = categories.join();
            vm.searchParams.item_type = itemTypes.join();
            // TODO: cleanup stupid Date handling...
            angular.extend(vm.searchParams, vm.dateObj);
            vm.reload();
        }

        function clearSearch(){
            vm.dateObj = {created__gte: null, created__lte: null};
            vm.searchParams = {};
            vm.selectedCategories = [];
            vm.selectedItemTypes = [];
            vm.categoryItems = [];
            vm.reload();
        }

        function clearField(key){
            vm.searchParams[key] = '';
            if(key in vm.dateObj){
                vm.dateObj[key] = '';
            }
            vm.reload();
        }
    }
