angular
    .module('returnHound.hotel.demo', ['returnHound.hotel.service'])
    .controller('HotelDemoCtrl', HotelDemoCtrl);

    HotelDemoCtrl.$inject = ['$scope', 'HotelFactory'];

    function HotelDemoCtrl($scope, HotelFactory){
        var vm = this;

        vm.formData = {};
        vm.dateOpen = false;
        vm.currentDate = moment().toDate();
        vm.showSuccess = false;
        vm.sendRequest = sendRequest;

        function sendRequest(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid || form.$error.server){
                HotelFactory.requestDemo(vm.formData, form).$promise.then(function (data) {
                    vm.showSuccess = true;
                })
            }
        }
    }
