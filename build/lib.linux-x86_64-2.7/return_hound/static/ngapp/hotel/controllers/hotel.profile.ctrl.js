angular
    .module('returnHound.hotel.profile', [
        'returnHound.user.service', 'returnHound.user.session',
        'returnHound.hotel.service', 'returnHound.utils',
        'formEditRevert'
    ])
    .controller('HotelProfileCtrl', HotelProfileCtrl);
    function HotelProfileCtrl(
        $scope, HotelFactory, UserFactory, AppUtils, Upload, Settings, API, EditRevert,
        anchorSmoothScroll, FormErrors, AlertFactory, ModalFactory,
        AuthService, UserSession, gettextCatalog
    ){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/hotel_settings.html'
        };
        vm.newUser = {};
        vm.tempEmailTemplate = {
            is_active: false
        };
        vm.translatedStrings = {
            customizeReportTheme: gettextCatalog.getString("Customize report theme"),
            customizeReportForm: gettextCatalog.getString("Custom report form")
        };
        vm.hotelTemplateTypes = [];
        vm.hotelSocialLinks = {};
        vm.editSocialLinks = {};
        vm.hotelUserData = {};
        vm.passwordData = {};
        vm.showCreateForm = false;
        vm.coverUploading = false;
        vm.reportUploading = false;
        vm.showChangePassword = false;
        vm.linkToolTipText = '';
        vm.embedToolTipText = '';

        vm.s3Settings = AppUtils.getS3Settings();
        vm.userProfile = API.UserProfile.get();
        vm.hotelProfile = API.HotelProfile.get();
        vm.reportStyle = {};
        vm.getHotelReportStyle = HotelFactory.getHotelReportStyle().$promise.then(function(data){
            vm.tempReportStyle = data;
            vm.reportStyle = angular.copy(vm.tempReportStyle)
        });

        function retrieveSocialLinks () {
            HotelFactory.getHotelSocialLinks().$promise.then(function(result) {
                vm.socialLinks = result;

                HotelFactory.getSocialProviders().$promise.then(function(result) {
                    vm.socialProviders = result;

                    angular.forEach(vm.socialProviders, function (provider) {
                        vm.editSocialLinks[provider.name] = false;
                        var socialProvider = vm.socialProvider(provider);
                        if(socialProvider) vm.hotelSocialLinks[provider.name] = socialProvider.link;
                    });
                });
            });
        }

        vm.emailTemplateTypes = HotelFactory.getEmailTemplateTypes().$promise.then(function(res){
            for(var i=0; i<res.length; i++){
                if(vm.hotelEmailTemplates(res[i])) vm.hotelTemplateTypes.push(res[i]);
            }
        });

        vm.embedLink = window.location.origin;
        vm.scrollTo = anchorSmoothScroll.scrollTo;
        vm.deleteUser = deleteUser;
        vm.updateUser = updateUser;
        vm.updateCorrespondenceChain = updateCorrespondenceChain;
        vm.updateHotelUser = updateHotelUser;
        vm.imageUpload = imageUpload;
        vm.copyOnError = copyOnError;
        vm.copyOnSuccess = copyOnSuccess;
        vm.changePassword = changePassword;
        vm.editHotelUserDetails = editHotelUserDetails;
        vm.updateReportStyle = updateReportStyle;
        vm.revertReportStyleChanges = revertReportStyleChanges;
        vm.emailLogoUpload = emailLogoUpload;
        vm.updateEmailTemplatePreview = updateEmailTemplatePreview;
        vm.saveSocialLink = saveSocialLink;
        vm.getHotelUsers = function() {
            HotelFactory.getHotelUsers().then(function (paginatedUsers) {
                vm.hotelUsers = paginatedUsers
            })
        };

        // ----------------------------------------------------------------
        // Init calls
        // ----------------------------------------------------------------
        EditRevert.init(vm);
        retrieveSocialLinks();

        // ----------------------------------------------------------------
        // Form save handlers
        // ----------------------------------------------------------------
        vm.updateUserDetails = function(form){
            FormErrors.clearErrors(form);
            if (form.$valid) {
                UserFactory.updateUserProfile(vm.userProfile, form)
                    .catch(function (errors) {
                        FormErrors.show(form, errors.data)
                    })
                    .then(function () {
                        vm.doEditDone(form);
                    })
            }
        };

        vm.updateHotelDetails = function(form){
            FormErrors.clearErrors(form);
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid) {
                var data = _.pick(vm.hotelProfile, ['name', 'domain_name', 'number_of_rooms', 'address']);
                return API.HotelProfile.partial({}, data, function (response) {
                    UserSession.reloadUser();
                    AlertFactory.add('info', 'Hotel profile updated.');
                    vm.doEditDone(form);
                }, function (errors) {
                    FormErrors.show(form, errors.data)
                });
            }
        };
        // ----------------------------------------------------------------

        vm.hotelEmailTemplates = function(item){
            var rh = ['new_demo_request', 'welcome_hotel_user_admin', 'welcome_hotel_user', 'welcome_client_user', 'new_message_received', 'password_reset_email'];
            return rh.indexOf(item.name) == -1;
        };

        vm.socialProvider = function(provider){
            var item = null;
            if (vm.socialLinks) {
                for (var i = 0; i < vm.socialLinks.count; i++) {
                    if (vm.socialLinks.results[i].provider.value == provider.value) {
                        item = vm.socialLinks.results[i];
                        break;
                    }
                }
            }
            return item
        };

        vm.activateEmailTemplate = function(item){
            vm.emailPreviewFrame = null;
            vm.activeEmailTemplate = item;
            vm.tempEmailTemplate = {
                is_active: false,
                template: item.value
            };
            API.EmailTemplate.query({template: item.name}).$promise.then(function(res){
                if (res.results.length > 0){
                    vm.updateEmailTemplatePreview(res.results[0]);
                    vm.tempEmailTemplate = res.results[0];
                }
            });
        };

        vm.clearTmpEmailTemplate = function(){
            var modal = ModalFactory({text: 'Are you sure you want to delete this email template?'});
            modal.result.then(function(){
                if(vm.tempEmailTemplate.id){
                    HotelFactory.deleteEmailTemplate(vm.tempEmailTemplate).$promise.then(function(){
                        AlertFactory.add('success', 'Email template successfully set to default');
                    });
                }
                vm.emailPreviewFrame = null;
                vm.tempEmailTemplate = {
                    is_active: false
                };
            });
        };

        vm.clearEmailTemplate = function(){
            if (vm.tempEmailTemplate.id) vm.tempEmailTemplate = API.EmailTemplate.get({id: vm.tempEmailTemplate.id});
            else {
                vm.clearTmpEmailTemplate();
            }
        };

        vm.saveEmailTemplate = function(){
            var data = angular.copy(vm.tempEmailTemplate);
            data.template = vm.activeEmailTemplate.value;
            if (typeof data.template == 'object') data.template = data.template.value;
            HotelFactory.saveEmailTemplateStyle(data).$promise.then(function(resp){
                var action = data.id ? 'updated' : 'saved';
                AlertFactory.add('success', 'Successfully ' + action + ' email template');
                vm.activateEmailTemplate(resp);
                vm.activeEmailTemplate = resp.template;
            }, function(err){
                if (err.data.non_field_errors) AlertFactory.add('danger', err.data.non_field_errors[0]);
            });
        };

        function updateEmailTemplatePreview(template){
            var token = AuthService.getToken();
            if (template.id) {
                vm.emailPreviewFrame = '/email-preview/' + template.id + '/?token=' + token;
            } else if (!vm.tempEmailTemplate.id) { // TODO: WAT???
                vm.emailPreviewFrame = '/email-preview/' + template.name + '/?token=' + token;
            }
        }

        // initialise radio button default values
        vm.newUser.isAdmin = false;
        vm.newUser.isSupervisor = false;
        vm.newUser.receiveSystemEmail = false;

        vm.createUser = function createUser(form){
            FormErrors.clearErrors(form);
            if (form.$valid) {
                HotelFactory.createHotelUser(vm.newUser, vm.hotelUsers).then(function(){
                    // reset newUser model/form
                    vm.newUser = {
                        isAdmin: false,
                        isSupervisor: false,
                        receiveSystemEmail: false,
                        isCreator: false
                    };
                    AlertFactory.add('info', 'User created.');
                })
                .catch(function (errors) {
                    FormErrors.show(form, errors.data);
                    AlertFactory.add('danger', 'Error creating user');
                });
            }
        };

        function deleteUser(user, index){
            HotelFactory.deleteUser(user, vm.hotelUsers, index)
        }

        function updateUser(user, isSupervisor, isAdmin, isCreator){
            HotelFactory.updateUser(user, isSupervisor, isAdmin, isCreator)
        }

        function updateCorrespondenceChain(user, receiveSystemEmail){
            HotelFactory.updateCorrespondenceChain(user, receiveSystemEmail);
        }

        function updateHotelUser(user, form){
            $scope.$broadcast('show-errors-check-validity', 'userUpdateForm');
            if(form.$valid || form.$error.server){
                HotelFactory.updateHotelUser(user, form)
            }
        }

        function copyOnError(e, action){
            var actionMsg = '';
            var actionKey = (e.action === 'cut' ? 'X' : 'C');

            if(/iPhone|iPad/i.test(navigator.userAgent)) {
                actionMsg = 'No support :(';
            }
            else if (/Mac/i.test(navigator.userAgent)) {
                actionMsg = 'Press ⌘-' + actionKey + ' to ' + e.action;
            }
            else {
                actionMsg = 'Press Ctrl-' + actionKey + ' to ' + e.action;
            }
            if (action === 'embed'){
                vm.embedToolTipText = actionMsg;
            }else{
                vm.linkToolTipText = actionMsg;
            }
        }
        
        function copyOnSuccess(e, action){
            if (action === 'embed'){
                vm.embedToolTipText = 'Copied!';
            }else{
                vm.linkToolTipText = 'Copied!';
            }
            e.clearSelection();
        }

        function changePassword(passwordForm){
            $scope.$broadcast('show-errors-check-validity');
            if (passwordForm.$valid || passwordForm.$error.server){
                UserFactory.changePassword(vm.passwordData, passwordForm).$promise.then(function(){
                    vm.passwordData = {};
                    vm.showChangePassword = false;
                })
            }
        }

        function editHotelUserDetails(hotelUser, form){
            if (hotelUser.showEdit){
                hotelUser.originalData = angular.copy(hotelUser);
            }else{
                FormErrors.clearErrors(form);
                hotelUser.user_first_name = hotelUser.originalData.user_first_name;
                hotelUser.user_last_name = hotelUser.originalData.user_last_name;
                hotelUser.user_contact_number = hotelUser.originalData.user_contact_number;
                hotelUser.originalData = {}
            }
        }

        function updateReportStyle(){
            HotelFactory.updateHotelReportStyle(vm.tempReportStyle, vm.reportStyle).$promise.then(function (data) {
                vm.tempReportStyle = data;
                vm.reportStyle = angular.copy(vm.tempReportStyle);
            })
        }

        function revertReportStyleChanges(){
            vm.tempReportStyle = angular.copy(vm.reportStyle);
        }

        function saveSocialLink(provider, link){
            vm.editSocialLinks[provider.name] = !vm.editSocialLinks[provider.name];
            if (!vm.editSocialLinks[provider.name]) {
                var params = {};
                var linkObj = vm.socialProvider(provider);
                var data = {provider: provider.value, link: link};
                if (linkObj && linkObj.id) params['id'] = linkObj.id;

                HotelFactory.updateSocialLink(params, data).$promise.then(function () {
                    AlertFactory.add('success', provider.label + ' has been successfully saved');
                    retrieveSocialLinks();
                }, function (err) {
                    vm.hotelSocialLinks[provider.name] = linkObj ? linkObj.link : null;  // Reset field value
                    if (err.data.non_field_errors) {
                        AlertFactory.add('danger', err.data.non_field_errors[0]);
                    }else if (err.data.link) {
                        AlertFactory.add('danger', err.data.link[0]);
                    }
                });
            }
        }

        function emailLogoUpload(files, errFiles){
            if (vm.emailLogoUploading) vm.emailLogo = files;
            AppUtils.imageUpload(files, vm.s3Settings, function(resp, fileURL){
                HotelFactory.uploadEmailLogoImage(fileURL, vm.tempEmailTemplate).$promise.then(function(data){
                    AlertFactory.add('success', 'Logo successfully uploaded');
                    vm.tempEmailTemplate['logo'] = [data];
                }, function(err){
                    AlertFactory.add('danger', 'Logo could not uploaded, an error occurred');
                });

                vm.emailLogoUploading = false;
            });
        }

        function imageUpload(files, errFiles){
            if (vm.coverUploading){
                vm.coverImage = files;
            }else{
                vm.reportImage = files;
            }
            angular.forEach(files, function(file, index) {
                if(file !== null && (vm.coverImage || vm.reportImage)){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            vm.imagePath = vm.s3Settings.url + path;
                            if (vm.coverUploading){
                                HotelFactory.uploadHotelLogo(vm.imagePath, vm.reportStyle).$promise.then(function(data){
                                    vm.reportStyle.hotel_logo = [{id: data.id, path: data.path}]
                                    vm.coverUploading = false;
                                })
                            }else if (vm.reportUploading){
                                HotelFactory.uploadReportImage(vm.imagePath, vm.reportStyle).$promise.then(function(data) {
                                    vm.reportStyle.cover_images = [{id: data.id, path: data.path}]
                                    vm.reportUploading = false;
                                })
                            }
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                   }
                }
            })
        }
    }
