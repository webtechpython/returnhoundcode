angular
    .module('returnHound.inventory.detail', ['returnHound.item.service'])
    .controller('InventoryDetailCtrl', InventoryDetailCtrl);

    InventoryDetailCtrl.$inject = ['$routeParams', 'STATIC_VIEWS_URL', 'ItemFactory', 'anchorSmoothScroll'];

    function InventoryDetailCtrl($routeParams, STATIC_VIEWS_URL, ItemFactory, anchorSmoothScroll){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'hotel/views/hotel_nav.html',
            content: STATIC_VIEWS_URL + 'hotel/views/inventory_detail.html'
        };

        vm.foundItem = ItemFactory.getFoundItem($routeParams['id']).$promise.then(function(data){
            vm.foundItem = data;
            if (vm.foundItem.match_count > 0){
                vm.matchedItems = ItemFactory.getMatchedLostItems(vm.foundItem)
            }
        });

        vm.compareItems = compareItems;
        vm.deleteFoundItem = deleteFoundItem;
        vm.selectFoundItemMatch = selectFoundItemMatch;
        vm.scrollTo = scrollTo;

        function deleteFoundItem(){
            ItemFactory.deleteFoundItem(vm.foundItem)
        }

        function selectFoundItemMatch(matchedItem){
            ItemFactory.selectFoundItemMatch(vm.foundItem, matchedItem)
        }

        function compareItems(matchedItem){
            matchedItem.comparedItem = matchedItem
        }

        function scrollTo(eID){
            anchorSmoothScroll.scrollTo(eID);
        }

    }
