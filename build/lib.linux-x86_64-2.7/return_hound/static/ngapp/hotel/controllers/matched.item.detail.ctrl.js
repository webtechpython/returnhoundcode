angular
    .module('returnHound.matched.item.detail', ['returnHound.item.service'])
    .controller('MatchedItemDetailCtrl', MatchedItemDetailCtrl);

    MatchedItemDetailCtrl.$inject = ['$routeParams', 'STATIC_VIEWS_URL', 'ItemFactory'];

    function MatchedItemDetailCtrl($routeParams, STATIC_VIEWS_URL, ItemFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'hotel/views/hotel_nav.html',
            content: STATIC_VIEWS_URL + 'hotel/views/matched_item_detail.html'
        };

        ItemFactory.getHotelMatchedItem($routeParams['id']).$promise.then(function(data){
            vm.matchedItem = data;
            ItemFactory.getLostItemAddress(data.id).$promise.then(function(data){
                vm.address = data
            })
        });
    }
