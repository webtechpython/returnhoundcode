from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered, NotRegistered


def admin_disable(model_class):
    """
    Disable a contrib or other autodiscovered admin
    """
    try:
        admin.site.unregister(model_class)
    except NotRegistered:
        pass  # No harm done


def admin_replace(model_class, admin_class):
    """
    Replace an autodiscovered admin class with our own
    """
    try:
        admin.site.register(model_class, admin_class)
    except AlreadyRegistered:
        admin.site.unregister(model_class)
        admin.site.register(model_class, admin_class)
