from admin_tools.dashboard import modules, Dashboard
from django.utils.translation import ugettext_lazy as _


class CustomDashboard(Dashboard):
    title = ''
    columns = 2

    def init_with_context(self, context):
        user = context['user']
        modules_default = [
            modules.ModelList(
                title=_('Site Content'),
                models=(
                    'return_hound.models.EmailTemplate',
                    'return_hound.models.SocialLink',
                    'return_hound.apps.cms.*',
                ),
            ),
            modules.ModelList(
                title=_('Return Hound'),
                models=(
                    'return_hound.models.*',
                ),
                exclude=(
                    'return_hound.models.EmailTemplate',
                    'return_hound.models.SocialLink',
                )
            ),
            modules.RecentActions(
                title=_('Recent Actions'),
                limit=5
            ),
        ]
        # Superuser only modules:
        modules_superuser = [
            modules.ModelList(
                title=_('Administration'),
                models=(
                    'return_hound.apps.api.*',  # API Tokens
                    'return_hound.apps.logger.*',
                    'django.contrib.*',
                    'axes.*',
                ),
            ),
            modules.ModelList(
                title=_('Audit Log'),
                models=(
                    'actstream.models.Action',
                ),
            ),
            modules.ModelList(
                title=_('Geodata'),
                models=(
                    'return_hound.apps.aramex.*',
                    'cities_light.*',
                ),
            ),
        ]

        if user.is_superuser:
            self.children.extend(modules_superuser)
        self.children.extend(modules_default)
