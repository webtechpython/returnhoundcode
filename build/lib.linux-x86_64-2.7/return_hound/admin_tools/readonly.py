from collections import OrderedDict
from django.contrib import admin
from django.contrib.auth import get_permission_codename


class ReadOnlyAdmin(admin.ModelAdmin):
    change_form_template = 'admin/read_only_form.html'
    change_list_template = 'admin/read_only_list.html'
    actions_disabled = ['delete_selected']

    def get_actions(self, request):
        actions = super(ReadOnlyAdmin, self).get_actions(request)
        return OrderedDict([(k, v) for k, v in actions.items() if k not in self.actions_disabled])

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        codename = get_permission_codename('change', self.opts)
        has_perm = request.user.has_perm("%s.%s" % (self.opts.app_label, codename))
        return False if obj and request.method == 'POST' else has_perm

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.opts.fields] + list(self.readonly_fields)
