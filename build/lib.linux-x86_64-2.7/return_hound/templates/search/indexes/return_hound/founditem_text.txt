{{ object.room_number|lower }}
{{ object.serial_number|lower }}
{{ object.category.name|lower }}
{{ object.item_type.name|lower }}
{{ object.description|lower }}
{{ object.specification|lower }}
{{ object.location|lower }}
