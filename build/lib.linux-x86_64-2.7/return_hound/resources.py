import datetime
from decimal import Decimal
from django.core.exceptions import ValidationError
from django.core import validators
from import_export import fields
from import_export.resources import ModelResource
from .models import User, FoundItem

DATE_FORMAT = '%Y-%m-%d'


def is_decimal(val):
    try:
        return bool(Decimal(val))
    except:
        raise ValueError("Incorrect decimal value")


def is_date(val):
    try:
        datetime.datetime.strptime(val, DATE_FORMAT)
    except ValueError:
        raise ValueError("Incorrect date format, should be YYYY-MM-DD")


def is_integer(val):
    if val:
        return validators.integer_validator(val)
    return True


class CSVField(fields.Field):
    def __init__(self, **kwargs):
        self.is_required = kwargs.pop('required', False)
        self.validators = kwargs.pop('validators', [])
        super(CSVField, self).__init__(**kwargs)

    def clean(self, data):
        value = data.get(self.column_name)
        if not value and self.is_required:
            raise ValueError("Column '%s': %s" % (self.column_name, 'is required.'))

        for i in self.validators:
            try:
                i(value)
            except ValidationError as e:
                raise ValueError("Column '%s': %s" % (self.column_name, e))
        return super(CSVField, self).clean(data)


class CreatedByField(CSVField):
    """
    Custom created by field that takes a user's pk/email as user's unique identifier
    """
    def clean(self, data):
        value = super(CreatedByField, self).clean(data)
        is_pk = value.isdigit() if value else False
        user_filter = dict(pk=value) if is_pk else dict(email=value)
        user = User.objects.filter(**user_filter).first()
        if not user:
            raise ValueError("user '{}': {}".format(value, 'does not exist.'))
        return user


class FoundItemModelResource(ModelResource):
    """
    Custom FoundItem CSV Import Model Resource
    """
    created_by = CreatedByField(attribute='created_by', required=True)
    id = CSVField(attribute='id', validators=[is_integer])
    found_by = CSVField(attribute='found_by', required=True)
    item_type = CSVField(attribute='item_type_id', validators=[is_integer])
    date = CSVField(attribute='date', validators=[is_date], required=True)
    hotel = CSVField(attribute='hotel_id', validators=[is_integer], required=True)
    category = CSVField(attribute='category_id', validators=[is_integer], required=True)
    width = CSVField(attribute='width', validators=[is_decimal], required=True)
    weight = CSVField(attribute='weight', validators=[is_decimal], required=True)
    length = CSVField(attribute='length', validators=[is_decimal], required=True)
    height = CSVField(attribute='height', validators=[is_decimal], required=True)

    class Meta:
        model = FoundItem
