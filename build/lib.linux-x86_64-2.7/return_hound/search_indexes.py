from haystack import indexes
from return_hound.models import LostItem, FoundItem


def limit_word_length(text: str):
    # Xapian has limit of 245 chars per term/word...
    # 200 Chars should be enough for fieldname + term
    return ' '.join([word[:200] for word in text.split(' ')])


class LostItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    hotel = indexes.IntegerField(model_attr='hotel__pk')
    category = indexes.CharField(model_attr='category__name')
    date = indexes.DateField(model_attr='date')
    description = indexes.CharField(model_attr='description')
    serial_number = indexes.CharField(model_attr='serial_number', null=True)
    specification = indexes.CharField(model_attr='specification', null=True)
    room_number = indexes.CharField(model_attr='room_number', null=True)
    location = indexes.CharField(model_attr='location', null=True)
    is_deleted = indexes.BooleanField(model_attr='is_deleted')

    name = indexes.CharField(model_attr='created_by__first_name', indexed=False)
    email = indexes.CharField(model_attr='created_by__email', indexed=False)
    created = indexes.DateField(model_attr='created', indexed=False)
    last_name = indexes.CharField(model_attr='created_by__last_name', null=True, indexed=False)
    item_type = indexes.CharField(model_attr='item_type__name', null=True)
    contact_number = indexes.CharField(model_attr='created_by__contact_number', indexed=False)
    arrival_date = indexes.DateField(model_attr='arrival_date', null=True, indexed=False)
    departure_date = indexes.DateField(model_attr='departure_date', null=True, indexed=False)
    images = indexes.MultiValueField(indexed=False)

    def get_model(self):
        return LostItem

    def prepare_created(self, obj):
        return obj.created.date()

    def prepare_description(self, obj):
        return limit_word_length(obj.description)

    def prepare_images(self, obj):
        return [obj.path for obj in obj.images.filter(is_deleted=False)]

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.unmatched()


class FoundItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    hotel = indexes.IntegerField(model_attr='hotel__pk')
    date = indexes.DateField(model_attr='date')
    category = indexes.CharField(model_attr='category__name')
    location = indexes.CharField(model_attr='location')
    is_deleted = indexes.BooleanField(model_attr='is_deleted')
    description = indexes.CharField(model_attr='description')
    room_number = indexes.CharField(model_attr='room_number', null=True)
    serial_number = indexes.CharField(model_attr='serial_number', null=True)
    specification = indexes.CharField(model_attr='specification', null=True)

    created = indexes.DateField(model_attr='created', indexed=False)
    found_by = indexes.CharField(model_attr='found_by', indexed=False)
    item_type = indexes.CharField(model_attr='item_type__name', null=True)
    images = indexes.MultiValueField(indexed=False)

    def get_model(self):
        return FoundItem

    def prepare_created(self, obj):
        return obj.created.date()

    def prepare_description(self, obj):
        return limit_word_length(obj.description)

    def prepare_images(self, obj):
        return [obj.path for obj in obj.images.filter(is_deleted=False)]

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.unmatched()
