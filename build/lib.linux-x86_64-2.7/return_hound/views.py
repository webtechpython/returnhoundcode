import datetime
from django.conf import settings
from django.views.generic import TemplateView, DetailView
from django.core.exceptions import PermissionDenied

from return_hound.apps.api.models import Token
from . import models


class IndexView(TemplateView):
    template_name = 'base.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'CURRENCY': settings.DEFAULT_CURRENCY,
            'SERVICE_CHARGE_PERCENTAGE': settings.DEFAULT_SERVICE_CHARGE_PERCENTAGE,
            'INSURANCE_FEE_PERCENTAGE': settings.DEFAULT_INSURANCE_FEE_PERCENTAGE,
            'YAMMER_CLIENT_ID': settings.YAMMER_CLIENT_ID,
            'RECAPTCHA_SITE_KEY': settings.RECAPTCHA_SITE_KEY,
            'social_links': models.SocialLink.objects.system_default()
        })
        return context


class TokenRequiredMixin(object):
    user = None

    def dispatch(self, request,  *args, **kwargs):
        try:
            token = Token.objects.get(key=request.GET.get('token'))
            self.user = token.user
        except (ValueError, Token.DoesNotExist):
            raise PermissionDenied
        return super(TokenRequiredMixin, self).dispatch(request, *args, **kwargs)


class EmailTemplateMixin(TokenRequiredMixin):
    def get_template_names(self):
        kw_template = self.object.template.name
        if kw_template == 'password_reset_email':
            template = 'registration/{}.html'.format(kw_template)
        elif kw_template == 'email_base':
            template = '{}.html'.format(kw_template)
        else:
            template = 'emails/{}.html'.format(kw_template)
        return [template]

    def get_email_template(self, hotel):
        if not getattr(self, 'object', None):
            return models.EmailTemplate.objects.get_outgoing_email(self.kwargs.get('template'), hotel)
        return self.object

    def get_hotel(self):
        if self.user.is_hotel_user:
            return self.user.get_hotel
        return models.Hotel.objects.all().first()

    def get_context_data(self, **kwargs):
        user = self.user or self.request.user
        hotel = self.get_hotel()
        today = datetime.date.today()
        email_obj = self.get_email_template(hotel)

        item = {
            'id': 1,
            'category': {'name': 'Clothing'},
            'item_type': {'name': 'shirt'},
            'arrival_date': today, 'departure_date': today,
            'date': today,
            'hotel': hotel,
            'created_by': user,
            'room_number': '123',
            'description': 'description 123',
            'serial_number': 'sn 123',
            'specification': 'specification 123',
        }
        ctx = {
            'user_created': True,
            'shipping': {
                'tracking_number': '123',
                'get_delivery_address': user.get_address
            },
            'matched_item': {
                'lost_item': item,
                'found_item': item
            },
            'item': item,
            'lost_item': item,
            'found_item': item,
            'full_name': user.get_full_name(),
            'email': user.email,
            'contact_number': user.contact_number,
            'password': 'password',
            'email_obj': email_obj,
            'user': user,
            'client': user,
            'hotel_user': user.get_hotel_user,
            'hotel': hotel,
            'hotel_name': hotel.name if hotel else 'test hotel',
            'social_links': models.SocialLink.objects.system_default()
        }
        return ctx


class EmailTemplatePreviewView(EmailTemplateMixin, TemplateView):
    """ intended for outgoing email template"""


class EmailTemplateDetailPreviewView(EmailTemplateMixin, DetailView):

    def get_queryset(self):
        return models.EmailTemplate.objects.filter(hotel=self.user.get_hotel)


class AdminEmailPreviewView(EmailTemplateMixin, DetailView):
    queryset = models.EmailTemplate.objects.all()
