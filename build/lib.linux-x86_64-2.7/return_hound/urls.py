from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from .admin import setup_admin_site
from . import views

setup_admin_site()

urlpatterns = [
    url(r'^api/', include('return_hound.apps.api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/reports/', include('return_hound.apps.admin_reports.urls')),
    url(r'^admin/supervisor/', include('return_hound.apps.admin_supervisor.urls')),
    url(r'^admin/salmonella/', include('salmonella.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^s3direct/', include('s3direct.urls')),
    url(r'^email-preview/(?P<pk>\d+)/$', views.EmailTemplateDetailPreviewView.as_view(), name='email_preview'),
    url(r'^email-preview/(?P<template>\w+)/$', views.EmailTemplatePreviewView.as_view(), name='email_preview_template'),
    url(r'^email/', TemplateView.as_view(template_name='emails/welcome_client_user.html'), name='email'),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^.*$', views.IndexView.as_view(), name='ngapp'),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
