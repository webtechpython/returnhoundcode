# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations, models
from return_hound.migrations.utils.fixtures import load_fixture


def populate_currency_field(apps, schema):
    """
    Uses the data from:
    https://raw.githubusercontent.com/mledoze/countries/master/dist/countries.json
    """
    AramexCountry = apps.get_model('aramex.AramexCountry')
    data = load_fixture('return_hound', 'countries.json.zip')
    for row in data:
        code2 = row['cca2']
        ccy_code = row['currency'][0] if row['currency'] else None
        if ccy_code:
            AramexCountry.objects.filter(code2=code2).update(currency=ccy_code)


class Migration(migrations.Migration):
    dependencies = [
        ('aramex', '0003_aramexcountry_currency'),
    ]

    operations = [
        migrations.RunPython(populate_currency_field, reverse_code=migrations.RunPython.noop)
    ]
