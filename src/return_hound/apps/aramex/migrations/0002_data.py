# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations, models

from return_hound.migrations.utils.fixtures import load_fixture


def country_state_data(apps, schema):
    data = load_fixture('return_hound', 'aramex_country_state.json.zip')
    for row in data:
        model = apps.get_model(row['model'])
        clean_data = {}
        for k, v in row['fields'].items():
            model_field = model._meta.get_field(k)
            if model_field.rel:
                instance = model_field.rel.to.objects.get(pk=v)
                clean_data[k] = instance
            else:
                clean_data[k] = v
        if 'id' in row:
            model.objects.create(id=row['id'], **clean_data)
        else:
            model.objects.create(**clean_data)


class Migration(migrations.Migration):

    dependencies = [
        ('aramex', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(country_state_data, reverse_code=migrations.RunPython.noop)
    ]
