import logging
import ssl
import operator
import pytz
from datetime import datetime, timedelta, time
from math import ceil
from base64 import b64decode
from django.conf import settings
from django.utils import timezone
from bo.suds.service import SoapService
from cachetools import cachedmethod
from django.utils.functional import cached_property
from suds import TypeNotFound
from suds.sudsobject import asdict
from urllib3.exceptions import HTTPError
from return_hound.apps.aramex.cache import DjangoCache
from return_hound.models import ShippingItem, User, MatchedItem
from return_hound.apps.logger.plugins import TaggedLogPlugin


log = logging.getLogger('aramex')


class AramexAPIError(Exception):
    """Aramex API exception"""


class AramexBase(SoapService):
    """
    Aramex service base class.
    For common methods and utils. eg: ClientInfo (auth credentials)
    """
    def __init__(self, **kwargs):
        self.account = settings.ARAMEX_ACCOUNTS[self.conf('ACCOUNT', settings.ARAMEX_ACCOUNT)]
        self.version = settings.ARAMEX_VERSION
        self.soap_client = self.soap  # TODO: update methods and remove this alias
        super().__init__(**kwargs)

    @cached_property
    def transport(self):
        http_kw = {
            'cert_reqs': 'CERT_REQUIRED',
            'ssl_version': ssl.PROTOCOL_TLSv1
        }
        return self.transport_class(http_kw=http_kw)

    def e(self, tn, **kw):
        """
        Shortcut to construct a SOAP Element.
        :param str tn: SOAP Type name
        """
        element = self.soap.factory.create(tn)
        for k, v in kw.items():
            if hasattr(element, k):
                setattr(element, k, v)
            else:
                raise Exception('Invalid property "{}" for {}'.format(k, tn))
        return element

    def client_info(self, country_code=None):
        account = self.account.copy()
        account.update(settings.ARAMEX_COUNTRY_ACCOUNTS.get(country_code, {}))

        ci = self.soap.factory.create('ClientInfo')
        ci.Version = self.version
        ci.UserName = account['USER_NAME']
        ci.Password = account['PASSWORD']
        ci.AccountCountryCode = account['COUNTRY_CODE']
        ci.AccountEntity = account['ACCOUNT_ENTITY']
        ci.AccountNumber = account['ACCOUNT_NUMBER']
        ci.AccountPin = account['PIN']
        return ci

    def chargeable_weight(self, length, width, height, unit='KG'):
        weight = self.soap_client.factory.create('Weight')
        weight.Unit = unit
        weight.Value = (0.5 * ceil(2.0 * (float(length * width * height) / 5000)))
        return weight

    def shipment_type(self, origin_country, destination_country, domestic_type):
        """
        Used by Rate, Client Ship client.

        As per Maroot

        For Shipping Client:
        For Domestic Shipments (UAE to UAE), use:
        [ProductGroup] => DOM
        [ProductType] => CDS
        For International Shipments (UAE to Outside), use:
        [ProductGroup] => EXP
        [ProductType] => PPX

        For Rate Calculator Client:
        For Domestic Shipments (UAE to UAE), use:
        [ProductGroup] => DOM
        [ProductType] => ONP
        For International Shipments (UAE to Outside), use:
        [ProductGroup] => EXP
        [ProductType] => PPX

        :param str origin_country: Country code
        :param str destination_country: Country code
        :param str domestic_type: domestic product type
        """
        if origin_country == destination_country:
            shipment_type = {
                'group': 'DOM',
                'group_name': 'Domestic',
                'type': domestic_type,  # rate and shipment product types differ for domestic shipments
                'type_name': 'Priority Parcel Domestic'
            }
        else:
            shipment_type = {
                'group': 'EXP',
                'group_name': 'International Express',
                'type': 'PPX',
                'type_name': 'Priority Parcel Express'
            }
        return shipment_type


class AramexRateClient(AramexBase):
    """
    Facade of the Aramex Rate client. Calculates shipping rates.

    Methods:
        CalculateRate
    """
    plugins = [TaggedLogPlugin(logger='aramex.rate')]
    conf_prefix = 'ARAMEX_RATE_'

    def calculate_rate(self, transaction, origin_address, destination_address, shipment_details):
        """
        Basic calculation method that does not require our model.
        """
        client_info = self.client_info()
        return self.soap.service.CalculateRate(
            client_info, transaction, origin_address, destination_address, shipment_details)

    def get_rate(self, obj: MatchedItem):
        """
        Get shipping Rate for MatchedItem model
        """
        delivery_address = obj.get_delivery_address
        found_item = obj.found_item
        lost_item = obj.lost_item
        hotel = found_item.hotel
        hotel_address = hotel.address

        origin_address = self.e(
            'Address',
            Line1=hotel_address.line1, Line2=hotel_address.line2,
            City=hotel_address.city, CountryCode=hotel_address.country.code2,
            StateOrProvinceCode=hotel_address.region.name if hotel_address.region else '',
            PostCode=hotel_address.post_code)

        destination_address = self.e(
            'Address', Line1=delivery_address.line1, Line2=delivery_address.line2,
            City=delivery_address.city, CountryCode=delivery_address.country.code2,
            StateOrProvinceCode=delivery_address.region.name if delivery_address.region else '',
            PostCode=delivery_address.post_code)

        customs_value = self.e('Money', Value=1, CurrencyCode='AED')  # FIXME: Hardcoded value,currency

        dimensions = self.e('Dimensions', Unit='CM', Height=found_item.height,
                            Length=found_item.length, Width=found_item.width)

        description = '{} - {} {}'.format(found_item.description,
                                          found_item.item_type.name,
                                          found_item.category.name)

        transaction = self.e('Transaction', Reference1=found_item.pk, Reference2=lost_item.pk)

        actual_weight = self.e('Weight', Unit='KG', Value=found_item.weight)
        chargeable_weight = self.chargeable_weight(length=found_item.length,
                                                   width=found_item.width,
                                                   height=found_item.height)

        shipment_type = self.shipment_type(origin_country=hotel.country.code2,
                                           destination_country=delivery_address.country.code2,
                                           domestic_type='ONP')
        shipment_item = self.e(
            'ShipmentItem',
            Quantity=1,
            PackageType=found_item.item_type.name,
            Weight=actual_weight,
            Comments=description,
            Reference=found_item.pk)

        shipment_details = self.e(
            'ShipmentDetails',
            Dimensions=dimensions,
            NumberOfPieces=1,
            ActualWeight=actual_weight,
            ChargeableWeight=chargeable_weight,
            ProductGroup=shipment_type['group'],
            ProductType=shipment_type['type'],
            PaymentType='P',  # Prepaid Type
            DescriptionOfGoods=description,
            GoodsOriginCountry=hotel.country.code2,
            Items=[shipment_item],
            CustomsValueAmount=customs_value)

        try:
            response = self.soap_client.service.CalculateRate(
                self.client_info(country_code=hotel.country.code2),
                transaction, origin_address, destination_address, shipment_details)
        except HTTPError as exc:
            msg = str(exc)
            log.exception(msg)
            self.transport.http.clear()  # clear http connection pool
            raise AramexAPIError(msg)

        # FIXME: we need better exception handling:
        if not hasattr(response, 'HasErrors'):
            raise AramexAPIError

        errors = response.Notifications.Notification if response.HasErrors else []
        return {
            'amount': str(response.TotalAmount.Value),
            'currency': str(response.TotalAmount.CurrencyCode),
            'shipping_type': shipment_type['type_name'],
            'shipping_group': shipment_type['group_name'],
            'errors': errors,
            'has_error': response.HasErrors,
        }


class AramexShippingClient(AramexBase):
    """
    Facade of the Aramex Shipping service.
    Creates shipments and requests a pickup of a shipment.

    Methods:
        CreateShipments
        CreatePickup
        CancelPickup
        PrintLabel
    """
    plugins = [TaggedLogPlugin(logger='aramex.shipping')]
    conf_prefix = 'ARAMEX_SHIP_'

    def contact_type(self, name, company_name, phone_number, email):
        contact = self.soap_client.factory.create('Contact')
        contact.PersonName = name
        contact.CompanyName = company_name
        contact.PhoneNumber1 = phone_number
        contact.CellPhone = phone_number
        contact.EmailAddress = email
        return contact

    def party_type(self, address, contact, reference_1=None, reference_2=None, account_number=None):
        party = self.soap_client.factory.create('Party')
        party.Reference1 = reference_1
        party.Reference2 = reference_2
        party.AccountNumber = account_number
        party.PartyAddress = address
        party.Contact = contact
        return party

    def label_info_type(self):
        label = self.soap_client.factory.create('LabelInfo')
        label.ReportID = 9201
        label.ReportType = 'RPT'
        return label

    def print_label(self, shipment_number, product_group, origin=None, transaction=None):
        label_info = self.label_info_type()
        client_info = self.client_info()
        return self.soap_client.service.PrintLabel(client_info, transaction, shipment_number, product_group, origin, label_info)

    def create_shipment(self, user: User, shipping_item: ShippingItem):
        # instance = shipping_item
        # obj = matched_item
        # assert obj == shipping_item.parcel
        obj = shipping_item.parcel  # MatchedItem
        delivery_address = obj.get_delivery_address
        found_item = obj.found_item
        lost_item = obj.lost_item
        hotel = found_item.hotel
        hotel_address = hotel.address
        # TODO: timezone handling
        now = timezone.now()
        now = now.astimezone(pytz.timezone(settings.TIME_ZONE))
        pickup_datetime = self.get_pickup_datetime(now)
        third_party_address = self.e(
            'Address',
            Line1='9th Floor', Line2='Indigo Tower', Line3='Jumeirah Lake Towers',
            City='Dubai', CountryCode='AE',
            StateOrProvinceCode='UAE',
            PostCode='PO Box 9950')

        origin_address = self.e(
            'Address',
            Line1=hotel_address.line1, Line2=hotel_address.line2,
            City=hotel_address.city, CountryCode=hotel_address.country.code2,
            StateOrProvinceCode=hotel_address.region.name if hotel_address.region else '',
            PostCode=hotel_address.post_code)
        destination_address = self.e(
            'Address',
            Line1=delivery_address.line1, Line2=delivery_address.line2,
            City=delivery_address.city, CountryCode=delivery_address.country.code2,
            StateOrProvinceCode=delivery_address.region.name if delivery_address.region else '',
            PostCode=delivery_address.post_code)

        third_party_contact = self.contact_type(
            name='Smita John',
            company_name='ReturnHound',
            phone_number='+97145126260',
            email='smita.john@returnhound.com')

        delivery_contact = self.contact_type(
            name='{} {}'.format(lost_item.created_by.first_name, lost_item.created_by.last_name),
            company_name='{} {}'.format(lost_item.created_by.first_name, lost_item.created_by.last_name),
            phone_number=lost_item.created_by.contact_number,
            email=lost_item.created_by.email)

        collection_contact = self.contact_type(
            name='{} {}'.format(user.first_name, user.last_name),
            company_name=found_item.hotel.name,
            phone_number=user.contact_number,
            email=user.email)

        customs_value = self.e('Money', Value=1, CurrencyCode='AED')  # FIXME: Hardcoded value,currency

        dimensions = self.e('Dimensions', Unit='CM', Height=found_item.height,
                            Length=found_item.length, Width=found_item.width)

        transaction = self.e('Transaction',
                             Reference1=found_item.pk, Reference2=lost_item.pk)

        # actual_weight = self.weight(found_item.weight)
        actual_weight = self.e('Weight', Unit='KG', Value=found_item.weight)
        chargeable_weight = self.chargeable_weight(length=found_item.length,
                                                   width=found_item.width,
                                                   height=found_item.height)

        description = '{} - {} {}'.format(found_item.description,
                                          found_item.item_type.name,
                                          found_item.category.name)

        # hotel = shipping_item.parcel.found_item.hotel
        shipment_type = self.shipment_type(
            origin_country=hotel.country.code2,
            destination_country=delivery_address.country.code2,
            domestic_type='CDS')

        shipment_item = self.e(
            'ShipmentItem',
            Quantity=1,
            PackageType=found_item.item_type.name,
            Weight=actual_weight,
            Comments=description,
            Reference=found_item.pk)

        shipment_details = self.e(
            'ShipmentDetails',
            Dimensions=dimensions,
            NumberOfPieces=1,
            ActualWeight=actual_weight,
            ChargeableWeight=chargeable_weight,
            ProductGroup=shipment_type['group'],
            ProductType=shipment_type['type'],

            PaymentType='3',
            DescriptionOfGoods=description,
            GoodsOriginCountry=hotel.country.code2,
            Items=[shipment_item],
            CustomsValueAmount=customs_value)

        shipper = self.party_type(origin_address, collection_contact, account_number=self.account['ACCOUNT_NUMBER'])
        consignee = self.party_type(destination_address, delivery_contact)
        third_party = self.party_type(third_party_address, third_party_contact, account_number=self.account['ACCOUNT_NUMBER'])
        shipment = self.e('Shipment',
                          Shipper=shipper,
                          Consignee=consignee,
                          ThirdParty=third_party,
                          ShippingDateTime=now,
                          Reference1=found_item.pk,
                          Reference2=lost_item.pk,
                          PickupLocation='Reception',
                          # Comments=None,
                          # OperationsInstructions=None,
                          # ForeignHAWB=waybill_number,
                          Details=shipment_details)

        shipment_array = self.e('ArrayOfShipment', Shipment=shipment)
        label_info = self.label_info_type()
        client_info = self.client_info()
        volume = dimensions.Length * dimensions.Width * dimensions.Height
        pickup_detail = self.e(
            'PickupItemDetail',
            Payment='P',  # Payment
            ProductGroup=shipment_type['group'],
            ProductType=shipment_type['type'],
            NumberOfShipments=1,
            NumberOfPieces=1,
            ShipmentDimensions=dimensions,
            ShipmentWeight=actual_weight,
            ShipmentVolume=self.e('Volume', Unit='Cm3', Value=volume),
            CashAmount=self.e('Money', Value=0),
            ExtraCharges=self.e('Money', Value=0),
        )
        pickup_item_detail_array = self.e('ArrayOfPickupItemDetail', PickupItemDetail=pickup_detail)
        pickup = self.e('Pickup',
                        Reference1=found_item.pk,
                        Reference2=lost_item.pk,
                        Shipments=shipment_array,
                        PickupAddress=origin_address,
                        PickupContact=collection_contact,
                        PickupLocation='Reception',
                        PickupDate=pickup_datetime.date(),
                        ReadyTime=pickup_datetime.time(),
                        LastPickupTime=time(*settings.ARAMEX_PICKUP_LATEST_TIME),
                        ClosingTime=time(*settings.ARAMEX_PICKUP_HOTEL_CLOSE_TIME),
                        PickupItems=pickup_item_detail_array,
                        Status='Ready')  # Pending or Ready
        response = self.soap.service.CreatePickup(client_info, transaction, pickup, label_info)
        # TODO: better exception handling
        if response.HasErrors:
            errors = ['{n.Code}: {n.Message}'.format(n=n) for n in response.Notifications.Notification]
            log.exception('|'.join(errors))
            raise AramexAPIError({'server_error': errors})
        # TODO: always single shipment?
        shipment = response.ProcessedPickup.ProcessedShipments.ProcessedShipment[0]
        # shipment can also have errors:
        if shipment.HasErrors:
            errors = ['{n.Code}: {n.Message}'.format(n=n) for n in shipment.Notifications.Notification]
            log.exception('|'.join(errors))
            raise AramexAPIError({'server_error': errors})
        return {
            'id': str(shipment.ID),
            'label': b64decode(shipment.ShipmentLabel.LabelFileContents)
        }

    def get_pickup_datetime(self, now: datetime) -> datetime:
        now_date = now.date()
        earliest_ts = now + timedelta(minutes=settings.ARAMEX_LEAD_TIME_MINUTES)
        aramex_closing_time = time(*settings.ARAMEX_PICKUP_LATEST_TIME, tzinfo=now.tzinfo)
        hotel_open_time = time(*settings.ARAMEX_PICKUP_HOTEL_OPEN_TIME, tzinfo=now.tzinfo)
        latest_ts = datetime.combine(now_date, aramex_closing_time)
        if earliest_ts >= latest_ts:
            next_day = now_date + timedelta(days=1)  # TODO: business days. skip weekends
            earliest_ts = datetime.combine(next_day, hotel_open_time)
        return earliest_ts


class AramexLocationClient(AramexBase):
    plugins = [TaggedLogPlugin(logger='aramex.location')]
    conf_prefix = 'ARAMEX_LOCATION_'

    # Aramex error message is not consistent.
    # Use a normalized error message:
    ADDRESS_ERROR = {
        'city': 'Invalid combination of city/zipcode',
        'post_code': 'Invalid combination of city/zipcode',
    }

    def __init__(self):
        super(AramexLocationClient, self).__init__()
        self.cache = DjangoCache('default', timeout=None)  # never expire

    def validate_address(self, country_code, city=None, line_1=None, line_2=None, line_3=None, state_code=None, post_code=None):
        address = self.e(
            'Address',
            Line1=line_1 or '', Line2=line_2, Line3=line_3, City=city,
            StateOrProvinceCode=state_code, CountryCode=country_code, PostCode=post_code)
        try:
            response = self.soap_client.service.ValidateAddress(self.client_info(), None, address)
            if response.HasErrors:
                errors = dict(self.ADDRESS_ERROR)
                for n in response.Notifications.Notification:
                    unxpected_error = not ('City' in n.Message or 'Zip' in n.Message)
                    if unxpected_error:
                        errors['aramex'] = str(n.Message)
                raise AramexAPIError(errors)
            return response
        except TypeNotFound:
            raise AramexAPIError(self.ADDRESS_ERROR)

    @cachedmethod(operator.attrgetter('cache'))
    def get_countries(self):
        resp = self.soap.service.FetchCountries(self.client_info())
        if resp.HasErrors:
            raise AramexAPIError(resp.Notifications.Notification)
        return [{
            'Name': str(c['Name']),
            'Code': str(c['Code']),
            'IsoCode': str(c['IsoCode']),
            'StateRequired': bool(c['StateRequired']),
            'PostCodeRequired': bool(c['PostCodeRequired']),
        } for c in resp.Countries.Country]

    @cachedmethod(operator.attrgetter('cache'))
    def get_country(self, country):
        resp = self.soap.service.FetchCountry(self.client_info(), Transaction=None, Code=country)
        if resp.HasErrors:
            raise AramexAPIError(resp.Notifications.Notification)
        return asdict(resp.Country)

    @cachedmethod(operator.attrgetter('cache'))
    def get_states(self, country):
        resp = self.soap.service.FetchStates(self.client_info(), Transaction=None, CountryCode=country)
        if resp.HasErrors:
            raise AramexAPIError(resp.Notifications.Notification)
        if resp.States:
            return [{'code': str(s.Code).strip(), 'name': str(s.Name)} for s in resp.States.State]
        else:
            return []

    def get_cities(self, country, starts_with=None, state=None):
        """
        Search for cities in a country.

        :param str country: ISO country code like ZA, AE
        :param str starts_with: City name to search
        :param str state: State/Province name
        """
        resp = self.soap.service.FetchCities(self.client_info(), Transaction=None,
                                             CountryCode=country, NameStartsWith=starts_with, State=state)
        if resp.HasErrors:
            raise AramexAPIError(resp.Notifications.Notification)
        if resp.Cities:
            return [str(c) for c in resp.Cities.string]
        else:
            return []
