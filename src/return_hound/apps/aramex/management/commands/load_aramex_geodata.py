from django.core.management.base import BaseCommand
from return_hound.apps.aramex.api import AramexLocationClient
from return_hound.apps.aramex.models import AramexCountry, AramexState


aramex = AramexLocationClient()


def load_country(aramex_country):
    print('{c[Code]}: {c[Name]}'.format(c=aramex_country))
    name = aramex_country['Name']
    code2 = aramex_country['Code']
    code3 = aramex_country['IsoCode']
    ac, created = AramexCountry.objects.get_or_create(code2=code2, defaults={
        'name': name,
        'code3': code3,
        'state_required': aramex_country['StateRequired'],
        'postcode_required': aramex_country['PostCodeRequired'],
    })
    if not created:
        ac.state_required = aramex_country['StateRequired']
        ac.postcode_required = aramex_country['PostCodeRequired']
        ac.save()

    # populate State/Region/Province
    for state in aramex.get_states(country=code2):
        AramexState.objects.get_or_create(
            aramex_country=ac, name=state['name'],
            defaults={'code': state['code']})


class Command(BaseCommand):
    help = 'Load aramex country data'

    def handle(self, *args, **options):
        countries = aramex.get_countries()
        for country in countries:
            load_country(country)
