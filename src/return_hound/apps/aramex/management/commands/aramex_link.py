import os
from functools import partial
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.utils.termcolors import colorize

from return_hound.apps.aramex.models import AramexState
from return_hound.models import Address, Hotel, AramexLocationFields

red = partial(colorize, fg='red')
yellow = partial(colorize, fg='yellow')


class Command(BaseCommand):
    help = 'Link our existing address data to Aramex country/state'

    def get_related(self, obj, rel):
        try:
            return getattr(obj, rel)
        except ObjectDoesNotExist:
            return None

    def find_aramex_state(self, obj):
        aramex_state = self.get_related(obj.old_region, 'aramexstate')
        if aramex_state:
            return aramex_state

        # no simple match found... extend the search
        filters = [
            dict(aramex_country__code2=obj.old_country.code2, name__iexact=obj.old_region.name),
            dict(aramex_country__code2=obj.old_country.code2, aramexcity__name__iexact=obj.city)
        ]
        aramex_state = None
        tries = 1
        for f in filters:
            try:
                aramex_state = AramexState.objects.get(**f)
            except ObjectDoesNotExist:
                aramex_state = None
            if aramex_state:
                break
            else:
                tries += 1

        if not aramex_state:
            if obj.aramex_country.state_required:
                print(red('No (region -> state) match found: {}'.format(obj.old_region)))
                # import ipdb; ipdb.sset_trace()

        if aramex_state and tries > 1:
            print(yellow('Partial match selected: {} -> {}'.format(obj.old_region, aramex_state.name)))
            print('Tries: {}'.format(tries))
        return aramex_state

    def link_aramex_fields(self, model: AramexLocationFields):
        qs = model.objects.filter(aramex_state__isnull=True)
        print('-' * 80)
        print('{} count: {}'.format(model.__name__, qs.count()))
        for obj in qs:
            # country...
            if not obj.old_country:
                print(red('No country on {}'.format(obj)))
                continue

            aramex_country = self.get_related(obj.old_country, 'aramexcountry')
            if aramex_country:
                obj.aramex_country = aramex_country
            else:
                print(red('-') * 80)
                print('No aramex country matched: '.format(obj.old_country))
                print('Object:  {0} pk:{0.pk}'.format(obj))
                print(red('-') * 80)
                # exit()
            obj.aramex_state = self.find_aramex_state(obj)
            obj.save(update_fields=['aramex_country', 'aramex_state'])
        print('Link done...')
        print('No country: {}'.format(qs.filter(aramex_country__isnull=True).count()))
        print('No state: {}'.format(qs.filter(aramex_state__isnull=True).count()))

    def do_reset(self):
        Address.objects.update(aramex_country=None, aramex_state=None)
        Hotel.objects.update(aramex_country=None, aramex_state=None)

    def handle(self, *args, **options):
        if os.getenv('RESET'):
            self.do_reset()
        self.link_aramex_fields(Hotel)
        self.link_aramex_fields(Address)
