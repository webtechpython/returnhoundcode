from django.db import models
from moneyed.classes import CURRENCIES


CURRENCY_CHOICES = [(code, '{}: {.name}'.format(code, ccy)) for code, ccy in sorted(CURRENCIES.items())]


class AramexCountryManager(models.Manager):
    def get_by_natural_key(self, code2):
        return self.get(code2=code2)


class AramexCountry(models.Model):
    objects = AramexCountryManager()

    name = models.CharField(max_length=200)
    code2 = models.CharField(max_length=2)
    code3 = models.CharField(max_length=3)
    state_required = models.BooleanField()
    postcode_required = models.BooleanField()
    currency = models.CharField(max_length=3, blank=True, choices=CURRENCY_CHOICES)

    class Meta:
        verbose_name = 'Aramex Country'
        verbose_name_plural = 'Aramex Countries'
        ordering = ['name']

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.code2, )


class PostalCodeFormat(models.Model):
    aramex_country = models.ForeignKey(AramexCountry, blank=True, null=True, related_name='postal_code_formats')
    code_format = models.CharField(max_length=30, blank=True)


class AramexState(models.Model):
    aramex_country = models.ForeignKey(AramexCountry)
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class AramexLocationFields(models.Model):
    """
    Abstract model base class to add aramex versions of `cities_light` fields.
    Needed to migrate the values from country,region,city.
     see Hotel, Address models
    """
    aramex_country = models.ForeignKey(AramexCountry, blank=True, null=True)
    aramex_state = models.ForeignKey(AramexState, blank=True, null=True)
    # TODO: TextField is overkill:
    line1 = models.TextField()
    line2 = models.TextField(blank=True)
    # line1 = models.CharField(max_length=250)
    # line2 = models.CharField(max_length=250, blank=True)
    city = models.CharField(max_length=100)
    post_code = models.CharField(max_length=15, blank=True)

    class Meta:
        abstract = True
