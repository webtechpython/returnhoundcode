from django_filters import filters, FilterSet
from return_hound.apps.aramex.models import AramexCountry, AramexState
from return_hound import enums, models


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    """
    A `NumberFilter` that uses the `in` look up expression.
    """


class CategoryItemFilter(FilterSet):
    category = NumberInFilter()

    class Meta:
        model = models.CategoryItem
        fields = {'category': ['in']}


class LostItemFilter(FilterSet):
    pk = filters.NumberFilter(name='id')
    matcheditem__found_item = filters.ModelChoiceFilter(queryset=models.FoundItem.objects.filter(is_deleted=False),
                                                        exclude=True)
    category = NumberInFilter()
    item_type = NumberInFilter()

    class Meta:
        model = models.LostItem
        fields = {'category': ['in'], 'created': ['lte', 'gte'], 'pk': ['exact'], 'hotel': ['exact'],
                  'item_type': ['in'], 'matcheditem__found_item': ['exact']}


class FoundItemFilter(FilterSet):
    pk = filters.NumberFilter(name='id')
    matcheditem__lost_item = filters.ModelChoiceFilter(queryset=models.LostItem.objects.filter(is_deleted=False),
                                                       exclude=True)
    category = NumberInFilter()
    item_type = NumberInFilter()

    class Meta:
        model = models.FoundItem
        fields = {'pk': ['exact'], 'category': ['in'], 'created': ['lte', 'gte'], 'date': ['lte', 'gte'],
                  'matcheditem__lost_item': ['exact'], 'item_type': ['in'], 'hotel': ['exact']}


class MatchedItemFilter(FilterSet):
    pk = filters.NumberFilter(name='id')
    found_item__category = NumberInFilter()
    found_item__item_type = NumberInFilter()

    class Meta:
        model = models.MatchedItem
        fields = {
            'created': ['lte', 'gte'], 'pk': ['exact'], 'found_item__category': ['in'], 'found_item__item_type': ['in']}


class ShippingItemFilter(FilterSet):
    parcel__found_item__category = NumberInFilter()
    parcel__found_item__item_type = NumberInFilter()

    class Meta:
        model = models.ShippingItem
        fields = {'parcel__found_item': ['exact'], 'created': ['lte', 'gte'], 'details__collected_date': ['lte', 'gte'],
                  'parcel__found_item__category': ['in'], 'parcel__found_item__item_type': ['in']}


class TransactionReportFilter(FilterSet):
    class Meta:
        model = models.Transaction
        fields = {'created': ['lte', 'gte']}


def filter_template(qs, val):
    return qs.filter(template=getattr(enums.EmailTemplates, val)) if val else qs


class EmailTemplateFilter(FilterSet):
    template = filters.CharFilter(action=filter_template)

    class Meta:
        model = models.EmailTemplate
        fields = ['template']


class ShippingRegionFilter(FilterSet):
    country = filters.ModelChoiceFilter(
        name='aramex_country',
        queryset=AramexCountry.objects.all(), to_field_name='code2')

    class Meta:
        model = AramexState
        fields = ['country']


class HotelFilter(FilterSet):
    country = filters.ModelChoiceFilter(
        name='address__aramex_country',
        queryset=AramexCountry.objects.all(),
        to_field_name='code2',
    )
    region = filters.ModelChoiceFilter(
        name='address__aramex_state',
        queryset=AramexState.objects.all(),
    )

    class Meta:
        model = models.Hotel
        fields = ['country', 'region']
