from rest_framework.pagination import PageNumberPagination


class CustomPageNumberPagination(PageNumberPagination):
    """
    Override to set `page_size_query_param`, allows user to set page size
    """
    page_size_query_param = 'page_size'
    max_page_size = 1000


class HotelPagination(PageNumberPagination):
    """
    Pagination for HotelViewSet.
    """
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 1500
