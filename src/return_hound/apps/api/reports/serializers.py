from rest_framework import serializers
from return_hound import enums
from return_hound.models import ShippingItem
from return_hound.apps.api.serializers import HotelSerializer


class HistoryItemsSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField()
    id = serializers.IntegerField(source='parcel.id')
    found_item = serializers.CharField(source='parcel.found_item.item_type')
    category = serializers.CharField(source='parcel.found_item.category.name')
    collector_name = serializers.CharField(source='details.collector_name')
    is_collection = serializers.BooleanField(source='details.is_collection')
    status = serializers.SerializerMethodField()
    collected_date = serializers.DateTimeField(source='details.collected_date')
    hotel = serializers.CharField(source='parcel.found_item.hotel.name')

    class Meta:
        model = ShippingItem
        fields = ('created', 'id', 'found_item', 'category', 'collector_name', 'status', 'collected_date', 'hotel', 'is_collection')

    def get_status(self, obj):
        return enums.FoundItemStatus(obj.parcel.status).label


class CollectionReadySerializer(serializers.Serializer):
    created = serializers.DateTimeField()
    hotel = serializers.CharField(source='parcel.found_item.hotel.name')
    found_item_id = serializers.IntegerField(source='parcel.found_item.id')
    found_item_name = serializers.CharField(source='parcel.found_item.item_type.name')
    found_item_category = serializers.CharField(source='parcel.found_item.category.name')
    is_collection = serializers.BooleanField(source='details.is_collection')
    collector_name = serializers.CharField(source='details.collector_name')
    is_paid = serializers.BooleanField(source='details.transaction.is_paid')
    is_complete = serializers.BooleanField(source='details.is_complete')


class MatchedItemSerializer(serializers.Serializer):
    created = serializers.DateTimeField()
    modified = serializers.DateTimeField()
    id = serializers.IntegerField()
    lost_item = serializers.CharField(source='lost_item.item_type.name')
    found_item = serializers.CharField(source='found_item.item_type.name')
    hotel = serializers.CharField(source='found_item.hotel.name')
    status_display = serializers.ReadOnlyField(source='get_status_display')


class FoundItemSerializer(serializers.Serializer):
    created = serializers.DateTimeField()
    id = serializers.IntegerField()
    category = serializers.CharField(source='category.name')
    item_type = serializers.CharField(source='item_type.name')
    description = serializers.CharField()
    hotel = HotelSerializer()


