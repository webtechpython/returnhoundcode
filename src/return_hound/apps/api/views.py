from collections import namedtuple

from actstream import action
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpRequest
from django.template.loader import render_to_string
from rest_framework.decorators import detail_route, list_route
from rest_framework.generics import GenericAPIView, RetrieveUpdateAPIView, get_object_or_404
from rest_framework.renderers import StaticHTMLRenderer, BaseRenderer
from rest_framework.response import Response
from rest_framework import status, permissions, mixins, viewsets
from rest_framework.settings import api_settings
from rest_framework_csv.renderers import CSVRenderer

from axes.decorators import is_already_locked, check_request
from bo_drf.views import EnumList
from return_hound import enums

from return_hound.apps.api.filter_backend import FormFilter
from return_hound.apps.api.filter_forms import CityFilterForm
from return_hound.apps.api.paginator import HotelPagination
from return_hound.apps.api import serializers
from return_hound.apps.api import filterset
from return_hound.apps.api.models import Token
from return_hound.apps.api.serializers import CountrySerializer
from return_hound.apps.aramex.api import AramexLocationClient
from return_hound.apps.aramex.models import AramexCountry, AramexState
from return_hound.enums import (FoundItemStatus, EmailTemplates, SocialProviders, MatchedItemStatus, LostItemStatus,
                                ItemValueCurrencies)
from return_hound.models import (Hotel, Category, CategoryItem, LostItem, MatchedItem, FoundItem, Image, SocialLink,
                                 HotelUser, Shipment, ShippingItem, Transaction, Invoice)

from io import BytesIO
from PyPDF2 import PdfFileReader, PdfFileWriter
from weasyprint import HTML


class YammerLoginView(GenericAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.YammerLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
        return Response({'key': token.key.hex}, status=status.HTTP_200_OK)


class ItemValueCurrenciesView(EnumList):
    queryset = ItemValueCurrencies
    pagination_class = None

    def list(self, request, *args,**kwargs):
        print(request.query_params.get('id'))
        hotel = Hotel.objects.get(id=request.query_params.get('hotel'))
        currency_code = hotel.address.get_currency_code
        if currency_code == ItemValueCurrencies.AED.name:
            return Response([self.serializer_class(ItemValueCurrencies.AED).data])
        else:
            return Response([self.serializer_class(ItemValueCurrencies.USD).data])

class FoundItemStatusView(EnumList):
    queryset = FoundItemStatus
    pagination_class = None


class MatchedItemStatusView(EnumList):
    queryset = MatchedItemStatus
    pagination_class = None


class LostItemStatusView(EnumList):
    queryset = LostItemStatus
    pagination_class = None


class EmailTemplateTypeView(EnumList):
    queryset = EmailTemplates
    pagination_class = None


class ExportCSVMixin(object):
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES + [CSVRenderer]

    @property
    def paginator(self):
        fmt = self.request.query_params.get('format', 'json')
        if fmt.lower() == 'csv':
            return None
        return super(ExportCSVMixin, self).paginator


class HotelUserRequiredPermission(permissions.BasePermission):
    """
    Check if user belongs to a hotel.
    """

    def has_permission(self, request, view):
        if not request.user.is_authenticated() or not request.user.is_hotel_user:
            return False
        return True


class HotelAdminRequiredPermission(permissions.BasePermission):
    """
    Check if user is either a hotel admin, supervisor or creator.
    """
    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False
        hotel_user = request.user.get_hotel_user
        return hotel_user.is_admin or hotel_user.is_supervisor or hotel_user.is_creator if hotel_user else False


class HotelCreatorRequiredPermission(permissions.BasePermission):
    """
    Check if a user is the hotel creator.
    """

    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False
        hotel_user = request.user.get_hotel_user
        return hotel_user.is_creator if hotel_user else False


class EnforceHotelAdminPermissionMixin(object):
    """
    Enforces `HotelAdminRequiredPermission` for specific actions.
    """
    def enforce_admin_permission(self, request):
        if not HotelAdminRequiredPermission().has_permission(request, self):
            self.permission_denied(request)


class AmazonS3View(GenericAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.AmazonS3Serializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            data = serializer.save()
            return Response(data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EmailCheckViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.UniqueEmailSerializer


class LoginView(GenericAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.LoginSerializer

    _axes_request = namedtuple('AxesRequest', 'method user GET POST META')

    @staticmethod
    def lockout_response():
        message = 'Your account has been locked for 24 hours! Too many incorrect login attempts. For urgent account reactivation, please send an email to support@returnhound.com'
        return Response({api_settings.NON_FIELD_ERRORS_KEY: [message]}, status=status.HTTP_429_TOO_MANY_REQUESTS)

    def post(self, request, *args, **kwargs):
        """
        Prevents users from authenticating after repeated incorrect login attempts.
        """
        # FIXME: Axes needs raw request compatible object
        raw_request = self._axes_request(
            user=request.user, method='POST',
            META=request.META, POST=request.data, GET={})

        if is_already_locked(raw_request):
            return self.lockout_response()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            token, created = Token.objects.get_or_create(user=serializer.validated_data['user'])
            if check_request(request, login_unsuccessful=False):
                return Response({'key': token.key.hex}, status=status.HTTP_200_OK)
        else:
            if check_request(raw_request, login_unsuccessful=True):
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return self.lockout_response()


class HotelProfileView(EnforceHotelAdminPermissionMixin, RetrieveUpdateAPIView):
    permission_classes = (HotelUserRequiredPermission, )
    serializer_class = serializers.HotelSerializer

    def get_object(self, queryset=None):
        return self.request.user.get_hotel

    def update(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(HotelProfileView, self).update(request, *args, **kwargs)


class SimpleCreateModelMixin(object):
    """
    Simlified version of CreateModelMixin.
    Does not return the validated data on succesful request
    """
    # noinspection PyUnresolvedReferences
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        if isinstance(instance, dict):
            data = {'id': instance['id']} if 'id' in instance else instance
        elif isinstance(instance, Model):
            data = {'id': instance.pk}
        else:
            raise TypeError('Invalid type returned by serializer.save(): {}'.format(instance))
        return Response(data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()


class HotelRegistrationView(SimpleCreateModelMixin, viewsets.GenericViewSet):
    permission_classes = ()
    serializer_class = serializers.HotelRegistrationSerializer


class ReportRegisterView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = ()
    serializer_class = serializers.ReportRegisterSerializer


class HotelUserViewSet(EnforceHotelAdminPermissionMixin, viewsets.ModelViewSet):
    permission_classes = (HotelUserRequiredPermission, )
    serializer_class = serializers.HotelUserSerializer
    filter_fields = ('employer',)

    def get_queryset(self):
        return HotelUser.objects.filter(employer=self.request.user.get_hotel)

    def create(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(HotelUserViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        # Deny permission to normal hotel users for updating following fields or other users' details.
        admin_fields = ['is_admin', 'is_creator', 'employer', 'email', 'is_supervisor', 'is_mail_recipient']
        instance = self.get_object()
        hotel_user = request.user.get_hotel_user
        is_admin_user = hotel_user.is_admin or hotel_user.is_supervisor or hotel_user.is_creator
        if not is_admin_user and (hotel_user != instance or any(field in request.data for field in admin_fields)):
            self.permission_denied(request)
        return super(HotelUserViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(HotelUserViewSet, self).destroy(request, *args, **kwargs)


class HotelReportStyleView(EnforceHotelAdminPermissionMixin, RetrieveUpdateAPIView):
    permission_classes = (HotelUserRequiredPermission, )
    serializer_class = serializers.HotelReportStyleSerializer

    def get_object(self, queryset=None):
        return self.request.user.get_hotel.get_hotel_report_style

    def update(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(HotelReportStyleView, self).update(request, *args, **kwargs)


class HotelReportViewSet(viewsets.GenericViewSet):
    """
    Fetch hotel data and styling config for rebranded hotel specific report-wizard
    """
    permission_classes = (permissions.AllowAny, )
    queryset = Hotel.objects.all()
    serializer_class = serializers.HotelReportSerializer

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        if not obj.slug == request.query_params.get('slug', None):
            raise Http404
        serializer = self.get_serializer(obj)
        return Response(serializer.data)


class ReportWizardViewSet(SimpleCreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.ReportWizardSerializer

    # print (serializer_class)

    # permission_classes = (permissions.AllowAny,)


class MyLostItemViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MyLostItemSerializer

    @detail_route()
    def matched_items(self, request, pk=None):
        """
        Get matched items for lost item
        """
        obj = self.get_object()
        qs = obj.matcheditem_set.exclude(status=MatchedItem.STATUS.declined)
        serializer = serializers.MatchedLostItemSerializer(instance=qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        super().perform_update(serializer)
        data = serializer.validated_data
        lost_item = serializer.instance  # type: LostItem
        action.send(self.request.user, verb='updated', target=lost_item, data=data, public=False)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        MatchedItem.objects.filter(lost_item=instance).update(is_deleted=True, status=MatchedItem.STATUS.declined)
        instance.is_deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        return LostItem.objects.load_related().filter(created_by=self.request.user, is_deleted=False,
                                                      matcheditem__assigned_status=None)


class MatchedItemViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.MatchedLostItemSerializer

    def perform_update(self, serializer):
        super().perform_update(serializer)
        data = serializer.validated_data
        matched_item = serializer.instance  # type: MatchedItem
        action.send(self.request.user, verb='updated', target=matched_item, data=data, public=False)

    @detail_route()
    def shipping_details(self, request, pk=None):
        """
        Get shipping detail for matched item
        """
        obj = self.get_object()
        serializer = serializers.ShipmentItemSerializer(instance=obj.shippingitem_set, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(['get', 'put'], permission_classes=[permissions.AllowAny], lookup_field='token')
    def known_owner_feedback(self, request, pk=None):
        """
        Get matched item with the token field instead of pk, this method is used for the known owner functionality,
        if a hotel knows the owner of an item, they get sent an email with a link to give feedback on the item.
        """
        queryset = MatchedItem.objects.matched().filter(status=MatchedItem.STATUS.pending)
        filter_kwargs = {self.lookup_field: pk}
        obj = get_object_or_404(queryset, **filter_kwargs)
        serializer = serializers.KnownOwnerItemSerializer(instance=obj)
        Token.objects.get_or_create(user=obj.lost_item.created_by)
        if request.method == 'PUT':
            serializer = serializers.KnownOwnerItemSerializer(instance=obj, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        return MatchedItem.objects.matched().filter(lost_item__created_by=self.request.user)


class HotelMatchedItemViewSet(ExportCSVMixin, viewsets.ModelViewSet):
    permission_classes = (HotelAdminRequiredPermission,)
    serializer_class = serializers.HotelMatchedItemSerializer
    filter_class = filterset.MatchedItemFilter
    search_fields = ('lost_item__description', 'found_item__description', 'lost_item__room_number',
                     'found_item__room_number',)

    def perform_update(self, serializer):
        super().perform_update(serializer)
        data = serializer.validated_data
        matched_item = serializer.instance  # type: MatchedItem
        action.send(self.request.user, verb='updated', target=matched_item, data=data, public=False)

    @list_route(methods=['patch'])
    def update_status_multiple(self, request):
        objects = request.data.get('objects')
        new_status = request.data.get('assigned_status')
        status_text = request.data.get('status_text')
        if not objects:
            return Response({'message': 'No item IDs in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not new_status:
            return Response({'message': 'No status set in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not status_text:
            return Response({'message': 'No status text set in response data.'}, status.HTTP_400_BAD_REQUEST)

        items = MatchedItem.objects.filter(found_item__hotel=self.request.user.get_hotel, pk__in=objects)
        if not items:
            return Response({'message': 'Could not find selected items.'}, status.HTTP_400_BAD_REQUEST)
        try:
            count = items.update(assigned_status=enums.MatchedItemStatus(new_status), status_text=status_text)
            # Audit log...
            for item in items: # type: MatchedItem
                data = {'assigned_status': new_status, 'status_text': status_text}
                action.send(self.request.user, verb='updated', target=item, data=data, public=False)
            return Response({'message': 'Successfully updated %d items' % count}, status.HTTP_200_OK)
        except Exception as e:
            return Response({
                'message': 'Could not update selected items.',
                'error': e
            })

    @detail_route()
    def lost_item_address(self, request, pk=None):
        """
        Get the delivery address for an lost item
        """
        obj = self.get_object()
        serializer = serializers.AddressSerializer(obj.lost_item.created_by.get_address)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        qs = MatchedItem.objects.matched().filter(shippingitem__isnull=True, found_item__hotel=self.request.user.get_hotel)
        if 'pk' not in self.kwargs:  # only detail view is allowed to show actioned items (with status)
            qs = qs.filter(assigned_status__isnull=True)
        return qs


class HotelArchivedLostItemSerializer(viewsets.ReadOnlyModelViewSet):
    filter_class = filterset.LostItemFilter
    serializer_class = serializers.HotelArchivedLostItemSerializer
    search_fields = ('description', 'location', 'room_number', 'specification', 'serial_number')

    def get_queryset(self):
        return LostItem.objects.unmatched().filter(
            hotel=self.request.user.get_hotel,
            status__isnull=False)


class HotelArchivedFoundItemViewSet(viewsets.ReadOnlyModelViewSet):
    filter_class = filterset.FoundItemFilter
    serializer_class = serializers.HotelArchivedFoundItemSerializer
    search_fields = ('description', 'location', 'room_number', 'specification', 'serial_number')

    def get_queryset(self):
        return FoundItem.objects.unmatched().filter(
            hotel=self.request.user.get_hotel,
            status__isnull=False)  # FIXME: un-actioned items have null status. WAT???


class HotelArchivedMatchedItemViewSet(viewsets.ReadOnlyModelViewSet):
    filter_class = filterset.MatchedItemFilter
    serializer_class = serializers.HotelArchivedMatchedItemSerializer
    search_fields = ('lost_item__description', 'found_item__description', 'lost_item__room_number',
                     'found_item__room_number',)

    def get_queryset(self):
        return MatchedItem.objects.filter(found_item__hotel=self.request.user.get_hotel,
                                          assigned_status__isnull=False)


class HotelLostItemViewSet(ExportCSVMixin, viewsets.ReadOnlyModelViewSet):
    permission_classes = (HotelAdminRequiredPermission,)
    serializer_class = serializers.HotelLostItemSerializer
    filter_class = filterset.LostItemFilter
    search_fields = ('description', 'specification', 'room_number', 'serial_number',)

    @list_route(methods=['patch'])
    def update_status_multiple(self, request):
        objects = request.data.get('objects')
        new_status = request.data.get('status')
        status_text = request.data.get('status_text')
        if not objects:
            return Response({'message': 'No item IDs in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not new_status:
            return Response({'message': 'No status set in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not status_text:
            return Response({'message': 'No status text set in response data.'}, status.HTTP_400_BAD_REQUEST)

        items = LostItem.objects.filter(hotel=self.request.user.get_hotel, pk__in=objects)
        if not items:
            return Response({'message': 'Could not find selected item.'}, status.HTTP_400_BAD_REQUEST)
        try:
            count = items.update(status=enums.LostItemStatus(new_status), status_text=status_text)
            return Response({'message': 'Successfully updated %d items' % count}, status.HTTP_200_OK)
        except Exception as e:
            return Response({
                'message': 'Could not update selected item.',
                'error': e
            })

    @detail_route()
    def matched_items(self, request, pk=None):
        """
        Method for items matching a specific lost item
        :returns list of matching found items with a search score > x
        """
        obj = self.get_object()
        sqs = serializers.match_lost_items(obj)
        serializer = serializers.ItemMatchSerializer(sqs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_queryset(self):
        qs = LostItem.objects.unmatched().filter(hotel=self.request.user.get_hotel)
        if 'pk' not in self.kwargs:
            qs = qs.filter(status__isnull=-True)
        return qs


class HotelFoundItemViewSet(ExportCSVMixin, viewsets.ModelViewSet):
    permission_classes = (HotelUserRequiredPermission,)
    serializer_class = serializers.HotelFoundItemSerializer
    filter_class = filterset.FoundItemFilter
    search_fields = ('description', 'location', 'room_number', 'specification', 'serial_number')

    def perform_create(self, serializer):
        super().perform_create(serializer)
        data = serializer.validated_data
        found_item = serializer.instance  # type: FoundItem
        action.send(self.request.user, verb='created', target=found_item, data=data, public=False)

    def perform_update(self, serializer):
        super().perform_update(serializer)
        data = serializer.validated_data
        found_item = serializer.instance  # type: FoundItem
        action.send(self.request.user, verb='updated', target=found_item, data=data, public=False)

    @list_route(methods=['patch'])
    def update_status_multiple(self, request):
        objects = request.data.get('objects')
        new_status = request.data.get('status')
        status_text = request.data.get('status_text')
        if not objects:
            return Response({'message': 'No item IDs in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not new_status:
            return Response({'message': 'No status set in request data.'}, status.HTTP_400_BAD_REQUEST)
        if not status_text:
            return Response({'message': 'No status text set in response data.'}, status.HTTP_400_BAD_REQUEST)

        items = FoundItem.objects.filter(hotel=self.request.user.get_hotel, pk__in=objects)
        if not items:
            return Response({'message': 'Could not find selected items.'}, status.HTTP_400_BAD_REQUEST)
        try:
            count = items.update(status=enums.FoundItemStatus(new_status), status_text=status_text)
            # Audit log...
            for item in items: # type: FoundItem
                data = {'assigned_status': new_status, 'status_text': status_text}
                action.send(self.request.user, verb='updated', target=item, data=data, public=False)
            return Response({'message': 'Successfully updated %d items' % count}, status.HTTP_200_OK)
        except Exception as e:
            return Response({
                'message': 'Could not update selected items.',
                'error': e
            })

    @detail_route()
    def matched_items(self, request, pk=None):
        """
        Method for items matching a specific found item
        :returns list of matching found items with a search score > x
        """
        obj = self.get_object()
        sqs = serializers.match_found_items(obj)
        serializer = serializers.ItemMatchSerializer(sqs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        MatchedItem.objects.filter(found_item=instance).update(is_deleted=True, status=MatchedItem.STATUS.declined)
        instance.is_deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        qs = FoundItem.objects.unmatched().filter(hotel=self.request.user.get_hotel)
        if 'pk' not in self.kwargs:  # only detail view is allowed to show actioned items (with status)
            qs = qs.filter(status__isnull=True)
        return qs


class HotelCollectItemViewSet(mixins.UpdateModelMixin, viewsets.ReadOnlyModelViewSet):
    """
    Creates the Aramex shipment on a PATCH (update) request... WAT?
    """
    permission_classes = (HotelAdminRequiredPermission,)
    serializer_class = serializers.HotelCollectionSerializer
    filter_class = filterset.ShippingItemFilter
    search_fields = ('parcel__found_item__description', 'parcel__found_item__location',
                     'parcel__found_item__room_number', 'parcel__found_item__specification',
                     'parcel__found_item__serial_number', 'details__collector_name', 'details__collector_last_name',
                     'details__collector_email')

    def perform_update(self, serializer):
        super().perform_update(serializer)
        data = serializer.validated_data
        shipping_item = serializer.instance  # type: ShippingItem
        action.send(self.request.user, verb='updated', target=shipping_item, data=data, public=False)

    def get_queryset(self):
        qs = ShippingItem.objects.select_related('parcel__found_item', 'parcel__lost_item', 'details')
        return qs.filter(parcel__found_item__hotel=self.request.user.get_hotel, details__is_complete=False).active()


class HotelItemHistoryViewSet(ExportCSVMixin, viewsets.ReadOnlyModelViewSet):
    permission_classes = (HotelUserRequiredPermission,)
    serializer_class = serializers.HotelCollectionSerializer
    filter_class = filterset.ShippingItemFilter
    search_fields = ('parcel__found_item__description', 'parcel__found_item__location',
                     'parcel__found_item__room_number', 'parcel__found_item__specification',
                     'parcel__found_item__serial_number', 'details__collector_name', 'details__collector_last_name',
                     'details__collector_email')

    def get_queryset(self):
        qs = ShippingItem.objects.select_related('parcel__found_item', 'parcel__lost_item', 'details')
        return qs.filter(parcel__found_item__hotel=self.request.user.get_hotel, details__is_complete=True)


class ImageBaseView(mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        return Image.objects.all()


class LostItemImageView(ImageBaseView):
    permission_classes = ()
    serializer_class = serializers.LostItemImageSerializer


class FoundItemImageView(ImageBaseView):
    serializer_class = serializers.FoundItemImageSerializer


class EmailLogoImageView(mixins.UpdateModelMixin, ImageBaseView):
    permission_classes = (HotelUserRequiredPermission, )
    serializer_class = serializers.EmailLogoImageSerializer


class HotelLogoView(ImageBaseView, mixins.UpdateModelMixin):
    serializer_class = serializers.HotelLogoSerializer


class ReportImageView(ImageBaseView, mixins.UpdateModelMixin):
    serializer_class = serializers.ReportImageSerializer


class CategoryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.simple(Category, fields=('id', 'name', ))
    queryset = Category.objects.all()


class CategoryItemViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.simple(CategoryItem, fields=('id', 'name', ))
    queryset = CategoryItem.objects.all()
    filter_class = filterset.CategoryItemFilter


class HotelViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.simple(Hotel, fields=('id', 'name', ))
    queryset = Hotel.objects.filter(is_verified=True, is_hidden=False)
    pagination_class = HotelPagination
    filter_class = filterset.HotelFilter


class CountryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pagination_class = None  # TODO: fix pagination
    permission_classes = (permissions.AllowAny, )
    serializer_class = CountrySerializer
    queryset = AramexCountry.objects.all()
    filter_fields = ['code2']
    search_fields = ['name']


class RegionViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pagination_class = None  # TODO: fix pagination
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.simple(AramexState, ('id', 'name', 'code'))
    queryset = AramexState.objects.all()
    filter_class = filterset.ShippingRegionFilter


class CityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    City names from the Aramex API
    """
    permission_classes = (permissions.AllowAny, )
    pagination_class = None  # TODO: fix pagination
    serializer_class = serializers.AramexCitySerializer
    filter_backends = (FormFilter,)
    filter_form = CityFilterForm

    def get_queryset(self):
        return []

    def filter_with_form(self, request, form, queryset):
        aramex = AramexLocationClient()
        state = form.cleaned_data.get('state')
        state_name = state.name if state else None
        return aramex.get_cities(
            country=form.cleaned_data['country'].code2,
            state=state_name or None,
            starts_with=form.cleaned_data.get('name') or None,
        )


class ShippingAddressValidation(viewsets.GenericViewSet):
    """
    Address validation view for client-side validation...
    Hotel register form (step1) needs to validate the address to display validation errors on the step1 form.
    """
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.AddressSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'valid': True}, status=status.HTTP_200_OK)


class ContactViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.ContactSerializer


class HotelDemoViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.AllowAny, )
    serializer_class = serializers.HotelDemoSerializer


class ShipmentViewSet(mixins.CreateModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.ShipmentSerializer

    def perform_create(self, serializer):
        super().perform_create(serializer)
        data = serializer.validated_data
        shipment = serializer.instance  # type: Shipment
        matched_item = data['parcel']  # type: MatchedItem
        found_item = matched_item.found_item
        action.send(self.request.user, verb='created shipment/collection',
                    action_object=shipment, target=found_item, public=False)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # Audit log...
        for shippingitem in instance.shippingitem_set.all():
            found_item = shippingitem.parcel.found_item
            action.send(self.request.user, verb='deleted shipment', target=found_item, public=False)
        if Transaction.objects.filter(shipping_details=instance).exists() and instance.get_delivery_address:
            instance.transaction.delete()
            instance.get_delivery_address.delete()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        return Shipment.objects.filter(is_complete=False)


class ShippingRateView(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.ShippingRateSerializer

    def get_queryset(self):
        return MatchedItem.objects.filter(status=MatchedItem.STATUS.accepted, is_deleted=False,
                                          lost_item__created_by=self.request.user)


class PaymentViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.PaymentSerializer

    def perform_create(self, serializer):
        super().perform_create(serializer)
        data = serializer.validated_data
        transaction = data['transaction']  # type: Transaction
        shipment = transaction.shipping_details  # type: Shipment
        matched_item = MatchedItem.objects.get(shippingitem__details=shipment)
        found_item = matched_item.found_item
        action.send(self.request.user, verb='created payment',
                    action_object=transaction, target=found_item, public=False)


class PaymentReportViewSet(ExportCSVMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.PaymentReportSerializer
    permission_classes = [HotelCreatorRequiredPermission]
    filter_class = filterset.TransactionReportFilter

    def get_queryset(self):
        user = self.request.user
        hotel = user.get_hotel_user.employer
        shipped_items = Shipment.objects.filter(shippingitem__parcel__found_item__hotel=hotel)
        return Transaction.objects.filter(shipping_details__in=shipped_items)


class PaymentStatusViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.PaymentStatusSerializer
    queryset = Transaction.objects.filter(shipping_details__is_collection=False)

    @detail_route(methods=['put'])
    def charges(self, request, pk=None):
        """
        Adds the service charge and insurance fee for transaction, users can opt out and not pay these charges
        """
        obj = self.get_object()
        serializer = serializers.ChargesSerializer(instance=obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        if not obj.shipping_details.shippingitem_set.filter(parcel__lost_item__created_by=self.request.user).exists():
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        return super(PaymentStatusViewSet, self).retrieve(request, *args, **kwargs)


class EmailTemplateView(EnforceHotelAdminPermissionMixin, viewsets.ModelViewSet):
    permission_classes = (HotelUserRequiredPermission, )
    serializer_class = serializers.EmailTemplateSerializer
    filter_class = filterset.EmailTemplateFilter

    def get_queryset(self):
        return self.serializer_class.Meta.model.objects.filter(hotel=self.request.user.get_hotel)

    def create(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(EmailTemplateView, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(EmailTemplateView, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        self.enforce_admin_permission(request)
        return super(EmailTemplateView, self).destroy(request, *args, **kwargs)


class SocialProviderListView(EnumList):
    queryset = SocialProviders
    pagination_class = None


class SocialLinkView(viewsets.ModelViewSet):
    permission_classes = (HotelUserRequiredPermission,)
    serializer_class = serializers.SocialLinkSerializer

    def get_queryset(self):
        return SocialLink.objects.filter(hotel=self.request.user.get_hotel)


class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.InvoiceSerializer
    permission_classes = (HotelAdminRequiredPermission,)

    def get_queryset(self):
        return self.serializer_class.Meta.model.objects.filter(
            shipment__shippingitem__parcel__found_item__hotel=self.request.user.get_hotel)


class InvoiceItemViewSet(mixins.DestroyModelMixin, viewsets.GenericViewSet):
    permission_classes = (HotelAdminRequiredPermission,)
    serializer_class = serializers.InvoiceItemSerializer

    def get_queryset(self):
        return self.serializer_class.Meta.model.objects.filter(
            invoice__shipment__shippingitem__parcel__found_item__hotel=self.request.user.get_hotel)


class InvoicePDFRenderer(BaseRenderer):
    format = 'pdf'
    media_type = 'application/pdf'
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        view = renderer_context['view']
        request = renderer_context['request']  # type: HttpRequest
        url = reverse('invoice_pdf-detail', kwargs={'pk': view.object.pk})
        abs_url = request.build_absolute_uri(url + '?token=' + request.GET.get('token'))
        invoice_buffer = BytesIO()
        invoice_buffer.seek(0)
        html = HTML(url=abs_url)
        html.write_pdf(target=invoice_buffer)

        if view.object.shipment.label_file.name:
            buffer = BytesIO()
            writer = PdfFileWriter()
            invoice_reader = PdfFileReader(invoice_buffer)
            label_file = view.object.shipment.label_file.file
            label_buffer = BytesIO()
            label_buffer.write(label_file.read())
            label_buffer.seek(0)
            label_reader = PdfFileReader(label_buffer)

            for page_num in range(label_reader.numPages):
                writer.addPage(label_reader.getPage(page_num))

            for page_num in range(invoice_reader.numPages):
                writer.addPage(invoice_reader.getPage(page_num))

            writer.write(buffer)
            buffer.seek(0)

            # cleanup...
            label_file.close()

            # return the combined pages file buffer
            return buffer.getvalue()
        else:
            return invoice_buffer.getvalue()


class InvoicePDF(viewsets.GenericViewSet):
    renderer_classes = [StaticHTMLRenderer, InvoicePDFRenderer]

    def get_queryset(self):
        hotel = self.request.user.get_hotel
        return Invoice.objects.filter(shipment__shippingitem__parcel__found_item__hotel=hotel)

    def retrieve(self, request, *args, **kwargs):
        self.object = self.get_object()
        html = render_to_string('return_hound/invoice_detail.html', {'object': self.object})
        return Response(html)
