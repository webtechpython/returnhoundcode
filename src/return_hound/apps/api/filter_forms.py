from django import forms
from return_hound.apps.aramex.models import AramexCountry, AramexState


class CityFilterForm(forms.Form):
    country = forms.ModelChoiceField(queryset=AramexCountry.objects.all(), to_field_name='code2')
    state = forms.ModelChoiceField(queryset=AramexState.objects.all(), required=False)
    name = forms.CharField(required=True, min_length='3')
