from django.template import loader
from rest_framework.compat import template_render
from rest_framework.filters import BaseFilterBackend


class FormFilter(BaseFilterBackend):
    template = 'api/filter_form.html'

    def get_form(self, request, view):
        formdata = getattr(view, 'filter_initial', {}).copy()
        formdata.update(request.query_params.dict())
        return view.filter_form(formdata)

    def filter_queryset(self, request, queryset, view):
        form = self.get_form(request, view)
        if form.is_valid():
            meth = getattr(view, 'filter_with_form', self.filter_with_form)
            return meth(request, form, queryset)
        return queryset

    def to_html(self, request, queryset, view):
        context = {'form': self.get_form(request, view)}
        template = loader.get_template(self.template)
        return template_render(template, context)

    # noinspection PyUnusedLocal
    def filter_with_form(self, request, form, queryset):
        return queryset.filter(**form.cleaned_data)
