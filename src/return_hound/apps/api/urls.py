from bo_drf.routers import FlexiRouter
from django.conf.urls import url, include
from rest_auth import views as auth_views
from return_hound.apps.api.reports.urls import router as reports_router

from . import views
from . import cms_views


router = FlexiRouter()
router.add(r'^login/$', views.LoginView.as_view(), name='login')
router.add(r'yammer-login', views.YammerLoginView.as_view(), name='yammer_login')
router.add(r'^logout/$', auth_views.LogoutView.as_view(), name='logout')
router.add(r'^reset-password/$', auth_views.PasswordResetView.as_view(), name='reset-password')
router.add(r'^reset-password-confirm/$', auth_views.PasswordResetConfirmView.as_view(), name='reset-password-confirm')
router.add(r'^password-change/$', auth_views.PasswordChangeView.as_view(), name='password-change')
router.add(r'^user/$', auth_views.UserDetailsView.as_view(), name='user')
router.add(r'^hotel-profile/$', views.HotelProfileView.as_view(), name='hotel_profile')
router.add(r'^hotel-report-style/$', views.HotelReportStyleView.as_view(), name='hotel_report_style')
router.register(r'report-wizard', views.ReportWizardViewSet, base_name='report_wizard')
router.register(r'register-hotel', views.HotelRegistrationView, base_name='register_hotel')
router.register(r'report-register', views.ReportRegisterView, base_name='report_register')
router.register(r'my-lost-items', views.MyLostItemViewSet, base_name='my_lost_items')
router.register(r'matched-items', views.MatchedItemViewSet, base_name='matched_items')
router.register(r'hotel-archived-found-items', views.HotelArchivedFoundItemViewSet, base_name='hotel_archived_found_items')
router.register(r'hotel-archived-lost-items', views.HotelArchivedLostItemSerializer, base_name='hotel_archived_lost_items')
router.register(r'hotel-archived-matched-items', views.HotelArchivedMatchedItemViewSet, base_name='hotel_archived_matched_items')
router.register(r'hotel-lost-items', views.HotelLostItemViewSet, base_name='hotel_lost_items')
router.register(r'hotel-found-items', views.HotelFoundItemViewSet, base_name='hotel_found_items')
router.register(r'hotel-matched-items', views.HotelMatchedItemViewSet, base_name='hotel_matched_items')
router.register(r'hotel-collect-items', views.HotelCollectItemViewSet, base_name='hotel_collect_items')
router.register(r'hotel-item-history', views.HotelItemHistoryViewSet, base_name='hotel_item_history')
router.register(r'found-item-image', views.FoundItemImageView, base_name='found_item_image')
router.register(r'lost-item-image', views.LostItemImageView, base_name='lost_item_image')
router.register(r'hotel-logo', views.HotelLogoView, base_name='hotel_logo')
router.register(r'report-image', views.ReportImageView, base_name='report_image')
router.register(r'hotel-users', views.HotelUserViewSet, base_name='hotel_users')
router.register(r'hotel-report', views.HotelReportViewSet, base_name='hotel_report')
router.register(r'hotel-list', views.HotelViewSet, base_name='hotel')
router.register(r'hotel-demo', views.HotelDemoViewSet, base_name='hotel_demo')
# country data:
router.register(r'country', views.CountryViewSet, base_name='country')
router.register(r'region', views.RegionViewSet, base_name='region')
# shipping address APIs:
router.register(r'shipping-country', views.CountryViewSet, base_name='shipping-country')
router.register(r'shipping-region', views.RegionViewSet, base_name='shipping-region')
router.register(r'shipping-city', views.CityViewSet, base_name='shipping-city')
router.register(r'shipping-rate', views.ShippingRateView, base_name='shipping_rate')
router.register(r'shipping-address-validation', views.ShippingAddressValidation, base_name='shipping_address')

# other...
router.register(r'category', views.CategoryViewSet, base_name='category')
router.register(r'category-item', views.CategoryItemViewSet, base_name='category_item')
router.register(r'contact', views.ContactViewSet, base_name='contact')
router.register(r'email-check', views.EmailCheckViewSet, base_name='email_check')
router.register(r'shipment', views.ShipmentViewSet, base_name='shipment')
router.register(r'payment', views.PaymentViewSet, base_name='payment')
router.register(r'payment-report', views.PaymentReportViewSet, base_name='payment_report')
router.register(r'payment-status', views.PaymentStatusViewSet, base_name='payment_status')
router.register(r'found-item-status', views.FoundItemStatusView)
router.register(r'matched-item-status', views.MatchedItemStatusView)
router.register(r'lost-item-status', views.LostItemStatusView)
router.register(r'item-value-currencies', views.ItemValueCurrenciesView)

router.register(r'invoice', views.InvoiceViewSet, base_name='invoice')
router.register(r'invoice-item', views.InvoiceItemViewSet, base_name='invoice_item')
router.register(r'invoice-pdf', views.InvoicePDF, base_name='invoice_pdf')

router.register(r'social-link', views.SocialLinkView, base_name='social_link')
router.register(r'social-providers', views.SocialProviderListView)
router.register(r'email-template', views.EmailTemplateView, base_name='email_template')
router.register(r'email-template-type', views.EmailTemplateTypeView)
router.register(r'email-logo-image', views.EmailLogoImageView, base_name='email_logo')

router.register(r'content', cms_views.ContentViewSet, base_name='content')


urlpatterns = [
    url(r'^amazon-s3/$', views.AmazonS3View.as_view(), name='amazon_s3'),
    url(r'^reports/', include(reports_router.urls, namespace='reports')),
    url(r'^', include(router.urls)),
]