from collections import OrderedDict
from rest_framework.relations import PrimaryKeyRelatedField


class ModelField(PrimaryKeyRelatedField):
    """
    Uses pk/id for input.
    Returns a complex model on read via `read_serializer`
    """
    read_serializer = None

    def __init__(self, read_serializer, **kwargs):
        self.read_serializer = read_serializer
        if hasattr(read_serializer, 'Meta') and hasattr(read_serializer.Meta, 'model'):
            kwargs.setdefault('queryset', read_serializer.Meta.model.objects.all())
        super(ModelField, self).__init__(**kwargs)

    def use_pk_only_optimization(self):
        return False

    def to_internal_value(self, data):
        if isinstance(data, dict):
            pk = data.get('id')  # might receive a blank JSON object eg: {}
            return super(ModelField, self).to_internal_value(pk)
        return super(ModelField, self).to_internal_value(data)

    def to_representation(self, value):
        return self.read_serializer(instance=value).data

    def get_choices(self, cutoff=None):
        """
        XXX: Override to fix rendering of choices with BrowsableAPIRenderer
        """
        queryset = self.get_queryset()
        if queryset is None:
            # Ensure that field.choices returns something sensible
            # even when accessed with a read-only field.
            return {}

        if cutoff is not None:
            queryset = queryset[:cutoff]

        return OrderedDict([
            (
                item.pk,  # NB: We cant use `to_representation` like PrimaryKeyRelatedField
                self.display_value(item)
            )
            for item in queryset
        ])
