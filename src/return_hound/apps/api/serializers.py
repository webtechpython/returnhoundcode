from datetime import timedelta
from functools import partial

from actstream import action
from django.core.files.base import ContentFile
from django.db import transaction
from django.db.models import Q
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.utils import timezone
from haystack.query import SearchQuerySet
from uuid import uuid4
from passwords.validators import LengthValidator, ComplexityValidator
from rest_auth.serializers import PasswordChangeSerializer, PasswordResetConfirmSerializer
from rest_framework import serializers
from return_hound.apps.api.serializer_fields import ModelField
from return_hound.apps.aramex.api import AramexAPIError, AramexRateClient, AramexShippingClient, AramexLocationClient
from return_hound.apps.api.models import Token
from return_hound.apps.aramex.models import AramexCountry, AramexState, PostalCodeFormat
from return_hound.apps.urbanbuz import tasks as urbanbuz
from return_hound.models import (
    User, LostItem, Hotel, HotelUser, Address, MatchedItem, Image, FoundItem, ReportStyle, Category,
    CategoryItem, Shipment, ShippingItem, Transaction, Invoice, InvoiceItem, EmailTemplate, SocialLink, CURRENCY_CHOICES
)
from return_hound import mails
from actstream.models import Action, target_stream
from bo_drf.serializers import EnumSerializer
from yampy import Yammer

import requests
import hashlib
import base64
import hmac
import json
import string,traceback

from return_hound import enums

ALLOWED_PWD_CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789' + string.punctuation
PASSWORD_MIN_LENGTH_VALIDATOR = LengthValidator(min_length=9)
PASSWORD_COMPLEXITY_VALIDATOR = ComplexityValidator(complexities=dict(
    UPPER=1,
    DIGITS=1,
    SPECIAL=1,
))


def get_recaptcha_response(url, data, **kwargs):
    return requests.post(url, data, **kwargs).json()


def recaptcha_validator(value):
    """
    Validates a Google reCaptcha response.
    """
    data = {
        'secret': settings.RECAPTCHA_SECRET_KEY,
        'response': value,
    }
    proxy_settings = getattr(settings, 'PROXY_MAP', None)
    response = get_recaptcha_response('https://www.google.com/recaptcha/api/siteverify', data, proxies=proxy_settings)
    if not response.get('success'):
        error_codes = response.get('error-codes', [])
        conditions = [
            'invalid-input-response' in error_codes,
            'missing-input-response' in error_codes,
        ]
        if any(conditions):
            raise serializers.ValidationError('Please make sure you pass the reCAPTCHA challenge')


class YammerLoginSerializer(serializers.Serializer):
    access_token = serializers.CharField()
    user_id = serializers.IntegerField()

    def validate(self, attrs):
        yammer = Yammer(access_token=attrs['access_token'])
        user_details = yammer.users.find(attrs['user_id'])
        email_address = user_details['email']
        try:
            user = HotelUser.objects.get(employee__email=email_address).employee
        except HotelUser.DoesNotExist:
            hotel = Hotel.objects.filter(domain_name=user_details['network_name']).first()
            if not hotel:
                raise serializers.ValidationError('The domain name does not exist. Could not connect user with hotel.')

            contact_number = next((n for n in user_details['contact']['phone_numbers']), '')
            user = User.objects.filter(email=email_address).first()
            if not user:
                user = User(email=email_address)
                user.set_unusable_password()
            user.contact_number = contact_number
            user.first_name = user_details['first_name']
            user.last_name = user_details['last_name']
            user.terms = True
            user.save()
            HotelUser.objects.create(employee=user, employer=hotel)
        attrs['user'] = user
        return attrs


def match_lost_items(obj):
    """
    Function for matching lost items with found items

    :param obj: LostItem
    :return: list of SearchResult objects
    """
    matched_items = MatchedItem.objects.filter(lost_item__hotel=obj.hotel, status__lt=MatchedItem.STATUS.declined)
    if MatchedItem.objects.filter(lost_item=obj).exists():
        matched_items = MatchedItem.objects.filter(lost_item=obj, status=MatchedItem.STATUS.declined)
    matched_items = matched_items.values_list('found_item__pk', flat=True)
    sqs = SearchQuerySet().models(FoundItem)
    sqs = sqs.filter(category=obj.category.name, hotel=obj.hotel.pk, date__range=[(obj.date - timedelta(days=20)),
                                                                                  (obj.date + timedelta(days=20))],
                     is_deleted=False).more_like_this(obj)
    sqs = [i for i in sqs if i.score > getattr(settings, 'MATCH_SCORE', 30) and i.pk not in matched_items]
    return sqs


def match_found_items(obj):
    """
    Function for matching found items with lost items

    :param obj: FoundItem
    :return: list of SearchResult objects
    """
    matched_items = MatchedItem.objects.filter(found_item__hotel=obj.hotel, status__lt=MatchedItem.STATUS.declined)
    if MatchedItem.objects.filter(found_item=obj).exists():
        matched_items = MatchedItem.objects.filter(found_item=obj, status=MatchedItem.STATUS.declined)
    matched_items = matched_items.values_list('lost_item__pk', flat=True)
    sqs = SearchQuerySet().models(LostItem)
    sqs = sqs.filter(category=obj.category.name, hotel=obj.hotel.pk, date__range=[(obj.date - timedelta(days=20)),
                                                                                  (obj.date + timedelta(days=20))],
                     is_deleted=False).more_like_this(obj)
    sqs = [i for i in sqs if i.score > getattr(settings, 'MATCH_SCORE', 30) and i.pk not in matched_items]
    return sqs


def simple(model_class, fields=(), sources=None, name=None):
    """
    Serializer Factory to use from View so we have less of those simple boilerplate classes.
    For simple serializers that only need a model and list of fields.
    Optionally takes a map like {"fieldname":"dotted.path"} to create readonly fields like these:
    key = serializers.Field(source='auth_token.key')
    """
    name = name or '{model}Serializer'.format(model=model_class.__name__)
    sources = sources or {}
    meta_props = {'model': model_class, 'fields': fields or None}
    class_props = {'Meta': type('Meta', (object,), meta_props)}
    class_props.update({k: serializers.ReadOnlyField(source=v) for k, v in sources.items()})
    return type(name, (serializers.ModelSerializer,), class_props)


class ImageIncludeSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    path = serializers.ReadOnlyField()


class UserIncludeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'contact_number', 'email')
        read_only_fields = fields


class TransactionIncludeSerializer(serializers.ModelSerializer):
    item_value_currency = EnumSerializer(enums.ItemValueCurrencies,
                                         enum_field='value', fields=('name', 'value', 'label'))
    calculated_service_charge = serializers.ReadOnlyField()
    calculated_insurance_fee = serializers.ReadOnlyField()

    class Meta:
        model = Transaction
        fields = ('id', 'is_paid', 'charged_amount', 'payment_date', 'service_charge', 'currency', 'item_value',
                  'insurance_fee', 'calculated_service_charge', 'calculated_insurance_fee', 'item_value_currency')
        read_only_fields = fields


class NotificationSerializer(serializers.ModelSerializer):
    sent_by = serializers.SerializerMethodField()

    def get_sent_by(self, obj: Action):
        if obj.data and 'notification_sender' in obj.data:
            return obj.data['notification_sender']  # FIXME: DEPRECATED. Use Action.actor instead.
        else:
            return getattr(obj.actor, 'name', str(obj.actor))

    class Meta:
        model = Action
        fields = ('timestamp', 'verb', 'sent_by')


class ChangePasswordSerializer(PasswordChangeSerializer):

    new_password1 = serializers.CharField(max_length=128, validators=[
        PASSWORD_MIN_LENGTH_VALIDATOR,
        PASSWORD_COMPLEXITY_VALIDATOR,
    ])

    def save(self):
        super(ChangePasswordSerializer, self).save()
        # Queue the UrbanBuz update
        transaction.on_commit(partial(
            urbanbuz.update_user.delay,
            rh_id=self.set_password_form.user.id,
            password=self.set_password_form.user.password,
        ))


class UniqueEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    is_unique = serializers.ReadOnlyField()

    def create(self, validated_data):
        validated_data.update({
            'is_unique': not User.objects.filter(email=validated_data['email'].lower()).exists()
        })
        return validated_data


class ContactSerializer(serializers.Serializer):
    full_name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    contact_number = serializers.CharField(required=True)
    message = serializers.CharField(required=True)
    recaptcha = serializers.CharField(write_only=True, validators=[recaptcha_validator])

    def create(self, validated_data):
        mails.send_contact_mail(validated_data)
        return validated_data


class HotelDemoSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    full_name = serializers.CharField(required=True)
    demo_date = serializers.DateField(required=False, allow_null=True)
    demo_time = serializers.CharField(required=False, allow_blank=True)
    hotel_name = serializers.CharField(required=True)
    contact_number = serializers.CharField(required=True)

    def create(self, validated_data):
        mails.send_hotel_demo_mail(validated_data)
        return validated_data


class AmazonS3Serializer(serializers.Serializer):
    def get_policy(self):
        expires_at = timezone.now() + timedelta(hours=settings.AWS_S3_SIG_LIFETIME)
        return base64.b64encode(json.dumps({
            "expiration": expires_at.strftime('%Y-%m-%dT%H:%M:00Z'),
            "conditions": [
                {"bucket": settings.AWS_STORAGE_BUCKET_NAME},
                ["starts-with", "$key", ""],
                {"acl": "public-read"},
                ["starts-with", "$Content-Type", ""],
                ["starts-with", "$filename", ""],
                ["content-length-range", 0, 524288000]
            ]
        }).encode('utf-8'))

    def create(self, validated_data):
        policy = self.get_policy()
        aws_secret_key = settings.AWS_SECRET_ACCESS_KEY.encode('utf-8')
        signature = base64.b64encode(hmac.new(aws_secret_key, policy, hashlib.sha1).digest())
        data = {
            'key': settings.AWS_ACCESS_KEY_ID,
            'policy': policy.decode('utf-8'),
            'signature': signature.decode('utf-8'),
            'url': 'https://{}/{}/'.format(settings.AWS_S3_HOST, settings.AWS_STORAGE_BUCKET_NAME),
        }
        return data


class CountrySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    code2 = serializers.CharField(read_only=True)
    name = serializers.CharField(read_only=True)
    postal_codes = serializers.SerializerMethodField()
    state_required = serializers.ReadOnlyField()
    postcode_required = serializers.ReadOnlyField()

    def get_postal_codes(self, obj):
        return [{'code_format': p.code_format} for p in obj.postal_code_formats.all()]

    class Meta:
        model = AramexCountry
        fields = ('id', 'code2', 'name', 'state_required', 'postcode_required', 'postal_codes')


class RegionSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)


class AddressSerializer(serializers.ModelSerializer):
    line1 = serializers.CharField(required=True)
    line2 = serializers.CharField(required=False, allow_blank=True)
    country = ModelField(CountrySerializer,
                         queryset=AramexCountry.objects.all(),
                         source='aramex_country',
                         required=True)
    region = ModelField(RegionSerializer,
                        queryset=AramexState.objects.all(),
                        source='aramex_state',
                        required=False, allow_null=True)
    city = serializers.CharField(required=True, allow_blank=False)
    post_code = serializers.CharField(required=False, allow_blank=True)

    @staticmethod
    def do_address_validation(data):
        client = AramexLocationClient()
        try:
            country = data['aramex_country']
            state = data.get('aramex_state')
            client.validate_address(country_code=country.code2,
                                    state_code=state.name if state else None,
                                    city=data['city'], post_code=data.get('post_code'))
        except AramexAPIError as exc:
            raise serializers.ValidationError(exc.args[0])

    def validate(self, attrs):
        self.do_address_validation(attrs)
        return attrs

    class Meta:
        model = Address
        fields = ('id', 'line1', 'line2', 'post_code', 'city', 'country', 'region')


class AddressUpdateMixin(object):
    """
    Mixin for model serializers with a writable AddressSerializer field.
    eg: Hotel.address and User.address
    """
    def update(self, instance, validated_data):
        addrdata = validated_data.pop('address', None)
        if addrdata:
            if instance.address:
                for k, v in addrdata.items():
                    setattr(instance.address, k, v)
                instance.address.save()
            else:
                instance.address = Address.objects.create(**addrdata)
        return super(AddressUpdateMixin, self).update(instance, validated_data)


class HotelSerializer(AddressUpdateMixin, serializers.ModelSerializer):
    """
    Writable. Used to update the Hotel profile
    """
    number_of_rooms = serializers.IntegerField(allow_null=True, min_value=0)
    address = AddressSerializer()

    class Meta:
        model = Hotel
        fields = ('id', 'slug', 'name', 'domain_name', 'address', 'number_of_rooms')
        read_only_fields = ('id', 'slug')


class HotelUserIncludeSerializer(serializers.ModelSerializer):
    hotel_id = serializers.ReadOnlyField(source='employer.id')
    hotel_name = serializers.ReadOnlyField(source='employer.name')
    # TODO: too risky now. Untangle the user api and frontend persistence first
    # hotel = ModelField(HotelSerializer, source='employer')

    class Meta:
        model = HotelUser
        fields = (
            'id', 'is_admin', 'is_supervisor',
            'hotel_name', 'hotel_id'  # FIXME: just use nested/complex hotel field
        )
        read_only_fields = fields


class ReportRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, write_only=True)
    password = serializers.CharField(max_length=128, write_only=True)
    user = serializers.ReadOnlyField()
    token = serializers.ReadOnlyField()

    def create(self, validated_data):
        return validated_data

    def validate(self, attrs):
        email = attrs['email']
        password = attrs['password']
        if User.objects.filter(email=email).exists():
            user = authenticate(email=email, password=password)
            if not user:
                raise serializers.ValidationError('The email address or password you have entered is incorrect.')
        else:
            PASSWORD_MIN_LENGTH_VALIDATOR(password)
            PASSWORD_COMPLEXITY_VALIDATOR(password)
            user = User.objects.create_user(email=email.lower(), password=password)
        Token.objects.get_or_create(user=user)
        return {'user': user.pk, 'token': user.auth_token.key.hex}


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email').lower()
        password = attrs.get('password')
        user = authenticate(email=email, password=password)
        if user:
            if not user.is_active:
                raise serializers.ValidationError('This account has been disabled')
        else:
            raise serializers.ValidationError('The email address or password you have entered is incorrect.')
        attrs['user'] = user
        return attrs


class PasswordResetSerializer(serializers.Serializer):

    """
    Serializer for requesting a password reset e-mail.
    """

    email = serializers.EmailField(required=True)

    def validate_email(self, value):
        if not User.objects.filter(email=value).exists():
            raise serializers.ValidationError('This email does not belong to a registered user.')
        return value

    def create(self, validated_data):
        mails.send_password_reset_mail(User.objects.get(email=validated_data.get('email')))
        return validated_data


class PasswordConfirmResetSerializer(PasswordResetConfirmSerializer):

    new_password1 = serializers.CharField(max_length=128, validators=[
        PASSWORD_MIN_LENGTH_VALIDATOR,
        PASSWORD_COMPLEXITY_VALIDATOR,
    ])

    def save(self):
        super(PasswordConfirmResetSerializer, self).save()
        # Queue the UrbanBuz update
        transaction.on_commit(partial(
            urbanbuz.update_user.delay,
            rh_id=self.set_password_form.user.id,
            password=self.set_password_form.user.password,
        ))


class UserSerializer(AddressUpdateMixin, serializers.ModelSerializer):
    hotel = serializers.SerializerMethodField('get_hotel_user')
    address = AddressSerializer()
    is_hotel_user = serializers.ReadOnlyField()
    is_hotel_creator = serializers.ReadOnlyField()

    def get_hotel_user(self, obj):
        serializer = HotelUserIncludeSerializer(instance=obj.get_hotel_user)
        return serializer.data

    def update(self, instance, validated_data):
        instance = super(UserSerializer, self).update(instance, validated_data)
        # Queue the UrbanBuz update
        transaction.on_commit(partial(
            urbanbuz.update_user.delay,
            rh_id=instance.id,
            first=instance.first_name,
            last=instance.last_name,
            phone=instance.contact_number,
        ))
        return instance

    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'last_name', 'email', 'contact_number',
            'hotel', 'is_hotel_creator', 'is_hotel_user', 'address'
        )


class ReportWizardSerializer(serializers.Serializer):
    """
    Creates LostItem (default report wizard)
    """
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    contact_number = serializers.CharField(required=True)
    hotel = serializers.PrimaryKeyRelatedField(queryset=Hotel.objects.all())
    date = serializers.DateField(required=True)
    arrival_date = serializers.DateField(required=False, allow_null=True)
    departure_date = serializers.DateField(required=False, allow_null=True)
    location = serializers.CharField(required=False, allow_blank=True)
    room_number = serializers.CharField(required=False, allow_blank=True)
    address = AddressSerializer()
    items = serializers.ListField(required=True)
    recaptcha = serializers.CharField(write_only=True, validators=[recaptcha_validator])


    # print (email,first_name,last_name,contact_number,hotel,date,arrival_date,departure_date,location,room_number,address,items,recaptcha)

    def validate_items(self, value):
        # print ("heree",value)
        if len(value) > 0:
            for item in value:
                if not item.get('category'):
                    raise serializers.ValidationError([{'category': 'This field is required.'}])
                elif not item.get('item_type'):
                    raise serializers.ValidationError([{'item_type': 'This field is required.'}])
                elif not item.get('description'):
                    raise serializers.ValidationError([{'description': 'This field is required.'}])
                # WAT? maybe we should check for valid values... not just any values
                try:
                    cat = Category.objects.get(pk=item['category']['id'])
                    CategoryItem.objects.get(pk=item['item_type']['id'], category=cat)
                    print (cat)

                except Category.DoesNotExist:
                    raise serializers.ValidationError({'category': 'Invalid Category'})
                except CategoryItem.DoesNotExist:
                    raise serializers.ValidationError({'item_type': 'Invalid CategoryItem'})
            print ("Value",value)
            return value
        # print ("heeee")    
        raise serializers.ValidationError(['This field is required.'])

    def create(self, validated_data):
        print (validated_data)
        user = self.context['request'].user
        if not user.is_authenticated():
            try:
                user = User.objects.get(email=validated_data['email'].lower())
            except:
                user = User.objects.create(
                    email=validated_data['email'].lower(),
                    first_name=validated_data['first_name'],
                    last_name=validated_data.get('last_name'),
                    contact_number=validated_data['contact_number'],
                    is_active=False)
        # Update logged in user info...
        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.contact_number = validated_data['contact_number']
        user.address = Address.objects.create(content_object=user, **validated_data['address'])
        user.is_active = True
        user.save()

        # Create LostItem objects...
        for item in validated_data['items']:
            try:
                lost_item = LostItem.objects.create(
                    category=Category.objects.get(pk=item['category']['id']),
                    item_type=CategoryItem.objects.get(pk=item['item_type']['id']),
                    description=item['description'],
                    serial_number=item.get('serial', ''),
                    specification=item.get('specification', ''),
                    created_by=user,
                    hotel=validated_data['hotel'],
                    arrival_date=validated_data.get('arrival_date'),
                    departure_date=validated_data.get('departure_date'),
                    date=validated_data['date'],
                    location=validated_data.get('location'),
                    room_number=validated_data.get('room_number'),
                )
                for image in item.get('images', []):
                    Image.objects.create(
                        object_id=lost_item.pk,
                        path=image['url'],
                        content_type=ContentType.objects.get_for_model(LostItem)
                    )

                # Audit log...
                action.send(user, verb='reported', target=lost_item)

                mails.send_client_item_report_mail(lost_item)
                mails.send_hotel_item_report_mail(lost_item)
            except (Category.DoesNotExist, CategoryItem.DoesNotExist, Hotel.DoesNotExist) as e:
                raise serializers.ValidationError(e.args)
        return {}


class HotelRegistrationSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    domain_name = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField(required=True)
    number_of_rooms = serializers.IntegerField(required=False, allow_null=True, min_value=0)
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=False, allow_blank=True)
    contact_number = serializers.CharField(required=True)
    name = serializers.CharField(required=True)
    address = AddressSerializer()
    terms = serializers.BooleanField(default=False)
    recaptcha = serializers.CharField(write_only=True, validators=[recaptcha_validator])

    def validate_email(self, value):
        if User.objects.filter(email__iexact=value).exists():
            raise serializers.ValidationError('The email address already exists.')
        return value

    def create(self, validated_data):
        # Create User...
        password = User.objects.make_random_password(allowed_chars=ALLOWED_PWD_CHARS)
        user = User.objects.create(
            email=validated_data['email'].lower(),
            first_name=validated_data['first_name'],
            last_name=validated_data.get('last_name'),
            contact_number=validated_data['contact_number'],
            terms=validated_data['terms'],
            is_active=False)
        user.set_password(password)

        # User.address
        addr = validated_data['address']
        user.address = Address.objects.create(**addr)
        user.save()
        # Create Hotel...
        hotel = Hotel.objects.create(
            domain_name=validated_data['domain_name'],
            number_of_rooms=validated_data['number_of_rooms'],
            name=validated_data['name'],
            address=Address.objects.create(**addr)
        )
        hotel_user = HotelUser.objects.create(
            employee=user,
            employer=hotel,
            is_admin=True,
            is_creator=True,
        )
        ReportStyle.objects.create(hotel=hotel)
        # TODO: refactor...
        validated_data.update({
            'id': hotel.pk
        })
        mails.send_hotel_signup_mail(hotel_user)
        mails.send_hotel_signup_admin_mail(hotel_user, password)
        # Create Urban Buz user when registering hotel
        transaction.on_commit(partial(
            urbanbuz.signup_user.delay,
            rh_id=hotel_user.employee.id,
            hotel=hotel_user.employer.id,
            email=hotel_user.employee.email,
            password=user.password,
            first=hotel_user.employee.first_name,
            last=hotel_user.employee.last_name,
            phone=hotel_user.employee.contact_number,
        ))
        return validated_data


class HotelUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(write_only=True, max_length=100)
    last_name = serializers.CharField(required=False, write_only=True, max_length=100, allow_blank=True)
    first_name = serializers.CharField(write_only=True, max_length=100)
    contact_number = serializers.CharField(write_only=True, max_length=100)
    user_last_name = serializers.ReadOnlyField(source='employee.last_name')
    user_first_name = serializers.ReadOnlyField(source='employee.first_name')
    user_contact_number = serializers.ReadOnlyField(source='employee.contact_number')

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError('This email address is in use.')
        return value

    def validate_employer(self, value):
        user = self.context['request'].user
        if not HotelUser.objects.filter(Q(is_admin=True) | Q(is_creator=True), employer=value, employee=user).exists():
            raise serializers.ValidationError('You are not authorised to add users to this hotel.')
        return value

    def validate_is_mail_recipient(self, val):
        if val:  # allow removal of recipients. only enforce a MAX number
            hotel = self.initial_data.get('employer', self.instance and self.instance.employer)
            existing = getattr(self.instance, 'pk', None)  # exclude current instance (update) from count
            recipients = HotelUser.objects.filter(employer=hotel, is_mail_recipient=True).exclude(pk=existing)
            max_users = settings.HOTEL_MAX_MAIL_USERS
            if recipients.count() >= max_users:
                raise serializers.ValidationError('A maximum of {} users has been reached.'.format(max_users))
        return val

    def validate_is_creator(self, val):
        if self.instance and val is False:
            creators = HotelUser.objects.filter(employer=self.instance.employer, is_creator=True)
            if not creators.exclude(pk=self.instance.pk).exists():
                raise serializers.ValidationError('There must be at least one account owner.')
        return val

    def update(self, instance, validated_data):
        if validated_data.get('first_name') or validated_data.get('last_name') or validated_data.get('contact_number'):
            instance.employee.first_name = validated_data.get('first_name', instance.employee.first_name)
            instance.employee.last_name = validated_data.get('last_name', instance.employee.last_name)
            instance.employee.contact_number = validated_data.get('contact_number', instance.employee.contact_number)
            instance.employee.save()
            # Queue the UrbanBuz update
            transaction.on_commit(partial(
                urbanbuz.update_user.delay,
                rh_id=instance.employee.id,
                first=instance.employee.first_name,
                last=instance.employee.last_name,
                phone=instance.employee.contact_number,
            ))
            return instance
        instance = super(HotelUserSerializer, self).update(instance, validated_data)
        mails.send_hotel_user_permissions_mail(instance)
        return instance

    def create(self, validated_data):
        password = User.objects.make_random_password(allowed_chars=ALLOWED_PWD_CHARS)
        user = User.objects.create(
            email=validated_data['email'].lower(),
            first_name=validated_data['first_name'],
            last_name=validated_data.get('last_name'),
            contact_number=validated_data['contact_number'],
            terms=True)
        user.set_password(password)
        user.save()
        hotel_user = HotelUser.objects.create(
            employee=user,
            employer=validated_data['employer'],
            is_admin=validated_data['is_admin'],
            is_supervisor=validated_data['is_supervisor'],
            is_mail_recipient=validated_data.get('is_mail_recipient', False),
            is_creator=False,
        )
        mails.send_hotel_user_created_mail(hotel_user, password)
        # Queue the UrbanBuz user creation
        transaction.on_commit(partial(
            urbanbuz.signup_user.delay,
            rh_id=hotel_user.employee.id,
            hotel=hotel_user.employer.id,
            email=hotel_user.employee.email,
            password=user.password,
            first=hotel_user.employee.first_name,
            last=hotel_user.employee.last_name,
            phone=hotel_user.employee.contact_number,
        ))
        return hotel_user

    class Meta:
        model = HotelUser
        fields = (
            'id', 'created', 'is_admin', 'is_creator', 'employer', 'email', 'first_name', 'last_name',
            'contact_number', 'user_first_name', 'user_last_name', 'is_supervisor', 'user_contact_number',
            'is_mail_recipient',
        )


class HotelReportSerializer(HotelSerializer):
    """Hotel data used on hotel (white-labeled/rebranded) report-wizard"""
    report_style = serializers.SerializerMethodField()

    def get_report_style(self, obj):
        serializer = HotelReportStyleSerializer(instance=obj.get_hotel_report_style)
        return serializer.data

    class Meta(HotelSerializer.Meta):
        fields = list(HotelSerializer.Meta.fields) + ['report_style']
        read_only_fields = fields


class ImageBaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('id', 'path', 'object_id', 'is_deleted')


class LostItemImageSerializer(ImageBaseSerializer):

    def create(self, validated_data):
        validated_data.update({
            'content_type': ContentType.objects.get_for_model(LostItem)
        })
        return super(LostItemImageSerializer, self).create(validated_data)


class FoundItemImageSerializer(ImageBaseSerializer):

    def create(self, validated_data):
        validated_data.update({
            'content_type': ContentType.objects.get_for_model(FoundItem)
        })
        return super(FoundItemImageSerializer, self).create(validated_data)


class EmailLogoImageSerializer(ImageBaseSerializer):

    def create(self, validated_data):
        validated_data.update({
            'content_type': ContentType.objects.get_for_model(EmailTemplate)
        })
        return super(EmailLogoImageSerializer, self).create(validated_data)


class HotelLogoSerializer(ImageBaseSerializer):

    def create(self, validated_data):
        validated_data.update({
            'content_type': ContentType.objects.get_for_model(Hotel)
        })
        return super(HotelLogoSerializer, self).create(validated_data)


class ReportImageSerializer(ImageBaseSerializer):

    def create(self, validated_data):
        validated_data.update({
            'content_type': ContentType.objects.get_for_model(ReportStyle)
        })
        return super(ReportImageSerializer, self).create(validated_data)


class HotelLostItemSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    created_by = UserIncludeSerializer(read_only=True)
    match_count = serializers.SerializerMethodField()
    category = ModelField(simple(Category, fields=('id', 'name')))
    item_type = ModelField(simple(CategoryItem, fields=('id', 'name', 'category')))
    status = EnumSerializer(
        enums.FoundItemStatus, enum_field='value', fields=('name', 'value', 'label'), required=False, allow_blank=True)

    def get_match_count(self, obj):
        sqs = match_lost_items(obj)
        return len(sqs)

    def get_images(self, obj):
        serializer = ImageIncludeSerializer(instance=obj.images.filter(is_deleted=False), many=True)
        return serializer.data

    def validate(self, attrs):
        if attrs.get('status') and not attrs.get('status_text'):
            raise serializers.ValidationError({'status_text': 'Please specify a resolution.'})
        return attrs

    class Meta:
        model = LostItem
        fields = ('id', 'category', 'item_type', 'description', 'serial_number', 'specification', 'images',
                  'created', 'match_count', 'created_by', 'location', 'room_number',
                  'arrival_date', 'departure_date', 'date', 'status', 'status_text')


class MyLostItemSerializer(HotelLostItemSerializer):
    hotel = ModelField(HotelSerializer, required=True)
    notifications = serializers.SerializerMethodField()

    def get_notifications(self, obj):
        serializer = NotificationSerializer(instance=target_stream(obj), many=True)
        return serializer.data

    class Meta:
        model = LostItem
        fields = ('id', 'created_by', 'hotel', 'arrival_date', 'departure_date', 'description', 'specification',
                  'serial_number', 'images', 'location', 'room_number', 'created',  'date', 'notifications',
                  'category', 'item_type')


class PartialLostItemSerializer(HotelLostItemSerializer):
    """
    Remove unnecessary fields which is not required to display on the Hotel matched items view
    """

    class Meta:
        model = LostItem
        fields = ('id', 'created_by', 'arrival_date', 'departure_date', 'description', 'specification',
                  'serial_number', 'images',
                  'location', 'room_number', 'created',  'date', 'category', 'item_type')


class HotelArchivedLostItemSerializer(serializers.ModelSerializer):
    category = ModelField(simple(Category, fields=('id', 'name')))
    item_type = ModelField(simple(CategoryItem, fields=('id', 'name', 'category')))
    status = EnumSerializer(
        enums.LostItemStatus, enum_field='value', fields=('name', 'value', 'label'))

    class Meta:
        model = LostItem
        fields = ('id', 'created', 'category', 'item_type', 'description', 'status', 'status_text')
        read_only_fields = fields


class HotelArchivedFoundItemSerializer(serializers.ModelSerializer):
    category = ModelField(simple(Category, fields=('id', 'name')))
    item_type = ModelField(simple(CategoryItem, fields=('id', 'name', 'category')))
    status = EnumSerializer(
        enums.FoundItemStatus, enum_field='value', fields=('name', 'value', 'label'))

    class Meta:
        model = FoundItem
        fields = ('id', 'created', 'category', 'item_type', 'description', 'status', 'status_text')
        read_only_fields = fields


class HotelFoundItemSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    item_type_name = serializers.ReadOnlyField(source='item_type.name')
    match_count = serializers.SerializerMethodField()
    category_name = serializers.ReadOnlyField(source='category.name')
    status = EnumSerializer(
        enums.FoundItemStatus, enum_field='value', fields=('name', 'value', 'label'), required=False, allow_blank=True)

    def get_images(self, obj):
        serializer = ImageIncludeSerializer(instance=obj.images.filter(is_deleted=False), many=True)
        return serializer.data

    def get_match_count(self, obj):
        sqs = match_found_items(obj)
        return len(sqs)

    def create_lost_and_matched_item(self, obj):
        """
        Method to create a match if the hotel knows who the owner of the item is, this will send an email to the
        created user to confirm or decline the match
        :param obj: FoundItem
        :return: FoundItem
        """
        if MatchedItem.objects.filter(found_item=obj, lost_item__created_by__email__iexact=obj.owner_email).exists():
            raise serializers.ValidationError({'owner_email': ['This item was already matched to this user.']})
        user, user_created = User.objects.get_or_create(email=obj.owner_email.lower())
        password = User.objects.make_random_password(allowed_chars=ALLOWED_PWD_CHARS)
        if user_created:
            user.first_name = obj.owner_name
            user.last_name = obj.owner_surname
            user.contact_number = obj.owner_contact_number
            user.set_password(password)
            user.terms = True
            user.save()
        lost_item = LostItem.objects.create(created_by=user,
                                            hotel=obj.hotel,
                                            category=obj.category,
                                            item_type=obj.item_type,
                                            description=obj.description,
                                            location=obj.location,
                                            date=obj.date,
                                            room_number=obj.room_number,
                                            serial_number=obj.serial_number)
        matched_item = MatchedItem.objects.create(lost_item=lost_item, found_item=obj)
        if obj.owner_send_email:
            mails.send_known_owner_match_mail(matched_item, user_created, password)

        # Audit log... Hotel reported LostItem
        hotel_user = self.context['request'].user
        action.send(hotel_user, verb='reported', target=lost_item)

        return obj

    def validate_hotel(self, value):
        user = self.context['request'].user
        if not HotelUser.objects.filter(employee=user, employer=value).exists():
            raise serializers.ValidationError('You are not authorized to post on this hotels behalf.')
        return value

    def validate(self, attrs):
        if attrs.get('status') == enums.FoundItemStatus.ARCHIVED and not attrs.get('status_text'):
            raise serializers.ValidationError({'status_text': 'Please specify a resolution.'})
        return attrs

    def update(self, instance, validated_data):
        if instance.owner_email != validated_data.get('owner_email') and validated_data.get('owner_email'):
            instance = super(HotelFoundItemSerializer, self).update(instance, validated_data)
            return self.create_lost_and_matched_item(instance)
        return super(HotelFoundItemSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        instance = super(HotelFoundItemSerializer, self).create(validated_data)
        if instance.owner_email:
            return self.create_lost_and_matched_item(instance)
        return instance

    class Meta:
        model = FoundItem
        fields = ('id', 'created', 'created_by', 'category', 'item_type', 'description', 'serial_number', 'hotel',
                  'specification', 'found_by', 'date', 'room_number', 'location', 'owner_name', 'owner_email', 'images',
                  'owner_surname', 'owner_contact_number', 'item_type', 'category_name', 'match_count',
                  'item_type_name', 'width', 'height', 'length', 'weight', 'stored_at', 'status', 'status_text',
                  'owner_send_email')


class FoundItemSerializer(HotelFoundItemSerializer):
    created_by = UserIncludeSerializer(read_only=True)

    class Meta:
        model = FoundItem
        fields = ('id', 'created', 'category', 'item_type', 'description', 'serial_number', 'specification', 'hotel',
                  'found_by', 'date', 'room_number', 'location', 'owner_name', 'owner_email', 'owner_surname',
                  'owner_contact_number', 'item_type', 'category_name', 'item_type_name', 'images', 'width', 'height',
                  'length', 'weight', 'stored_at', 'status', 'status_text', 'created_by')


class MatchedLostItemSerializer(serializers.ModelSerializer):
    found_item = FoundItemSerializer(read_only=True)
    status_display = serializers.ReadOnlyField(source='get_status_display')
    shipment_status = serializers.ReadOnlyField(source='get_shipment_status')
    assigned_status = EnumSerializer(
        enums.MatchedItemStatus, enum_field='value', fields=('name', 'value', 'label'), required=False, allow_blank=True)

    class Meta:
        model = MatchedItem
        fields = ('id', 'created', 'found_item', 'status', 'status_display', 'shipment_status', 'assigned_status', 'status_text')


class KnownOwnerItemSerializer(serializers.ModelSerializer):
    user_token = serializers.ReadOnlyField(source='lost_item.created_by.auth_token.key')
    status_display = serializers.ReadOnlyField(source='get_status_display')

    def update(self, instance, validated_data):
        """
        Clear all owner details and set the user as inactive when a match is declined, the user was created
        by the system, and might not want to user the system at all
        """
        instance = super(KnownOwnerItemSerializer, self).update(instance, validated_data)
        if instance.status == MatchedItem.STATUS.declined:
            instance.found_item.owner_name = ''
            instance.found_item.owner_surname = ''
            instance.found_item.owner_contact_number = ''
            instance.found_item.owner_email = ''
            instance.found_item.save()
            instance.lost_item.is_deleted = True
            instance.lost_item.save()
            instance.lost_item.created_by.is_active = False
            instance.lost_item.created_by.save()
        return instance

    class Meta:
        model = MatchedItem
        fields = ('id', 'created', 'found_item', 'status', 'user_token', 'lost_item', 'status_display')
        read_only_fields = ('found_item', 'lost_item')


class HotelMatchedItemSerializer(serializers.ModelSerializer):
    status_display = serializers.ReadOnlyField(source='get_status_display')
    lost_item_object = PartialLostItemSerializer(source='lost_item', read_only=True)
    found_item_object = FoundItemSerializer(source='found_item', read_only=True)
    assigned_status = EnumSerializer(
        enums.MatchedItemStatus, enum_field='value', fields=('name', 'value', 'label'), required=False, allow_blank=True)

    def create(self, validated_data):
        instance = super(HotelMatchedItemSerializer, self).create(validated_data)
        mails.send_hotel_item_found_mail(instance)
        return instance

    def validate(self, attrs):
        if attrs.get('assigned_status') and not attrs.get('status_text'):
            raise serializers.ValidationError({'status_text': 'Please specify a resolution.'})
        return attrs

    class Meta:
        model = MatchedItem
        fields = ('id', 'created', 'lost_item', 'found_item', 'status', 'status_display', 'lost_item_object',
                  'found_item_object', 'assigned_status', 'status_text')


class HotelArchivedMatchedItemSerializer(serializers.ModelSerializer):
    found_item_category = serializers.ReadOnlyField(source='found_item.category.name')
    found_item_type = serializers.ReadOnlyField(source='found_item.item_type.name')
    found_item_Specification = serializers.ReadOnlyField(source='found_item.specification')
    found_item_description = serializers.ReadOnlyField(source='found_item.description')

    lost_item_category = serializers.ReadOnlyField(source='lost_item.category.name')
    lost_item_type = serializers.ReadOnlyField(source='lost_item.item_type.name')
    lost_item_specification = serializers.ReadOnlyField(source='lost_item.specification')
    lost_item_description = serializers.ReadOnlyField(source='lost_item.description')

    assigned_status = EnumSerializer(
        enums.FoundItemStatus, enum_field='value', fields=('name', 'value', 'label'))

    class Meta:
        model = MatchedItem
        fields = ('id', 'created', 'lost_item', 'found_item', 'assigned_status', 'status_text', 'found_item_category',
                  'found_item_type', 'found_item_Specification', 'found_item_description', 'lost_item_category',
                  'lost_item_type', 'lost_item_specification', 'lost_item_description')
        read_only_fields = fields


class MatchedItemIncludeSerializer(serializers.ModelSerializer):
    created_by = UserIncludeSerializer(source='lost_item.created_by', read_only=True)
    found_item = FoundItemSerializer(read_only=True)

    class Meta:
        model = MatchedItem
        fields = ('id', 'created_by', 'found_item')


class HotelReportStyleSerializer(serializers.ModelSerializer):
    hotel = serializers.ReadOnlyField(source='hotel.pk')
    hotel_logo = serializers.SerializerMethodField()
    cover_images = serializers.SerializerMethodField()

    def get_hotel_logo(self, obj):
        serializer = ImageIncludeSerializer(instance=obj.hotel.logo.all().filter(is_deleted=False), many=True)
        return serializer.data

    def get_cover_images(self, obj):
        serializer = ImageIncludeSerializer(instance=obj.cover_images.filter(is_deleted=False), many=True)
        return serializer.data

    class Meta:
        model = ReportStyle
        fields = ('id', 'cover_images', 'hotel_logo', 'form_label_color', 'form_header_color', 'form_background_color',
                  'form_field_color', 'form_field_border_color', 'form_field_border_radius', 'button_border_radius',
                  'button_primary_color', 'button_primary_text_color', 'button_secondary_color', 'header_color',
                  'button_secondary_text_color', 'header_active_color', 'header_active_text_color', 'show_cover_image',
                  'header_inactive_color', 'header_inactive_text_color', 'hotel', 'icon_color', 'show_hotel_logo')


class ItemMatchSerializer(serializers.Serializer):
    pk = serializers.ReadOnlyField()
    text = serializers.ReadOnlyField()
    date = serializers.ReadOnlyField()
    score = serializers.ReadOnlyField()
    hotel = serializers.ReadOnlyField()
    created = serializers.ReadOnlyField()
    description = serializers.ReadOnlyField()
    serial_number = serializers.ReadOnlyField()
    specification = serializers.ReadOnlyField()
    category = serializers.ReadOnlyField()
    item_type = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    arrival_date = serializers.ReadOnlyField()
    departure_date = serializers.ReadOnlyField()
    room_number = serializers.ReadOnlyField()
    location = serializers.ReadOnlyField()
    last_name = serializers.ReadOnlyField()
    images = serializers.ReadOnlyField()
    email = serializers.ReadOnlyField()
    contact_number = serializers.ReadOnlyField()
    found_by = serializers.ReadOnlyField()


class ShipmentItemSerializer(serializers.ModelSerializer):
    transaction = TransactionIncludeSerializer(source='details.transaction')
    delivery_address = AddressSerializer(source='details.get_delivery_address', read_only=True)

    class Meta:
        model = ShippingItem
        fields = ('parcel', 'transaction', 'details', 'delivery_address')
        depth = 1


class InvoiceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceItem
        exclude = ('created', 'modified')
        extra_kwargs = {
            "id": {"read_only": False, "required": False},
            "quantity": {"read_only": False, "required": True},
        }


class InvoiceItemIncludeSerializer(InvoiceItemSerializer):
    invoice = serializers.ReadOnlyField(source='invoice.pk')


class InvoiceAddressSerializer(AddressSerializer):
    class Meta(AddressSerializer.Meta):
        extra_kwargs = {
            "id": {"read_only": False, "required": True}
        }


class InvoiceSerializer(serializers.ModelSerializer):
    items = InvoiceItemIncludeSerializer(many=True)
    address = InvoiceAddressSerializer()

    class Meta:
        model = Invoice
        exclude = ()

    def create(self, validated_data):
        items = validated_data.pop('items')
        address = validated_data.pop('address')
        instance = Invoice.objects.create(**validated_data)
        items = [InvoiceItem(invoice=instance, **x) for x in items]
        InvoiceItem.objects.bulk_create(items)

        addr_obj = Address.objects.get(pk=address['id'])
        for key, val in address.items():
            setattr(addr_obj, key, val)
        addr_obj.save()
        return instance

    def update(self, instance, validated_data):
        items = validated_data.pop('items')
        address = validated_data.pop('address')
        instance = super(InvoiceSerializer, self).update(instance, validated_data)
        InvoiceItem.objects.bulk_create([InvoiceItem(invoice=instance, **x) for x in items if not x.get('id')])

        addr_obj = Address.objects.get(pk=address['id'])
        for key, val in address.items():
            setattr(addr_obj, key, val)

        addr_obj.save()
        for item in [x for x in items if x.get('id')]:
            inv_item = InvoiceItem.objects.get(invoice=instance, pk=item['id'])
            for key, val in item.items():
                setattr(inv_item, key, val)
            inv_item.save()
        return instance

    def validate(self, attrs):
        if not attrs.get('items'):
            raise serializers.ValidationError('An Invoice needs at least 1 Item')
        return attrs


class ShipmentSerializer(serializers.ModelSerializer):
    parcel = serializers.PrimaryKeyRelatedField(queryset=MatchedItem.objects.filter(is_deleted=False), required=True,
                                                write_only=True)
    transaction = TransactionIncludeSerializer(read_only=True)
    charged_amount = serializers.DecimalField(max_digits=10, decimal_places=2, write_only=True, required=False)
    item_value = serializers.DecimalField(max_digits=10, decimal_places=2, write_only=True)
    item_value_currency = EnumSerializer(enums.ItemValueCurrencies,
                                         enum_field='value', fields=('name', 'value', 'label'), write_only=True)
    service_charge = serializers.DecimalField(max_digits=10, decimal_places=2, write_only=True)
    insurance_fee = serializers.DecimalField(max_digits=10, decimal_places=2, write_only=True)
    currency = serializers.ChoiceField(write_only=True, required=False, choices=CURRENCY_CHOICES)
    delivery_address = AddressSerializer(source='get_delivery_address', read_only=True)
    invoice = InvoiceSerializer(many=False, read_only=True)

    def create(self, validated_data):
        user = self.context['request'].user
        shipment = Shipment.objects.create(collector_name=validated_data.get('collector_name', ''),
                                           collector_last_name=validated_data.get('collector_last_name', ''),
                                           collector_email=validated_data.get('collector_email', ''),
                                           collector_contact_number=validated_data.get('collector_contact_number', ''),
                                           is_collection=validated_data['is_collection'])

        if not shipment.is_collection:
            address = user.get_address
            # create shipment address from user address:
            Address.objects.create(line1=address.line1,
                                   line2=address.line2,
                                   aramex_country=address.aramex_country,
                                   aramex_state=address.aramex_state,
                                   city=address.city,
                                   post_code=address.post_code,
                                   content_object=shipment)
            Transaction.objects.create(shipping_details=shipment,
                                       service_charge=validated_data['service_charge'],
                                       charged_amount=validated_data['charged_amount'],
                                       item_value=validated_data['item_value'],
                                       item_value_currency=validated_data['item_value_currency'],
                                       insurance_fee=validated_data['insurance_fee'],
                                       surcharge_amount=validated_data['charged_amount'],
                                       currency=validated_data['currency'],
                                       is_paid=False)
        shipping_item, created = ShippingItem.objects.get_or_create(parcel=validated_data['parcel'], details=shipment)
        validated_data.update({
            'id': shipment.pk
        })
        mails.send_user_shipment_created_mail(shipping_item)
        mails.send_hotel_shipment_created_mail(shipping_item)
        return shipment

    class Meta:
        model = Shipment
        fields = ('id', 'collector_name', 'collector_last_name', 'collector_email', 'collector_contact_number',
                  'is_collection', 'parcel', 'charged_amount', 'is_complete', 'label_file', 'tracking_number',
                  'transaction', 'collected_date', 'delivery_address', 'invoice', 'currency', 'item_value',
                  'insurance_fee', 'service_charge', 'item_value_currency')
        read_only_fields = ('is_complete', 'tracking_number', 'collected_date', 'label_file', 'invoice')


class HotelCollectionSerializer(serializers.ModelSerializer):
    """
    Creates the Aramex shipment on a PATCH (update) request... WAT?
    """
    parcel = MatchedItemIncludeSerializer()
    details = ShipmentSerializer()

    def update(self, instance: ShippingItem, validated_data):
        if not instance.details.tracking_number and not instance.details.is_collection:
            if not instance.details.transaction.is_paid:
                raise serializers.ValidationError('The shipping is not yet paid by the client.')
            user = self.context['request'].user
            aramex = AramexShippingClient()
            try:
                shipment = aramex.create_shipment(user, shipping_item=instance)
                instance.details.tracking_number = shipment['id']
                instance.details.shipping_date = instance.modified
                instance.details.label_file = ContentFile(shipment['label'], name=shipment['id'] + '.pdf')
                instance.details.save()
                mails.send_shipment_scheduled_mail(instance)
                return instance
            except AramexAPIError as exc:
                raise serializers.ValidationError(exc.args[0])
        instance.details.is_complete = True
        instance.details.collected_date = timezone.now()
        instance.details.save()
        mails.send_shipment_collected_mail(instance)
        return instance

    class Meta:
        model = ShippingItem


class ShippingRateSerializer(serializers.ModelSerializer):
    shipping_rate = serializers.SerializerMethodField()

    def get_shipping_rate(self, obj: MatchedItem):
        aramex = AramexRateClient()
        try:
            aramex_rate = aramex.get_rate(obj)
        except AramexAPIError:
            raise serializers.ValidationError({'server_error': 'Aramex api is offline, please try again later.'})

        # Currencies other than AED should be converted to USD
        currency = aramex_rate['currency']
        if currency != 'AED':
            amount = float(aramex_rate['amount'])
            try:
                conversion_rate = settings.ARAMEX_CONVERSION_RATES[currency]['rate']
            except KeyError:
                raise serializers.ValidationError({
                    'server_error': '{} is an unsupported currency.'.format(currency)
                })
            aramex_rate['amount'] = str(round(amount / conversion_rate, 2))
            aramex_rate['currency'] = 'USD'
        return aramex_rate

    class Meta:
        model = MatchedItem
        fields = ('shipping_rate', )


class PaymentSerializer(serializers.Serializer):
    is_paid = serializers.ReadOnlyField()
    transaction = serializers.PrimaryKeyRelatedField(queryset=Transaction.objects.filter(
        shipping_details__is_complete=False, shipping_details__is_collection=False, is_paid=False))
    charged_amount = serializers.ReadOnlyField()
    transaction_url = serializers.DictField(read_only=True)
    terms = serializers.BooleanField(default=False, write_only=True)

    def validate_transaction(self, value):
        user = self.context['request'].user
        if not value.shipping_details.shippingitem_set.exists():
            raise serializers.ValidationError('There is no items linked to this transaction.')
        elif not value.shipping_details.get_delivery_address:
            raise serializers.ValidationError('Please add an delivery address for this shipment.')
        elif value.get_shipping_item.lost_item.created_by != user:
            raise serializers.ValidationError('This item does not belong to you.')
        return value

    def validate_terms(self, val):
        if not val:
            raise serializers.ValidationError('Please accept the terms and conditions.')
        return val

    def create(self, validated_data):
        user = self.context['request'].user
        cart_id = str(uuid4())
        transaction = validated_data['transaction']  # type: Transaction
        delivery_address = transaction.shipping_details.get_delivery_address  # type: Address
        assert delivery_address is not None
        base_return_url = '{}{}/dashboard/my-reports/{}/payment/'.format(getattr(settings, 'SITE_PROTOCOL'),
                                                                         Site.objects.get_current().domain,
                                                                         transaction.get_shipping_item.lost_item.pk)
        data = {
            'ivp_framed': '1',
            'ivp_method': 'create',
            'ivp_store': getattr(settings, 'TELR_STORE_ID', ''),
            'ivp_authkey': getattr(settings, 'TELR_AUTH_KEY', ''),
            'ivp_currency': transaction.currency,
            'ivp_cart': cart_id,
            'ivp_test': getattr(settings, 'TELR_MODE', ''),
            'ivp_amount': transaction.charged_amount + transaction.service_charge + transaction.insurance_fee,
            'ivp_desc': transaction.get_shipping_item.lost_item.description,
            'bill_email': user.email,
            'bill_fname': user.first_name,
            'bill_sname': user.last_name if user.last_name else user.first_name,
            'bill_addr1': delivery_address.line1,
            'bill_addr2': delivery_address.line2,
            'bill_city': delivery_address.city,
            'bill_region': delivery_address.region.name if delivery_address.region else '',
            'bill_country': delivery_address.country.code2,
            'bill_zip': delivery_address.post_code,
            'return_auth': '{}success/{}/{}'.format(base_return_url, cart_id, transaction.pk),
            'return_can': '{}cancelled/{}/{}'.format(base_return_url, cart_id, transaction.pk),
            'return_decl': '{}failed/{}/{}'.format(base_return_url, cart_id, transaction.pk)
        }
        resp = requests.post(getattr(settings, 'TELR_URL', 'https://secure.telr.com/gateway/order.json'), data=data)
        resp = resp.json()

        if 'error' in resp.keys():
            raise serializers.ValidationError({
                'non_field_errors': resp.get('error').get('note') or resp.get('error').get('message')
            })

        transaction.reference_number = resp.get('order').get('ref')
        transaction.save()
        return {
            'is_paid': transaction.is_paid,
            'transaction': transaction,
            'charged_amount': transaction.charged_amount,
            'transaction_url': resp
        }


class PaymentReportSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField(read_only=True)
    matched_item = serializers.SerializerMethodField(read_only=True)
    tracking_number = serializers.ReadOnlyField(source='shipping_details.tracking_number')

    def get_user(self, obj):
        return UserSerializer(obj.shipping_details.shippingitem_set.first().parcel.lost_item.created_by).data

    def get_matched_item(self, obj):
        return MatchedLostItemSerializer(obj.shipping_details.shippingitem_set.first().parcel).data

    class Meta:
        model = Transaction
        fields = (
            'id', 'is_paid', 'reference_number', 'charged_amount', 'payment_date',
            'user', 'matched_item', 'service_charge', 'tracking_number'
        )


class PaymentStatusSerializer(serializers.ModelSerializer):
    transaction_status = serializers.SerializerMethodField(read_only=True)

    def get_transaction_status(self, obj):
        data = {
            'ivp_method': 'check',
            'ivp_store': getattr(settings, 'TELR_STORE_ID', ''),
            'ivp_authkey': getattr(settings, 'TELR_AUTH_KEY', ''),
            'order_ref': obj.reference_number,
            'ivp_test': getattr(settings, 'TELR_MODE', '1'),
        }
        resp = requests.post(getattr(settings, 'TELR_URL', 'https://secure.telr.com/gateway/order.json'), data=data)
        resp = resp.json()
        if resp.get('order').get('status').get('code') == Transaction.STATUS.paid and not obj.is_paid:
            obj.payment_date = timezone.now()
            obj.is_paid = True
            obj.save()
        return resp

    class Meta:
        model = Transaction
        fields = ('id', 'charged_amount', 'transaction_status', 'is_paid')


class ChargesSerializer(serializers.ModelSerializer):
    """
    Serializer for adding insurance fee and service charge to transactions
    """

    class Meta:
        model = Transaction
        fields = ('id', 'service_charge', 'insurance_fee')


class EmailTemplateSerializer(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField()
    hotel = serializers.ReadOnlyField(source='hotel.pk')
    template = EnumSerializer(enums.EmailTemplates, enum_field='value', fields=('name', 'value', 'label'))

    class Meta:
        model = EmailTemplate
        exclude = ()

    def get_logo(self, obj):
        qs = obj.logo.filter(is_deleted=False)
        return EmailLogoImageSerializer(qs, many=True).data

    def validate(self, attrs):
        attrs.update({'hotel': self.context['request'].user.get_hotel})
        if not attrs.get('template'):
            raise serializers.ValidationError('Please select an email template')
        return attrs


class SocialLinkSerializer(serializers.ModelSerializer):
    provider = EnumSerializer(
        enums.SocialProviders, enum_field='value', fields=('name', 'value', 'label'))

    class Meta:
        model = SocialLink
        exclude = ('hotel',)

    def validate(self, attrs):
        provider = attrs['provider']
        if '{}.com'.format(provider.name.lower()) not in attrs['link']:
            raise serializers.ValidationError('{} is not a valid {} url'.format(attrs['link'], provider.label))
        return super(SocialLinkSerializer, self).validate(attrs)

    def create(self, data):
        hotel = self.context['request'].user.get_hotel
        data.update({'hotel': hotel})
        instance = SocialLink.objects.create(hotel=hotel, provider=data['provider'], link=data['link'])
        return instance


class AramexCitySerializer(serializers.Serializer):
    """
    Aramax client returns a list of names
    """
    def to_representation(self, instance):
        return {'name': instance}
