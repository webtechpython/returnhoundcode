from django.conf.urls import url
from .admin_views import *

urlpatterns = [
    url(r'^hotels/$', HotelsReport.as_view(), name='admin-reports-hotels'),
    url(r'^overview/$', OverviewReport.as_view(), name='admin-reports-overview'),
    url(r'^found-items/$', FoundItemReport.as_view(), name='admin-reports-found-items'),
    url(r'^matched-items/$', MatchedItemReport.as_view(), name='admin-reports-matched-items'),
    url(r'^history-items/$', HistoryItemReport.as_view(), name='admin-reports-history-items'),
    url(r'^payment-items/$', PaymentItemReport.as_view(), name='admin-reports-payment-items'),
    url(r'^collection-ready-items/$', CollectionReadyReport.as_view(), name='admin-reports-collection-ready-items'),
]
