from django.core.urlresolvers import reverse, NoReverseMatch
from return_hound.admin_tools.readonly import ReadOnlyAdmin
from .models import action_description


def obj_url(obj, content_type):
    app_label, model = content_type.app_label, content_type.model
    viewname = 'admin:%s_%s_change' % (app_label, model)
    try:
        link = reverse(viewname, args=[obj.pk])
    except NoReverseMatch:
        return str(obj)
    else:
        return u'<a href="%s">%s</a>' % (link, obj)


class AuditLogAdmin(ReadOnlyAdmin):
    list_display = ['timestamp', 'action_description', 'actor_url', 'verb', 'target_url']
    list_filter = ['timestamp', 'public']
    search_fields = ['description', 'verb']
    fields = ['timestamp', 'description', 'data']

    def actor_url(self, obj):
        return obj_url(obj.actor, obj.actor_content_type)
    actor_url.allow_tags = True
    actor_url.short_description = 'Actor'

    def target_url(self, obj):
        return obj_url(obj.target, obj.target_content_type)
    target_url.allow_tags = True
    target_url.short_description = 'Target'

    def action_description(self, obj):
        return obj.description or action_description(obj)
