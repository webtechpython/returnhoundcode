# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorful.fields
import return_hound.apps.cms.models
import model_utils.fields
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0027_auto_20161108_1731'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUsPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('main_header', models.CharField(max_length=225, blank=True)),
                ('main_text', models.CharField(max_length=225, blank=True)),
                ('section_header', models.CharField(max_length=225, blank=True)),
                ('section_text', models.TextField(null=True, blank=True)),
                ('section_footer_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('line_1', models.CharField(max_length=225, blank=True)),
                ('line_2', models.CharField(max_length=225, blank=True)),
                ('line_3', models.CharField(max_length=225, blank=True)),
                ('po_box', models.CharField(max_length=225, blank=True)),
                ('country', models.CharField(max_length=225, blank=True)),
                ('city', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContactUsPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('header_info_text', models.CharField(max_length=225, blank=True)),
                ('form_header_text', models.CharField(max_length=225, blank=True)),
                ('contact_number', models.CharField(max_length=225, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('address_icon_text', models.CharField(max_length=225, blank=True)),
                ('phone_icon_text', models.CharField(max_length=225, blank=True)),
                ('email_icon_text', models.CharField(max_length=225, blank=True)),
                ('address', models.OneToOneField(null=True, to='cms.Address', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('question', models.CharField(max_length=225, blank=True)),
                ('answer', models.TextField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FaqPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('faqs', models.ForeignKey(null=True, to='cms.Faq', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('about_us_link_text', models.CharField(max_length=255, blank=True)),
                ('how_it_works_link_text', models.CharField(max_length=255, blank=True)),
                ('press_releases_link_text', models.CharField(max_length=255, blank=True)),
                ('faq_link_text', models.CharField(max_length=255, blank=True)),
                ('report_lost_item_link_text', models.CharField(max_length=255, blank=True)),
                ('log_in_link_text', models.CharField(max_length=225, blank=True)),
                ('sign_up_link_text', models.CharField(max_length=225, blank=True)),
                ('contact_us_link_text', models.CharField(max_length=255, blank=True)),
                ('terms_and_conditions_link_text', models.CharField(max_length=255, blank=True)),
                ('site_name_text', models.CharField(max_length=225, blank=True)),
                ('connect_with_us_text', models.CharField(max_length=225, blank=True)),
                ('we_accept_in_aed_text', models.CharField(max_length=225, blank=True)),
                ('copyright_text', models.CharField(max_length=225, blank=True)),
                ('address', models.OneToOneField(null=True, to='cms.Address', blank=True)),
                ('social_links', models.ForeignKey(null=True, to='return_hound.SocialLink', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='Header',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('report_lost_item_link_text', models.CharField(max_length=225, blank=True)),
                ('login_link_text', models.CharField(max_length=225, blank=True)),
                ('hotel_zone_link_text', models.CharField(max_length=255, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='HomePage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('main_header_text', models.CharField(max_length=255, blank=True)),
                ('sub_header_text', models.CharField(max_length=225, blank=True)),
                ('video_header_text', models.CharField(max_length=255, blank=True)),
                ('video_link', models.URLField(blank=True)),
                ('video_background_colour', colorful.fields.RGBColorField(blank=True)),
                ('video_shadow_colour', colorful.fields.RGBColorField(blank=True)),
                ('how_it_works_header_text', models.CharField(max_length=255, blank=True)),
                ('how_it_works_sub_header_text', models.CharField(max_length=225, blank=True)),
                ('how_it_works_background_colour', colorful.fields.RGBColorField(blank=True)),
                ('press_header_text', models.CharField(max_length=255, blank=True)),
                ('press_info_text', models.CharField(max_length=225, blank=True)),
                ('press_comment_text', models.CharField(max_length=225, blank=True)),
                ('press_background_colour', colorful.fields.RGBColorField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='HomePageHowItWorksStep',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('main_text', models.CharField(max_length=225, blank=True)),
                ('info_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HotelZonePage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('main_header_text', models.CharField(max_length=255, blank=True)),
                ('sub_header_text', models.CharField(max_length=225, blank=True)),
                ('video_header_text', models.CharField(max_length=255, blank=True)),
                ('video_link', models.URLField(blank=True)),
                ('video_background_colour', colorful.fields.RGBColorField(blank=True)),
                ('video_shadow_colour', colorful.fields.RGBColorField(blank=True)),
                ('question', models.CharField(max_length=225, blank=True)),
                ('lost_found_header_text', models.CharField(max_length=255, blank=True)),
                ('lost_found_sub_header_text', models.CharField(max_length=255, blank=True)),
                ('lost_found_description', models.TextField(null=True, blank=True)),
                ('lost_found_background_colour', colorful.fields.RGBColorField(blank=True)),
                ('process_header_text', models.CharField(max_length=255, blank=True)),
                ('process_description', models.TextField(null=True, blank=True)),
                ('get_started_header_text', models.CharField(max_length=255, blank=True)),
                ('get_started_description', models.TextField(null=True, blank=True)),
                ('get_started_background_colour', colorful.fields.RGBColorField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='HowItWorksPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('footer_text', models.CharField(max_length=225, blank=True)),
                ('footer_background_color', colorful.fields.RGBColorField(blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='HowItWorksSection',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('background_color', colorful.fields.RGBColorField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HowItWorksStep',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('content', models.TextField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LogInPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('header_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='PressRelease',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('short_description', models.TextField(null=True, blank=True)),
                ('link', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PressReleasePage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('background_colour', colorful.fields.RGBColorField(blank=True)),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('press_releases', models.ForeignKey(null=True, to='cms.PressRelease', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='ReportLostItemPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('header_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='RequestDemoPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('header_text', models.CharField(max_length=225, blank=True)),
                ('info_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='SignUpPage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('header_text', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, return_hound.apps.cms.models.SingleInstanceMixin),
        ),
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('number', models.PositiveSmallIntegerField(unique=True)),
                ('title', models.CharField(max_length=225, blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('author', models.CharField(max_length=225, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='howitworkssection',
            name='steps',
            field=models.ForeignKey(null=True, to='cms.HowItWorksStep', blank=True),
        ),
        migrations.AddField(
            model_name='howitworkspage',
            name='sections',
            field=models.ForeignKey(null=True, to='cms.HowItWorksSection', blank=True),
        ),
        migrations.AddField(
            model_name='hotelzonepage',
            name='testimonials',
            field=models.ForeignKey(null=True, to='cms.Testimonial', blank=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='how_it_works_steps',
            field=models.ForeignKey(null=True, to='cms.HomePageHowItWorksStep', blank=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='testimonials',
            field=models.ForeignKey(null=True, to='cms.Testimonial', blank=True),
        ),
    ]
