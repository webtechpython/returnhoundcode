from celery import shared_task
from return_hound.apps.urbanbuz.service import UrbanBuz


@shared_task
def signup_user(**kwargs):
    client = UrbanBuz()
    client.signup(**kwargs)


@shared_task
def update_user(**kwargs):
    client = UrbanBuz()
    client.user_update(**kwargs)


@shared_task
def award_point(**kwargs):
    client = UrbanBuz()
    client.award_point(**kwargs)
