from django.conf.urls import url
from braces.views import PermissionRequiredMixin, StaffuserRequiredMixin
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic.list import ListView
from django.views.generic.detail import BaseDetailView

from return_hound.apps.api.models import Token
from return_hound.models import HotelUser, Hotel


class LoginAsMixin(PermissionRequiredMixin, StaffuserRequiredMixin):
    permission_required = 'return_hound.login_as'

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return HotelUser.objects.all().order_by('employer')
        else:
            chain_or_brand = Q(brand__users=user) | Q(brand__chain__users=user)
            hotels = Hotel.objects.filter(chain_or_brand)
            return HotelUser.objects.filter(employer__in=hotels).order_by('employer')


class SupervisorLoginList(LoginAsMixin, ListView):
    template_name = 'admin_supervisor/login_list.html'


class SupervisorLoginAsUser(LoginAsMixin, BaseDetailView):
    def get(self, request, *args, **kwargs):
        hoteluser = self.get_object()  # type: HotelUser
        token, created = Token.objects.get_or_create(user=hoteluser.employee)
        return HttpResponseRedirect('/login-as/{key}'.format(key=token.key))


urlpatterns = [
    url(r'^login/$', SupervisorLoginList.as_view(), name='supervisor_login_list'),
    url(r'^login/(?P<pk>\d+)$', SupervisorLoginAsUser.as_view(), name='supervisor_login_as'),
]
