from django.core.management.base import BaseCommand
from return_hound.models import Currency, CountryCurrency
from cities_light.models import Country

from urllib.request import urlopen
import json


class Command(BaseCommand):
    help = 'Create currency entries and link them to a country'

    def handle(self, *args, **options):
        response = urlopen('https://raw.githubusercontent.com/mledoze/countries/master/dist/countries.json')
        json_data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))

        for country in json_data:
            if country.get('currency'):
                currency, created = Currency.objects.get_or_create(code=country.get('currency')[0],
                                                                   defaults={'name': country.get('currency')[0]})
                if Country.objects.filter(code2=country.get('cca2')).exists():
                    CountryCurrency.objects.create(country=Country.objects.get(code2=country.get('cca2')), currency=currency)
