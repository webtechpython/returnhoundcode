# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('aramex', '0001_initial'),
        ('return_hound', '0034_auto_20170118_1825'),
    ]

    operations = [
        migrations.RenameField(
            model_name='address',
            old_name='line_1',
            new_name='line1',
        ),
        migrations.RenameField(
            model_name='address',
            old_name='line_2',
            new_name='line2',
        ),
        migrations.RenameField(
            model_name='address',
            old_name='zip_code',
            new_name='post_code',
        ),
        migrations.RenameField(
            model_name='hotel',
            old_name='zip_code',
            new_name='post_code',
        ),
        migrations.AddField(
            model_name='address',
            name='aramex_country',
            field=models.ForeignKey(blank=True, null=True, to='aramex.AramexCountry'),
        ),
        migrations.AddField(
            model_name='address',
            name='aramex_state',
            field=models.ForeignKey(blank=True, null=True, to='aramex.AramexState'),
        ),
        migrations.AddField(
            model_name='address',
            name='created',
            field=model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False),
        ),
        migrations.AddField(
            model_name='address',
            name='modified',
            field=model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False),
        ),
        migrations.AddField(
            model_name='hotel',
            name='aramex_country',
            field=models.ForeignKey(blank=True, null=True, to='aramex.AramexCountry'),
        ),
        migrations.AddField(
            model_name='hotel',
            name='aramex_state',
            field=models.ForeignKey(blank=True, null=True, to='aramex.AramexState'),
        ),
        migrations.AddField(
            model_name='hotel',
            name='line1',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='hotel',
            name='line2',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='shipment',
            name='hotel',
            field=models.ForeignKey(blank=True, null=True, to='return_hound.Hotel'),
        ),
        migrations.AlterField(
            model_name='address',
            name='content_type',
            field=models.ForeignKey(blank=True, null=True, to='contenttypes.ContentType'),
        ),
        migrations.AlterField(
            model_name='address',
            name='country',
            field=models.ForeignKey(blank=True, null=True, to='cities_light.Country'),
        ),
        migrations.AlterField(
            model_name='address',
            name='object_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='region',
            field=models.ForeignKey(blank=True, null=True, to='cities_light.Region'),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='country',
            field=models.ForeignKey(blank=True, null=True, to='cities_light.Country'),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='region',
            field=models.ForeignKey(blank=True, null=True, to='cities_light.Region'),
        ),
    ]
