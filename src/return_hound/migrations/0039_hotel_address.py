# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0038_auto_20170129_2110'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='address',
            field=models.ForeignKey(to='return_hound.Address', blank=True, null=True),
        ),
    ]
