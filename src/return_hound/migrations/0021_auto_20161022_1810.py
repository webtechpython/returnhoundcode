# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0020_auto_20161021_0929'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='emailtemplate',
            unique_together=set([]),
        ),
    ]
