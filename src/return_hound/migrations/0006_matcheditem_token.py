# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0005_hoteluser_is_supervisor'),
    ]

    operations = [
        migrations.AddField(
            model_name='matcheditem',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, null=True),
        ),
    ]
