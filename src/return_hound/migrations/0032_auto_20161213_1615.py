# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0031_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'permissions': (('reports', 'Can see reports'),)},
        ),
    ]
