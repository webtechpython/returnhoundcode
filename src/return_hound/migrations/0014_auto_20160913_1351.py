# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0013_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hoteluser',
            old_name='correspondence_chain',
            new_name='is_mail_recipient',
        ),
    ]
