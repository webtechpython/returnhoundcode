# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0033_auto_20170103_1642'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hotel',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='hotel',
            name='is_hidden',
            field=models.BooleanField(default=False),
        ),
    ]
