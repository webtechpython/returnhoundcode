# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import return_hound.enums
import enumfields.fields
import colorful.fields
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0015_founditem_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('subject', models.CharField(max_length=255)),
                ('template', enumfields.fields.EnumIntegerField(enum=return_hound.enums.EmailTemplates)),
                ('is_active', models.BooleanField(default=False)),
                ('is_system_default', models.BooleanField(default=False)),
                ('banner_colour', colorful.fields.RGBColorField()),
                ('banner_border_colour', colorful.fields.RGBColorField()),
                ('body_colour', colorful.fields.RGBColorField()),
                ('body_text', models.TextField()),
                ('body_text_colour', colorful.fields.RGBColorField()),
                ('body_text_colour_2', colorful.fields.RGBColorField()),
                ('footer_colour', colorful.fields.RGBColorField()),
                ('footer_text', models.TextField()),
                ('footer_text_colour', colorful.fields.RGBColorField()),
                ('footer_border_colour', colorful.fields.RGBColorField()),
                ('hotel', models.ForeignKey(blank=True, to='return_hound.Hotel', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SocialLink',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('link', models.URLField()),
                ('provider', enumfields.fields.EnumIntegerField(enum=return_hound.enums.SocialProviders)),
                ('is_system_default', models.BooleanField(default=False)),
                ('hotel', models.ForeignKey(to='return_hound.Hotel')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterUniqueTogether(
            name='email',
            unique_together=set([('template', 'hotel')]),
        ),
    ]
