# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0003_founditem_stored_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='service_charge',
            field=models.DecimalField(decimal_places=2, default=18.0, max_digits=10),
            preserve_default=False,
        ),
    ]
