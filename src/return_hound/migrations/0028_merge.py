# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0027_auto_20161108_1731'),
        ('return_hound', '0017_hotel_domain_name'),
    ]

    operations = [
    ]
