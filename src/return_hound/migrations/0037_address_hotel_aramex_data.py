# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.utils.termcolors import colorize
from functools import partial
from django.db import migrations, models


red = partial(colorize, fg='red')


def find_aramex_state(apps, obj):
    AramexState = apps.get_model('aramex.AramexState')
    try:
        aramex_state = AramexState.objects.get(
            aramex_country__code2=obj.old_country.code2,
            name__iexact=obj.old_region.name)
    except ObjectDoesNotExist:
        aramex_state = None
    if not aramex_state:
        if obj.aramex_country.state_required:
            print(red('State required. No state match found for: {} - {}'.format(
                obj.old_country.name,
                obj.old_region.name)))
    return aramex_state


def populate_aramex_fields(apps, model):
    AramexCountry = apps.get_model('aramex.AramexCountry')
    qs = model.objects.filter(aramex_state__isnull=True)
    print('')
    print('-' * 80)
    print('{} objects to process: {}'.format(model.__name__, qs.count()))
    for obj in qs:
        # country...
        if not obj.old_country:
            print(red('No country on {}'.format(obj)))
            continue
        try:
            obj.aramex_country = AramexCountry.objects.get(code2=obj.old_country.code2)
        except ObjectDoesNotExist:
            raise Exception('No aramex country match for: {}'.format(obj.old_country))
        obj.aramex_state = find_aramex_state(apps, obj)
        obj.save(update_fields=['aramex_country', 'aramex_state'])
    print('Aramex data link done... results:')
    print('No country: {}'.format(qs.filter(aramex_country__isnull=True).count()))
    print('No state: {}'.format(qs.filter(aramex_state__isnull=True).count()))


def migrate_data(apps, schema):
    Address = apps.get_model('return_hound.Address')
    Hotel = apps.get_model('return_hound.Hotel')
    populate_aramex_fields(apps, Address)
    populate_aramex_fields(apps, Hotel)
    # populate blank Hotel.line1 from old address field:
    for hotel in Hotel.objects.all():
        if hotel.old_address and not hotel.line1:
            hotel.line1 = hotel.old_address
            hotel.save()


class Migration(migrations.Migration):

    dependencies = [
        ('aramex', '0002_data'),
        ('return_hound', '0036_old_fields_rename'),
    ]

    operations = [
        migrations.RunPython(migrate_data, reverse_code=migrations.RunPython.noop)
    ]
