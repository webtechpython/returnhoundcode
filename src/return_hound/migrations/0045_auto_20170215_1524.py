# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0044_social_links_schema'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='currency',
            field=models.CharField(default='AED', choices=[('AED', 'AED'), ('EGP', 'EGP'), ('USD', 'USD'), ('KWD', 'KWD'), ('SAR', 'SAR'), ('JOD', 'JOD'), ('QAR', 'QAR'), ('BHD', 'BHD'), ('OMR', 'OMR')], max_length=3),
        ),
    ]
