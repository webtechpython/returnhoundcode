# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorful.fields


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0018_auto_20161020_0140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailtemplate',
            name='banner_border_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='banner_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text_colour_2',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_border_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_text',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='footer_text_colour',
            field=colorful.fields.RGBColorField(null=True, blank=True),
        ),
    ]
