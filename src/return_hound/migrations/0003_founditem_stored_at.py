# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0002_auto_20160503_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='founditem',
            name='stored_at',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
