# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.db.models import Count


def migrate_data(apps, schema):
    SocialLink = apps.get_model('return_hound.SocialLink')

    # delete system default links not used in cms (see: cms.Footer.social_links)
    system_links_not_in_footer = SocialLink.objects.filter(hotel__isnull=True, footer__isnull=True)
    system_links_not_in_footer.delete()

    # Remove other duplicate links...
    dupe_links = SocialLink.objects.values('hotel', 'link').annotate(cnt=Count('id')).filter(cnt__gt=1)
    for dup in dupe_links:
        links = SocialLink.objects.filter(link=dup['link'])
        last_created_link = links.order_by('-pk')[:1][0]
        links.exclude(pk=last_created_link.pk).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0042_user_address_data'),
    ]

    operations = [
        migrations.RunPython(migrate_data, reverse_code=migrations.RunPython.noop)
    ]
