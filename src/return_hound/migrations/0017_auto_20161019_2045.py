# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import colorful.fields
import return_hound.enums
import enumfields.fields
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0016_auto_20161019_1727'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', default=django.utils.timezone.now, editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', default=django.utils.timezone.now, editable=False)),
                ('subject', models.CharField(max_length=255)),
                ('template', enumfields.fields.EnumIntegerField(enum=return_hound.enums.EmailTemplates)),
                ('is_active', models.BooleanField(default=False)),
                ('is_system_default', models.BooleanField(default=False)),
                ('banner_colour', colorful.fields.RGBColorField()),
                ('banner_border_colour', colorful.fields.RGBColorField()),
                ('body_colour', colorful.fields.RGBColorField()),
                ('body_text', models.TextField()),
                ('body_text_colour', colorful.fields.RGBColorField()),
                ('body_text_colour_2', colorful.fields.RGBColorField()),
                ('footer_colour', colorful.fields.RGBColorField()),
                ('footer_text', models.TextField()),
                ('footer_text_colour', colorful.fields.RGBColorField()),
                ('footer_border_colour', colorful.fields.RGBColorField()),
                ('hotel', models.ForeignKey(null=True, blank=True, to='return_hound.Hotel')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='email',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='email',
            name='hotel',
        ),
        migrations.DeleteModel(
            name='Email',
        ),
        migrations.AlterUniqueTogether(
            name='emailtemplate',
            unique_together=set([('template', 'hotel')]),
        ),
    ]
