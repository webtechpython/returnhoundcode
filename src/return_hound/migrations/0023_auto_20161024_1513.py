# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0022_auto_20161023_0923'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='emailtemplate',
            options={'ordering': ['-modified']},
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body_text',
            field=models.TextField(blank=True, null=True),
        ),
    ]
