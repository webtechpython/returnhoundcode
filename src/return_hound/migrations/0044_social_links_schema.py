# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('return_hound', '0043_social_link_data_fix'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='sociallink',
            unique_together=set([('hotel', 'provider')]),
        ),
        migrations.RemoveField(
            model_name='sociallink',
            name='is_system_default',
        ),
    ]
