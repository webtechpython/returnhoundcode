from actstream.models import Action
from django import forms
from django.contrib import admin
from django.contrib.auth.forms import UserChangeForm as DefaultUserChangeForm
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url
from django.contrib.auth.decorators import permission_required
from import_export.admin import ImportMixin
from import_export.formats import base_formats
from salmonella.admin import SalmonellaMixin

from return_hound import models
from return_hound.admin_tools.readonly import ReadOnlyAdmin
from return_hound.admin_tools.utils import admin_replace
from return_hound.apps.auditlog.admin import AuditLogAdmin
from .resources import FoundItemModelResource
from .views import AdminEmailPreviewView

from authtools.admin import UserAdmin


class UserChangeForm(DefaultUserChangeForm):
    class Meta:
        model = models.User
        fields = ("email", "first_name", "last_name", "contact_number")


class UserAddForm(forms.ModelForm):
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput,
                                help_text=_("Enter the same password as above, for verification."))

    def __init__(self, *args, **kwargs):
        super(UserAddForm, self).__init__(*args, **kwargs)

        for key in self.fields:
            self.fields[key].required = True

    def clean_email(self):
        email = self.cleaned_data.get("email").lower()
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super(UserAddForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get("password1"))

        if commit:
            user.save()
        return user

    class Meta:
        model = models.User
        fields = ("email", "first_name", "last_name", "contact_number")


class SocialLinkInline(admin.TabularInline):
    model = models.SocialLink
    fields = ['link', 'provider']


@admin.register(models.User)
class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserAddForm

    fieldsets = (
        (_('Personal info'), {'fields': ("email", "first_name", "last_name", "contact_number", "password")}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ("email", "first_name", "last_name", "contact_number", "password1", "password2")}
         ),
    )


@admin.register(models.HotelBrand)
class HotelBrandAdmin(admin.ModelAdmin):
    list_display = ['name', 'chain', 'hotel_count', 'user_count']
    list_filter = ['chain']
    filter_horizontal = ['users']
    search_fields = ['name']

    def hotel_count(self, obj):
        return models.Hotel.objects.filter(brand=obj).count()

    def user_count(self, obj):
        return obj.users.count()


@admin.register(models.HotelChain)
class HotelChainAdmin(admin.ModelAdmin):
    list_display = ['name', 'hotel_count', 'user_count']
    filter_horizontal = ['users']
    search_fields = ['name']

    def hotel_count(self, obj):
        return models.Hotel.objects.filter(brand__chain=obj).count()

    def user_count(self, obj):
        return obj.users.count()


@admin.register(models.Hotel)
class HotelAdmin(SalmonellaMixin, admin.ModelAdmin):
    list_display = ['name', 'id', 'created', 'is_deleted', 'is_verified', 'is_hidden', 'brand', 'chain']
    list_filter = ['is_verified', 'is_hidden', 'brand', 'brand__chain', 'address__aramex_country']
    search_fields = ['name']
    fields = [
        'is_verified', 'is_hidden',
        'brand', 'name', 'domain_name',
        'number_of_rooms',
        'address',
    ]
    salmonella_fields = ['address']


@admin.register(models.HotelUser)
class HotelUserAdmin(admin.ModelAdmin):
    list_display = ['employee', 'employer', 'created', 'is_admin', 'is_creator']
    list_filter = ['is_creator', 'is_admin', 'employer']
    search_fields = ['employee__email']


class CategoryItemInline(admin.StackedInline):
    model = models.CategoryItem
    extra = 1

    fieldsets = (
        (None, {
            'fields': ('name', 'is_deleted',)
        }),
    )

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'id', 'created', 'is_deleted']
    list_filter = ['is_deleted']
    inlines = [CategoryItemInline]

    fieldsets = (
        (None, {
            'fields': ('name', 'is_deleted',)
        }),
    )


@admin.register(models.CategoryItem)
class CategoryItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'id']


@admin.register(models.LostItem)
class LostItemAdmin(admin.ModelAdmin):
    list_display = ['created_by', 'created', 'hotel', 'is_deleted']
    list_filter = ['is_deleted', 'hotel']


@admin.register(models.FoundItem)
class FoundItemAdmin(ImportMixin, LostItemAdmin):
    formats = [base_formats.CSV]
    resource_class = FoundItemModelResource


@admin.register(models.MatchedItem)
class MatchedItemAdmin(admin.ModelAdmin):
    list_display = ['lost_item', 'found_item', 'created', 'get_status_display', 'is_deleted', 'token']
    list_filter = ['is_deleted', 'status']


@admin.register(models.ShippingItem)
class ShippingItemAdmin(admin.ModelAdmin):
    list_display = ['parcel', 'created', 'is_deleted']
    list_filter = ['details__is_collection', 'details__is_complete']


class InvoiceItemInline(admin.TabularInline):
    model = models.InvoiceItem


@admin.register(models.Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'hotel', 'tracking_number', 'tax_rate', 'sub_total', 'total'
    ]
    inlines = [InvoiceItemInline]


@admin.register(models.EmailTemplate)
class EmailTemplateAdmin(admin.ModelAdmin):
    list_display = ['hotel', 'template', 'subject', 'is_active']

    def get_urls(self):
        urls = [
            url(
                r'^preview/(?P<pk>\d+)/$',
                permission_required('is_staff')(AdminEmailPreviewView.as_view()),
                name="email_preview"
            ),
        ]
        return urls + super(EmailTemplateAdmin, self).get_urls()


@admin.register(models.SocialLink)
class SocialLinkAdmin(SalmonellaMixin, admin.ModelAdmin):
    list_display = ['id', 'hotel', 'system_default', 'provider', 'link']
    list_filter = [
        'provider',
        ('hotel', admin.RelatedOnlyFieldListFilter)
    ]
    salmonella_fields = ['hotel']
    fields = ['hotel', 'provider', 'link']

    def get_queryset(self, request):
        return self.model.objects.order_by('-hotel')

    def system_default(self, obj):
        return obj.hotel is None
    system_default.boolean = True


@admin.register(models.Address)
class AddressAdmin(SalmonellaMixin, admin.ModelAdmin):
    list_display = [
        'aramex_country', 'aramex_state',
        'old_country', 'old_region',
        'city', 'content_type', 'created']
    list_filter = [
        'created',
        ('content_type', admin.RelatedOnlyFieldListFilter),
        ('old_country', admin.RelatedOnlyFieldListFilter),
        ('aramex_country', admin.RelatedOnlyFieldListFilter),
    ]
    search_fields = ['city']
    salmonella_fields = [
        'old_country', 'old_region',
        'aramex_country', 'aramex_state',
    ]
    fields = salmonella_fields + [
        'city', 'post_code',
        'line1', 'line2'
    ]


@admin.register(models.Shipment)
class ShipmentAdmin(SalmonellaMixin, admin.ModelAdmin):
    list_display = [
        'id', 'created', 'tracking_number',
        'shipping_date', 'collected_date', 'is_complete',
        'hotel',
    ]
    list_filter = [
        'is_complete', 'is_collection', 'shipping_date', 'collected_date',
        'delivery_address__old_country',
    ]

    def hotel(self, obj):
        # TODO: FK on Shipment to Hotel
        item = obj.shippingitem_set.get()  # type: models.ShippingItem
        matcheditem = item.parcel
        return matcheditem.lost_item.hotel


@admin.register(models.Transaction)
class TransactionAdmin(ReadOnlyAdmin):
    actions = ['make_paid']
    list_display = [
        'created',
        'user',
        'hotel',
        'is_paid',
        'payment_date',
        'get_service_charge',
        'charged_amount',
        'reference_number',
    ]
    list_filter = [
        'created', 'shipping_details__is_complete', 'is_paid'
    ]
    fields = [
        'created', 'payment_date',
        'reference_number', 'charged_amount',
        'surcharge_amount', 'is_paid', 'service_charge'
    ]
    readonly_fields = fields
    search_fields = ['reference_number']

    def get_queryset(self, request):
        return self.model.objects.select_related('shipping_details').all()

    def make_paid(self, request, queryset):
        for trn in queryset.filter(is_paid=False):
            trn.payment_date = timezone.now()
            trn.is_paid = True
            trn.save()
    make_paid.short_description = 'Mark selected transactions as Paid'

    def get_service_charge(self, obj):
        return obj.service_charge if obj.service_charge != 0 else None
    get_service_charge.short_description = 'Service Charge'

    def user(self, obj):
        # TODO: more model cleanups
        item = obj.shipping_details.shippingitem_set.first()  # type: models.ShippingItem
        parcel = item and item.parcel  # type: models.MatchedItem
        return parcel and parcel.lost_item.created_by

    def hotel(self, obj):
        # TODO: more model cleanups
        item = obj.shipping_details.shippingitem_set.first()  # type: models.ShippingItem
        parcel = item and item.parcel  # type: models.MatchedItem
        return parcel and parcel.lost_item.hotel


def setup_admin_site():
    """
    Replace any auto-registered admin classes here
    """
    admin_replace(Action, AuditLogAdmin)
