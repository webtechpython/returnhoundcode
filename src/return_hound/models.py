from decimal import Decimal
from uuid import uuid4

from actstream.models import Action
from django.db import models
from django.db.models import Q, UUIDField, QuerySet
from django.db.models.signals import post_save, pre_save
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

from authtools.models import AbstractEmailUser
from actstream import action
from autoslug import AutoSlugField
from colorful.fields import RGBColorField
from model_utils import Choices, FieldTracker
from model_utils.models import TimeStampedModel
from cities_light import models as cities_light
from enumfields import EnumIntegerField
from return_hound.apps.aramex.models import AramexLocationFields
from return_hound.apps.auditlog.models import action_description
from return_hound.apps.urbanbuz.service import UrbanBuz

from . import enums


CURRENCY_CHOICES = sorted([(ccy, ccy) for ccy in settings.ARAMEX_CONVERSION_RATES.keys()])


class SafeDelete(models.Model):
    """
    Model to add is_deleted flag / field to model, used to not have to delete the db entry
    """
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Category(SafeDelete, TimeStampedModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Categories'


class CategoryItem(SafeDelete, TimeStampedModel):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Image(SafeDelete, TimeStampedModel):
    path = models.TextField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class Address(AramexLocationFields, TimeStampedModel):
    old_country = models.ForeignKey(cities_light.Country, blank=True, null=True)
    old_region = models.ForeignKey(cities_light.Region, blank=True, null=True)

    # TODO: remove these temp properties after geo data migration
    @property
    def country(self):
        return self.aramex_country

    @property
    def region(self):
        return self.aramex_state

    # TODO Pieter: Remove generic foreign keys
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def get_currency_code(self):
        """
        Delivery address currency.
        Used when getting Aramex shipping rate (Customs value)
        :rtype str
        """
        return self.aramex_country.currency

    def __str__(self):
        return '{}, {}, {}'.format(self.line1, self.city, self.aramex_country.name)

    class Meta:
        verbose_name_plural = 'Addresses'


class User(AbstractEmailUser, TimeStampedModel):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True)
    contact_number = models.CharField(max_length=50, blank=True)
    address = models.ForeignKey(Address, blank=True, null=True)
    terms = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ("reports", "Can see reports"),
            ("login_as", "Can login as other users"),
        )

    @property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def get_address(self):  # TODO: remove this junk...
        return self.address

    @property
    def is_hotel_user(self):
        return self.hoteluser_set.filter(employer__is_verified=True).exists()

    @property
    def is_hotel_creator(self):
        return self.hoteluser_set.filter(employer__is_verified=True, is_creator=True).exists()

    @property
    def get_hotel_user(self):
        if self.is_hotel_user:
            return self.hoteluser_set.first()
        return None

    @property
    def get_hotel(self):
        if self.is_hotel_user:
            return self.get_hotel_user.employer
        return None


class HotelChain(TimeStampedModel):
    name = models.CharField(max_length=200)
    users = models.ManyToManyField(User, blank=True, verbose_name='Admin Users',
                                   limit_choices_to={'is_staff': True})

    def __str__(self):
        return self.name


class HotelBrand(TimeStampedModel):
    chain = models.ForeignKey(HotelChain, blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=200)
    users = models.ManyToManyField(User, blank=True, verbose_name='Admin Users',
                                   limit_choices_to={'is_staff': True})

    def __str__(self):
        return self.name


class Hotel(SafeDelete, TimeStampedModel):
    brand = models.ForeignKey(HotelBrand, blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=50)
    domain_name = models.CharField(max_length=50, blank=True)
    number_of_rooms = models.PositiveIntegerField(blank=True, null=True)
    slug = AutoSlugField(populate_from='name')
    logo = GenericRelation(Image)
    address = models.ForeignKey(Address, blank=True, null=True)

    # TODO: remove these temp properties after geo data migration
    @property
    def country(self):
        return self.address.aramex_country

    @property
    def region(self):
        return self.address.aramex_state

    # TODO: remove old fields when old data is fully migrated
    old_address = models.CharField(max_length=200, blank=True)
    old_country = models.ForeignKey(cities_light.Country, blank=True, null=True)
    old_region = models.ForeignKey(cities_light.Region, blank=True, null=True)

    is_verified = models.BooleanField(default=False)
    is_hidden = models.BooleanField(default=False)

    @property
    def chain(self):
        return self.brand.chain if self.brand_id else None

    @property
    def get_hotel_owner(self):
        return self.hoteluser_set.filter(is_creator=True).first()

    @property
    def get_hotel_report_style(self):
        return self.reportstyle_set.first()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class ReportStyle(SafeDelete, TimeStampedModel):
    """
    Model for custom styling of hotel report forms
    """
    hotel = models.ForeignKey(Hotel)
    cover_images = GenericRelation(Image)
    icon_color = RGBColorField(default='#027fba')
    form_label_color = RGBColorField(default='#2e2e2e')
    form_header_color = RGBColorField(default='#666666')
    form_background_color = RGBColorField(default='#FFFFFF')
    form_field_color = RGBColorField(default='#2e2e2e')
    form_field_border_color = RGBColorField(default='#cccccc')
    form_field_border_radius = models.IntegerField(default=6)
    button_border_radius = models.IntegerField(default=6)
    button_primary_color = RGBColorField(default='#027fba')
    button_primary_text_color = RGBColorField(default='#FFFFFF')
    button_secondary_color = RGBColorField(default='#484848')
    button_secondary_text_color = RGBColorField(default='#FFFFFF')
    header_color = RGBColorField(default='#027fba')
    header_active_color = RGBColorField(default='#2e2e2e')
    header_active_text_color = RGBColorField(default='#FFFFFF')
    header_inactive_color = RGBColorField(default='#027fba')
    header_inactive_text_color = RGBColorField(default='#027fba')
    show_hotel_logo = models.BooleanField(default=False)
    show_cover_image = models.BooleanField(default=False)

    def __str__(self):
        return '{} report form style'.format(self.hotel.name)


class HotelUser(SafeDelete, TimeStampedModel):
    employee = models.ForeignKey(User)
    employer = models.ForeignKey(Hotel)
    is_admin = models.BooleanField(default=False)
    is_creator = models.BooleanField(default=False)
    is_supervisor = models.BooleanField(default=False)
    is_mail_recipient = models.BooleanField(default=False)

    def __str__(self):
        return '{} of {}'.format(self.employee.first_name, self.employer.name)

    class Meta:
        ordering = ['id']


class ItemBase(SafeDelete, TimeStampedModel):
    hotel = models.ForeignKey(Hotel)
    created_by = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    item_type = models.ForeignKey(CategoryItem, blank=True, null=True)
    date = models.DateField()
    location = models.TextField(blank=True)
    room_number = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    serial_number = models.CharField(max_length=100, blank=True)
    specification = models.CharField(max_length=100, blank=True)
    images = GenericRelation(Image)

    class Meta:
        abstract = True


class LostItem(ItemBase):
    """
    Split reported items and items found
    """
    arrival_date = models.DateField(blank=True, null=True)
    departure_date = models.DateField(blank=True, null=True)

    status = EnumIntegerField(enums.LostItemStatus, null=True, blank=True)
    status_text = models.TextField(blank=True)

    class LostItemQuerySet(models.QuerySet):
        def load_related(self):
            """
            Select related to minimise queries, seeing that we need to return almost all of this information for each
            of the lost items, on list and detail views
            """
            return self.select_related('category',  'item_type', 'created_by', 'hotel')

        def unmatched(self):
            """
            Return only lost items which are not matched
            """
            qs = self.load_related().filter(
                (Q(matcheditem__isnull=True) | Q(matcheditem__status=MatchedItem.STATUS.declined)),
                is_deleted=False).exclude(matcheditem__status__lte=MatchedItem.STATUS.pending).distinct()
            return qs

    objects = LostItemQuerySet.as_manager()

    def __str__(self):
        return '# {} {} lost a {}'.format(self.id, self.created_by.first_name, self.item_type.name)

    class Meta:
        ordering = ['-id']


class FoundItem(ItemBase):
    found_by = models.CharField(max_length=100)
    owner_name = models.CharField(max_length=50, blank=True)
    owner_email = models.EmailField(max_length=50, blank=True)
    owner_surname = models.CharField(max_length=50, blank=True)
    owner_contact_number = models.CharField(max_length=50, blank=True)
    stored_at = models.CharField(max_length=100, blank=True)

    status = EnumIntegerField(enums.FoundItemStatus, null=True, blank=True)
    status_text = models.TextField(blank=True, null=True)

    # Item dimensions
    width = models.DecimalField(max_digits=10, decimal_places=2)
    height = models.DecimalField(max_digits=10, decimal_places=2)
    length = models.DecimalField(max_digits=10, decimal_places=2)
    weight = models.DecimalField(max_digits=10, decimal_places=2)

    # This field is for the hotel user to check if the known owner will receive an email
    owner_send_email = models.BooleanField(default=False)

    class FoundItemQuerySet(models.QuerySet):
        def load_related(self):
            """
            Select related to minimise queries, seeing that we need to return almost all of this information for each
            of the found items, on list and detail views
            """
            return self.select_related('category',  'item_type', 'created_by')

        def unmatched(self):
            """
            Return only found items which are not matched
            """
            qs = self.load_related().filter(
                (Q(matcheditem__isnull=True) | Q(matcheditem__status=MatchedItem.STATUS.declined)),
                is_deleted=False).exclude(matcheditem__status__lte=MatchedItem.STATUS.pending).distinct()
            return qs

    objects = FoundItemQuerySet.as_manager()

    def __str__(self):
        return '{} found {}'.format(self.found_by, self.item_type)

    class Meta:
        ordering = ['-id']


class MatchedItem(SafeDelete, TimeStampedModel):
    token = UUIDField(default=uuid4, unique=True)
    lost_item = models.ForeignKey(LostItem)
    found_item = models.ForeignKey(FoundItem)

    assigned_status = EnumIntegerField(enums.MatchedItemStatus, null=True, blank=True)
    status_text = models.TextField(blank=True)

    STATUS = Choices(
        (1, 'accepted', 'Accepted'),
        (2, 'pending', 'Pending'),
        (3, 'declined', 'Declined'),
    )
    status = models.SmallIntegerField(choices=STATUS, default=STATUS.pending)
    tracker = FieldTracker(fields=['status'])  # type: FieldTracker.tracker_class

    class MatchedItemQuerySet(models.QuerySet):
        def load_related(self):
            """
            Select related to minimise queries, all information for both found and lost items are returned to display to
            the user.
            """
            return self.select_related('lost_item__created_by', 'lost_item__category', 'lost_item__item_type',
                                       'found_item__category', 'found_item__item_type')

        def matched(self):
            """
            Return matched item which are currently pending or accepted
            """
            return self.load_related().filter(is_deleted=False).exclude(status=MatchedItem.STATUS.declined)

    objects = MatchedItemQuerySet.as_manager()

    @property
    def get_delivery_address(self):
        lost_item_report_user = self.lost_item.created_by  # type: User
        return lost_item_report_user.address

    @property
    def get_shipment_status(self):
        return self.shippingitem_set.exists()

    def __str__(self):
        return '{} matched {}\'s lost item #{}'.format(self.found_item.found_by, self.lost_item.created_by,
                                                       self.lost_item.id)

    class Meta:
        ordering = ['-id']


class Shipment(SafeDelete, TimeStampedModel):
    hotel = models.ForeignKey(Hotel, blank=True, null=True)
    label_file = models.FileField(upload_to='shipment/labels', blank=True, null=True)
    shipping_date = models.DateTimeField(blank=True, null=True)
    collected_date = models.DateTimeField(blank=True, null=True)
    tracking_number = models.CharField(max_length=100, blank=True)
    collector_name = models.CharField(max_length=100, blank=True)
    collector_last_name = models.CharField(max_length=100, blank=True)
    collector_contact_number = models.CharField(max_length=100, blank=True)
    collector_email = models.EmailField(blank=True)
    is_collection = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    delivery_address = GenericRelation(Address)

    tracker = FieldTracker(fields=[
        'is_collection',
        'is_complete',
        'tracking_number'
    ])  # type: FieldTracker.tracker_class

    @property
    def get_delivery_address(self):
        return self.delivery_address.order_by('-pk').first()


class ShippingItem(SafeDelete, TimeStampedModel):
    parcel = models.ForeignKey(MatchedItem)
    details = models.ForeignKey(Shipment)

    def delete(self, **kwargs):
        self.is_deleted = True
        self.save()

    class ShippingItemQS(QuerySet):
        def active(self):
            return self.filter(is_deleted=False)

        def delete(self):
            return self.update(is_deleted=True)

    objects = ShippingItemQS.as_manager()


class Transaction(TimeStampedModel):
    shipping_details = models.OneToOneField(Shipment, related_name='transaction')
    payment_date = models.DateTimeField(blank=True, null=True)
    service_charge = models.DecimalField(max_digits=10, decimal_places=2)
    charged_amount = models.DecimalField(max_digits=10, decimal_places=2)
    surcharge_amount = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=3, default='AED', choices=CURRENCY_CHOICES)
    reference_number = models.CharField(max_length=100, blank=True)
    is_paid = models.BooleanField()

    item_value = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))])
    item_value_currency = EnumIntegerField(enums.ItemValueCurrencies, null=True)

    insurance_fee = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))])

    # Status choices for Telr
    STATUS = Choices(
        (1, 'pending', 'Pending'),
        (2, 'authorised', 'Authorised'),
        (3, 'paid', 'Paid'),
        (-1, 'expired', 'Expired'),
        (-2, 'cancelled', 'Cancelled'),
        (-3, 'declined', 'Declined')
    )
    tracker = FieldTracker(fields=['is_paid'])  # type: FieldTracker.tracker_class

    @property
    def calculated_service_charge(self):
        service_charge = self.charged_amount * settings.DEFAULT_SERVICE_CHARGE_PERCENTAGE / 100
        return int(round(service_charge))

    @property
    def calculated_insurance_fee(self):
        insurance_fee = self.item_value * (Decimal(settings.DEFAULT_INSURANCE_FEE_PERCENTAGE) / Decimal(100.0))
        return int(round(insurance_fee))

    @property
    def get_shipping_item(self):
        # TODO Pieter: Shipping for multiple items
        return self.shipping_details.shippingitem_set.first().parcel


class Currency(TimeStampedModel):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class CountryCurrency(TimeStampedModel):
    country = models.ForeignKey(cities_light.Country)
    currency = models.ForeignKey(Currency)

    def __str__(self):
        return '{} - {}'.format(self.country.name, self.currency.name)


class SocialLink(TimeStampedModel):
    link = models.URLField()
    hotel = models.ForeignKey(Hotel, null=True, blank=True, help_text='Leave blank for system default')
    provider = EnumIntegerField(enums.SocialProviders)

    class Meta:
        unique_together = ('hotel', 'provider')

    class QS(models.QuerySet):
        def system_default(self):
            """ return return hound social links not linked to a hotel """
            return self.filter(hotel__isnull=True)

    objects = QS.as_manager()

    def __str__(self):
        return self.link


class EmailTemplate(TimeStampedModel):
    logo = GenericRelation(Image)
    subject = models.CharField(max_length=255, null=True, blank=True)
    hotel = models.ForeignKey(Hotel, null=True, blank=True)
    template = EnumIntegerField(enums.EmailTemplates)

    is_active = models.BooleanField(default=False)
    is_system_default = models.BooleanField(default=False)  # is rh template

    banner_colour = RGBColorField(default='#fff')
    banner_border_colour = RGBColorField(default='#000')
    banner_image = GenericRelation(Image, blank=True, null=True)

    body_colour = RGBColorField(default='#fff')
    body_text = models.TextField(blank=True, null=True)
    body_text_colour = RGBColorField(default='#000')
    body_text_colour_2 = RGBColorField(default='#000')

    footer_colour = RGBColorField(default='#fff')
    footer_text = models.TextField(blank=True, null=True)
    footer_text_colour = RGBColorField(default='#000')
    footer_border_colour = RGBColorField(default='#000')

    @property
    def rh(self):
        """ System default email template object """
        objects = self.__class__.objects
        email_base = enums.EmailTemplates.email_base
        kw = {'hotel__isnull': True, 'is_system_default': True, 'is_active': True}

        if objects.filter(template=self.template, **kw).exists():
            return objects.filter(template=self.template, **kw).order_by('-pk').first()
        elif objects.filter(template=email_base, **kw).exists():
            return objects.filter(template=email_base, **kw).order_by('-pk').first()
        return None

    @property
    def rh_footer_colour(self):
        """ System email template's footer colour """
        return self.rh.footer_colour if self.rh else None

    @property
    def rh_footer_text(self):
        """ System email template's footer text """
        return self.rh.footer_text if self.rh else None

    @property
    def rh_footer_text_colour(self):
        """ System email template's footer text colour """
        return self.rh.footer_text_colour if self.rh else None

    @property
    def rh_footer_border_colour(self):
        """ System email template's footer border colour """
        return self.rh.footer_border_colour if self.rh else None

    def __str__(self):
        if self.is_system_default:
            # toDo siteName
            return '{} {} Email template'.format('RH', self.template)
        return '{} {} Email template'.format(self.hotel.name, self.template)

    class Meta:
        unique_together = ('hotel', 'template')
        ordering = ['-modified']

    class QS(models.QuerySet):
        def get_outgoing_email(self, template, hotel=None):
            hotel_kw = dict(
                hotel=hotel, is_active=True, is_system_default=False,
                template=getattr(enums.EmailTemplates, template))
            hotel_base = dict(
                hotel=hotel, is_active=True, is_system_default=False,
                template=enums.EmailTemplates.email_base)
            system_kw = dict(
                hotel__isnull=True, is_active=True, is_system_default=True,
                template=getattr(enums.EmailTemplates, template))
            system_base = dict(
                hotel__isnull=True, is_active=True, is_system_default=True,
                template=enums.EmailTemplates.email_base)

            hotel_email_exists = self.filter(**hotel_kw).exists()
            system_email_exists = self.filter(**system_kw).exists()
            system_base_exists = self.filter(**system_base).exists()
            hotel_base_exists = self.filter(**hotel_base).exists()

            if any([hotel_email_exists, hotel_base_exists]):
                return (self.filter(**hotel_kw) if hotel_email_exists else self.filter(**hotel_base)).last()
            if any([system_base_exists, system_email_exists]):
                return (
                    self.filter(**system_kw) if system_email_exists else self.filter(**system_base)).last()
            return None

    objects = QS.as_manager()

    def clean(self):
        if self.is_system_default and self.hotel:
            # system emails should not be linked to a specific hotel
            raise ValidationError('System emails should not be linked to a specific hotel')
        if not self.is_system_default and not self.hotel:
            raise ValidationError('Hotel is required for non system default emails')


class Invoice(TimeStampedModel):
    rep = models.CharField(max_length=255, blank=True)
    # toDo: This awb field might be redundant, investigate.
    awb = models.CharField(max_length=255, blank=True)
    fob = models.CharField(max_length=255, blank=True)
    shipment = models.OneToOneField(Shipment)
    tax_rate = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    shipping_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    company = models.CharField(max_length=255, blank=True, null=True)
    hotel = models.CharField(max_length=255, blank=True)
    tracking_number = models.CharField(max_length=255, blank=True, null=True)

    contact_number = models.CharField(max_length=50, blank=True)
    attention = models.CharField(max_length=255, blank=True, null=True)

    @property
    def item(self):
        return ShippingItem.objects.filter(details=self.shipment).first().parcel

    @property
    def sub_total(self):
        """ total of all invoice items(item price * invoice tax rate) """
        return round(sum([x.total for x in self.items.all()]), 2)

    @property
    def total(self):
        """ total of all invoice items (item price * invoice tax rate) + shipping price """
        return round(self.sub_total + self.shipping_price, 2)

    @property
    def user(self):
        return self.item.lost_item.created_by

    @property
    def address(self):
        return self.shipment.get_delivery_address


class InvoiceItem(TimeStampedModel):
    invoice = models.ForeignKey(Invoice, related_name='items')
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveSmallIntegerField(default=1)

    @property
    def total(self):
        """ total of invoice items (incl. tax) """
        return round((self.price + (self.price * Decimal(self.invoice.tax_rate or 0) / 100)) * self.quantity, 2)

    @property
    def item_value_currency(self):
        if self.invoice.shipment.transaction.item_value_currency:
            return self.invoice.shipment.transaction.item_value_currency.name
        return None


@receiver(post_save, sender=Transaction)
def shipment_paid(instance: Transaction, **kwargs):
    if not kwargs['created']:
        if instance.tracker.has_changed('is_paid') and instance.is_paid:
            shipment = instance.shipping_details
            for shipping_item in shipment.shippingitem_set.all():
                match = shipping_item.parcel    # type: MatchedItem
                lost_item = match.lost_item     # type: LostItem
                action.send(lost_item.created_by, verb='paid', action_object=instance, target=lost_item)


@receiver(post_save, sender=ShippingItem)
def shipment_or_collection_selected(instance: ShippingItem, **kwargs):
    shipping_item = instance
    shipment = shipping_item.details    # type: Shipment
    if kwargs['created'] or shipment.tracker.has_changed('is_collection'):
        match = shipping_item.parcel    # type: MatchedItem
        lost_item = match.lost_item     # type: LostItem
        hotel = match.found_item.hotel  # type: Hotel
        creator = lost_item.created_by  # type: User
        if shipment.is_collection:
            action.send(creator,
                        verb='opted for collection',
                        action_object=shipment, target=lost_item)
            action.send(hotel,
                        verb='awaiting collection',
                        action_object=shipment, target=lost_item)
        else:
            action.send(creator,
                        verb='opted for shipping',
                        action_object=shipment, target=lost_item)
            action.send(hotel,
                        verb='awaiting payment',
                        action_object=shipment, target=lost_item)


@receiver(post_save, sender=Shipment)
def shipment_scheduled(instance, **kwargs):
    if not kwargs['created']:
        if instance.tracker.has_changed('tracking_number') and instance.tracking_number:
            for shipping_item in instance.shippingitem_set.all():
                match = shipping_item.parcel    # type: MatchedItem
                lost_item = match.lost_item     # type: LostItem
                hotel = match.found_item.hotel  # type: Hotel
                action.send(hotel,
                            verb='scheduled shipment',
                            action_object=instance, target=lost_item,
                            tracking_number=instance.tracking_number)


@receiver(post_save, sender=Shipment)
def urban_buz_award(instance, **kwargs):
    condition = (
        instance.tracker.has_changed('is_complete'),
        instance.is_complete,
        instance.tracking_number)
    if not kwargs['created']:
        if all(condition):
            for item in instance.shippingitem_set.all():
                client = UrbanBuz()
                client.award_point(
                    rh_id=item.parcel.found_item.created_by.id,
                    item_id=item.parcel.found_item.id,
                    item_description=item.parcel.found_item.description,
                    submission_date=item.parcel.found_item.created.strftime("%Y-%m-%d %H:%M:%S"),
                )


@receiver(post_save, sender=Shipment)
def shipment_collected(instance: Shipment, **kwargs):
    if not kwargs['created']:
        if instance.tracker.has_changed('is_complete') and instance.is_complete:
            # noinspection PyUnresolvedReferences
            for shipping_item in instance.shippingitem_set.all():  # type: ShippingItem
                match = shipping_item.parcel    # type: MatchedItem
                lost_item = match.lost_item     # type: LostItem
                hotel = match.found_item.hotel  # type: Hotel
                release_type = 'collected' if instance.is_collection else 'shipped'
                action.send(hotel, verb='released',
                            description='Item was {}'.format(release_type),
                            action_object=shipping_item, target=lost_item)


@receiver(post_save, sender=MatchedItem)
def item_match_created(instance: MatchedItem, **kwargs):
    if kwargs['created']:
        hotel = instance.found_item.hotel  # type: Hotel
        action.send(hotel, verb='matched', action_object=instance, target=instance.lost_item)


@receiver(post_save, sender=MatchedItem)
def item_match_status_change(instance: MatchedItem, **kwargs):
    if instance.tracker.has_changed('status'):
        match = instance                # type: MatchedItem
        lost_item = match.lost_item     # type: LostItem
        creator = lost_item.created_by  # type: User

        if instance.status == MatchedItem.STATUS.declined:
            action.send(creator, verb='declined', action_object=match, target=lost_item)

        elif instance.status == MatchedItem.STATUS.accepted:
            action.send(creator, verb='accepted', action_object=match, target=lost_item)


@receiver(pre_save, sender=Action)
def set_description(instance, **kwargs):
    if not instance.description:
        instance.description = action_description(instance)
