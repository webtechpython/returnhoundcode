from django import template
from django.template.context import Context
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def get_style(obj, attr, def_syle):
    return getattr(obj, attr, def_syle) if obj else def_syle


@register.simple_tag
def render_context(value, **ctx):
    return template.Template(value).render(Context(ctx or {}))


@register.filter
def boolean_icon(value):
    return mark_safe(
        '<img src="/static/admin/img/icon-{}.svg" alt="{}">'.format(
            'yes' if value else 'no',
            str(value)
        ))
