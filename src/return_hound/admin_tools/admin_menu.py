from admin_tools.utils import get_admin_site_name
from django.core.urlresolvers import reverse
from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        self.children += [
            items.MenuItem('Dashboard', reverse('%s:index' % site_name))
        ]

        if context.request.user.has_perm('return_hound.reports'):
            self.children += [
                items.MenuItem('Reports', children=[
                    items.MenuItem('Hotels Report', reverse('admin-reports-hotels')),
                    items.MenuItem('Overview Report', reverse('admin-reports-overview')),
                    items.MenuItem('Found Items', reverse('admin-reports-found-items')),
                    items.MenuItem('Matched Items', reverse('admin-reports-matched-items')),
                    items.MenuItem('History Items', reverse('admin-reports-history-items')),
                    items.MenuItem('Payments', reverse('admin-reports-payment-items')),
                    items.MenuItem('Collection Ready', reverse('admin-reports-collection-ready-items')),
                ])
            ]

        if context.request.user.has_perm('return_hound.login_as'):
            self.children += [
                items.MenuItem('Login as User', reverse('supervisor_login_list'))
            ]
