from django.apps import AppConfig


default_app_config = 'return_hound.admin_tools.Config'


class Config(AppConfig):
    name = 'return_hound.admin_tools'
    label = 'return_hound_admin'
    verbose_name = 'Returnhound Admin'
