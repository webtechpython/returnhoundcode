from django.contrib.admin import SimpleListFilter


class NullFilter(SimpleListFilter):
    def lookups(self, request, model_admin):
        return (
            ('1', 'Yes'),
            ('0', 'No'),
        )

    def queryset(self, request, queryset):
        try:
            val = bool(int(self.value()))
        except (TypeError, ValueError):
            val = None

        if val is not None:
            return queryset.filter(**{'%s__isnull' % self.parameter_name: val})
        return queryset

    @staticmethod
    def on(fieldname: str):
        return type('NullFilterOn{}'.format(fieldname.title()), (NullFilter,), {
            'title': '{} is None'.format(fieldname.title()),
            'parameter_name': fieldname,
        })
