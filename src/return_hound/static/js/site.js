$(function() {
    // check window width to trigger different scrolling points for blue header
    checkWidth();

    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('header').addClass('sticky')
        }else{
            $('header').removeClass('sticky')
        }
        checkWidth();
    });

    $('.fancybox').fancybox({
        fitToView: true,
        loop: false
    });
});

function checkWidth() {
    // check window width to trigger different scrolling points for blue header
    var windowsize = $(window).width();

    if (windowsize < 768) {
        if ($(this).scrollTop() > 10) {
            $('.navbar').addClass('bg-color')
        }else{
            $('.navbar').removeClass('bg-color')
        }
    }else if (windowsize >= 768) {
        if ($(this).scrollTop() > 180) {
            $('.navbar').addClass('bg-color')
        }else{
            $('.navbar').removeClass('bg-color')
        }
    }
}
