angular
    .module('returnHound.modal', [])
    .factory('ModalFactory', function ($uibModal, Settings) {
        return function (opts) {
            return $uibModal.open({
                windowClass: opts.modal_class || 'modal-confirm',
                templateUrl: opts.template || Settings.ROOT + 'utils/views/modal.html',
                controller: opts.controller || ["$scope", function ($scope) {
                    $scope.modal_title = opts.title || 'Please Confirm';
                    $scope.confirm_text = opts.text;
                    $scope.modal_ok_button = opts.modal_ok_button || 'OK';
                    $scope.modal_cancel_button = opts.modal_cancel_button || 'Cancel';
                    $scope.data = opts.data || {};
                }]
            })
        };
    })
    .factory('Modal', function ($uibModal, Settings) {
        return {
            error: function (opts) {
                return $uibModal.open({
                    templateUrl: Settings.ROOT + 'utils/views/modal_error.html',
                    controller: opts.controller || function () {
                        var vm = this;
                        vm.title = opts.title || 'Error';
                        vm.text = opts.text;
                        vm.errors = opts.errors;
                    },
                    controllerAs: 'vm'
                })
            }
        }
    });
