angular
    .module('returnHound.api', ['returnHound.settings'])

    .factory('Res', function (Settings, $resource) {
        /**
         * Default API Res(ource)
         */
        return function Res(url_pattern, params, actions) {
            var url = Settings.API_ROOT + url_pattern;
            // store resource baseURL for inspection (eg: AppUtils.csvFromAPI)
            var resource = $resource(url, params || {}, _.extend({}, {
                query: {method: 'GET'},  // default pagination is object body (isArray:False)
                update: {method: 'PUT'},
                partial: {method: 'PATCH'},
                // generic actions...
                LIST: {method: 'GET', isArray: true},
                PATCH: {method: 'PATCH'},
                PUT: {method: 'PUT'},
                POST: {method: 'POST'}
            }, actions || {}));
            resource.baseURL = url_pattern.split('/')[0];
            return resource
        }
    })

    .factory('API', function (Res) {
        return {
            Login: Res('login/'),
            Logout: Res('logout/'),
            UserProfile: Res('user/'),
            HotelProfile: Res('hotel-profile/'),
            HotelLogo: Res('hotel-logo/:id/'),
            HotelReportStyle: Res('hotel-report-style/'),
            HotelUsers: Res('hotel-users/:id/'),
            HotelReport: Res('hotel-report/:id/'),
            HotelDemo: Res('hotel-demo/'),
            PasswordReset: Res('reset-password/'),
            PasswordResetConfirm: Res('reset-password-confirm/'),
            PasswordChange: Res('password-change/'),
            ReportWizard: Res('report-wizard/'),
            RegisterHotel: Res('register-hotel/'),
            ReportImage: Res('report-image/:id/'),
            LostItemImage: Res('lost-item-image/'),
            FoundItemImage: Res('found-item-image/:id/'),
            HotelFoundItems: Res('hotel-found-items/:id/'),
            HotelFoundItemsMultipleStatus: Res('hotel-found-items/update_status_multiple/', {}, {
                partial: {method: 'PATCH'}
            }),
            HotelLostItems: Res('hotel-lost-items/:id/'),
            HotelLostItemsMultipleStatus: Res('hotel-lost-items/update_status_multiple/', {}, {
                partial: {method: 'PATCH'}
            }),
            HotelMatchedItems: Res('hotel-matched-items/:id/'),
            HotelMatchedItemsMultipleStatus: Res('hotel-matched-items/update_status_multiple/', {}, {
                partial: {method: 'PATCH'}
            }),
            HotelArchivedFoundItems: Res('hotel-archived-found-items/:id/'),
            HotelArchivedLostItems: Res('hotel-archived-lost-items/:id/'),
            HotelArchivedMatchedItems: Res('hotel-archived-matched-items/:id/'),
            LostItemAddress: Res('hotel-matched-items/:id/lost_item_address/'),
            // FIXME: be consistent or remove default pagination... some routes do not paginate... (isArray)
            LostItemMatches: Res('hotel-lost-items/:id/matched_items/', {}, {query: {method: 'GET', isArray: true}}),
            FoundItemMatches: Res('hotel-found-items/:id/matched_items/', {}, {query: {method: 'GET', isArray: true}}),
            HotelCollectItem: Res('hotel-collect-items/:id/'),
            HotelItemHistory: Res('hotel-item-history/:id/'),
            // shipment
            ShippingRate: Res('shipping-rate/:id/'),
            Shipment: Res('shipment/:id/'),
            ShipmentDetails: Res('matched-items/:id/shipping_details/', {}, {query: {method: 'GET', isArray: true}}),
            // payment
            Payment: Res('payment/:id/'),
            PaymentReport: Res('payment-report/:id/'),
            PaymentStatus: Res('payment-status/:id/'),
            Charges: Res('payment-status/:id/charges/'),
            MyLostItems: Res('my-lost-items/:id/'),
            MyLostItemMatch: Res('my-lost-items/:id/matched_items/', {}, {query: {method: 'GET', isArray: true}}),
            MyItemHistory: Res('my-item-history/:id/'),
            MatchedItems: Res('matched-items/:id/'),
            KnownOwnerMatch: Res('matched-items/:id/known_owner_feedback/'),

            // (aramex) location data:
            Country: Res('country', {}, {query: {method: 'GET', cache: true, isArray: true}}),
            Region: Res('region', {}, {query: {method: 'GET', cache: true, isArray: true}}),
            City: Res('shipping-city', {}, {query: {method: 'GET', isArray: true}}),
            AddressValidation: Res('shipping-address-validation'),

            Hotel: Res('hotel-list/'),
            Category: Res('category/'),
            CategoryItem: Res('category-item/'),
            FoundItemStatus: Res('found-item-status/', {}, {query: {method: 'GET', isArray: true}}),
            LostItemStatus: Res('lost-item-status/', {}, {query: {method: 'GET', isArray: true}}),
            MatchedItemStatus: Res('matched-item-status/', {}, {query: {method: 'GET', isArray: true}}),
            ItemValueCurrencies: Res('item-value-currencies/', {}, {query: {method: 'GET', isArray: true}}),
            Invoice: Res('invoice/:id'),
            InvoiceItem: Res('invoice-item/:id'),
            InvoicePDF: Res('invoice-pdf/:id'),
            SocialLink: Res('social-link/:id/'),
            SocialProvider: Res('social-providers/', {}, {query: {method: 'GET', isArray: true}}),
            EmailTemplate: Res('email-template/:id/'),
            EmailTemplateType: Res('email-template-type/', {}, {query: {method: 'GET', isArray: true}}),
            EmailLogoImage: Res('email-logo-image/:id/'),
            S3Settings: Res('amazon-s3/'),
            Contact: Res('contact/'),
            YammerLogin: Res('yammer-login'),
            ReportRegister: Res('report-register'),
            /* cms */
            Content: Res('content/')
        }
    });
