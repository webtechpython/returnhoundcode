angular
    .module('returnHound.formErrors', [])
    .factory('FormErrors', FormErrors);

    FormErrors.$inject = [];

    function FormErrors() {
        return {
            clearErrors: function(form){
                _.each(form, function (field, key) {
                    if (!~key.indexOf('$')) { // ignore special properties staring with '$'
                        form[key].serverError = null;
                        form[key].$setValidity('server', true);
                    }
                });
                form.$serverErrors = undefined;
            },
            show: function (form, resp_data) {
                // clear any previous errors:
                this.clearErrors(form);
                // set new errors on form controller:
                _.each(resp_data, function (errors, key) {
                    _.each(errors, function (e) {
                        var field = form[key];
                        var isNested = _.isObject(e);
                        if (field && !isNested) {
                            field.serverError = e;
                            field.$dirty = true;
                            field.$setValidity('server', false);
                        }
                    })
                });
                // put full error response on form to show validation errors
                form.$serverErrors = resp_data;
            }
        }
    }
