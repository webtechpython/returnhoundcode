angular
    .module('returnHound.utils.urls', ['returnHound.settings'])

    /**
     * Provider of default route params/options.
     * See app config ($routeProvider)
     * eg:
     *   $routeProvider.when('/', view('home.html'))
     */
    .provider('view', ['Settings', function (Settings) {
        this.view = function (template, opts) {
            var route = angular.copy({
                reloadOnSearch: false,  // dont reload when url params change (api/items/?status=1)
                templateUrl: opts['templateRoot'] || Settings.ROOT + template,
                perms: ['authenticated'] // by default all routes require login
            });
            if (opts.controller) {
                angular.extend(route, {controllerAs: 'vm'});
            }
            return angular.extend(route, opts)
        };

        this.$get = function () {
            return this.view
        };
    }]);
