angular
    .module('simplePager.directive', ['returnHound.settings'])
    .directive('simplePager', simplePager);

    simplePager.$inject = ['STATIC_VIEWS_URL', '$location'];

    function simplePager(STATIC_VIEWS_URL, $location){
        return {
            restrict: 'E',
            templateUrl: STATIC_VIEWS_URL + 'utils/views/simple_pager.html',
            scope: {resource: '='},
            link: function(scope, element, attrs){
                scope.updatePageParam = function(val){
                     $location.search('page', val);
                }
            }
        }
    }
