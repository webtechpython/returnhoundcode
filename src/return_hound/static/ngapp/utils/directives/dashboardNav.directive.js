angular
    .module('nav.directive', ['returnHound.constants'])
    .directive('dashboardNav', dashboardNav);

    dashboardNav.$inject = ["$location", "USER_NAV_TABS", "HOTEL_ADMIN_TABS", "HOTEL_USER_TABS", "HOTEL_CREATOR_TABS"];

    function dashboardNav($location, USER_NAV_TABS, HOTEL_ADMIN_TABS, HOTEL_USER_TABS, HOTEL_CREATOR_TABS){
        return {
            restrict: 'E',
            templateUrl: 'static/ngapp/utils/views/dashboard_nav.html',
            link: function (scope, element, attr) {
                if(scope.user.is_hotel_user){
                    if (scope.user.is_hotel_creator == true){
                        scope.pages = HOTEL_CREATOR_TABS;
                    }
                    else{
                        scope.pages = (scope.user.hotel.is_admin||scope.user.hotel.is_supervisor) ? HOTEL_ADMIN_TABS : HOTEL_USER_TABS
                    }
                }else{
                    scope.pages = USER_NAV_TABS;
                }
                scope.isActive = function (page) {
                var path = page.href.replace('#!', '');
                return $location.path().indexOf(path) != -1;
                };
            }
        };
    }
