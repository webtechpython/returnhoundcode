(function() {
    'use strict';

    angular
        .module('returnHound.filters', [])
        .filter('interpolate', ['version', function(version) {
            return function(text) {
                return String(text).replace(/\%VERSION\%/mg, version);
            }
        }])

        .filter('timesince', [function() {
            return function(date) {
                return moment(date).fromNow();
            }
        }])

        .filter('default', [function() {
            return function(val, default_val) {
                return val? val: default_val;
            }
        }])

        .filter('dateFormat', function($filter) {
            return function(text) {
                var date = moment.utc(text).toDate();
                return $filter('date')(date, 'd/M/yyyy hh:mm')
            }
        })
})();

