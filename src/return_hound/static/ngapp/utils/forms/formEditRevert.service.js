angular
    .module('formEditRevert', [])

    /**
     * Form edit/revert component.
     * Keeps track of active forms. Supports rollback/revert of changes.
     */
    .service('EditRevert', function (Scroll) {
        this.init = function(parentScope, scrollToForm) {
            var vm = parentScope;

            vm.activeForm = {}; // track active/shown forms
            vm.formRevertData = {};  // backup copies of form data for revert

            /**
             * Activate form for editing.
             * @param form
             * @param modelForRevert: optional. If set `cancelEdit` action will revert edit.
             */
            vm.doEdit = function (form, modelForRevert) {
                if (modelForRevert) {
                    // make a backup copy so we can revert/cancel the edit
                    vm.formRevertData[form.$name] = {
                        model: modelForRevert,
                        backup: angular.copy(modelForRevert)
                    };
                }
                vm.activeForm[form.$name] = true;

                if (scrollToForm) Scroll(form.$name)
            };

            /**
             * Close form after saving to API.
             */
            vm.doEditDone = function (form) {
                vm.activeForm[form.$name] = false;
            };

            /**
             * Close form and optionally reverts changes.
             */
            vm.cancelEdit = function (form) {
                // optional revert to backup copy
                if (form.$name in vm.formRevertData) {
                    // console.log('Revert...', form);
                    var revert = vm.formRevertData[form.$name];
                    angular.extend(revert['model'], revert['backup'])
                }
                vm.doEditDone(form)
            };

            vm.isFormActive = function (form) {
                return vm.activeForm[form.$name] === true;
            };
        }
    });
