angular
    .module('returnHound.home', ['returnHound.user.session'])

    .controller('HomeCtrl', function HomeCtrl(UserSession){
        // Logout User (if logged in)
        UserSession.getUser().then(function (user) {
            if (user) UserSession.logout();
        });
    });