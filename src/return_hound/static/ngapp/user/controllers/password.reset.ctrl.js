angular
    .module('returnHound.password.reset', ['returnHound.user.service'])
    .controller('PasswordResetCtrl', PasswordResetCtrl);

    PasswordResetCtrl.$inject = ['UserFactory'];

    function PasswordResetCtrl(UserFactory){
        var vm = this;

        vm.defaults = {
            email: '',
            errors: {}
        };

        vm.sendPasswordResetMail = sendPasswordResetMail;

        function sendPasswordResetMail(){
            UserFactory.resetPassword(vm);
        }
    }
