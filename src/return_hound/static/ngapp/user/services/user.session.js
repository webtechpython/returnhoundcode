angular
    .module('returnHound.user.session', [
        'returnHound.api', // API.UserProfile
        'returnHound.auth.service', // AuthService to get/set token
        'returnHound.permissions' // RoutePerms
    ])

    /**
     * UserSession: current logged in user
     */
    .factory('UserSession', function ($q, $location, API, AuthService, RoutePerms) {
        var session = {};
        var scope;
        var user;

        return angular.extend(session, {
            destroy: function () {
                AuthService.destroyTokens();
                this.setUser(null);
            },
            getUser: function () {
                if (!AuthService.getToken()) {
                    return $q.resolve(null);
                } else {
                    return user.$promise
                }
            },
            setUser: function (user) {
                scope.user = user;
            },

            // TODO: Refactor and remove this... or at least get it from the API.
            setPermission: function (user) {
                // add `authenticated` property as templates use this
                user['authenticated'] = true;
                //noinspection JSUnresolvedVariable
                if (user.is_hotel_user) {
                    if (user.hotel.is_admin || user.hotel.is_supervisor || user['is_hotel_creator']) {
                        user.permission = 'hotelAdmin';
                    } else {
                        user.permission = 'hotelUser';
                    }
                } else {
                    user.permission = 'reportUser';
                }
            },

            load: function () {
                // check for a valid token first...
                if (!AuthService.getToken()) return $q.resolve(null);
                // load the User profile data...
                user = API.UserProfile.get();
                user.$promise.then(function () {
                    session.setPermission(user)
                });
                this.setUser(user);
                return user.$promise
            },

            reloadUser: function() {
                return this.load()
            },

            logout: function () {
                API.Logout.save({}, function () {
                    session.destroy();
                    $location.path('/');
                })
            },

            login: function (data) {
                var deferred = $q.defer();
                API.Login.save(data, function (res) {
                    AuthService.setToken(res.key, data['rememberMe']);
                    session.load().then(function () {
                        deferred.resolve(session.getUser());
                    });
                }, function (res_err) {
                    deferred.reject(res_err)
                });
                return deferred.promise;
            },

            autoLogin: function (token) {
                AuthService.setToken(token);
                return session.load()
            },

            yammerLogin: function (data) {
                var deferred = $q.defer();
                API.YammerLogin.save(data, function (res) {
                    AuthService.setToken(res.key, data['rememberMe']);
                    session.load().then(function () {
                        deferred.resolve(session.getUser());
                    });
                }, function (res_err) {
                    deferred.reject(res_err)
                });
                return deferred.promise;
            },

            // ----------------------------------------------------------------
            init: function (initScope) {
                scope = initScope;

                // loads the user data if we have a valid auth token
                session.load();

                // expose logout as global click handler on scope
                scope.doLogout = session.logout;

                // setup route permission checks
                scope.$on("$routeChangeStart", function (event, next, current) {
                    RoutePerms.checkPermission(session.getUser(), next, current, session)
                });

                // refresh user data when `user-updated` event received
                scope.$on('user-update', function (event) {
                    session.load()
                })
            }
        })
    });
