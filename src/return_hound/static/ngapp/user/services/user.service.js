angular
    .module('returnHound.user.service', ['returnHound.api', 'returnHound.user.session'])
    .factory('UserFactory', UserFactory);
    function UserFactory($location, API, AlertFactory, UserSession, FormErrors){
        return {
            updateUserProfile: updateUserProfile,
            resetPassword: resetPassword,
            changePassword: changePassword,
            resetPasswordConfirm: resetPasswordConfirm
        };

        function updateUserProfile(userProfile){
            var patchData = {
                first_name: userProfile.first_name,
                last_name: userProfile.last_name || '',
                contact_number: userProfile.contact_number
            };
            return API.UserProfile.partial({}, patchData).$promise
        }

        function resetPassword(vm){
            vm.defaults.errors = {};
            API.PasswordReset.save({email: vm.defaults.email}, function(response){
                return vm.defaults.email = '';
            }, function(error){
                return vm.defaults.errors = error.data;
            });
        }

        function resetPasswordConfirm(vm, uid, token){
            vm.defaults.errors = {};
            API.PasswordResetConfirm.save({
                uid: uid,
                token:token,
                new_password1: vm.defaults.formData.new_password1,
                new_password2: vm.defaults.formData.new_password2
            }, function(response){
                vm.defaults.formData.new_password1 = '';
                vm.defaults.formData.new_password2 = '';
                $location.path('/login');
            }, function(error){
                return vm.defaults.errors = error.data;
            });
        }

        function changePassword(passwordData, form){
            FormErrors.clearErrors(form);
            return API.PasswordChange.save(passwordData, function(){
                AlertFactory.add('success', 'Password changed.');
            },function(errors){
                FormErrors.show(form, errors.data);
            })
        }
    }
