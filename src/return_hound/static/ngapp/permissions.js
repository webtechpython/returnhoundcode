angular
    .module('returnHound.permissions', [])

    .factory('RoutePerms', function ($location) {
        /**
         * Check route permissions. Redirect when permission check fails.
         */
        function checkPermission(user, next, current, session) {
            var route = next.$$route;

            if (!route) return null;  // Error pages (404, 500)

            console.log('Route Controller:', route.controller);
            // console.log('route.perms', route.perms);
            // console.log('user', user);

            // no permissions required. exit...
            if (route.perms === null) return null;

            // Must be logged out for this route.
            // If logged in, destroy the session and remain on requested page.
            if (user && _.contains(route.perms, 'notAuthed')) {
                session.destroy();
                return null;
            }

            // Must exit on 'notAuthed' route when not authenticated
            if (!user && _.contains(route.perms, 'notAuthed')) {
                return null;
            }

            // authentication required...
            // route.perms not null... (['authenticated'] or more complex)
            // redirect to the login page:
            if (!user && route.perms !== null) {
                return $location.path('/login');
            }

            // User should be authenticated at this point...
            if (!user) throw 'Route permission check fail. not authenticated.';
            var isHotelUser = user['is_hotel_user'];
            // Dashboard url. Depends on user role.
            var dashboard = isHotelUser ? '/hotel-dashboard/log-item' : '/dashboard/my-reports';

            // User must fill in address
            // Scenario: Hotel logs found item (known user flow)
            // When User logs in he has to fill in address first.
            var hasAddress = user.address && user.address.country && user.address.country.id;
            if (!isHotelUser && !hasAddress) {
                if (next.params['next']) {
                    return $location.path('/my-address/' + next.params['next'])
                } else {
                    return $location.path('/my-address')
                }
            }
            // Check user role
            // TODO: refactor user roles...
            if (!_.contains(route.perms, user.permission)) {
                return $location.path(dashboard)
            }
        }

        /**
         * Resolve the user, check route permissions.
         * @param userPromise: user (promise or ngResource)
         * @param next: see $routeChangeStart event
         * @param current: see $routeChangeStart event
         * @param session: the UserSession object
         */
        function resolveThenCheck(userPromise, next, current, session) {
            userPromise.then(function (user) {
                if (user && user.$promise) {  // ngResource
                    user.$promise.then(function (userRes) {
                        checkPermission(userRes, next, current, session)
                    })
                } else {
                    checkPermission(user, next, current, session)
                }
            })
        }

        return {
            checkPermission: resolveThenCheck
        }
    });