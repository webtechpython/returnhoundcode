angular
    .module('shipping', ['returnHound.api'])

    .factory('Data', function ($window) {
        function NamespacedStorage(ns) {
            return {
                getKey: function(key, defaultVal){
                    if (ns) key = ns + '.' + key;
                    return angular.fromJson($window.sessionStorage.getItem(key)) || defaultVal;
                },
                setKey: function(key, val){
                    if (ns) key = ns + '.' + key;
                    $window.sessionStorage.setItem(key, angular.toJson(val));
                }
            }
        }
        return {
            storage: function (namespace) {
                return NamespacedStorage(namespace);
            }
        }
    })

    .factory('LastAddress', function (Data) {
        /**
         * Helper to load last address data
         */
        // address fields. only these are loaded from user/session data
        var addrFields = ['line1', 'line2', 'country', 'region', 'city', 'post_code'];
        return {
            fromSession: function(namespace){
                var storage = Data.storage(namespace);
                var savedAddress = {};
                try {
                    savedAddress = _.pick(storage.getKey('address'), addrFields);
                } catch (err) {
                    console.log('Error loading last shipping address from session.');
                }
                return savedAddress;
            },
            fromUser: function (user) {
                return (user.authenticated && user.address.country) ?
                    _.pick(user.address, addrFields) : {}
            }
        }
    })

    .controller('ShippingAddressCtrl', function (API, FormErrors, $scope) {
        var vm = this;
        // Track what is loading. Used by `city` field (typeahead widget)
        vm.loading = {regions: null, cities: null};
        vm.countries = API.Country.query();
        vm.regions = [];

        /**
         * Search method used by typeahead `city` input
         * @param name: first letters of city name
         * @returns []: list of suggested city names
         */
        vm.searchCities = function (name) {
            var address = $scope.model;
            // typeahead search
            if (address.country) {
                return API.City.query({
                    country: address.country['code2'],
                    state: address.region ? address.region['id'] : '',
                    name: name
                }).$promise
            } else {
                return [];
            }
        };

        /**
         * Update regions/states when country selected
         */
        $scope.$watch("model.country", function(newCountry){
            var address = $scope.model;
            if (address && !address.country.postcode_required) address.post_code = ''; // clear post code when selecting new country and postcode not required
            if (newCountry) {
                vm.regions = API.Region.query({country: newCountry['code2']});
            } else {
                vm.regions = []
            }
        });


        /**
         * Disable inputs if parent `editMode` is false
         */
        $scope.$watch('editMode', function (editMode) {
            var useEditMode = 'editMode' in $scope;
            vm.inputDisabled = (!useEditMode) ? false : !editMode;
        });
    })

    .directive('shippingAddress', function (Settings) {
        return {
            restrict: 'E',
            templateUrl: Settings.ROOT + 'common/shipping/shipping_address_form.html',
            scope: {
                editMode: '=?', // optional edit mode hook
                model: '=',
                form: '='
            },
            controller: 'ShippingAddressCtrl',
            controllerAs: 'vm'
        }
    })

;