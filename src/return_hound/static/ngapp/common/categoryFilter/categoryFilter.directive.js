angular
    .module('returnHound.CategoryFilter', ['returnHound.api'])

    .controller('CategoryFilterController', function(API, $scope) {
        var vm = this;
        vm.multiSelectEventHandlers = {onItemSelect: setModel, onItemDeselect: setModel};
        vm.multiselectSettings = {
            enableSearch: false,
            displayProp: 'name',
            showCheckAll: false,
            showUncheckAll: false,
            buttonClasses: 'form-control multiselect',
            scrollableHeight: 'auto'
        };
        vm.translationTexts = {
            buttonDefaultText: 'Select category',
            dynamicButtonTextSuffix: 'selected'
        };

        vm.categories = API.Category.query({page_size: 100}, setSelected);
        vm.selectedCategories = [];

        $scope.$watch('model', setSelected);

        function setModel() {
            $scope.model = toIDs(vm.selectedCategories);
        }

        function setSelected() {
            vm.selectedCategories = $scope.model ? fromIDs($scope.model) : [];
        }

        function toIDs() {
            var ids = [];
            angular.forEach(vm.selectedCategories, function (category) {
                ids.push(category.id);
            });
            return ids.join();
        }

        function fromIDs(modelStr) {
            var ids = modelStr ? modelStr.split(',') : [];
            return _.filter(vm.categories.results || [], function (cat) {
                return _.contains(ids, cat.id.toString())
            })
        }

    })

    .directive('categoryFilter', function (Settings) {
        return {
            scope: {model: '='},
            templateUrl: Settings.ROOT + 'common/categoryFilter/categoryFilter.html',
            controller: 'CategoryFilterController',
            controllerAs: 'vm'
        }
    })
;