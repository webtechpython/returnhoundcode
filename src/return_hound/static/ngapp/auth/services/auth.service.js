angular
    .module('returnHound.auth.service', [])
    .factory('AuthService', function AuthService($cookies, $window) {
        var k = '_authToken'; // name/key used to store token in cookies/storage
        var storage = $window.sessionStorage;
        var token = null;

        return {
            destroyTokens: function(){
                storage.removeItem(k);
                $cookies.remove(k);
                token = null;
            },
            getToken: function () {
                return token ? token : storage.getItem(k) || $cookies.get(k)
            },
            setToken: function (newToken, rememberMe) {
                token = newToken;
                storage.setItem(k, newToken);
                if (rememberMe) {
                    var date = new Date();
                    date.setDate(date.getDate() + 5);
                    $cookies.put(k, newToken, {expires: date.toString()})
                }
            }
        };

    });
