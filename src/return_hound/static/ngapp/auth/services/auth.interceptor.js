angular
    .module('returnHound.auth.interceptor', ['returnHound.auth.service'])

    /**
     * AuthInterceptor
     * Adds Authorization header to all our API requests.
     */
    .factory('AuthInterceptor', function AuthInterceptor($q, $location, AuthService) {

        var excluded_urls  = ['amazonaws'];  // TODO: more cleanup...

        function excluded(config){
            return _.any(excluded_urls, function (url) {
                return ~config.url.indexOf(url)
            });
        }

        return {
            request: function (config) {
                config.headers = config.headers || {};
                var token = AuthService.getToken();
                if (token && !excluded(config)) {
                    config.headers.Authorization = 'Token ' + token;
                }
                return config;
            },
            responseError: function (response) {
                if (response.status === 401) {
                    AuthService.destroyTokens();
                    $location.path('/');
                }
                return $q.reject(response);
            }
        };
    });
