angular
    .module('returnHound.config', [
        'returnHound.permissions',
        'returnHound.utils.urls',
        'returnHound.auth.interceptor',
        'returnHound.loading.interceptor',
        'returnHound.user.session'
    ])

    .config(function ($routeProvider, $resourceProvider, $httpProvider, $locationProvider,
                      viewProvider, AngularyticsProvider, ngIntlTelInputProvider) {

        $locationProvider.hashPrefix('!');
        var view = viewProvider.view;

        $routeProvider
            .when('/', view('home/views/home.html', {controller: 'HomeCtrl', perms: null}))
            .when('/login', view('user/views/login.html', {controller: 'LoginCtrl', perms: ['notAuthed']}))
            .when('/login-as/:token', {controller: 'LoginWithTokenCtrl', template: '', perms: null})
            .when('/forgot-password', view('user/views/forgot_password.html', {controller: 'PasswordResetCtrl', perms: null}))
            .when('/password-reset-confirm/:uid/:token', view('user/views/password_reset_confirm.html', {controller: 'PasswordConfirmCtrl', perms: null}))

            // default (non-whitelabled) report lost item wizard...
            .when('/report/step-:id', view('report/views/report_wizard.html', {controller: 'ReportRegisterCtrl', perms: null}))

            // Hotel whitelabled report lost item wizard
            .when('/report/:slug/:id/step-:step', view('report/views/hotel_report_wizard.html', {controller: 'HotelReportCtrl', perms: null}))

            // hotel dashboard (Use by Hotel staff users)
            .when('/hotel-dashboard/log-item', view('user/views/dash_base.html', {controller: 'LogItemCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/inventory', view('user/views/dash_base.html', {controller: 'InventoryCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/inventory/:id', view('user/views/dash_base.html', {controller: 'InventoryDetailCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/inventory/:id/edit', view('user/views/dash_base.html', {controller: 'InventoryEditCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/matched-items', view('user/views/dash_base.html', {controller: 'MatchedItemCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/matched-items/:id', view('user/views/dash_base.html', {controller: 'MatchedItemDetailCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/lost-items', view('user/views/dash_base.html', {controller: 'LostItemCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/lost-items/:id', view('user/views/dash_base.html', {controller: 'LostItemDetailCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/collection-ready', view('user/views/dash_base.html', {controller: 'CollectionCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/collection-ready/:item/invoice/:id?', view('user/views/dash_base.html', {controller: 'InvoiceCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/collection-ready/:id', view('user/views/dash_base.html', {controller: 'CollectionDetailCtrl', perms: ['hotelAdmin']}))
            .when('/hotel-dashboard/history', view('user/views/dash_base.html', {controller: 'HotelHistoryCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/history/:id', view('user/views/dash_base.html', {controller: 'HotelHistoryDetailCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/settings', view('user/views/dash_base.html', {controller: 'HotelProfileCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/reports', view('user/views/dash_base.html', {controller: 'HotelReportsCtrl', perms: ['hotelUser', 'hotelAdmin']}))
            .when('/hotel-dashboard/archived', view('user/views/dash_base.html', {controller: 'ArchiveListCtrl', perms: ['hotelUser', 'hotelAdmin']}))

            // hotel-zone (Where new Hotel can signup for ReturnHound service)
            .when('/hotel-zone', view('hotel/views/hotel_home.html', {controller: 'HotelHomeCtrl', perms: null}))
            .when('/hotel-zone/register/step-:id', view('hotel/views/hotel_registration.html', {controller: 'HotelRegisterCtrl', perms: ['notAuthed']}))
            .when('/hotel-zone/request-a-demo', view('hotel/views/request_demo.html', {controller: 'HotelDemoCtrl', perms: null}))

            // user dashboard.
            // Where normal user can view status of his logged Lost items, update his address, profile, settings, etc:
            .when('/dashboard/my-reports', view('user/views/dash_base.html', {controller: 'MyReportCtrl', perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id', view('user/views/dash_base.html', {controller: 'MyReportDetailCtrl', perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/edit', view('user/views/dash_base.html', {controller: 'MyReportEditCtrl', perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/payment/:transaction', view('user/views/dash_base.html', {controller: 'PaymentCtrl', perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/payment-failed/:ref/:transaction', view('user/views/dash_base.html', {controller: 'PaymentFailedCtrl', perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/payment-success/:ref/:transaction', view('user/views/dash_base.html', {controller: 'PaymentSuccessCtrl',  perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/payment/:status/:ref/:transaction', view('user/views/dash_base.html', {controller: 'PaymentStatusCtrl',  perms: ['reportUser']}))
            .when('/dashboard/my-reports/:id/payment/:ref/:transaction', view('user/views/dash_base.html', {controller: 'PaymentStatusCtrl', perms: ['reportUser']}))
            .when('/my-address/:next?', view('report/views/my_address.html', {controller: 'MyAddressCtrl', perms: ['reportUser']}))
            .when('/my-item/:id/:status', view('report/views/my_item.html', {controller: 'MyItemCtrl', perms: null}))
            .when('/dashboard/settings', view('user/views/dash_base.html', {controller: 'MyProfileCtrl', perms: ['reportUser']}))

            // site content, FAQ, etc:
            .when('/about', view('support/views/about.html', {controller: 'SupportCtrl', perms: null}))
            .when('/partners', view('support/views/partners.html', {controller: 'SupportCtrl', perms: null}))
            .when('/how-it-works', view('support/views/how_it_works.html', {controller: 'SupportCtrl', perms: null}))
            .when('/urban-buz-rewards', view('support/views/urban-buz-rewards.html', {controller: 'SupportCtrl', perms: null}))
            .when('/press-releases', view('support/views/press_releases.html', {controller: 'PressReleasesCtrl', perms: null}))
            .when('/support', view('support/views/support.html', {controller: 'SupportCtrl', perms: null}))
            .when('/faq', view('support/views/faq.html', {controller: 'SupportCtrl', perms: null}))
            .when('/contact-us', view('support/views/contact.html', {controller: 'ContactCtrl', perms: null}))
            .when('/terms-and-conditions', view('support/views/terms_and_conditions.html', {controller: 'SupportCtrl', perms: null}))

            // error pages:
            .when('/permission-denied-403', view('support/views/403.html', {controller: 'SupportCtrl', perms: null}))
            .when('/page-not-found-404', view('support/views/404.html', {controller: 'SupportCtrl', perms: null}))
            .when('/something-went-wrong', view('support/views/500.html', {controller: 'SupportCtrl', perms: null}))
            .otherwise({redirectTo: '/page-not-found-404'});

        ngIntlTelInputProvider.set({nationalMode:false, defaultCountry: 'ae'});
        $httpProvider.interceptors.push('LoadingInterceptor'); // loading interceptor to show loading indicator
        $httpProvider.interceptors.push('AuthInterceptor'); // add auth token to all requests
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $resourceProvider.defaults.stripTrailingSlashes = false;
        AngularyticsProvider.setEventHandlers(
            window['ga'] ? ['GoogleUniversal'] : ['Console']
        );
        $locationProvider.html5Mode(true);
    })

    .run(function($route, $rootScope, $location, gettextCatalog, Angularytics, UserSession){
        // setup auth
        UserSession.init($rootScope);

        // analytics
        Angularytics.init();

        // TODO: wat?
        $location.update_path = function (path, keep_previous_path_in_history) {
            if ($location.path() === path) return;

            var routeToKeep = $route.current;
            $rootScope.$on('$locationChangeSuccess', function () {
                if (routeToKeep) {
                    $route.current = routeToKeep;
                    routeToKeep = null;
                }
            });

            $location.path(path);
            if (!keep_previous_path_in_history) $location.replace();
        };

        // i18n
        gettextCatalog.setCurrentLanguage('en');
        gettextCatalog.debug = false;
    });
