angular
    .module('returnHound.payment', ['returnHound.payment.service', 'returnHound.paymentGateway'])
    .controller('PaymentStatusCtrl', PaymentStatusCtrl)
    .controller('PaymentFailedCtrl', PaymentFailedCtrl)
    .controller('PaymentSuccessCtrl', PaymentSuccessCtrl)
    .controller('PaymentCtrl', PaymentCtrl);

    function PaymentStatusCtrl($window, $routeParams, STATIC_VIEWS_URL, PaymentFactory){
        var vm = this;
        var parentScope = $window.parent.angular.element($window.frameElement).scope();

        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/payment_status.html'
        };

        vm.transaction = {};
        vm.itemId = $routeParams['id'];
        vm.transactionId = $routeParams['transaction'];

        function notifyParent() {
            if($routeParams['status'] == 'success'){
                parentScope.$emit('redirect_url', '/dashboard/my-reports/'+$routeParams['id']+'/payment-success/'+$routeParams['ref']+'/'+$routeParams['transaction'])
            }else if($routeParams['status'] == 'failed'){
                parentScope.$emit('redirect_url', '/dashboard/my-reports/'+$routeParams['id']+'/payment-failed/'+$routeParams['ref']+'/'+$routeParams['transaction'])
            }else{
                parentScope.$emit('redirect_url', '/dashboard/my-reports/'+$routeParams['id'])
            }
            parentScope.$apply();
        }
        notifyParent();
    }

    function PaymentFailedCtrl($routeParams, STATIC_VIEWS_URL, PaymentFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/payment_failed.html'
        };
        vm.itemId = $routeParams['id'];
        vm.transactionId = $routeParams['transaction'];
        vm.transaction = PaymentFactory.getPaymentStatus($routeParams['transaction']);
    }

    function PaymentSuccessCtrl($routeParams, STATIC_VIEWS_URL, PaymentFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/payment_success.html'
        };
        vm.itemId = $routeParams['id'];
        vm.transaction = PaymentFactory.getPaymentStatus($routeParams['transaction']);
    }

    function PaymentCtrl($scope, $routeParams, $location, STATIC_VIEWS_URL, PaymentFactory, $sce){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'user/views/user_nav.html',
            content: STATIC_VIEWS_URL + 'report/views/payment.html'
        };

        PaymentFactory.getPaymentURL($routeParams['transaction'], $routeParams['terms']).then(function(data){
            vm.transaction = data;
            vm.iframeUrl = $sce.trustAsResourceUrl(data.transaction_url.order.url);
        });

        $scope.$on('redirect_url', function(e, url) {
            $location.path(url)
        });
    }
