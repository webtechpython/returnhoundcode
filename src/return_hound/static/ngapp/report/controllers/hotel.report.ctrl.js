angular
    .module('returnHound.report.hotel', ['returnHound.utils', 'returnHound.report.service', 'returnHound.item.service'])
    .controller('HotelReportCtrl', HotelReportCtrl);
    // Hotel branded lost item report wizard
    function HotelReportCtrl($scope, $routeParams, $location, AppUtils, Data, API, FormErrors,
                             UserSession, ReportFactory, ItemFactory, Upload, vcRecaptchaService){
        var vm = this;
        var storage = Data.storage('hotel-report'), formDataKey = 'formData';

        vm.step = 1;
        vm.formData = {};
        vm.isIframe = $routeParams['iframe'];
        vm.numberOfSteps = 4;
        vm.lostDateOpen = false;
        vm.arrivalDateOpen = false;
        vm.departureDateOpen = false;

        vm.setStep = setStep;
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.updateItemTypes = updateItemTypes;
        vm.removeItemImage = removeItemImage;
        vm.removeLostItemImage = removeLostItemImage;
        vm.setCaptchaFieldValidity = setCaptchaFieldValidity;

        vm.categories = AppUtils.getItemCategory();
        vm.s3Settings = AppUtils.getS3Settings();
        vm.imageUpload = imageUpload;
        vm.hotel = API.HotelReport.get({id: $routeParams['id'], slug: $routeParams['slug']});
        vm.hotelReportForm = vm.hotel;  // FIXME: stupid naming causes confusion and bugs

        // init... after User loaded
        UserSession.getUser().then(function (user) {
            vm.user = user; // need to check user role before creating report
            vm.formData = storage.getKey(formDataKey, {});
            vm.formData.stepThree = {
                arrivalDate: AppUtils.strToDate(vm.formData.stepThree && vm.formData.stepThree.arrivalDate),
                departureDate: AppUtils.strToDate(vm.formData.stepThree && vm.formData.stepThree.departureDate),
                lostDate: AppUtils.strToDate(vm.formData.stepThree && vm.formData.stepThree.lostDate),
                currentDate: moment().toDate(),
                lastSeen: (vm.formData.stepThree && vm.formData.stepThree.lastSeen) || '',
                roomNumber: (vm.formData.stepThree && vm.formData.stepThree.roomNumber) || ''
            };
            vm.formData.stepFour = {
                terms: false
            };
            if (user){
                vm.formData.stepTwo = {
                    firstName: user.first_name,
                    lastName: user.last_name,
                    contactNumber: user.contact_number,
                    email: user.email,
                    terms: false
                },
                vm.formData.stepFour = {
                    delivery: user.address
                }
            }
            updateItemTypes((vm.formData.stepOne && vm.formData.stepOne.lostItem.category) ? vm.formData.stepOne.lostItem.category.id : '');

            setStep($routeParams['step']);

            initDone(); // register watch expressions, etc.
        });

        function goStep(step) {
            var id = $routeParams['id'];
            var slug = $routeParams['slug'];
            // Navigate to the specified step
            return function () {
                vm.step = step;
                $location.update_path('/report/'+slug+'/'+id+'/step-' + step);
            }
        }

        function setStep(step, form){
            $scope.$broadcast('show-errors-check-validity');
            if(form !== undefined) {
                if (form.$valid) {
                    if (vm.step === 4) {
                        if(vm.formData.stepFour.terms && vm.formData.stepFour.recaptcha) {
                            var isHotelUser = vm.user && vm.user.hotel && vm.user.hotel['hotel_id'];
                            if (isHotelUser) {
                                // Dont create a report when logged in as Hotel user.
                                // Hotel users only need to see the styling of the wizard.
                                goStep(step)()
                            } else {
                                ReportFactory.createHotelReport(vm.formData)
                                    .then(function () {
                                        // clear saved formdata, and show success/summary screen
                                        storage.setKey(formDataKey, {});
                                        goStep(step)()
                                    }, function (errors) {
                                        FormErrors.show(form, errors.data);
                                        vcRecaptchaService.reload();
                                    })
                            }
                        }
                    }else{
                        goStep(step)()
                    }
                }
            }else if(step > vm.numberOfSteps || !vm.formData.stepOne){
                goStep(1)()
            }else if(!vm.formData.stepOne && form){
                goStep(2)()
            }else if(!vm.formData.stepTwo.roomNumber && form){
                goStep(3)()
            }else{
                goStep(step)()
            }
        }

        function addItem(form){
            $scope.$broadcast('show-errors-check-validity');
            if(form.$valid){
                // Add stepOne key to addItem function, so that the function can be used by both report wizards
                ItemFactory.addItem(vm.formData, 'stepOne')
            }
        }

        function removeItem(index){
            ItemFactory.removeItem(vm.formData.stepOne.items, index)
        }

        function updateItemTypes(category){
            vm.categoryItems = AppUtils.getItem(category || '');
        }

        function removeItemImage(itemIndex, itemImageIndex){
            ItemFactory.removeItemImage(vm.formData.stepOne.items, itemIndex, itemImageIndex)
        }

        function removeLostItemImage(index){
            vm.formData.stepOne.lostItem.images.splice(index, 1)
        }

        function imageUpload(files, errFiles){
            vm.files = files;
            angular.forEach(files, function(file, index) {
                if(file !== null && vm.files.length){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = vm.s3Settings.url + path;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            if (!vm.formData.stepOne.lostItem.images){
                                vm.formData.stepOne.lostItem.images = [{'url': fileURL}];
                            }else{
                                vm.formData.stepOne.lostItem.images.push({'url': fileURL});
                            }
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                   }
                }
            })
        }

        /**
         * Do any post-init stuff (like register watch expressions)
         */
        function initDone() {
            // save form data to session storage
            $scope.$watch("vm.formData", function (newFormData) {
                storage.setKey(formDataKey, newFormData)
            }, true);
        }

        function setCaptchaFieldValidity(form, isValid){
            form['recaptcha'].$setValidity('server', isValid);
        }
    }
