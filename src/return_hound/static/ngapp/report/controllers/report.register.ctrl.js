angular
    .module('returnHound.report.register', [
        'returnHound.api',
        'returnHound.utils',
        'returnHound.report.service',
        'returnHound.item.service',
        'hotelselect',
        'shipping'
    ])
    // Default lost item report wizard...
    .controller('ReportRegisterCtrl', function ($scope, $routeParams, $location, API, AppUtils,
                                                Data, $q, ReportFactory, ItemFactory, Upload, UserSession,
                                                anchorSmoothScroll, FormErrors, vcRecaptchaService) {
        var vm = this;
        var storage = Data.storage();

        vm.step = 1;
        vm.formData = {};
        vm.summary = {}; // copy of formData is set here after succesful Report save/submit
        vm.numberOfSteps = 5;
        vm.lostDateOpen = false;
        vm.arrivalDateOpen = false;
        vm.departureDateOpen = false;

        vm.setStep = setStep;
        vm.addItem = addItem;
        vm.removeItem = removeItem;
        vm.updateItemTypes = updateItemTypes;
        vm.removeItemImage = removeItemImage;
        vm.removeLostItemImage = removeLostItemImage;
        vm.reportLoginRegister = reportLoginRegister

        vm.categories = AppUtils.getItemCategory();
        vm.s3Settings = AppUtils.getS3Settings();
        vm.imageUpload = imageUpload;

        vm.scrollTo = function (eID) {
            anchorSmoothScroll.scrollTo(eID);
        };

        // init... after User loaded
        UserSession.getUser().then(function (user) {
            var data = {};
            if (user) {  // some values come from user profile
                vm.loggedInUser = true;
                data.stepTwo = {
                    firstName: user.first_name,
                    lastName: user.last_name,
                    contactNumber: user.contact_number,
                    email: user.email
                },
                data.stepFour = {
                    delivery: user.address
                }
            }
            angular.extend(vm.formData, data);

            // clean values... saved data might have nulls. TODO: fix this
            vm.formData.stepThree = {
                arrivalDate: (vm.formData.stepThree && vm.formData.stepThree.arrivalDate) ? moment(vm.formData.stepThree.arrivalDate).toDate() : '',
                departureDate: (vm.formData.stepThree && vm.formData.stepThree.departureDate) ? moment(vm.formData.stepThree.departureDate).toDate() : '',
                lostDate: (vm.formData.stepThree && vm.formData.stepThree.lostDate) ? moment(vm.formData.stepThree.lostDate).toDate() : '',
                currentDate: moment().toDate(),
            };
            updateItemTypes((vm.formData.stepOne && vm.formData.stepOne.lostItem.category) ? vm.formData.stepOne.lostItem.category.id : '');

            setStep($routeParams['id']);

            initDone(); // register watch expressions, etc.
        });

        function goStep(step) {
            // Navigate to the specified step
            return function () {
                vm.step = step;
                $location.update_path('/report/step-' + step);
            }
        }

        function setStep(step, form){
            $scope.$broadcast('show-errors-check-validity');
            if(form !== undefined) {
                if (form.$valid && !form.$pending) {
                    if (vm.step==2 && !vm.formData.stepTwo) {
                        vm.reportLoginRegister();
                    }
                    else if (vm.step==4){
                        ReportFactory.createReport(vm.formData)
                            .then(function () {
                                // make a copy of the valid formData for display on summary screen
                                vm.summary = angular.copy(vm.formData);
                                // clear saved formdata
                                vm.formData = {};
                                goStep(5)();  // show final success page/step
                            })
                            .catch(function (errors) {
                                FormErrors.show(form, errors.data);
                                vcRecaptchaService.reload();
                            })
                    } else {
                        goStep(step)()
                    }
                }
            }else if(step > vm.numberOfSteps || !vm.formData.stepOne){
                goStep(1)()
            }else if(!vm.formData.stepTwo && form){
                goStep(2)();
            }else if(!vm.formData.stepThree.roomNumber && form){
                goStep(3)()
            }else{
                goStep(step)()
            }
        }

        function reportLoginRegister () {

            vm.credentials.errors = {}; // clear any non_field_errors
            vm.credentials.errors.email = !vm.credentials.email;
            vm.credentials.errors.password = !vm.credentials.password;

            API.ReportRegister.save(vm.credentials, function (resp){
                if (resp.token){
                    UserSession.autoLogin(resp.token);
                    UserSession.getUser().then(function (user) {
                        var data = {};
                        data.stepTwo = {
                            firstName: user.first_name,
                            lastName: user.last_name,
                            contactNumber: user.contact_number,
                            email: user.email
                        },
                        data.stepFour = {
                            delivery: user.address
                        }
                        angular.extend(vm.formData, data)
                    })
                }
            },
            function (error) {
                vm.credentials.errors = error.data;
            })
        }

        function addItem(form){
            $scope.$broadcast('show-errors-check-validity');
            if(form.$valid){
                // Add stepOne key to addItem function, so that the function can be used by both report wizards
                ItemFactory.addItem(vm.formData, 'stepOne')
            }
        }

        function removeItem(index){
            ItemFactory.removeItem(vm.formData.stepOne.items, index)
        }

        function updateItemTypes(category){
            vm.categoryItems = AppUtils.getItem(category || '');
        }

        function removeItemImage(itemIndex, itemImageIndex){
            ItemFactory.removeItemImage(vm.formData.stepOne.items, itemIndex, itemImageIndex)
        }

        function removeLostItemImage(index){
            vm.formData.stepOne.lostItem.images.splice(index, 1)
        }

        function imageUpload(files, errFiles){
            vm.files = files;
            angular.forEach(files, function(file, index) {
                if(file !== null && vm.files.length){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = vm.s3Settings.url + path;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            if (!vm.formData.stepOne.lostItem.images){
                                vm.formData.stepOne.lostItem.images = [{'url': fileURL}];
                            }else{
                                vm.formData.stepOne.lostItem.images.push({'url': fileURL});
                            }
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                   }
                }
            })
        }

        /**
         * Do any post-init stuff (like register watch expressions)
         */
        function initDone() {
            // save form data to session storage
            $scope.$watch("vm.formData", function (newData) {
                storage.setKey('report', newData);
            }, true);
        }

    });
