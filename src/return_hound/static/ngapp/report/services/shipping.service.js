angular
    .module('returnHound.shipping.service', ['returnHound.api', 'returnHound.modal'])
    .factory('ShippingFactory', function ShippingFactory($routeParams, $location, API, ModalFactory){

        return {
            cancelShipment: cancelShipment,
            processShipment: processShipment,
            getShippingRate:  getShippingRate,
            getShippingDetails: getShippingDetails
        };

        function cancelShipment(vm){
            var modal = ModalFactory({
                title: 'Please confirm',
                text: vm.shipment.details.is_collection ? 'Are you sure you want to cancel this collection request?' : 'Are you sure you want to cancel this shipping order?'
            });
            modal.result.then(function(){
                return API.Shipment.delete({id:vm.shipment.details.id}, function(response){
                    vm.shipment = null;
                    vm.matchedItem.shipment_status = false;
                    vm.getShippingRate(vm.matchedItem.id)
                }, function(errors){

                })
            })
        }

        function processShipment(isCollection, vm){
            if (isCollection){
                var modal = ModalFactory({
                    title: 'Collector details',
                    data: {matchedItem:vm.matchedItem, isCollection:isCollection},
                    text: '',
                    template: '/static/ngapp/report/views/_confirm_shipping.html'
                });
                modal.result.then(function(){
                    return API.Shipment.save({
                        collector_name: vm.matchedItem.collector.name,
                        collector_last_name: '',
                        collector_email: vm.matchedItem.collector.email,
                        collector_contact_number: vm.matchedItem.collector.contactNumber,
                        item_value: 0.0,
                        // When Item is not an Aramex shipment, save default item value currency as AED
                        item_value_currency: 2, // 2 is the enum value for AED
                        service_charge: 0.0,
                        insurance_fee: 0.0,
                        is_collection: isCollection,
                        parcel: vm.matchedItem.id,
                        charged_amount: 0.0
                    }, function(response){
                        API.ShipmentDetails.query({id: vm.matchedItem.id}, function(data){
                            vm.shipping = null;
                            vm.shipment = data[0];
                            vm.matchedItem.shipment_status = true;
                            $location.path('/dashboard/my-reports/'+$routeParams['id'])
                        })
                    })
                })
            }else{
                var currencies = API.ItemValueCurrencies.query({'hotel': vm.matchedItem.found_item.hotel});
                var modal = ModalFactory({
                    title: 'Customs Charges',
                    data: {matchedItem:vm.matchedItem, isCollection:isCollection, currencies:currencies},
                    modal_ok_button: 'Proceed',
                    template: '/static/ngapp/report/views/_confirm_proceed_to_payment.html'
                });
                modal.result.then(function() {
                    return API.Shipment.save({
                        is_collection: isCollection,
                        parcel: vm.matchedItem.id,
                        charged_amount: vm.shipping.shipping_rate.amount,
                        service_charge: 0.0,
                        insurance_fee: 0.0,
                        item_value: vm.matchedItem.item_value,
                        item_value_currency: vm.matchedItem.item_value_currency.value,
                        currency: vm.shipping.shipping_rate.currency

                    }, function(response) {
                        API.ShipmentDetails.query({id: vm.matchedItem.id}, function(data){
                            vm.shipping = null;
                            vm.shipment = data[0];
                            vm.serviceCharge = vm.shipment.transaction.calculated_service_charge;
                            vm.insuranceFee = vm.shipment.transaction.calculated_insurance_fee;
                            vm.matchedItem.shipment_status = true;
                        })

                    })
                })
            }
        }

        function getShippingRate(id){
            return API.ShippingRate.get({id: id})
        }

        function getShippingDetails(id){
            return API.ShipmentDetails.query({id: id})
        }
    });
