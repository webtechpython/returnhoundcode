angular
    .module('returnHound.report.service', ['returnHound.api', 'returnHound.storage'])
    .factory('ReportFactory', ReportFactory);
    function ReportFactory($routeParams, API, SStorage){
        return {
            createReport: createReport,
            createHotelReport: createHotelReport
        };

        function createReport(formData){
            // called by ReportRegisterCtrl - normal lost item report wizard
            return API.ReportWizard.save({
                email: formData.stepTwo.email,
                first_name: formData.stepTwo.firstName,
                last_name: formData.stepTwo.lastName || '',
                contact_number: formData.stepTwo.contactNumber,
                // terms: formData.stepFour.terms,
                // delivery/shipping address:
                address: formData.stepFour.delivery,
                // hotel,items data:
                hotel: formData.stepThree.hotel.id,
                location: formData.stepThree.lastSeen || '',
                room_number: formData.stepThree.roomNumber || '',
                date: moment(formData.stepThree.lostDate).format('YYYY-MM-DD'),
                arrival_date:  formData.stepThree.arrivalDate ? moment(formData.stepThree.arrivalDate).format('YYYY-MM-DD') : null,
                departure_date: formData.stepThree.departureDate ? moment(formData.stepThree.departureDate).format('YYYY-MM-DD') : null,
                items:formData.stepOne.items,
                // recapthca response to validate on server
                recaptcha: formData.stepFour.recaptcha
            }).$promise;
        }

        function createHotelReport(formData){
            // Called by HotelReportCtrl - Hotel branded lost item report wizard
            return API.ReportWizard.save({
                email: formData.stepTwo.email,
                first_name: formData.stepTwo.firstName,
                last_name: formData.stepTwo.lastName || '',
                contact_number: formData.stepTwo.contactNumber,
                terms: formData.stepFour.terms,
                hotel: $routeParams['id'],
                arrival_date:  formData.stepThree.arrivalDate ? moment(formData.stepThree.arrivalDate).format('YYYY-MM-DD') : null,
                departure_date: formData.stepThree.departureDate ? moment(formData.stepThree.departureDate).format('YYYY-MM-DD') : null,
                date: moment(formData.stepThree.lostDate).format('YYYY-MM-DD'),
                location: formData.stepThree.lastSeen || '',
                room_number: formData.stepThree.roomNumber || '',
                address: formData.stepFour.delivery,
                items:formData.stepOne.items,
                recaptcha: formData.stepFour.recaptcha
            }).$promise;
        }

    }
