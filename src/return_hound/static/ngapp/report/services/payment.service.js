angular
    .module('returnHound.payment.service', ['returnHound.api'])
    .factory('PaymentFactory', PaymentFactory);

    PaymentFactory.$inject = ['$routeParams', '$location', 'API', 'AlertFactory'];

    function PaymentFactory($routeParams, $location, API, AlertFactory){
        return {
            getPaymentURL: getPaymentURL,
            getPaymentStatus: getPaymentStatus
        };

        // FIXME: where do I even start to explain what is wrong with this...
        function getPaymentURL(id, terms){
            var promise = API.Payment.save({transaction: id, terms: terms}).$promise;
            promise.catch(function (error) {
                if (error.data['non_field_errors']){
                    AlertFactory.add('danger', error.data.non_field_errors);
                }
                if (error.data['terms']){
                    AlertFactory.add('danger', error.data.terms[0]);
                }
                $location.path('dashboard/my-reports/' + $routeParams['id'])
            });
            return promise;
        }

        function getPaymentStatus(id){
            return API.PaymentStatus.get({id:id})
        }
    }
