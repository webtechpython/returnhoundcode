angular
    .module('returnHound.invoice', ['returnHound.item.service', 'returnHound.shipping.service'])
    .controller('InvoiceCtrl', function ($routeParams, $location, AppUtils, ItemFactory, Settings,
                                         ModalFactory, FormErrors, API, AlertFactory) {
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/invoice.html'
        };
        vm.itemCount = 1;
        vm.range = AppUtils.range;
        vm.defaults = {invoice: {items:[]}};
        vm.hotel = API.HotelProfile.get();

        vm.getInvoice = function(invoiceID){
            return API.Invoice.get({id: invoiceID}, function(data){
                vm.invoice = angular.extend(vm.defaults.invoice, data);
                vm.itemCount = data.items.length > 0 ? vm.invoice.items.length : 1;
            });
        };

        ItemFactory.getCollectionItem($routeParams['item']).$promise.then(function(collectionData){
            vm.collectionItem = collectionData;
            var invoiceID = vm.collectionItem.details.invoice ? $routeParams['id'] || vm.collectionItem.details.invoice.id : null;

            vm.defaults.invoice = {
                attention: collectionData.parcel.created_by.first_name + ' ' + collectionData.parcel.created_by.last_name,
                contact_number: collectionData.parcel.created_by.contact_number,
                hotel: vm.hotel.name,
                tracking_number: collectionData.details.tracking_number,
                items: [{
                    quantity: 1,
                    name: collectionData.parcel.found_item.category_name,
                    description: collectionData.parcel.found_item.description || collectionData.parcel.found_item.item_type_name,
                    price: collectionData.details.transaction.item_value
                }],
                address: collectionData.details.delivery_address  // TODO: refactor
            };
            vm.invoice = invoiceID ?  vm.getInvoice(invoiceID) : vm.defaults.invoice;
        });


        vm.addItem = function(){
            vm.itemCount += 1;
        };

        vm.removeInvoiceItem = function(id){
            return API.InvoiceItem.delete({id: id});
        };

        vm.removeItem = function(index){
            var opts = {text: 'Are you sure you want to delete this Item?'};
            var is_saved = vm.invoice.items[index].id;
            var removeIndex = function(id){
                vm.invoice.items.splice(id, 1);
                vm.collectionItem.details.invoice.items.splice(id, 1);
            };
            ModalFactory(opts).result.then(function(){
                vm.itemCount -= 1;
                is_saved ? vm.removeInvoiceItem(vm.invoice.items[index].id).$promise.then(function(){removeIndex(index)}) : removeIndex(index);
                is_saved ? AlertFactory.add('success', 'Invoice item successfully deleted') : AlertFactory.add('success', 'Invoice item successfully removed')
            });
        };

        vm.preparePostData = function(invoice){
            var postObj = angular.copy(invoice);
            postObj.shipment = vm.collectionItem.details.id;
            // TODO: make sure API accepts complex objects instead of just IDs
            // postObj.address.country = postObj.address.country.id;
            // postObj.address.region = postObj.address.region.id;
            return postObj;
        };

        vm.update = function(postObj, form){
            API.Invoice.update({id: postObj.id}, postObj).$promise.then(
                function(data){
                    vm.collectionItem.invoice = data;
                    AlertFactory.add('success', 'Invoice successfully updated');
                    $location.url('/hotel-dashboard/collection-ready/'+$routeParams['item']);
                },
                function(err){
                    FormErrors.show(form, err.data);
                    if(err.data.non_field_errors) AlertFactory.add('danger', err.data.non_field_errors[0]);
            });
        };

        vm.save = function (postObj, form){
            API.Invoice.save(postObj).$promise.then(
                function(data){
                   vm.collectionItem.details.invoice = data;
                   AlertFactory.add('success', 'Invoice successfully created');
                   $location.url('/hotel-dashboard/collection-ready/'+$routeParams['item']);
                },
                function(err){
                    FormErrors.show(form, err.data);
                    if(err.data.non_field_errors) AlertFactory.add('danger', err.data.non_field_errors[0]);
            });
        };

        vm.saveInvoice = function(form){
            FormErrors.clearErrors(form);
            var postObj = vm.preparePostData(vm.invoice);
            if(!vm.invoice.id) {vm.save(postObj, form)}
            else {vm.update(postObj, form)}
        };
    });
