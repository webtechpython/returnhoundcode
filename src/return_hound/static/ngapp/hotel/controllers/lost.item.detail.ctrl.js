angular
    .module('returnHound.lost.item.detail', ['returnHound.item.service'])
    .controller('LostItemDetailCtrl', LostItemDetailCtrl);

    LostItemDetailCtrl.$inject = ['$routeParams', 'STATIC_VIEWS_URL', 'ItemFactory'];

    function LostItemDetailCtrl($routeParams, STATIC_VIEWS_URL, ItemFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'hotel/views/hotel_nav.html',
            content: STATIC_VIEWS_URL + 'hotel/views/lost_item_detail.html'
        };

        vm.selectLostItemMatch = selectLostItemMatch;

        vm.lostItem = ItemFactory.getMatchedLostItem($routeParams['id']).$promise.then(function(data){
            vm.lostItem = data;
            if (vm.lostItem.match_count > 0){
                vm.matchedItems = ItemFactory.getMatchedFoundItems(vm.lostItem)
            }
        });

        function selectLostItemMatch(matchedItem){
            ItemFactory.selectLostItemMatch(vm.lostItem, matchedItem)
        }

    }
