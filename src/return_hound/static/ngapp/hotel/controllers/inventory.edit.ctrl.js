angular
    .module('returnHound.inventory.edit', ['returnHound.utils', 'returnHound.item.service'])
    .controller('InventoryEditCtrl', InventoryEditCtrl);

    function InventoryEditCtrl($scope, $routeParams, ItemFactory, Upload, AppUtils, API){
        var vm = this;
        vm.templates = {
            nav: AppUtils.viewUrl('hotel/views/hotel_nav.html'),
            content: AppUtils.viewUrl('hotel/views/inventory_edit.html')
        };

        vm.formData = ItemFactory.getFoundItem($routeParams['id']).$promise.then(function(data){
            init(data);
        });

        vm.categories = AppUtils.getItemCategory();

        vm.imageUpload = imageUpload;
        vm.updateFoundItem = updateFoundItem;
        vm.deleteFoundItem = deleteFoundItem;
        vm.updateItemTypes = updateItemTypes;
        vm.deleteFoundItemImage = deleteFoundItemImage;

        vm.s3Settings = AppUtils.getS3Settings();

        function init(data){
            vm.formData = {
                id: data.id,
                category: {
                    id: data.category,
                    name: data.category_name
                },
                item_type: {
                    id: data.item_type,
                    name: data.item_type_name
                },
                status: data.status,
                status_text: data.status_text,
                found_by: data.found_by,
                location: data.location,
                serial_number: data.serial_number,
                specification: data.specification,
                images: data.images,
                room_number: data.room_number,
                description: data.description,
                date: (data.date) ? moment(data.date).toDate() : moment().toDate(),
                width: parseFloat(data.width),
                height: parseFloat(data.height),
                length: parseFloat(data.length),
                weight: parseFloat(data.weight),
                stored_at: data.stored_at,
                owner_name: data.owner_name,
                owner_surname: data.owner_surname,
                owner_email: data.owner_email,
                owner_contact_number: data.owner_contact_number,
                owner_send_email: data.owner_send_email
            };
            updateItemTypes(vm.formData.category.id);
            vm.hasOwnerDetails = (data.owner_name && data.owner_email && data.owner_contact_number) ? true : false
        }

        function updateFoundItem(form){
            $scope.$broadcast('show-errors-check-validity');
            if (form.$valid || form.$error.server){
                if (!vm.hasOwnerDetails){
                    vm.formData.owner_name = '';
                    vm.formData.owner_surname = '';
                    vm.formData.owner_email = '';
                    vm.formData.owner_contact_number = '';
                }
                ItemFactory.updateFoundItem(vm.formData, form)
            }

        }

        function deleteFoundItem(){
            ItemFactory.deleteFoundItem(vm.foundItem)
        }

        function updateItemTypes(category){
            vm.categoryItems = AppUtils.getItem(category || '');
        }

        function imageUpload(files, errFiles){
            vm.files = files;
            angular.forEach(files, function(file, index) {
                if(file !== null && vm.files.length){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = vm.s3Settings.url + path;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            if (!vm.formData.images){
                                vm.formData.images = [{'path': fileURL}];
                            }else{
                                vm.formData.images.push({'path': fileURL});
                            }
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                    }
                }
            })
        }

        function deleteFoundItemImage(images, image, index){
            ItemFactory.deleteFoundItemImage(images, image, index);
        }
    }
