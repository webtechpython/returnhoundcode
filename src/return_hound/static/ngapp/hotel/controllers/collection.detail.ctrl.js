angular
    .module('returnHound.collection.detail', ['returnHound.item.service', 'returnHound.shipping.service'])
    .controller('CollectionDetailCtrl', CollectionDetailCtrl);
    function CollectionDetailCtrl($routeParams, $location, AuthService, API, Settings,
                                  AlertFactory, Modal, ModalFactory){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/collection_detail.html'
        };

        vm.collectionItem = API.HotelCollectItem.get({id: $routeParams['id']});

        vm.editInvoice = function(invoice){
            $location.url('/hotel-dashboard/collection-ready/'+vm.collectionItem.id+'/invoice/'+invoice.id);
        };

        vm.addInvoice = function(){
            $location.url('/hotel-dashboard/collection-ready/'+vm.collectionItem.id+'/invoice/');
        };

        vm.viewInvoice = function(invoice){
            var token = AuthService.getToken();
            window.open('/api/invoice-pdf/' + invoice.id + '/?token=' + token, '_blank')
        };

        vm.invoiceLabelPdf = function(invoice){
            var token = AuthService.getToken();
            window.open('/api/invoice-pdf/' + invoice.id + '/?token=' + token + '&format=pdf', '_blank')
        };

        vm.shipItem = function(isComplete){
            ModalFactory({
                text: isComplete ?
                    'Was this item collected?' :
                    'Are you sure you want to create a shipping request for this item?'
            }).result.then(function () {
                return API.HotelCollectItem.partial({id: $routeParams['id']}, {}, function (resp) {
                    if (isComplete) {
                        AlertFactory.add('info', 'Item was collected.');
                        $location.path('hotel-dashboard/collection-ready')
                    } else {
                        AlertFactory.add('success', 'Shipping request created.');
                        vm.collectionItem.details.tracking_number = resp.details.tracking_number;
                        vm.collectionItem.details.label_file = resp.details.label_file;
                    }
                }, function (err) {
                    Modal.error({title: 'Aramex API Error', errors: err.data['server_error']})
                })
            }).then(function () {
                // Show a modal dialog on successful shipment (UrbanBuz point awarded)
                if (isComplete && vm.collectionItem.details.tracking_number) {
                    var first_name = vm.collectionItem.parcel.found_item.created_by.first_name,
                        last_name = vm.collectionItem.parcel.found_item.created_by.last_name;
                    ModalFactory({
                        text: first_name + ' ' + last_name,
                        template: Settings.ROOT + 'hotel/views/_urban_buz_award.html'
                    });
                }
            });
        }
    }
