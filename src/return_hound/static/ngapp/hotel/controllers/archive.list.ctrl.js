angular
    .module('returnHound.hotel.archive', ['returnHound.api'])
    .controller('ArchiveListCtrl', function ($routeParams, $location, API, AppUtils, Settings, PaginatedRes){
        // Show "Archived" items  (/hotel-dashboard/archived)
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/archived_item_list.html'
        };

        vm.selectedCategories = [];
        vm.selectedItemTypes = [];
        vm.selectedItems = [];

        // Set the datePicker dates from routeParams, ui-datepicker only supports date objects.
        vm.dateObj = {
            created__gte: AppUtils.strToDate($routeParams['created__gte']),
            created__lte: AppUtils.strToDate($routeParams['created__lte'])
        };

        // Set the initial searchParams.
        vm.searchParams = angular.extend({
            pk: $routeParams['pk'] || '',
            search: $routeParams['search'] || '',
            category: $routeParams['category'] || '',
            item_type: $routeParams['item_type'] || '',
            found_item__category: $routeParams['found_item__category'] || '',
            found_item__item_type: $routeParams['found_item__item_type'] || ''
        }, vm.dateObj);  // TODO: cleanup stupid Date handling, remove separate dateObj

        vm.archivedItems = PaginatedRes(API.HotelArchivedFoundItems, {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        vm.goToArchivedItem = function (item) {
            $location.path('/hotel-dashboard/inventory/' + item.id);
        };

        vm.archivedMatchedItems = PaginatedRes(API.HotelArchivedMatchedItems, {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        vm.goToMatchedArchivedItem = function (item) {
            $location.path('/hotel-dashboard/matched-items/' + item.id)
        };

        vm.archivedLostItems = PaginatedRes(API.HotelArchivedLostItems, {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        vm.goToLostArchivedItem = function (item) {
            $location.path('/hotel-dashboard/lost-items/' + item.id)
        };


        //---------------------------------------------------------------
        // TODO: project-wide search code cleanup...
        //---------------------------------------------------------------

        function reload(){
            $location.search(AppUtils.cleanParams(vm.searchParams));
            vm.archivedItems.setFilter(vm.searchParams);
            vm.archivedMatchedItems.setFilter(vm.searchParams);
            vm.archivedLostItems.setFilter(vm.searchParams);
        }

        vm.search = function () {
            var categories = [],
                itemTypes = [];
            if (vm.selectedCategories) {
                angular.forEach(vm.selectedCategories, function (category) {
                    categories.push(category.id);
                })
            }
            if (vm.selectedItemTypes) {
                angular.forEach(vm.selectedItemTypes, function (itemType) {
                    itemTypes.push(itemType.id);
                })
            }
            // TODO: cleanup stupid Date handling...
            angular.extend(vm.searchParams, vm.dateObj);
            reload();
        };

        vm.clearSearch = function () {
            vm.dateObj = {created__gte: null, created__lte: null};
            vm.searchParams = {};
            vm.selectedCategories = [];
            vm.selectedItemTypes = [];
            vm.categoryItems = [];
            reload();
        };

    });
