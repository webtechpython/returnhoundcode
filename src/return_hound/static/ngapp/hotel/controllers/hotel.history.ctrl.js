angular
    .module('returnHound.hotel.history', ['returnHound.item.service'])
    .controller('HotelHistoryCtrl', function HotelHistoryCtrl(
        $routeParams, $location, API, AppUtils, AlertFactory, Settings, ItemFactory, PaginatedRes
    ){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/hotel_history.html'
        };

         // Set the datePicker dates from routeParams, ui-datepicker only supports date objects.
        vm.dateObj = {
            details__collected_date__gte: AppUtils.strToDate($routeParams['details__collected_date__gte']),
            details__collected_date__lte: AppUtils.strToDate($routeParams['details__collected_date__lte']),
            created__gte: AppUtils.strToDate($routeParams['created__gte']),
            created__lte: AppUtils.strToDate($routeParams['created__lte'])
        };

        vm.searchParams = angular.extend({
            search: $routeParams['search'] || '',
            parcel__found_item: $routeParams['parcel__found_item'] || ''
        }, vm.dateObj);  // TODO: cleanup stupid Date handling, remove separate dateObj

        // Paginated items...
        vm.historyItems = PaginatedRes(API.HotelItemHistory,  {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        vm.search = search;
        vm.reload = reload;
        vm.clearSearch = clearSearch;
        vm.clearField = clearField;

        vm.exportCSV = function() {
            AppUtils.csvFromAPI(API.HotelItemHistory, vm.searchParams);
        };

        function reload(){
            $location.search(AppUtils.cleanParams(vm.searchParams));
            vm.historyItems.setFilter(vm.searchParams);
        }

        function search(){
            // TODO: cleanup stupid Date handling...
            angular.extend(vm.searchParams, vm.dateObj);
            vm.reload();
        }

        function clearSearch(){
            vm.dateObj = {};
            vm.searchParams = {};
            vm.reload();
        }

        function clearField(key){
            vm.searchParams[key] = '';
            if(key in vm.dateObj){
                vm.dateObj[key] = '';
            }
            vm.reload();
        }

    });
