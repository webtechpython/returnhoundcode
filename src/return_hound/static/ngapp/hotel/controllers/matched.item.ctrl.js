angular
    .module('returnHound.matched.item', ['returnHound.item.service'])
    .controller('MatchedItemCtrl', function ($routeParams, $http, $location, Settings, AuthService, API, AppUtils,
                                             AlertFactory, ItemFactory, PaginatedRes) {
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/matched_items.html'
        };

        // Set the datePicker dates from routeParams, ui-datepicker only supports date objects.
        vm.dateObj = {
            created__gte: AppUtils.strToDate($routeParams['created__gte']),
            created__lte: AppUtils.strToDate($routeParams['created__lte'])
        };

        vm.searchParams = angular.extend({
            pk: $routeParams['pk'] || '',
            search: $routeParams['search'] || '',
            found_item__category: $routeParams['found_item__category'] || '',
            found_item__item_type: $routeParams['found_item__item_type'] || ''
        }, vm.dateObj);  // TODO: cleanup stupid Date handling, remove separate dateObj

        vm.selectedCategories = [];
        vm.selectedItemTypes = [];
        vm.selectedItems = [];

        vm.statuses = API.MatchedItemStatus.query();
        vm.itemAssignedStatus = '';
        vm.itemStatusText = '';

        // paginated Matched items:
        vm.matchedItems = PaginatedRes(API.HotelMatchedItems, {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        AppUtils.getItemCategory().$promise.then(function(data) {
            vm.categories = data.results
        });

        vm.search = search;
        vm.reload = reload;
        vm.clearSearch = clearSearch;
        vm.clearField = clearField;
        vm.updateItemTypes = updateItemTypes;
        vm.selectItem = selectItem;
        vm.updateItemStatuses = updateItemStatuses;

        // ng-dropdown-multiselect
        var multiselect = AppUtils.getMultiselectSettings(vm.updateItemTypes, vm.selectedCategories);
        vm.multiselectSettings = multiselect.settings;
        vm.multiselectEvents = multiselect.events;
        vm.multiselectTranslationTexts = multiselect.translationTexts;

        vm.exportCSV = function() {
            AppUtils.csvFromAPI(API.HotelMatchedItems, vm.searchParams)
        };

        function updateItemTypes(categories){
            var categoryIds = [];
            vm.categoryItems = [];
            vm.selectedItemTypes = [];
            if (categories.length > 0) {
                angular.forEach(categories, function (category) {
                    categoryIds.push(category.id);
                });
                AppUtils.getItem(categoryIds.join()).$promise.then(function(data) {
                    vm.categoryItems = data.results
                });
            }
        }

        function selectItem(id) {
            var index = vm.selectedItems.indexOf(id);
            if (index !== -1) {
                vm.selectedItems.splice(index, 1);
            } else {
                vm.selectedItems.push(id);
            }
        }

        function updateItemStatuses() {
            if (vm.itemAssignedStatus == null || vm.itemAssignedStatus == '' && vm.itemStatusText == null || vm.itemStatusText == '') {
                AlertFactory.add('danger', 'Select a status and add status description before updating');
            } else {
                if (vm.selectedItems.length > 0) {
                    ItemFactory.updateMultipleMatchedItemsStatus(vm.selectedItems, vm.itemAssignedStatus.value, vm.itemStatusText).$promise.then(function (data) {
                        AlertFactory.add('success', data.message);
                        vm.selectedItems = [];  // reset selected items
                        reload();
                    }).catch(function (error) {
                        AlertFactory.add('danger', error.message);
                    })
                } else {
                    AlertFactory.add('danger', 'No items selected');
                }
            }
        }

        function reload(){
            $location.search(vm.searchParams);
            vm.matchedItems.setFilter(vm.searchParams);
        }

        function search(){
            var categories = [],
                itemTypes = [];
            if (vm.selectedCategories) {
                angular.forEach(vm.selectedCategories, function (category) {
                    categories.push(category.id);
                })
            }
            if (vm.selectedItemTypes) {
                angular.forEach(vm.selectedItemTypes, function (itemType) {
                    itemTypes.push(itemType.id);
                })
            }
            vm.searchParams.found_item__category = categories.join();
            vm.searchParams.found_item__item_type = itemTypes.join();
            // TODO: cleanup stupid Date handling...
            angular.extend(vm.searchParams, vm.dateObj);
            vm.reload();
        }

        function clearSearch(){
            vm.dateObj = {created__gte: null, created__lte: null};
            vm.searchParams = {};
            vm.selectedCategories = [];
            vm.selectedItemTypes = [];
            vm.categoryItems = [];
            vm.reload();
        }

        function clearField(key){
            vm.searchParams[key] = '';
            if(key in vm.dateObj){
                vm.dateObj[key] = '';
            }
            vm.search();
        }
    });
