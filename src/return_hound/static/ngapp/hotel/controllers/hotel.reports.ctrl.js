angular
    .module('returnHound.hotel.reports', ['returnHound.item.service', 'returnHound.api'])
    .controller('HotelReportsCtrl', HotelReportsCtrl);
    function HotelReportsCtrl($routeParams, $location, Settings, AppUtils, AlertFactory, API, PaginatedRes){
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/hotel_reports.html'
        };

        vm.dateObj = {
            created__gte: AppUtils.strToDate($routeParams['created__gte']),
            created__lte: AppUtils.strToDate($routeParams['created__lte'])
        };

        vm.reports = PaginatedRes(API.PaymentReport, {filter: vm.dateObj});

        vm.exportReport = function() {
            AppUtils.csvFromAPI(API.PaymentReport, vm.dateObj);
        };

        vm.search = function () {
            $location.search(AppUtils.cleanParams(vm.dateObj));
            vm.reports.setFilter(vm.dateObj);
        };

        vm.paid_amount = function(item) {
            return parseFloat(item.charged_amount) + parseFloat(item.service_charge);
        };

        vm.clearSearch = function () {
            vm.dateObj = {};
            vm.search();
        };

        vm.search();

    }
