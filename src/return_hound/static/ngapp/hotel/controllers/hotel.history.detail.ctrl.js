angular
    .module('returnHound.hotel.history.detail', ['returnHound.item.service'])
    .controller('HotelHistoryDetailCtrl', HotelHistoryDetailCtrl);

    HotelHistoryDetailCtrl.$inject = ['$routeParams', 'STATIC_VIEWS_URL', 'ItemFactory'];

    function HotelHistoryDetailCtrl($routeParams, STATIC_VIEWS_URL, ItemFactory){
        var vm = this;
        vm.templates = {
            nav: STATIC_VIEWS_URL + 'hotel/views/hotel_nav.html',
            content: STATIC_VIEWS_URL + 'hotel/views/hotel_history_detail.html'
        };

        vm.historyItem = ItemFactory.getHistoryItem($routeParams['id']);
    }
