angular
    .module('returnHound.collection', ['returnHound.item.service'])
    .controller('CollectionCtrl', CollectionCtrl);


    function CollectionCtrl($routeParams, $location, API, AppUtils, Settings, PaginatedRes, ItemFactory){
        // Show "Ready for collection" items  (/hotel-dashboard/collection-ready)
        var vm = this;
        vm.templates = {
            nav: Settings.ROOT + 'hotel/views/hotel_nav.html',
            content: Settings.ROOT + 'hotel/views/collection_ready.html'
        };

        // Set the datePicker dates from routeParams, ui-datepicker only supports date objects.
        vm.dateObj = {
            created__gte: ($routeParams['created__gte']) ? moment($routeParams['created__gte']).toDate() : '',
            created__lte: ($routeParams['created__lte']) ? moment($routeParams['created__lte']).toDate() : ''
        };

        vm.searchParams = {
            search: $routeParams['search'] || '',
            parcel__found_item: $routeParams['parcel__found_item'] || '',
            parcel__found_item__category: $routeParams['parcel__found_item__category'] || '',
            parcel__found_item__item_type: $routeParams['parcel__found_item__item_type'] || '',
            created__gte: ($routeParams['created__gte']) ? moment(vm.dateObj.created__gte).format('YYYY-MM-DD') : '',
            created__lte: ($routeParams['created__lte']) ? moment(vm.dateObj.created__lte).format('YYYY-MM-DD') : ''
        };

        vm.selectedCategories = [];
        vm.selectedItemTypes = [];

        // Paginated collection items:
        vm.collectionItems = PaginatedRes(API.HotelCollectItem, {
            page_start: $routeParams['page'],
            filter: vm.searchParams
        });

        AppUtils.getItemCategory().$promise.then(function(data) {
            vm.categories = data.results
        });

        vm.search = search;
        vm.reload = reload;
        vm.clearSearch = clearSearch;
        vm.clearField = clearField;
        vm.updateItemTypes = updateItemTypes;

        // ng-dropdown-multiselect
        var multiselect = AppUtils.getMultiselectSettings(vm.updateItemTypes, vm.selectedCategories);
        vm.multiselectSettings = multiselect.settings;
        vm.multiselectEvents = multiselect.events;
        vm.multiselectTranslationTexts = multiselect.translationTexts;

        function updateItemTypes(categories){
            var categoryIds = [];
            vm.categoryItems = [];
            vm.selectedItemTypes = [];
            if (categories.length > 0) {
                angular.forEach(categories, function (category) {
                    categoryIds.push(category.id);
                });
                AppUtils.getItem(categoryIds.join()).$promise.then(function(data) {
                    vm.categoryItems = data.results
                });
            }
        }

        function reload(){
            $location.search(vm.searchParams);
            vm.collectionItems.setFilter(vm.searchParams);
        }

        function search(){
            var categories = [],
                itemTypes = [];
            if (vm.selectedCategories) {
                angular.forEach(vm.selectedCategories, function (category) {
                    categories.push(category.id);
                })
            }
            if (vm.selectedItemTypes) {
                angular.forEach(vm.selectedItemTypes, function (itemType) {
                    itemTypes.push(itemType.id);
                })
            }
            vm.searchParams.parcel__found_item__category = categories.join();
            vm.searchParams.parcel__found_item__item_type = itemTypes.join();
            vm.searchParams.created__gte = vm.dateObj.created__gte ? moment(vm.dateObj.created__gte).format('YYYY-MM-DD'): '';
            vm.searchParams.created__lte = vm.dateObj.created__lte ? moment(vm.dateObj.created__lte).format('YYYY-MM-DD'): '';
            vm.reload();
        }

        function clearSearch(){
            vm.dateObj = {created__gte: null, created__lte: null};
            vm.searchParams = {};
            vm.selectedCategories = [];
            vm.selectedItemTypes = [];
            vm.categoryItems = [];
            vm.reload();
        }

        function clearField(key){
            vm.searchParams[key] = '';
            if(key in vm.dateObj){
                vm.dateObj[key] = '';
            }
            vm.reload();
        }
    }
