angular
    .module('returnHound.log.item', ['returnHound.utils', 'returnHound.item.service'])
    .controller('LogItemCtrl', LogItemCtrl);

    // Hotel log (Found) item screen
    function LogItemCtrl($scope, AppUtils, ItemFactory, Upload, API){
        var vm = this;
        vm.templates = {
            nav: AppUtils.viewUrl('hotel/views/hotel_nav.html'),
            content: AppUtils.viewUrl('hotel/views/log_item.html')
        };

        vm.formData = {};
        vm.categories = AppUtils.getItemCategory();
        vm.statuses = API.FoundItemStatus.query();

        vm.logItem = logItem;
        vm.updateItemTypes = updateItemTypes;
        vm.imageUpload = imageUpload;
        vm.removeFoundItemImage = removeFoundItemImage;

        vm.s3Settings = AppUtils.getS3Settings();

        // initialise radio button default values
        vm.hasOwnerDetails = false;
        vm.formData.owner_send_email = false;

        function validateContactNumber(form){
            // A bug in the ng-intl-tel-input library sets the $viewValue of the contact number field to "+"
            // when the field is cleared, this then causes the field validation to fail
            // (this is a problem as this field is not required).
            // Below we check for this bug ("+") and then set the $viewValue to an empty string.
            if (vm.hasOwnerDetails && form.owner_contact_number.$viewValue === "+") {
                form.owner_contact_number.$setViewValue("");
            }
        }

        function logItem(form){
            $scope.$broadcast('show-errors-check-validity');
            validateContactNumber(form);
            if(form.$valid || form.$error.server){
                ItemFactory.logItem(vm.formData, form).$promise.then(function(data){
                    $scope.$broadcast('show-errors-reset');
                    vm.formData = {};
                })
            }
        }

        function updateItemTypes(category){
            vm.categoryItems = AppUtils.getItem(category || '');
        }

        function imageUpload(files, errFiles){
            vm.files = files;
            angular.forEach(files, function(file, index) {
                if(file !== null && vm.files.length){
                    if((file.size/1024/1024) <= 2.5){
                        var fileName = AppUtils.fileNameGenerator(file);
                        var path = 'uploads/'+ fileName;
                        var fileURL = vm.s3Settings.url + path;
                        Upload.upload({
                            url: vm.s3Settings.url,
                            method: 'POST',
                            data: {
                                key: path,
                                AWSAccessKeyId: vm.s3Settings.key,
                                acl: 'public-read',
                                policy: vm.s3Settings.policy,
                                signature: vm.s3Settings.signature,
                                "Content-Type": file.type != '' ? file.type : 'application/octet-stream',
                                filename: fileName,
                                file: file
                            }
                        }).then(function(resp){
                            if (!vm.formData.images){
                                vm.formData.images = [{'url': fileURL}];
                            }else{
                                vm.formData.images.push({'url': fileURL});
                            }
                        }, function (resp) {
                            file.error = 'Something went wrong';
                        }, function (evt) {
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }else{
                        alert(file.name + ' is too large, maximum size is 2.5MB.');
                        file.progress = 100;
                    }
                }
            })
        }

        function removeFoundItemImage(index){
            vm.formData.images.splice(index, 1)
        }

    }
