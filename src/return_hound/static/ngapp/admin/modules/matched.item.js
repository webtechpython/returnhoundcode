'use strict';

angular.module('matchedItemsReport', [
        'ngResource',
        'ui.bootstrap',
        'helpers.pagination',
        'helpers.utils'
    ])

    .config(['$resourceProvider', function ($resourceProvider) {
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }])

    .factory('API', function (Res) {
        return {
            MatchedItems: Res('matched-items/'),
            Hotels: Res('hotels/')
        }
    })

    .controller('MatchedItemsCtrl', function ($scope, API, ResourcePager){
        var DATE_FORMAT = 'YYYY-MM-DD';
        var ctrl = this;

        var threeMonthsAgo = function () {
            // default to start of month
            // need to create new moment, datepicker mutates it
            return moment().subtract(3, 'month').date(1).toDate();
        };

        ctrl.hotels = API.Hotels.GET_LIST();

        ctrl.filter = {
                startDate: threeMonthsAgo(),
                endDate: moment().toDate(),
                hotel: ''
            };
        ctrl.resetFilter = function () {
            ctrl.filter = {
                startDate: threeMonthsAgo(),
                endDate: moment().toDate(),
                hotel: ''
            }
        };

        ctrl.datepicker = {
            isOpen: {},
            maxDate: {
                endDate: moment().toDate()
            },
            open: function (name) {
                ctrl.datepicker.isOpen[name] = true;
            }
        };
        ctrl.filterDate = function (name) {
            return ctrl.filter[name] ? moment(ctrl.filter[name]).format(DATE_FORMAT) : null
        };

        ctrl.loadData = _.debounce(function () {
            ctrl.items = null;
            ctrl.pager = ResourcePager(API.MatchedItems, {
                filter: {
                    start_date: ctrl.filterDate('startDate'),
                    end_date: ctrl.filterDate('endDate'),
                    hotel: ctrl.filter.hotel
                },
                success: function (response) {
                    ctrl.items = response.results;
                }
            });
        }, 150);

        // reload data when changing date filter inputs
        $scope.$watch('ctrl.filter.startDate', ctrl.loadData);
        $scope.$watch('ctrl.filter.endDate', ctrl.loadData);
        $scope.$watch('ctrl.filter.hotel', ctrl.loadData);
    });
