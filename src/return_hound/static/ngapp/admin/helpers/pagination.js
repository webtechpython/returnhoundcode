angular.module('helpers.pagination', [])

  .value('ResourcePager', function ResourcePager(resource, opts) {
    opts = opts || {};
    var self = {}, query_method = opts['query_method'] || 'query';
    self.resource = resource;
    self.query = null;
    self.data = [];  // object rows received from API
    self.total = 0;
    self.page_count = 0;
    self.page_size = opts['page_size'] || 20;
    self.page = opts['page_start'] || 1;
    self.filter = opts.filter || {};

    self.setFilter = function (filter) {
      angular.extend(self.filter, filter)
    };

    self.params = function () {
      return angular.extend({page: self.page, page_size: self.page_size}, self.filter)
    };

    self.handle_response = function (response) {
      self.data = response.results;
      self.total = response.count;
      self.page_count = Math.ceil(self.total / self.page_size)
    };

    self.loadPage = function () {
      self.loading = true;
      self.query = self.resource[query_method](self.params(), self.handle_response);
      self.query.$promise
        .then(opts.success)
        .catch(opts.error)
        .finally(function(){ self.loading = false });
    };

    self.hasNext = function () {
      return self.page < self.page_count
    };

    self.hasPrev = function () {
      return self.page > 1
    };

    self.pageNext = function () {
      if (self.hasNext()) {
        self.page++;
        self.loadPage()
      }
    };

    self.pagePrev = function () {
      if (self.hasPrev()) {
        self.page--;
        self.loadPage()
      }
    };

    self.setPage = function (pagenum) {
      self.page = pagenum;
      self.loadPage()
    };

    self.loadPage();

    return self;
  })

  .directive('pager', function () {
    return {
      restrict: 'E',
      templateUrl: '/static/ngapp/admin/helpers/pager.html',
      scope: {pager: '='}
    }
  });
