from enumfields import Enum


class LostItemStatus(Enum):
    LOST_NOT_FOUND = 1


class ItemValueCurrencies(Enum):
    USD = 1
    AED = 2


class FoundItemStatus(Enum):
    SHIPPED = 1
    COLLECTED = 2
    ARCHIVED = 3
    OVERDUE = 4


class MatchedItemStatus(Enum):
    UNCLAIMED = 1
    COLLECTED = 2
    SHIPPED = 3
    GUEST_REJECTED = 4
    REGULATORY_HANDOVER = 5


class ItemStatus(Enum):
    # LostItemStatus:
    NOT_FOUND = 1
    # MatchedItemStatus:
    UNCLAIMED = 10
    COLLECTED = 20
    SHIPPED = 30
    GUEST_REJECTED = 40
    REGULATORY_HANDOVER = 50
    # FoundItemStatus:
    OVERDUE = 4


class SocialProviders(Enum):
    FACEBOOK = 1
    TWITTER = 2
    GOOGLE = 3
    YOUTUBE = 4
    LINKEDIN = 5


class EmailTemplates(Enum):
    email_base = 1
    item_found = 2
    item_found_reminder = 3
    hotel_shipment_created = 4
    hotel_account_user = 5
    known_owner_match_item = 6
    new_demo_request = 7
    new_hotel_user = 8
    new_message_received = 9
    new_report_received_hotel = 10
    report_details = 11
    shipment_collected = 12
    shipment_scheduled = 13
    shipment_created = 14
    welcome_client_user = 15
    welcome_hotel_user = 16
    welcome_hotel_user_admin = 17
    password_reset_email = 18
