from project.settings import *
from uuid import uuid4

DB_ENGINE = os.getenv('DB_ENGINE', 'sqlite3')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.{}'.format(DB_ENGINE),
        'NAME': ':memory:' if DB_ENGINE == 'sqlite3' else 'returnhound-{}'.format(uuid4().hex[:12]),
        'ATOMIC_REQUESTS': True,
    }
}

# Celery
CELERY_ALWAYS_EAGER = True  # Disable queue (execute inline)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

STATIC_ROOT = 'unused'

STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'

REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
        'rest_framework.filters.SearchFilter',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'return_hound.apps.api.authentication.CustomTokenAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'return_hound.apps.api.paginator.CustomPageNumberPagination',
    'PAGE_SIZE': 20,
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DATETIME_FORMAT': None  # Set to none to return DateTime obj, changed in DRF 3
}

# Telr payment gateway. (Unused) TODO: remove
TELR_STORE_ID = ''
TELR_AUTH_KEY = ''
TELR_MODE = '1'  # Live=0 Test=1
TELR_URL = 'https://secure.telr.com/gateway/order.json'


# -----------------------------------------------------------------------------
# DEBUG logging
# -----------------------------------------------------------------------------
if os.getenv('DEBUG', False):
    LOGGING['handlers']['console_debug'] = {'level': 'DEBUG', 'class': 'logging.StreamHandler'}
    LOGGING['loggers']['suds.client'] = {'level': 'DEBUG', 'handlers': ['console_debug'], 'propagate': False}


DEFAULT_SERVICE_CHARGE = 5

# Google reCAPTCHA credentials
RECAPTCHA_SITE_KEY = '6LfdOA0UAAAAAAE8SnKYo_3mWjAvoK-nTz9d8efU'  # Live: 6LezeBsTAAAAAJQyaJPqhiVnJClHOFdwk95hy2cZ
RECAPTCHA_SECRET_KEY = '6LfdOA0UAAAAABhBdTWfDxOgLTkl8VL7rKm0URYG'  # FIXME (Live): To be determined.

ARAMEX_ACCOUNTS = {
    'test-ship-location': {  # test account for shipping service
        'USER_NAME': 'testingapi@aramex.com',
        'PASSWORD': 'R123456789$r',
        'PIN': '654654',
        'ACCOUNT_ENTITY': 'AMM',
        'ACCOUNT_NUMBER': '20016',
        'COUNTRY_CODE': 'JO',
    },
    'rate-live': {  # account for rate/location service
        'USER_NAME': 'smita.john@returnhound.com',
        'PASSWORD': 'RHdxb2016',
        'COUNTRY_CODE': 'AE',
        'ACCOUNT_ENTITY': 'DXB',
        'ACCOUNT_NUMBER': '9007',  # NB: not same as ship-live
        'PIN': '654654',  # NB: not same as ship-live
    },
}

ARAMEX_ACCOUNT = 'test-ship-location'
ARAMEX_RATE_ACCOUNT = 'rate-live'
ARAMEX_LOCATION_ACCOUNT = ARAMEX_RATE_ACCOUNT
