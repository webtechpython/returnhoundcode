"""
Project settings.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""
import datetime
import uuid
import os
from copy import deepcopy
from django.utils.log import DEFAULT_LOGGING
from project import lazy_setting, pipeline_storage

DEBUG = bool(int(os.getenv('DEBUG', '0')))  # default: False

PROJECT_PKG_DIR = os.path.dirname(__file__)
BASE_DIR = os.getenv('BASE_DIR', PROJECT_PKG_DIR)  # Instance dir
STATIC_ROOT = os.getenv('STATIC_ROOT', os.path.join(BASE_DIR, 'static_root'))
MEDIA_ROOT = os.getenv('MEDIA_ROOT', os.path.join(BASE_DIR, 'media_root'))

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'ATOMIC_REQUESTS': True,
        'NAME': os.getenv('DB_NAME', 'returnhound'),
        'HOST': os.getenv('DB_HOST', ''),
        'USER': os.getenv('DB_USER', ''),
        'PASSWORD': os.getenv('DB_PASSWORD', ''),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    },
}

# logging config
LOGGING = deepcopy(DEFAULT_LOGGING)
LOGGING['handlers'].update({
    'null': {'class': 'logging.NullHandler'},
    'db': {
        'level': 'DEBUG',
        'class': 'return_hound.apps.logger.handler.DatabaseLogHandler'
    }
})
LOGGING['loggers'].update({
    # silence verbose suds logs...
    'aramex': {
        'level': 'DEBUG',
        'handlers': ['db'],
        'propagate': True,
    },
})

SECRET_KEY = 'ag$gf4h6mndk66^4qu(7v4nr$x8m58vn0#j8bqe=p3-#oxpcr6'

SITE_ID = 1

SITE_PROTOCOL = 'http://'

DEFAULT_CONTACT_US_EMAIL = 'support@returnhound.com'
DEFAULT_ADMIN_EMAIL = 'info@returnhound.com'
DEFAULT_FROM_EMAIL = 'LostandFound@ReturnHound.com'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'cities_light',
    'actstream',

    'return_hound',
    'return_hound.admin_tools',
    'return_hound.apps.admin_supervisor',
    'return_hound.apps.auditlog',
    'return_hound.apps.api',
    'return_hound.apps.aramex',
    'return_hound.apps.cms',
    'return_hound.apps.logger',
    'return_hound.apps.urbanbuz',

    'bo_drf.api_templates',  # flexi router templates

    'authtools',
    'pipeline',
    'django_filters',
    'rest_framework',
    'rest_auth',
    'axes',
    'colorful',
    'haystack',
    'ganalytics',
    'import_export',
    'tinymce',
    's3direct',
    'adminsortable',
    'salmonella',

    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',

    'django.contrib.admin',
)

# Django admin tools
ADMIN_TOOLS_THEMING_CSS = 'admin/admin_theme.css'
ADMIN_TOOLS_MENU = 'return_hound.admin_tools.admin_menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'return_hound.admin_tools.dashboard.CustomDashboard'

# AWS S3
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_STORAGE_BUCKET_NAME = 'returnhound'
AWS_S3_HOST = 's3-eu-west-1.amazonaws.com'
AWS_S3_SIG_LIFETIME = 24  # access signature lifetime (hours)

# The region of your bucket, more info
S3DIRECT_REGION = 'eu-west-1'
S3DIRECT_DESTINATIONS = {
    # Allow staff users to upload any MIME type
    'images': {
        'key': lambda original_filename: 'images/%s.%s' % (uuid.uuid4().hex, original_filename.split('.')[-1]),
        'allowed': ['image/jpeg', 'image/jpg', 'image/png'],
    },
}

ROOT_URLCONF = 'return_hound.urls'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        # 'APP_DIRS': True,
        'OPTIONS': {
            'debug': lazy_setting('DEBUG', bool),
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader',  # django-admin-tools
            ]
        },
    },
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'pipeline.finders.ManifestFinder',
]

STATICFILES_STORAGE = pipeline_storage()
PIPELINE_CACHED_STORAGE = 'return_hound.pipeline_storage.CompressedStorage'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Dubai'
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

# Custom user model
AUTH_USER_MODEL = 'return_hound.User'

# https://django-pipeline.readthedocs.org/en/latest/configuration.html
PIPELINE = {
    'COMPILERS': [
        'pipeline.compilers.less.LessCompiler',
    ],
    # Don't include ANSI escape codes in less tracebacks
    'LESS_ARGUMENTS': '--no-color',
    'STYLESHEETS': {
        'returnhound_bootstrap': {
            'source_filenames': [
              'styles/site.less',
            ],
            'output_filename': 'styles/site.css',
        },
    },
    'JS_COMPRESSOR': 'pipeline.compressors.NoopCompressor',
    'JAVASCRIPT': {
        'base': {
            'source_filenames': (
                'bower_components/es5-shim/es5-shim.min.js',
                'bower_components/jquery/dist/jquery.min.js',
                'bower_components/underscore/underscore-min.js',
                'bower_components/moment/min/moment.min.js',
                'bower_components/simple-uuid/uuid.js',
                'bower_components/bootstrap/dist/js/bootstrap.min.js',
                'bower_components/fancyBox/source/jquery.fancybox.pack.js',
                'bower_components/clipboard/dist/clipboard.min.js',
                'bower_components/intl-tel-input/build/js/intlTelInput.min.js',
                'bower_components/intl-tel-input/lib/libphonenumber/build/utils.js',
            ),
            'output_filename': 'base.js',
        },
        'angular': {
            'source_filenames': (
                'bower_components/angular/angular.min.js',
                'bower_components/angular*/*.min.js',
                'bower_components/angular*/dist/*.min.js',
                'bower_components/angular*/js/*.min.js',
                'bower_components/angular*/release/*.min.js',
                'bower_components/ng*/dist/*.min.js',
                # not safely glob-able...
                'bower_components/ng-file-upload/ng-file-upload-shim.min.js',
                'bower_components/ng-file-upload/ng-file-upload.min.js',
                'bower_components/angular-bootstrap-show-errors/src/showErrors.min.js',
            ),
            'output_filename': 'ngapp-libs.js',
        },
        'angular_app': {
            'source_filenames': (
                'ngapp/*.js',
                'ngapp/auth/*/*.js',
                'ngapp/utils/*.js',
                'ngapp/utils/*/*.js',
                'ngapp/user/*/*.js',
                'ngapp/common/*/*.js',
                'ngapp/admin/*/*.js',
                'ngapp/home/*/*.js',
                'ngapp/hotel/*/*.js',
                'ngapp/report/*/*.js',
                'ngapp/support/*/*.js',
                'ngapp/translations/translations.js',
            ),
            'output_filename': 'ngapp.js',
        }
    },
    'DISABLE_WRAPPER': True,  # JS anon func wrapper
}

# Haystack search
HAYSTACK = {
    'whoosh': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
    'xapian': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': os.path.join(BASE_DIR, 'xapian_index')
    },
}
HAYSTACK_CONNECTIONS = {
    'default': HAYSTACK[os.getenv('HAYSTACK', 'xapian')]
}

# Django-rest-framework settings
REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
        'rest_framework.filters.SearchFilter',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'return_hound.apps.api.authentication.CustomTokenAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'return_hound.apps.api.paginator.CustomPageNumberPagination',
    'PAGE_SIZE': 20,

    'DATETIME_FORMAT': DATETIME_FORMAT
}

# rest-auth settings
REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'return_hound.apps.api.serializers.UserSerializer',
    'PASSWORD_RESET_SERIALIZER': 'return_hound.apps.api.serializers.PasswordResetSerializer',
    'PASSWORD_RESET_CONFIRM_SERIALIZER': 'return_hound.apps.api.serializers.PasswordConfirmResetSerializer',
    'PASSWORD_CHANGE_SERIALIZER': 'return_hound.apps.api.serializers.ChangePasswordSerializer',
}
OLD_PASSWORD_FIELD_ENABLED = True

# django-axes settings:
AXES_LOGIN_FAILURE_LIMIT = 5
AXES_COOLOFF_TIME = 24  # lock for 24 hours
AXES_LOCKOUT_TEMPLATE = 'user_lockout.html'
AXES_USERNAME_FORM_FIELD = 'email'
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = True


# Django cities light settings
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['es', 'en', 'fr', 'ar', 'abbr']

# Django activity stream settings
ACTSTREAM_SETTINGS = {
    'FETCH_RELATIONS': True,
    'USE_JSONFIELD': True,
}

JSONFIELD_ENCODER_CLASS = 'return_hound.apps.api.json_encoder.SmarterJSONEncoder'


# ARAMEX
ARAMEX_AED_TO_USD_RATE = '3.678'  # FIXME: remove this HAX to convert AED to USD
ARAMEX_VERSION = 'v1.0'

ARAMEX_LOCATION_URL = 'https://ws.staging.aramex.net/ShippingAPI.V2/Location/Service_1_0.svc?singleWsdl'
ARAMEX_RATE_URL = 'https://ws.staging.aramex.net/ShippingAPI.V2/RateCalculator/Service_1_0.svc?singleWsdl'
ARAMEX_SHIP_URL = 'https://ws.staging.aramex.net/ShippingAPI.V2/Shipping/Service_1_0.svc?singleWsdl'

ARAMEX_PICKUP_LATEST_TIME = (17, 30)
ARAMEX_PICKUP_HOTEL_OPEN_TIME = (9, 0)
ARAMEX_PICKUP_HOTEL_CLOSE_TIME = (19, 30)
ARAMEX_LEAD_TIME_MINUTES = 180
ARAMEX_CONVERSION_RATES = {
    'AED': {'country': 'UAE', 'rate': 3.67},
    'OMR': {'country': 'Oman', 'rate': 0.385},
    'QAR': {'country': 'Qatar', 'rate': 3.64},
    'SAR': {'country': 'Saudi', 'rate': 3.75},
    'KWD': {'country': 'Kuwait', 'rate': 0.305},
    'JOD': {'country': 'Jordan', 'rate': 0.709},
    'BHD': {'country': 'Bahrain', 'rate': 0.376},
    'USD': {'country': 'Lebanon', 'rate': 1},
    'EGP': {'country': 'Egypt', 'rate': 18.1}
}

ARAMEX_COUNTRY_ACCOUNTS = {
    'AE': {
        'COUNTRY_CODE': 'AE',
        'ACCOUNT_ENTITY': 'DXB',
        'ACCOUNT_NUMBER': '850',
        'PIN': '553654',
    },
    'OM': {
        'COUNTRY_CODE': 'OM',
        'ACCOUNT_ENTITY': 'MCT',
        'ACCOUNT_NUMBER': '51112106',
        'PIN': '543543',
    },
    'QA': {
        'COUNTRY_CODE': 'QA',
        'ACCOUNT_ENTITY': 'DOH',
        'ACCOUNT_NUMBER': '1008',
        'PIN': '443543',
    },
    'SA': {
        'COUNTRY_CODE': 'SA',
        'ACCOUNT_ENTITY': 'JED',
        'ACCOUNT_NUMBER': 'cashjed',
        'PIN': '115215',
    },
    'KW': {
        'COUNTRY_CODE': 'KW',
        'ACCOUNT_ENTITY': 'KWI',
        'ACCOUNT_NUMBER': '202076',
        'PIN': '216216',
    },
    'JO': {
        'COUNTRY_CODE': 'JO',
        'ACCOUNT_ENTITY': 'AMM',
        'ACCOUNT_NUMBER': '001',
        'PIN': '543543',
    },
    'BH': {
        'COUNTRY_CODE': 'BH',
        'ACCOUNT_ENTITY': 'BAH',
        'ACCOUNT_NUMBER': '5000',
        'PIN': '221321',
    },
    'LB': {
        'COUNTRY_CODE': 'LB',
        'ACCOUNT_ENTITY': 'BEY',
        'ACCOUNT_NUMBER': '17426',
        'PIN': '432432',
    },
    'EG': {
        'COUNTRY_CODE': 'EG',
        'ACCOUNT_ENTITY': 'CAI',
        'ACCOUNT_NUMBER': '241517',
        'PIN': '216216',
    },
}

# Item Match score percentage
MATCH_SCORE = 25

DEFAULT_CURRENCY = 'AED'
DEFAULT_SERVICE_CHARGE_PERCENTAGE = 3
DEFAULT_INSURANCE_FEE_PERCENTAGE = 2.5

# Hotel Settings
HOTEL_MAX_MAIL_USERS = 3  # maximum number of users that "Receives System Emails"

# Celery
BROKER_URL = 'redis://'
CELERY_DEFAULT_QUEUE = 'return_hound'

# URBANBUZ
URBAN_BUZ_BASE_URL = 'https://newrhapi.urbanbuz.com/v1'
URBAN_BUZ_API_KEY = 'returnhound:'

