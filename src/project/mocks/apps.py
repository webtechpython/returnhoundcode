from django.apps import AppConfig
from django.conf import settings
from django.utils import module_loading


class Config(AppConfig):
    name = 'project.mocks'
    label = 'project_mocks'

    def ready(self):
        for module_path in getattr(settings, 'PROJECT_MOCKS', []):
            module_loading.import_string('{}.mock'.format(module_path))()
