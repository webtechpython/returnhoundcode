from functools import partial
from unittest.mock import MagicMock
from django.utils.termcolors import colorize


def mock():
    """
    Mock responses for Aramex shipping service
    """
    red = partial(colorize, fg='red')
    print(red('Mock Aramex responses enabled'))

    # problematic imports...
    from return_hound.apps.aramex.api import AramexAPIError, \
        AramexLocationClient, AramexRateClient, AramexShippingClient

    # -----------------------------------------------------------------------------
    # Address validation calls
    # -----------------------------------------------------------------------------
    def validate_address_mock(*args, **kwargs):
        if kwargs.get('post_code') == 'fail':
            raise AramexAPIError({
                'city': 'Invalid combination of city/zipcode',
                'post_code': 'Invalid combination of city/zipcode',
            })
        else:
            return True

    AramexLocationClient.validate_address = MagicMock(side_effect=validate_address_mock)
    AramexLocationClient.get_cities = MagicMock(return_value=['MOCK CITY'])
    # -----------------------------------------------------------------------------
    # Rate
    # -----------------------------------------------------------------------------
    AramexRateClient.get_rate = MagicMock(return_value={
        'amount': '200.0',
        'currency': 'AED',
        'shipping_group': 'Domestic',
        'shipping_type': 'Priority Parcel Domestic',
        'has_error': False,
        'errors': []
    })
    # -----------------------------------------------------------------------------
    # Shipment
    # -----------------------------------------------------------------------------
    AramexShippingClient.create_shipment = MagicMock(return_value={
        'id': 'ARAMEX-TRACKING-NUMBER-123',
        'label': b'moo',
    })
