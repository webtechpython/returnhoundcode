module.exports = function (grunt) {
    grunt.initConfig({
        nggettext_extract: {
            pot: {
                files: {
                    'po/template.pot': ['src/return_hound/templates/base.html',
                        'src/return_hound/static/ngapp/home/views/home.html' ,
                        'src/return_hound/templates/_includes/_topnav.html',
                        'src/return_hound/static/ngapp/hotel/views/*.html',
                        'src/return_hound/static/ngapp/report/views/*.html',
                        'src/return_hound/static/ngapp/user/views/*.html',
                        'src/return_hound/static/ngapp/support/views/*.html',
                        'src/return_hound/static/ngapp/utils/views/*.html'
                    ]
                }
            }
        },
        nggettext_compile: {
            all: {
                files: {
                    'src/return_hound/static/ngapp/translations/translations.js': ['po/*.po']
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-angular-gettext');
    grunt.registerTask('default', ['nggettext_extract']);
};
