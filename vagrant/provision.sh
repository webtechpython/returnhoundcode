#!/bin/sh -e
#
# Provision a ubuntu/trusty64 box to run Django.
#
sudo apt-get update
# Mercurial & Git (without pulling in heavy recommended GUI packages)
# These are needed for Pip, Bower, etc.
sudo apt-get install -y --no-install-recommends mercurial
sudo apt-get install -y git curl

# Python build dependencies
sudo apt-get install -y python3.4-dev zlib1g-dev libjpeg-dev libfreetype6-dev

# Virtualenv
sudo apt-get install -y python3.4-venv

# NodeJS + npm (for Bower)
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
npm config set prefix ~/.local

# Postgres
sudo apt-get install -y libpq-dev
sudo apt-get install -y postgresql postgresql-contrib
sudo -u postgres createuser --createdb --superuser vagrant || true

# setup env vars (but do not activate venv)
. ~/env.sh

# Setup db,env,etc and run check
/vagrant/run/django check
