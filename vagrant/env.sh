# Make sure user-local installs work
export PATH="$HOME/.local/bin":"$PATH"

# Activate the Django environment
export PYTHONPATH="/vagrant"
export STATIC_ROOT="/tmp/static_root"
export MEDIA_ROOT="/tmp/media_root"
export VENV="/home/vagrant/venv"

# run from mounted project root on host
cd /vagrant
